#!/bin/bash

#./run_pipeline_array.py  /gpfs/laur/sepia_compressed/sepiaArrayClusterTest/2019-02-07-10-53-18 --host-config ./.draco.cfg --local-config ./.draco.cfg --hostname draco --sshcmd="ssh mpibrsre@gatezero.mpcdf.mpg.de ssh -A draco" --keepfiles --uuid 12e2a260-4bc2-11e9-ae5b-d89ef312d1c2 


#./run_pipeline_array.py 2019-02-07-10-53-18  --inputFolder /gpfs/laur/sepia_compressed/sepiaArrayClusterTest --host-config ./.cobra.cfg --local-config ./.cobra.cfg  --hostname cobra --sshcmd="ssh gatezero ssh -A cobra" --keepfiles --uuid 4e13ae76-4f12-11e9-ba80-27161a20586a

#./run_pipeline_array.py  2019-02-07-10-53-18 --inputFolder /gpfs/laur/sepia_compressed/sepiaArrayClusterTest --host-config ./.ccluster.cfg --local-config ./.ccluster.cfg --hostname ccluster --keepfiles 


#./run_pipeline.py test/input_dir/sepia030-20170704-011/20170704-011.mov --host-config ./test/.cuttleline.cfg --local-config ./test/.cuttleline.cfg   --sleep-between-sync 40     --hostname draco --sshcmd="ssh gatezero ssh -A draco" --max-queued=50



#./run_pipeline.py test/input_dir/sepia030-20170704-011/20170704-011.mov --host-config ~/.cuttleline.cfg --local-config ./test/.cuttleline.cfg   --sleep-between-sync 40     --hostname ccluster  --max-queued=16 --uuid 35ac7dfe-4bed-11e9-bf75-d89ef312d1c2 --skip-registration

#./run_pipeline_array.py  2019-04-12-09-39-18 --inputFolder /gpfs/laur/sepia_compressed/sepia201-20190412-001 --host-config ./.ccluster.cfg --local-config ./.ccluster.cfg  --hostname ccluster --sshcmd="ssh ccluster" --skip-registration

./run_pipeline_array.py  2019-04-12-09-39-18 --inputFolder /gpfs/laur/sepia_compressed/sepia201-20190412-001_1 --host-config ./.cobra.cfg --local-config ./.cobra.cfg  --hostname cobra --sshcmd="ssh gatezero ssh -A cobra" --keepfiles --uuid 2d6b19b2-81e8-11e9-8204-2338169bca23 --skip-registration 

./run_pipeline_array.py  2019-04-11-14-21-55 --inputFolder /gpfs/laur/sepia_compressed/sepia201-20190411-001 --host-config ./.cobra.cfg --local-config ./.cobra.cfg  --hostname cobra --sshcmd="ssh gatezero ssh -A cobra" --keepfiles --uuid 2f37cc56-9979-11e9-9007-c349b64bc103 --PullRemoteFirst --skip-registration --skip-panorama 


#mpiexec -n 23 python chunking.py --array_params /gpfs/laur/sepia_tools/cameraConfigOverviewRec0Cam1.pkl --dir /gpfs/laur/sepia_compressed/sepia201-20190411-001 /gpfs/laur/sepia_compressed/2019-04-29-14-18-01.chunks


mpiexec -n 10 python stitch_chunk.py /gpfs/laur/sepia_processed/stitchingTest/2019-04-12-09-39-18-2207-4362.seg /gpfs/laur/sepia_processed/stitchingTest/2019-04-12-09-39-18-2207-4362.seg /gpfs/laur/sepia_processed/stitchingTest/2019-04-12-09-39-18-25-2181.seg /gpfs/laur/sepia_processed/stitchingTest/2019-04-12-09-39-18-2207-4362.cstitching

mpiexec -n 10 python stitch_chunk.py /gpfs/laur/sepia_processed/stitchingTest/2019-04-12-09-39-18-25-2181.seg /gpfs/laur/sepia_processed/stitchingTest/2019-04-12-09-39-18-2207-4362.seg /gpfs/laur/sepia_processed/stitchingTest/2019-04-12-09-39-18-25-2181.seg /gpfs/laur/sepia_processed/stitchingTest/2019-04-12-09-39-18-25-2181.cstitching

mpiexec -n 2 python stitch_all_chunks.py /gpfs/laur/sepia_processed/stitchingTest/2019-04-12-09-39-18-2207-4362.cstitching  /gpfs/laur/sepia_processed/stitchingTest/2019-04-12-09-39-18-25-2181.cstitching /gpfs/laur/sepia_processed/stitchingTest/testStitchAll

python generate_queen_frame_array.py /gpfs/laur/sepia_processed/stitchingTest/testStitchAll /gpfs/laur/sepia_processed/stitchingTest/testQueen

python generate_cleanqueen_array.py /gpfs/laur/sepia_processed/stitchingTest/testQueen /gpfs/laur/sepia_processed/stitchingTest/testCleanQueen

python extract_chromatophores_array_chunk.py /gpfs/laur/sepia_processed/stitchingTest/testCleanQueen --chunk /gpfs/laur/sepia_processed/stitchingTest/2019-04-12-09-39-18-25-2181.seg /gpfs/laur/sepia_processed/stitchingTest/testAreas-25-2181.careas

python extract_chromatophores_array_chunk.py /gpfs/laur/sepia_processed/stitchingTest/testCleanQueen --chunk /gpfs/laur/sepia_processed/stitchingTest/2019-04-12-09-39-18-2207-4362.seg /gpfs/laur/sepia_processed/stitchingTest/testAreas-2207-4362.careas

python combine_area_chunks.py /gpfs/laur/sepia_processed/stitchingTest/testAreas-25-2181.careas /gpfs/laur/sepia_processed/stitchingTest/testAreas-2207-4362.careas /gpfs/laur/sepia_processed/stitchingTest/testAreasAll


#mpiexec -n 20 python stitch_chunk.py /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-41424-43645.seg /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-45964-48199.seg /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-41424-43645.seg /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-80520-80672.cstitching


#mpiexec -n 20 python stitch_all_chunks.py /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-18631-20061.cstitching /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-4388-6542.cstitching /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-2207-4362.cstitching  /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-6569-8724.cstitching /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-25-2181.cstitching /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-8847-11119.cstitching /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-13736-16056.cstitching /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-11243-13612.cstitching /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-16181-18599.cstitching /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18.stitching


#python generate_cleanqueen_array.py /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18.queenframe /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18.cleanqueen

#python combine_area_chunks.py /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-8847-11119.careas /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-4388-6542.careas /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-25-2181.careas /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-2207-4362.careas /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18-6569-8724.careas /gpfs/laur/sepia_processed/sepia201-20190412-001/2019-04-12-09-39-18.areas



