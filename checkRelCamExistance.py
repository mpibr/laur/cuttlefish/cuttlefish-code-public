#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 13:07:43 2019

@author: reiters
"""

import os
import numpy as np
import pandas as pd
import json
import scipy.io

def checkRelCamExistance(directory, chunk, array_params, pano_params, relCams):
    
    
    print('in the function!')
           
    df =  pd.read_pickle(array_params)

    with open(relCams, 'r') as fr:
        relCams=fr.read(); 
    relCams = relCams.strip('[]\n').split()
    for ind in range(len(relCams)):
       relCams[ind] = relCams[ind].strip(',') 
          
      
    registration_files=[]
    goodRanks=[]
    goodRankInds=[]
    for img in range(len(relCams)):
        registration_fileName = (directory + '/' + df['names'][int(relCams[img])] + '-' + chunk + '.reg')
        if os.path.isfile(registration_fileName):
            registration_files.append(registration_fileName)  
            goodRanks.append(int(relCams[img]))
            goodRankInds.append(img)
            
    f = open(relCams, 'w')
    json.dump(goodRanks, f)
    f.close()
    print('got to the if! there are ' + str(len(goodRankInds)) + ' good files, and ' + str(len(relCams)) + ' relcams')
    #adjust panorama if a registration file drops out
    if len(goodRankInds) != len(relCams):
        print('about to modify the mat file')
        mFile=scipy.io.loadmat(pano_params) 
        uStruct=mFile['uStruct'] 
        vStruct=mFile['vStruct'] 
        m_v0_=mFile['m_v0_'].astype('int32')
        m_v1_=mFile['m_v1_'].astype('int32')
        m_u0_=mFile['m_u0_'].astype('int32')
        m_u1_=mFile['m_u1_'].astype('int32')
        mosaich=np.asscalar(mFile['mosaich'])
        mosaicw=np.asscalar(mFile['mosaicw'])
        
        uStruct = uStruct[0,goodRankInds]
        vStruct = vStruct[0,goodRankInds]
        m_v0_ =  m_v0_[goodRankInds]
        m_v1_ =  m_v1_[goodRankInds]
        m_u0_ =  m_u0_[goodRankInds]
        m_u1_ =  m_u1_[goodRankInds]
                
        scipy.io.savemat(pano_params, mdict={'uStruct': uStruct, 'vStruct': vStruct, \
                                             'm_v0_': m_v0_, \
                                             'm_v1_': m_v1_, \
                                             'm_u0_': m_u0_, \
                                             'm_u1_': m_u1_, \
                                             'mosaich':mosaich, \
                                             'mosaicw':mosaicw})
                                             
                                                       
    

       
        
