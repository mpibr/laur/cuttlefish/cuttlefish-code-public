#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import socket
import argparse
import subprocess
import time
import datetime
import numpy as np
from ssh_tools import expand_path, retry_ssh, retry_rsync

def submit_job(job_args, slurm_args=None, mpicmd='srun',
               max_queued=None, wait_time=None, partition=None, debug=False):
    """Submit job to SLURM using srun and return jobid.
    job_args: list
    slurm_args: list
    """

    # Default to parsed command-line arguments
    if max_queued is None:
        max_queued = int(args.max_queued)
    if wait_time is None:
        wait_time = int(args.wait_for_queue)
    if partition is None:
        partition = args.partition

    # Check current queue length
    hostname = socket.gethostname()
    user_queue = retry_ssh(\
        'ssh {}'.format(hostname), \
        'squeue --user {}'.format(os.path.expandvars('$USER')))

    if len(user_queue.split('\n')) > max_queued:
        print('Maximum number of queued jobs of {} reached, waiting for {} s.'.format(
            max_queued, wait_time))
        time.sleep(wait_time)

    # Submit job using sbatch via heredoc batch script
    sbatch_options = ['ssh', hostname, 'sbatch', '--parsable']
    if slurm_args is not None:
        sbatch_options += slurm_args
    if partition is not '':
        sbatch_options += ['--partition', partition]  # No default value available
    p = subprocess.Popen(sbatch_options, 
                         stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    batch_script_heredoc = ('#!/bin/bash\n' 
                            + '{} bash -c "{}"'.format(mpicmd, ' '.join(job_args)))

    stdout, _ = p.communicate(batch_script_heredoc.encode())

    if debug:
        print(' '.join(sbatch_options))
        print(batch_script_heredoc)
    
    # Retrieve jobid
    jobid = stdout.split()[0].decode()

    return jobid

if __name__ == '__main__':

    # Arguments used in run_pipeline
    p = argparse.ArgumentParser('Running pipeline on remote', add_help=False)
    p.add_argument('--background', help='Class name of background', type=str)
    p.add_argument('--foreground', help='Class name of chromatophores', type=str)
    p.add_argument('--chunk-stitching-slurm', default="")
    p.add_argument('--chunk-stitching-args', default="")
    p.add_argument('--stitching-slurm', default="")
    p.add_argument('--stitching-args', default="", help='Arguments for stitching')
    p.add_argument('--queenframe-slurm', default="", help='SLURM options for queenframe')
    p.add_argument('--queenframe-args', default="", help='Arguments for queenframe')
    p.add_argument('--average-slurm', default="", help='SLURM options for average')
    p.add_argument('--average-args', default="", help='Arguments for average')
    p.add_argument('--cleanqueen-slurm', default="", help='SLURM options for cleanqueen')
    p.add_argument('--cleanqueen-args', default="", help='Arguments for cleanqueen')
    p.add_argument('--areas-slurm', default="", help='SLURM options for areas')
    p.add_argument('--areas-args', default="", help='Arguments for areas')
    p.add_argument('--mpicmd', default='mpiexec', \
        help='Command to run jobs in parallel using MPI')
    p.add_argument('--partition', help='SLURM partition to submit to')
    p.add_argument('--debug', action='store_true')
    p.add_argument('--rerun-stitching', action='store_true')
    p.add_argument('--rerun-average', action='store_true')
    p.add_argument('--rerun-queenframe', action='store_true')
    p.add_argument('--rerun-cleanqueen', action='store_true')
    p.add_argument('--max-queued', default=299)
    p.add_argument('--sleep-between-submits', default=0.5)  # new
    p.add_argument('--wait-for-queue', default=900, help='In seconds')   
    p.add_argument('--check-file-patience', default=40, \
        help='Max seceonds to wait when checking input file dependencies.')
    p.add_argument('--output-prefix', required=True)
    p.add_argument('prefix', help='List of input prefixes', nargs='+')
    args = p.parse_args()

    # Setup file names
    code_dir = os.path.dirname(os.path.abspath(__file__))
    args.prefix = args.prefix
    dirname = os.path.dirname(args.output_prefix)
    chunktimes_list = [p + '.chunktimes' for p in args.prefix]
    stitching = args.output_prefix + '.stitching'
    stitching_log = args.output_prefix + '.stitching.log'
    queenframe = args.output_prefix + '.queenframe'
    queenframe_log = args.output_prefix + '.queenframe.log'
    average = args.output_prefix + '.average'
    average_log = args.output_prefix + '.average.log'
    colourlabels = args.output_prefix + '.colourlabels'
    colourlabels_log = args.output_prefix + '.colourlabels.log'
    cleanqueen = args.output_prefix + '.cleanqueen'
    cleanqueen_log = args.output_prefix + '.cleanqueen.log'
    areas = args.output_prefix + '.areas'
    areas_log = args.output_prefix + '.areas.log'
    filedeps_log = args.output_prefix + '.filedeps.log'
    
    now = datetime.datetime.now()
    # Add horizontal separator to file dependency log
    with open(filedeps_log, 'a') as f:
        f.write('======{}======\n'.format(now.strftime("%Y-%m-%d %H:%M:%S")))

    # Sort chunks by chunksize if chunk-parallel steps are not skipped
    masterframe = []
    chunkaverage = []
    chunk_stitching = []
    chunk_stitching_log = []
    chunk_stitching_jobid = []
    for chunktimes, prefix in zip(chunktimes_list, args.prefix):
        with open(chunktimes, 'r') as f:
            size_list = []
            line_list = []
            for line in f:
                line = line.strip('\n').split(' ')
                start_f, end_f, _, _, _ = line
                line_list.append(line)
        
        sorted_chunktimes = np.array(line_list)

        # Get all chunk filenames
        chunk_prefix = ['{}-{}-{}'.format(prefix, start_f, end_f) for 
            (start_f, end_f, _, _, _) in sorted_chunktimes]
        masterframe += [cp + '.masterframe' for cp in chunk_prefix]
        chunkaverage += [cp + '.chunkaverage' for cp in chunk_prefix]
        chunk_stitching += [os.path.join(os.path.dirname(args.output_prefix),
            os.path.split(cp)[1]) + '.cstitching' for cp in chunk_prefix]
        chunk_stitching_log += [os.path.join(os.path.dirname(args.output_prefix),
            os.path.split(cp)[1]) + '.cstitching.log' for cp in chunk_prefix]

    # Submit all chunk-stitching jobs for the chunks
    for chunkidx, output in enumerate(chunk_stitching):
        slurm_args = ['--kill-on-invalid-dep=yes', \
                    '--job-name', 'check-chunk-stitching', \
                    '--output', '/dev/null', \
                    '--error', filedeps_log, \
                    '--open-mode', 'append']

        job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                    '--required', masterframe[chunkidx], \
                    '--output', output, \
                    '--patience', str(args.check_file_patience)] 
        if args.rerun_stitching:
            job_args += ['--skip-mtime']
        check_chunk_stitching_jobid = submit_job(job_args, slurm_args, args.mpicmd)
        
        slurm_args = ['--dependency', 'afterok:' + check_chunk_stitching_jobid, \
                        '--output', chunk_stitching_log[chunkidx], \
                        '--kill-on-invalid-dep=yes', \
                        '--job-name', 'chunk_stitching']
        if args.chunk_stitching_slurm is not '':
            slurm_args += args.chunk_stitching_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'stitching', 'stitch_chunk.py'), \
                    masterframe[chunkidx]] \
                    + masterframe \
                    + [output]
        if args.chunk_stitching_args is not '':
            job_args += args.chunk_stitching_args.split(' ')
        chunk_stitching_jobid.append(
            submit_job(job_args, slurm_args, args.mpicmd))
        
        print('>>> Submitted {}: {}'.format(chunk_stitching_jobid[chunkidx], output))

        time.sleep(args.sleep_between_submits)

    # Submit stitching job
    output = stitching
    slurm_args = ['--dependency', 'afterany:' + ':'.join(chunk_stitching_jobid), \
                    '--kill-on-invalid-dep=yes', \
                    '--job-name', 'check-stitching', \
                    '--output', '/dev/null', \
                    '--error', filedeps_log, \
                    '--open-mode', 'append']
    #### why optional???
    job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                '--optional'] + chunk_stitching \
                + ['--output', output, \
                '--patience', str(args.check_file_patience)] 
    check_stitching_jobid = submit_job(job_args, slurm_args, args.mpicmd)           
    
    slurm_args = ['--dependency', 'afterok:' + check_stitching_jobid, \
                    '--output', stitching_log, \
                    '--kill-on-invalid-dep=yes', \
                    '--job-name', 'stitching']
    if args.stitching_slurm is not '':
        slurm_args += args.stitching_slurm.split(' ')
    job_args = [os.path.join(code_dir, 'stitching', 'stitch_all_chunks.py')] \
                + chunk_stitching \
                + [output]
    if args.stitching_args is not '':
        job_args += args.stitching_args.split(' ')
    stitching_jobid = submit_job(job_args, slurm_args, args.mpicmd)
    
    print('>>> Submitted {}: {}'.format(stitching_jobid, output))

    time.sleep(args.sleep_between_submits)

    # Submit queenframe job
    output = queenframe
    slurm_args = ['--dependency', 'afterany:' + stitching_jobid, \
                    '--kill-on-invalid-dep=yes', \
                    '--job-name', 'check-queenframe', \
                    '--output', '/dev/null', \
                    '--error', filedeps_log, \
                    '--open-mode', 'append']
    job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                '--required', stitching, \
                '--output', output, \
                '--patience', str(args.check_file_patience)] 
    if args.rerun_queenframe:
        job_args += ['--skip-mtime']
    check_queenframe_jobid = submit_job(job_args, slurm_args, args.mpicmd)           
    
    slurm_args = ['--dependency', 'afterok:' + check_queenframe_jobid, \
                    '--output', queenframe_log, \
                    '--kill-on-invalid-dep=yes', \
                    '--job-name', 'queenframe']
    if args.queenframe_slurm is not '':
        slurm_args += args.queenframe_slurm.split(' ')
    job_args = [os.path.join(code_dir, 'stitching', 'generate_queen_frame.py'), \
                stitching, \
                output]
    if args.queenframe_args is not '':
        job_args += args.queenframe_args.split(' ')
    queenframe_jobid = submit_job(job_args, slurm_args, args.mpicmd)
    
    print('>>> Submitted {}: {}'.format(queenframe_jobid, output))

    time.sleep(args.sleep_between_submits)

    # Submit average job
    output = average

    average_deps = stitching_jobid

    slurm_args = ['--dependency', 'afterany:' + average_deps, \
                    '--kill-on-invalid-dep=yes', \
                    '--job-name', 'check-average', \
                    '--output', '/dev/null', \
                    '--error', filedeps_log, \
                    '--open-mode', 'append']
    job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                stitching, \
                '--optional'] + chunkaverage \
                + ['--output', output, \
                '--patience', str(args.check_file_patience)] 
    if args.rerun_average:
        job_args += ['--skip-mtime']
    check_average_jobid = submit_job(job_args, slurm_args, args.mpicmd)           
    
    slurm_args = ['--dependency', 'afterok:' + check_average_jobid, \
                    '--output', average_log, \
                    '--kill-on-invalid-dep=yes', \
                    '--job-name', 'average']
    if args.average_slurm is not '':
        slurm_args += args.average_slurm.split(' ')
    # one place to store chunkaverage-ext??
    job_args = [os.path.join(code_dir, 'stitching', 'generate_average.py'), \
                stitching, \
                output, \
                '--chunkaverage-ext', 'chunkaverage']
    if args.average_args is not '':
        job_args += args.average_args.split(' ')
    average_jobid = submit_job(job_args, slurm_args, args.mpicmd)
    
    print('>>> Submitted {}: {}'.format(average_jobid, output))

    time.sleep(args.sleep_between_submits)

    # Submit cleanqueen job
    output = cleanqueen

    slurm_args = ['--dependency', 'afterany:' + queenframe_jobid, \
                    '--kill-on-invalid-dep=yes', \
                    '--job-name', 'check-cleanqueen', \
                    '--output', '/dev/null', \
                    '--error', filedeps_log, \
                    '--open-mode', 'append']
    job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                '--required', queenframe, \
                '--output', output, \
                '--patience', str(args.check_file_patience)] 
    if args.rerun_cleanqueen:
        job_args += ['--skip-mtime']
    check_cleanqueen_jobid = submit_job(job_args, slurm_args, args.mpicmd)           
    
    slurm_args = ['--dependency', 'afterok:' + check_cleanqueen_jobid, \
                    '--output', cleanqueen_log, \
                    '--kill-on-invalid-dep=yes', \
                    '--job-name', 'cleanqueen']
    if args.cleanqueen_slurm is not '':
        slurm_args += args.cleanqueen_slurm.split(' ')
    job_args = [os.path.join(code_dir, 'stitching', 'generate_cleanqueen.py'), \
                queenframe, \
                output]
    if args.cleanqueen_args is not '':
        job_args += args.cleanqueen_args.split(' ')
    cleanqueen_jobid = submit_job(job_args, slurm_args, args.mpicmd)
    
    print('>>> Submitted {}: {}'.format(cleanqueen_jobid, output))

    time.sleep(args.sleep_between_submits)

    # Submit area job
    output = areas

    slurm_args = ['--dependency', 'afterany:' + cleanqueen_jobid, \
                    '--kill-on-invalid-dep=yes', \
                    '--job-name', 'check-areas', \
                    '--output', '/dev/null', \
                    '--error', filedeps_log, \
                    '--open-mode', 'append']
    job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                '--required', cleanqueen, \
                '--output', output, \
                '--patience', str(args.check_file_patience)] 
    check_areas_jobid = submit_job(job_args, slurm_args, args.mpicmd)       
    
    slurm_args = ['--dependency', 'afterok:' + check_areas_jobid, \
                    '--output', areas_log, \
                    '--kill-on-invalid-dep=yes', \
                    '--job-name', 'areas']
    if args.areas_slurm is not '':
        slurm_args += args.areas_slurm.split(' ')
    job_args = [os.path.join(code_dir, 
                    'extract_chromatophores', 'extract_chromatophores.py'), \
                '--foreground', args.foreground,
                cleanqueen, \
                output]
    if args.areas_args is not '':
        job_args += args.areas_args.split(' ')

    areas_jobid = submit_job(job_args, slurm_args, args.mpicmd)
    
    print('>>> Submitted {}: {}'.format(areas_jobid, output))

    time.sleep(args.sleep_between_submits)

    # Submit final job
    slurm_args = ['--dependency', 'afterany:' + areas_jobid, \
                    '--kill-on-invalid-dep=yes', \
                    '--job-name', 'final']
    
    # Submit final job (continued)               
    job_args = ['hostname']
    final_jobid = submit_job(job_args, slurm_args).strip()

    print('FINAL_JOBID=', final_jobid)