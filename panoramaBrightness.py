#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 14 14:31:03 2019

@author: reiters
"""

#!/usr/bin/env python3

from mpi4py import MPI

import argparse
import cv2
import glob
import numpy as np
from tqdm import tqdm
import os
import h5py
import random
import os.path
import string
import pandas as pd
 
def calcBrightness(img):
    brightness=np.sum(img)
    return brightness

if __name__ == '__main__':
    
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank=comm.Get_rank()
    p = argparse.ArgumentParser('calculate brightness')
    p.add_argument('--dir', required=True, help='directory with panorama videos')
   # p.add_argument('--array_params', required=True, help='pickle file with camera list')
    p.add_argument('output', help='Output brightness file')
    args = p.parse_args()
  
    if comm.rank == 0:
        allVids=np.array(glob.glob(args.dir + '/*.avi'))
        cap = cv2.VideoCapture(allVids[0])
        num_frames = 10# int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        recvbuf = np.zeros([size,num_frames], dtype='int')
     #   df =  pd.read_pickle(args.array_params)
        
        # first make global pano
      #  goodVids=[]
       # for img in range(df.shape[0]):
       #     strMatch=np.array([fname.find(df['names'][img]) for fname in allVids])
      #      goodVids.append(allVids[strMatch>-1][0])
     #   allVids=goodVids
       
    else:
        allVids = None
        num_frames = None
        recvbuf = None
    
    vids = comm.scatter(allVids,root=0)


    print(vids)
    num_frames = comm.bcast(num_frames, root=0)
    rank=comm.Get_rank()
    cap = cv2.VideoCapture(vids)
    brightness = np.zeros(num_frames,dtype='int')
    
    
    for frame in tqdm(range(num_frames), \
           total=num_frames, \
           desc='calculating brightness'): 

        succ, img = cap.read()
        
        if succ:
            brightness[frame] = calcBrightness(img)
        if not succ:
            print('ERRRRRRROR!')
            break
        
        if frame == 0:
            cv2.imwrite(vids + '.png', img
                        )
            #amount_of_frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
        #frame_no=int(amount_of_frames/2)+frame
#    frame_no=800
#    cap.set(1,frame_no)
#    succ, img = cap.read()
#    cv2.imwrite(vids + '.png', img)
    
    comm.Gather(brightness, recvbuf, root=0)

    if comm.rank == 0:
       
        		       
         
        output_dirname = os.path.dirname(args.output)
        output_basename = os.path.basename(args.output)
        suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                for _ in range(4))
        tmp_output_basename = '.' + output_basename + '.' + suffix
        tmp_output = os.path.join(output_dirname, tmp_output_basename)
        brightness_file = h5py.File(tmp_output, 'w')
        
        brightness_dset = brightness_file.create_dataset( \
                    'brightness', \
                    shape=[size, num_frames], \
                    dtype='int')
        brightness_dset[...] = recvbuf
           
        video_names = brightness_file.create_dataset(\
                    'videoNames', \
                    shape=[size], \
                    dtype=h5py.special_dtype(vlen=str))
        video_names[...] = allVids
        
             
        
        brightness_file.close()
        os.rename(tmp_output, args.output)
