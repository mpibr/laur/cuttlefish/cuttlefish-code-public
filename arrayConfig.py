#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 08:34:08 2019

@author: reiters
"""

import numpy as np
import glob
import argparse
import cv2
import pandas as pd
from PIL import Image
from numpy.linalg import inv            
import h5py

def detectAndDescribe(gray_image,descriptor):
    (kps, features) = descriptor.detectAndCompute(gray_image, None)
   # kps = np.float32([kp.pt for kp in kps])
    return (kps, features)


def matchKeypoints(kpsA, kpsB, featuresA, featuresB,
                   ratio, ransac_threshold,MIN_MATCH_COUNT):
    
	# compute the raw matches and initialize the list of actual
	# matches
    matcher = cv2.DescriptorMatcher_create("BruteForce")
    rawMatches = matcher.knnMatch(featuresA, featuresB, 2)
    matches = []
    for m,n in rawMatches:
        if m.distance < ratio*n.distance:
            matches.append(m)
        
    if len(matches)>MIN_MATCH_COUNT:
        src_pts = np.float32([ kpsA[m.queryIdx].pt for m in matches ]).reshape(-1,1,2)
        dst_pts = np.float32([ kpsB[m.trainIdx].pt for m in matches ]).reshape(-1,1,2)
        M, mask = cv2.estimateAffine2D(src_pts, dst_pts,np.ones(len(src_pts)), cv2.RANSAC,ransac_threshold)
        goodMatches = mask.ravel().tolist()
        goodSrc=np.squeeze(src_pts[np.array(goodMatches)==1,:,:])
        goodDst=np.squeeze(dst_pts[np.array(goodMatches)==1,:,:])
    
        # make the points 3d so they can interact with the H mats
        goodSrc=np.append(goodSrc, np.ones([goodSrc.shape[0],1]),axis=1)
        goodDst=np.append(goodDst, np.ones([goodDst.shape[0],1]),axis=1)
    else:
        # reduce the filter
        #ransac_threshold += 5
        #ratio += .1
        
        #goodSrc, goodDst = matchKeypoints(kpsA, kpsB, featuresA, featuresB,
                   #ratio, ransac_threshold,MIN_MATCH_COUNT)
        goodSrc=None;goodDst=None; M= None
        
                  
    return(goodSrc,goodDst, M)
    
    

if __name__ == '__main__':
    
    p = argparse.ArgumentParser('global calibration')
    p.add_argument('--directory', required=True, help='image directory')
    p.add_argument('--prefix', required=True, help='image prefix')
    p.add_argument('--brightnessFile', help='camera config name')
    p.add_argument('output', help='camera config name')
    args = p.parse_args()
    
    file_list=np.array(sorted(glob.glob(args.directory + '/*.png')))
    
    
    if args.directory[-1]=='/':
        args.directory=args.directory[:-1]
   
    arraySize=[5,4]     
 #  camList = np.array([[2,6],[1,3],[1,4],[0,7],[np.nan,np.nan],
                    #   [2,3],[1,1],[1,0],[0,0],[0,4], 
                    #   [2,5],[2,1],[1,2],[0,5],[0,2],
                    #  [2,2],[2,0],[1,7],[0,6],[0,3], 
                    #   [2,4],[2,7],[1,5],[1,6],[0,1]])    
    
    camList = np.array([[0,4],[1,2],[1,5],[np.nan,np.nan],
                        [0,2],[1,1],[1,0],[2,3],
                        [0,1],[0,3],[1,3],[2,1],
                        [0,0],[0,5],[1,4],[2,2], 
                        [np.nan,np.nan],[2,4],[2,5],[np.nan,np.nan]])    
    
#    camList = np.array([[0,4],[2,2],[2,5],[np.nan,np.nan],
#                        [0,2],[2,1],[2,0],[1,3],
#                        [0,1],[0,3],[2,3],[1,1],
#                        [0,0],[0,5],[2,4],[1,2], 
#                        [np.nan,np.nan],[1,4],[1,5],[np.nan,np.nan]])     
    ransac_threshold = 5.0
    ratio=.5  
    MIN_MATCH_COUNT = 10
    
        
    bf=h5py.File(args.brightnessFile,'r')
    brightness=bf['brightness'][()]
    b=np.mean(brightness,1)
    file_listB=bf['videoNames'][()]
    
    
    ob = []
    for cam in camList:
        if  ~np.isnan(cam[0]):
            camInt=np.array(cam)
            camInt=camInt.astype(int)
            currStr = args.directory + '/rec' + str(camInt[0]) + '_cam' + str(camInt[1]) + '_' + args.prefix + '.avi'
            strMatch=np.array([fname.find(currStr) for fname in file_listB])
            ob.append(b[strMatch==0]) 
        else:
            ob.append(np.array([0])) 
    brightnessNorm=np.array(ob)
    
    goodBright=brightnessNorm[brightnessNorm>0]
   
    meanbright=np.mean(goodBright)
    brightnessNorm=brightnessNorm/meanbright

    #when I do this for real I should run panoramaBrightness.py and then extract the brightness norm here
    # for now I'll just hard code it. Thhis is 1/(np.mean(b,1)/np.mean(b))
   # brightnessNorm =[1.08525213, 1.19647201, 1.18899992, 0.73294461, 1.26682564,
     #  1.23531485, 0.64736209, 0.88651416, 0.69369519, 1.28196861,
      # 1.48468822, 1.1556168 , 0.91202458, 0.90781265, 1.18429526,
      # 1.26892809, 1.2907323 , 0.86122618, 0.77142763, 1.17453598,
      # 1.07455001, 0.97375717, 0.88212509, 1]   
    
   
            
    camNames = np.empty(arraySize, dtype=object)
    rowCol = np.empty(arraySize, dtype=object)
    camNumbers = np.empty(arraySize, dtype='int8')
    camNeighbors = np.empty(arraySize, dtype=object)
    cn=0
    cnNoNan=0
    for row in range(arraySize[0]):
        for column in range(arraySize[1]):
            if ~np.isnan(camList[cn,0]):
                camNumbers[row,column]=cnNoNan
                cnNoNan+=1  
            else:
                camNumbers[row,column]=-1
            cn+=1
        
    cn=0
    for row in range(arraySize[0]):
        for column in range(arraySize[1]):
            if ~np.isnan(camList[cn,0]):
                camNames[row,column]='rec' + str(int(camList[cn,0])) + '_cam' + str(int(camList[cn,1])) + '_'
                rowCol[row,column]=np.array([row,column])
                camNeighbors[row,column] = camNumbers[np.maximum(row-1,0):np.minimum(row+1,arraySize[0])+1,
                np.maximum(column-1,0):np.minimum(column+1,arraySize[1])+1]
            cn+=1
            
    d = {'row,col' : rowCol.ravel() ,'names' : camNames.ravel(), 'brightnessNorm' : brightnessNorm.ravel(), 'neighbors' : camNeighbors.ravel() }
    df = pd.DataFrame(d)
    df=df.dropna()
    df.index=np.array(range(len(df)))
    
    
    descriptor = cv2.xfeatures2d.SIFT_create()
    kps=[]
    features=[]
    for img in range(file_list.shape[0]):
        strMatch=np.array([fname.find(df['names'][img]) for fname in file_list])
        currImg = np.array(Image.open(file_list[strMatch>-1][0]).convert('L'))
      
        k, f = detectAndDescribe(currImg ,descriptor)
        kps.append(k)
        features.append(f)
    kps=np.array(kps)
    features=np.array(features)
    
    
    Hmat = np.empty([kps.shape[0],kps.shape[0]], dtype=object)
    
    for img in range(file_list.shape[0]):
        strMatch=np.array([fname.find(df['names'][img]) for fname in file_list])
        currImg = np.array(Image.open(file_list[strMatch>-1][0]).convert('L'))
        neighbors = df['neighbors'][img]    
        print('img = ' + str(img))
        for n in neighbors.ravel():
            if n != -1:
                strMatch=np.array([fname.find(df['names'][n]) for fname in file_list])
                currNeighbor = np.array(Image.open(file_list[strMatch>-1][0]).convert('L'))
        
                #if I don't already have this homography, and it's not an identity map
                if img == n :
                    Hmat[img,n] = np.array([[1,0,0],[0,1,0],[0,0,1]])
                elif np.any(Hmat[img,n] == None):
                              
                    src_pts, dst_pts, M = matchKeypoints(kps[img],kps[n], features[img], features[n], 
                                              ratio, ransac_threshold,MIN_MATCH_COUNT)
                    if np.array(M).size > 1: 
                        Hmat[img,n] = np.array([[M[0,0],M[0,1],M[0,2]],[M[1,0],M[1,1],M[1,2]],[0,0,1]])
                       # Hmat[img,n] = np.array([[1,0,M[0,2]],[0,1,M[1,2]],[0,0,1]])
                        Hmat[n, img] = inv(Hmat[img,n])
                    else: 
                        #remove not real neighbors
                        badNeighbor=df['neighbors'][img]==n
                        newNeighbors=neighbors.ravel()
                        newNeighbors[badNeighbor.ravel()]=-1
                        df['neighbors'][img]=newNeighbors
                        
                    
     
    for x in range(10):
        cams=np.array(range(file_list.shape[0]))
        for img in range(file_list.shape[0]):
            
            neighbors = np.array([np.any(Hmat[img,b] != None) for b in range(file_list.shape[0])])
            neighbors=cams[neighbors]                
            for n1 in neighbors.ravel():  
                for n2 in neighbors.ravel():
                   # if np.logical_and(n1 != -1, n2 != -1):
                    if np.logical_or(np.any(Hmat[n1,n2] == None),np.any(Hmat[n2,n1] == None)):
                        Hmat[n2,n1]= np.dot(Hmat[n2,img],Hmat[img,n1])
                        Hmat[n1,n2]= np.dot(Hmat[n1,img],Hmat[img,n2])
                        
                

    for img in range(file_list.shape[0]):
        df[str(img)] = np.array(Hmat[img])
                    
    
    df.to_pickle(args.output + '.pkl')
    #df.to_pickle('/gpfs/laur/sepia_tools/4x4cameraConfig.pkl')
#   df = pickle.load( open( "testArray.pkl", "rb" ) )
