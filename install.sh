#!/bin/bash
pip install opencv-python --user  #don't want to compile it myself :)
pip install h5py --user --upgrade #future warnings with prev. versions
pip install numpy --user --upgrade  #compatability issues with numpy 1.16
pip install cython --user --upgrade
pip install pandas --user --upgrade
pip install scikit-image --user --upgrade #system one is 1.13, not compatible with new numpy
pip install configparser --user
pip install tqdm --user
pip install scikit-learn --user

cd mpi_iter
pip install . --user
cd ..

cd buffertools3
pip install . --user
cd ..

cd chromas
./setup.py install --user
cd ..

cd libreg
pip install . --user
cd ..

cd moving_least_squares
pip install . --user --upgrade
cd ..

