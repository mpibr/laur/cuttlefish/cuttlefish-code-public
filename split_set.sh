#!/bin/bash
#
# Splits a cuttlefish dataset in subsets based on the chunktimes file in
# that folder.
# Copies all files for the times and writes new chunktimes files.
#
# usage: split_set.sh [--setsize count] <inputfolder>
#
# splits with a default set size of 50
#
# Author: Friedrich Kretschmer  <friedrich.kretschmer@brain.mpg.de>
# (C) Copyright 2017 Max-Planck Institute for Brain Research

setsize=50

for i in "$@"
do
    case $i in
        --setsize=*)
            setsize="${i#*=}"
            shift
            ;;
            
        *)  
            input=$i
            shift
             ;;
    esac
done

if [[ -z ${input+x} ]]; then
    echo "input directory must be specified"
    exit 1
fi
inputdir=${input%.*}
inputdir=${inputdir%/} #remove trailing slash

chunktimes=$(find $inputdir/*.chunktimes  -printf "%f")
basename="${chunktimes%.*}"
chunktimes="$inputdir/$chunktimes"

setidx=1
timeidx=1
targetdir="${inputdir}_set$setidx"
mkdir -p $targetdir

echo $targetdir
if [[ -f $chunktimes ]]; then
    while read -r start end video_start start_secs end_secs; do
        chunk="$inputdir/$basename-$start-$end.*"
        echo $start $end $video_start $start_secs $end_secs >> "$targetdir/$basename.chunktimes"
        cp $chunk $targetdir
        if [ $timeidx == $setsize ]; then
            setidx=$((setidx+1))
            timeidx=1
            targetdir="${inputdir}_set$setidx"
            mkdir -p $targetdir
        else
            timeidx=$((timeidx+1))
        fi
    done < $chunktimes
fi
