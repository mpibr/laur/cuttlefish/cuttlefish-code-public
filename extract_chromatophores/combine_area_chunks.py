#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 07:48:29 2019

@author: reiters
"""

import argparse
import numpy as np
import string
import random
import h5py
import os
from tqdm import tqdm
#import matplotlib.pyplot as plt

BLOCKSIZE_MB = 32


if __name__ == '__main__':

    p = argparse.ArgumentParser('combine chunk areas')
    p.add_argument('chunk_area_files', nargs='+')
    p.add_argument('output')
    args = p.parse_args()
    
    
    areasFile=h5py.File(args.chunk_area_files[0],'r')
    rel_cleanqueen =  areasFile.attrs['cleanqueen']
    cleanqueen = os.path.join(os.path.dirname(areasFile.filename), \
        rel_cleanqueen)

    # if I give it only 1 file, find the relevent files assuming normal naming conventions
    if len(args.chunk_area_files) == 1:
        args.chunk_area_files=args.chunk_area_files[0]
        rel_stitching =  areasFile.attrs['stitching']
        stitching = os.path.join(os.path.dirname(args.chunk_area_files), \
            rel_stitching)
        stitching_file=h5py.File(stitching,'r')
        chunks= stitching_file.attrs['chunks']
              
        cleanqueen_file = h5py.File(cleanqueen,'r')
        chunk_mask = cleanqueen_file.attrs['chunk_mask']
        chunks=chunks[chunk_mask]
        args.chunk_area_files = [\
            os.path.join(os.path.dirname(args.chunk_area_files), 
                         os.path.split(os.path.splitext(filename)[0])[1] + '.careas') \
                for filename in chunks]
 
        

    chunkEnd=[]
    chunkStart=[]
    for f in range(len(args.chunk_area_files)):
        chunkStart.append(int(args.chunk_area_files[f].split('.')[0].split('-')[-2]))
        chunkEnd.append(int(args.chunk_area_files[f].split('.')[0].split('-')[-1]))
   
    chunkOrder=np.argsort(chunkEnd)   
    sortedCS=np.array(chunkStart)[chunkOrder]
    sortedCE=np.array(chunkEnd)[chunkOrder]
    sortedFiles = [args.chunk_area_files[i] for i in chunkOrder]
      
    #get total number of frames 

    chunkVector=[]
    frameVector=[]
    for ind, chunkAreasFile in enumerate(sortedFiles):
        if os.path.exists(chunkAreasFile):
            currFile=h5py.File(chunkAreasFile,'r')
            chunkIndex=currFile['chunk_indexes'][:]
            chunkVector.extend(ind*np.ones(len(chunkIndex)))
            frameVector.extend(range(sortedCS[ind],sortedCS[ind]+len(chunkIndex)))
            all_num_labels = currFile['areas'].shape[1]
            currFile.close()
    chunkVector=np.array(chunkVector)
    frameVector=np.array(frameVector)
    num_frames=len(chunkVector)     

    
    # Prepare output file
    output_dirname = os.path.dirname(args.output)
    output_basename = os.path.basename(args.output)
    suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
            for _ in range(4))
    tmp_output_basename = '.' + output_basename + '.' + suffix
    tmp_output = os.path.join(output_dirname, tmp_output_basename)
    areas_file = h5py.File(tmp_output, 'w')
    chunksize = min(num_frames, max(1, BLOCKSIZE_MB * 1024 * 1024 // all_num_labels // 4))
   # chunksize=4
    areas = areas_file.create_dataset('areas', \
            shape=(num_frames, all_num_labels), \
            dtype='uint32', \
            compression='gzip', \
            shuffle=True, \
            chunks=(chunksize, all_num_labels))
    chunk_indexes = areas_file.create_dataset('chunk_indexes', \
            shape=(num_frames, ), \
            dtype='uint32', \
            compression='gzip', \
            shuffle=True)

    frame_indexes = areas_file.create_dataset('frame_indexes', \
            shape=(num_frames, ), \
            dtype='uint32', \
            compression='gzip', \
           shuffle=True)
    

    areas_file.attrs.create('chunks', sortedFiles, \
                                          dtype=h5py.special_dtype(vlen=str))
    
    areas_file.attrs.create('cleanqueen', \
                os.path.relpath(cleanqueen, \
                    os.path.dirname(args.output)),
                dtype=h5py.special_dtype(vlen=str))
    
    areas_file.attrs.create('chunk_areas', \
            args.chunk_area_files, \
            dtype=h5py.special_dtype(vlen=str))
            
    
    chunk_indexes[...]=chunkVector
    frame_indexes[...]=frameVector

    startPos=0
    for chunkAreasFile in tqdm(sortedFiles, \
                               total=len(sortedFiles), \
                               desc='Combining area files'):
         if os.path.exists(chunkAreasFile): 
             currFile=h5py.File(chunkAreasFile,'r')
             sizes=currFile['areas'][:]
             endPos=sizes.shape[0]+startPos
             areas[startPos:endPos]=sizes
             startPos=endPos
             currFile.close()
        
    areas_file.close()
    os.rename(tmp_output, args.output)       
             
 
   
           
 
 
