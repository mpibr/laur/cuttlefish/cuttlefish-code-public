#!/usr/bin/env python3

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
import sys;print(sys.version)
import argparse
import cv2
import numpy as np
import skimage.morphology
import skimage.measure
from tqdm import tqdm
import os.path
import h5py
from mpi4py import MPI
import warnings
import buffertools
import traceback
import os
import random
import string
import mpi_iter

BLOCKSIZE_MB = 32

def extract_areas(chunk_index, registration_small_map, segmentation, **kwargs):
    cleanqueen = kwargs['cleanqueen']
    stitching_grid_size = kwargs['stitching_grid_size']
    stitching_column = kwargs['stitching_column']
    registration_grid_sizes = kwargs['registration_grid_sizes']
    segmentation_labels = kwargs['segmentation_labels']
    foreground = kwargs['foreground']
    num_labels = kwargs['num_labels']

    stitching_small_map = stitching_column[chunk_index]
    
    if registration_small_map is not None:
        registration_grid_size = registration_grid_sizes[chunk_index]

        # Combine the maps
        small_maps = cv2.remap(registration_small_map, \
                stitching_small_map / registration_grid_size, None, \
                interpolation=cv2.INTER_LINEAR)
    else:
        small_maps = stitching_small_map
        
    enlarge_t = np.array([[stitching_grid_size, 0, 0], \
                        [0, stitching_grid_size, 0]], 'float32')
    maps = cv2.warpAffine(small_maps, enlarge_t, \
            segmentation.shape[:2][::-1])

    # Extract foreground
    label_index = segmentation_labels[chunk_index][foreground]
    if len(segmentation.shape) == 2:
        foreground_segmentation = segmentation == label_index
        registered = cv2.remap(foreground_segmentation.astype('uint8'), \
            maps, None, \
            interpolation=cv2.INTER_LINEAR).astype('bool')

    else:
        foreground_segmentation = segmentation[..., label_index]
        # Warp segmentation
        registered = cv2.remap(foreground_segmentation.astype('float32') / 255, \
            maps, None, \
            interpolation=cv2.INTER_LINEAR)

    # Identify regions based on the cleanqueen
    all_num_labels = np.sum(num_labels)
    areas = np.empty((all_num_labels, ), 'float32')
    offset = 0
    for label in range(cleanqueen.shape[-1]):
        if len(segmentation.shape) == 2:
            # pad to allow for different masterframe sizes
            registered_pad = np.zeros(cleanqueen[..., label].shape, dtype=registered.dtype)
            registered_pad[:registered.shape[0], :registered.shape[1]] = registered
            
            labelled_regions = registered_pad * cleanqueen[..., label]
            areas_label = np.bincount(labelled_regions.flatten(), \
                minlength=int(num_labels[label]) + 1)[1 : ]
        else:
            labelled_regions = cleanqueen[..., label]
            areas_label = np.bincount(labelled_regions.flatten(), \
                registered.flatten())[1 : ]
        areas[offset : offset + int(num_labels[label])] = areas_label
        offset += int(num_labels[label])
    
    return chunk_index, areas

if __name__ == '__main__':
    comm = MPI.COMM_WORLD
    p = argparse.ArgumentParser('Extract label areas')
    p.add_argument('cleanqueen')
    p.add_argument('output')
    p.add_argument('--foreground', required=True)
    args = p.parse_args()
    
    if comm.rank == 0:
        cleanqueen_file = h5py.File(args.cleanqueen, 'r')

        stitching_filename = os.path.join(os.path.dirname(args.cleanqueen), \
                cleanqueen_file.attrs['stitching'])
        stitching_file = h5py.File(stitching_filename, 'r')
        
        try:
            # Try opening reference chunk and chunk mask
            ref_chunk = stitching_file.attrs['ref_chunk']
            chunk_mask = stitching_file.attrs['chunk_mask']
        except KeyError:    
            raise RuntimeError('Rerun stitching!')
        
        # Read info all ranks need to know
        cleanqueen = cleanqueen_file[args.foreground.replace('/', '-')][:]
        if issubclass(cleanqueen.dtype.type, np.floating):
            warnings.warn('Casting cleanqueen to int. ' \
                    'This could be dangerous.')
            cleanqueen = cleanqueen.astype('uint32')
        if len(cleanqueen.shape) == 2:
            cleanqueen = cleanqueen[..., None]
        try:
            labels = cleanqueen_file.attrs['labels']
        except KeyError:
            labels = ['label_%d' for d in range(cleanqueen.shape[2])]
        stitching_column = stitching_file['stitching_matrix'][:, ref_chunk]
        stitching_grid_size = stitching_file['stitching_matrix'] \
                .attrs['grid_size']
        registration_grid_sizes = {}
        segmentation_labels = {}
        num_frames = 0
        masterframe_filenames = []
        for chunk_index, (masterframe_filename, m) \
                in enumerate(zip(stitching_file.attrs['chunks'], chunk_mask)):
            if not m:
                continue
            
            masterframe_filename = os.path.join( \
                    os.path.dirname(stitching_filename), \
                    masterframe_filename)
            try:
                masterframe_file = h5py.File(masterframe_filename, 'r')
            except IOError as e:
                print('Cannot open file \"%s\". %s occured: %s.\n%s)'
                        % (masterframe_filename, type(e).__name__, e,
                           traceback.format_exc()))
                continue
            
            if 'segmentation' in masterframe_file:
                segmentation_filename = masterframe_filename
            else:
                segmentation_filename = os.path.join( \
                        os.path.dirname(masterframe_filename), \
                        masterframe_file.attrs['segmentation'])
                registration_filename = os.path.join( \
                        os.path.dirname(masterframe_filename), \
                        masterframe_file.attrs['registration'])
           

            try:
                segmentation_file = h5py.File(segmentation_filename, 'r')
            except IOError as e:
                print('Cannot open file \"%s\". %s occured: %s.\n%s)'
                        % (segmentation_filename, type(e).__name__, e,
                           traceback.format_exc()))
                
            segmentation = segmentation_file['segmentation']
            num_frames += len(segmentation)
            
            if segmentation.dtype == 'bool':
                segmentation_labels_chunk = \
                        {segmentation.attrs['enum'][0]: 0,
                            segmentation.attrs['enum'][1]: 1}
            else:
                if 'labels' in segmentation_file.attrs.keys():
                    # Compatible with segmentNN
                    segmentation_labels_chunk = {label: i \
                        for i, label in enumerate(segmentation_file.attrs['labels'])}
                else:
                    segmentation_labels_chunk = h5py.check_dtype(enum=segmentation.dtype)
            segmentation_labels[chunk_index] = segmentation_labels_chunk
            assert args.foreground in segmentation_labels_chunk

            segmentation_file.close()

            if 'segmentation' not in masterframe_file:
                try:
                    registration_file = h5py.File(registration_filename, 'r')
                except IOError as e:
                    print('Cannot open file \"%s\". %s occured: %s.\n%s)'
                            % (registration_filename, type(e).__name__, e,
                            traceback.format_exc()))
                    continue
                try:
                    registration_grid_sizes[chunk_index] = \
                            registration_file['maps'].attrs['grid_size']
                finally:
                    registration_file.close()
            
            masterframe_file.close()
            masterframe_filenames.append((chunk_index, masterframe_filename))
        num_labels = np.max(cleanqueen, axis=(0, 1))
        all_num_labels = int(np.sum(num_labels))

        stitching_file.close()
        cleanqueen_file.close()

    else:
        cleanqueen = None
        stitching_column = None
        stitching_grid_size = None
        registration_grid_sizes = None
        segmentation_labels = None
        num_frames = None
        num_labels = None

    # Bcast information all ranks need to know
    cleanqueen, stitching_column, stitching_grid_size, \
            registration_grid_sizes, segmentation_labels, \
            num_frames, num_labels = \
            comm.bcast((cleanqueen, stitching_column, stitching_grid_size, \
            registration_grid_sizes, segmentation_labels, \
            num_frames, num_labels))

    # Prepare multi-processing map
    if comm.rank == 0:
        # Iterate over registration and segmentation
        def input_iter():
            for chunk_index, masterframe_filename in masterframe_filenames:
                masterframe_file = h5py.File(masterframe_filename, 'r')

                if 'segmentation' in masterframe_file:
                    segmentation_filename = masterframe_filename
                    registration_file = None
                else:
                    segmentation_filename = os.path.join( \
                            os.path.dirname(masterframe_filename), \
                            masterframe_file.attrs['segmentation'])
                    
                    registration_filename = os.path.join( \
                        os.path.dirname(masterframe_filename), \
                        masterframe_file.attrs['registration'])
                    registration_file = h5py.File(registration_filename, 'r')
                    
                segmentation_file = h5py.File(segmentation_filename, 'r')
                    
                if 'segmentation' in masterframe_file:
                    with buffertools.Reader( \
                            segmentation_file['segmentation'], \
                            segmentation_file['segmentation'].chunks[0]) \
                            as segmentation_reader:
                        while not segmentation_reader.done():
                            yield chunk_index, None, \
                                    segmentation_reader.read().squeeze()

                else:
                    with buffertools.Reader( \
                            segmentation_file['segmentation'], \
                            segmentation_file['segmentation'].chunks[0]) \
                            as segmentation_reader, \
                        buffertools.Reader( \
                            registration_file['maps'], \
                            registration_file['maps'].chunks[0]) \
                            as registration_reader:
                        while not (registration_reader.done() \
                                or segmentation_reader.done()):
                            yield chunk_index, registration_reader.read(), \
                                    segmentation_reader.read()
                                    
                    registration_file.close()

                segmentation_file.close()

                masterframe_file.close()
        iter_segmentation = input_iter()
        func = None

        # Generate a vector indicating the label of every area given by the
        # clean queen
        label_mask = np.zeros((all_num_labels, ), 'uint8')
        label_types = {}
        offset = 0
        for label_type, (label, num_label) in enumerate(zip(labels, num_labels)):
            label_types[label] = label_type
            label_mask[offset : offset + int(num_label)] = label_type
            offset += int(num_label)

        # Prepare output file
        output_dirname = os.path.dirname(args.output)
        output_basename = os.path.basename(args.output)
        suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                for _ in range(4))
        tmp_output_basename = '.' + output_basename + '.' + suffix
        tmp_output = os.path.join(output_dirname, tmp_output_basename)
        areas_file = h5py.File(tmp_output, 'w')
        chunksize = min(num_frames, max(1, BLOCKSIZE_MB * 1024 * 1024 / all_num_labels / 4))
        areas_file.create_dataset('labels',
                data=label_mask, \
                dtype=h5py.special_dtype(enum=('uint8', label_types)), \
                compression='gzip', \
                shuffle=True)
        areas = areas_file.create_dataset('areas', \
                shape=(num_frames, all_num_labels), \
                dtype='float32', \
                compression='gzip', \
                shuffle=True, \
                chunks=(chunksize, all_num_labels))
        chunk_indexes = areas_file.create_dataset('chunk_indexes', \
                shape=(num_frames, ), \
                dtype='uint16', \
                compression='gzip', \
                shuffle=True, \
                chunks=(chunksize, ))
        areas_file.attrs.create('cleanqueen', \
                os.path.relpath(args.cleanqueen, \
                    os.path.dirname(args.output)), \
                dtype=h5py.special_dtype(vlen=str))
        areas_file.attrs.create('stitching', \
                os.path.relpath(stitching_filename, \
                    os.path.dirname(args.output)),
                dtype=h5py.special_dtype(vlen=str))
    else:
        iter_segmentation = None

    def func(job): 
        chunk_index, registration_small_map, segmentation = job
        return extract_areas(chunk_index, registration_small_map, segmentation,
                    cleanqueen=cleanqueen, \
                    stitching_column=stitching_column, \
                    stitching_grid_size=stitching_grid_size, \
                    registration_grid_sizes=registration_grid_sizes, \
                    segmentation_labels=segmentation_labels, \
                    foreground=args.foreground, \
                    num_labels=num_labels)
    result_iter = mpi_iter.parallel_map(comm, func, iter_segmentation)

    if comm.rank == 0:
        with buffertools.Writer(areas, chunksize) \
                 as areas_writer, \
             buffertools.Writer(chunk_indexes, chunksize) \
                 as chunk_indexes_writer:
            for chunk_index, sizes in tqdm(result_iter, total=num_frames, \
                    desc='Extracting areas'):
                areas_writer.write(sizes)
                chunk_indexes_writer.write(chunk_index)

        areas_file.close()
        os.rename(tmp_output, args.output)

