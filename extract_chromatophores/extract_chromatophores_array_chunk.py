#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 07:48:29 2019

@author: reiters
"""

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import cv2
import numpy as np
from tqdm import tqdm
import os.path
import h5py
from mpi4py import MPI
import warnings
from buffertools import buffer_handle
import traceback
import os
import random
import string
from mpi_iter import parallel_map
import matplotlib.pyplot as plt

BLOCKSIZE_MB = 32

def extract_areas(job, **kwargs):
    chunk_index = job[0]
    segmentation = job[1]
    ref_chunk = job[2]
    cleanqueen = kwargs['cleanqueen']
    maps = kwargs['maps']
    num_labels = kwargs['num_labels']
   

    if chunk_index != ref_chunk:
        
        registered = cv2.remap(segmentation, \
            maps, None, \
            interpolation=cv2.INTER_LINEAR)/255
    else:
        registered=segmentation/255


    labeledBool=registered>0.6
    labeled_regions = labeledBool*cleanqueen
    areas = np.bincount(labeled_regions.flatten(),  \
                              minlength=int(num_labels + 1), \
                              weights = registered.flatten())[1 : ]

    return chunk_index, areas

if __name__ == '__main__':
    comm = MPI.COMM_WORLD
    p = argparse.ArgumentParser('Extract label areas')
    p.add_argument('--cleanqueen')
    p.add_argument('--chunk', required=True)
    p.add_argument('--foreground', default = 'Chromatophore') 
    p.add_argument('output')
    args = p.parse_args()
    
    if comm.rank == 0:
        cleanqueen_file = h5py.File(args.cleanqueen, 'r')
        stitching_filename = os.path.join(os.path.dirname(cleanqueen_file.filename), 
                                    cleanqueen_file.attrs['stitching'])
        
        chunk_mask = np.array(cleanqueen_file.attrs['chunk_mask'])
        ref_chunk = cleanqueen_file.attrs['ref_chunk']
        
        stitching_file = h5py.File(stitching_filename, 'r')
        stitching_column = stitching_file['stitching_matrix'][:, ref_chunk]

        if 'grid_size' in stitching_file.attrs.keys():
            stitching_grid_size = stitching_file.attrs['grid_size']
        else:
            # backward compatibility
            stitching_grid_size = stitching_file['stitching_matrix'] \
                .attrs['grid_size']
          
        rel_masterframe_filenames = stitching_file.attrs['chunks']
        masterframe_filenames = np.array([os.path.join(os.path.dirname(args.cleanqueen), \
                                              rel_masterframe_filename.split('/')[-1]) \
                                 for rel_masterframe_filename in rel_masterframe_filenames])
        
        masterframes_basenames = np.array([os.path.split(os.path.splitext(mf)[0])[1] \
            for mf in masterframe_filenames])
            
        currMF = masterframe_filenames[masterframes_basenames \
            == os.path.split(os.path.splitext(args.chunk)[0])[1]][0]

        #get index of currMF
        for i, mf in enumerate(masterframe_filenames):
            
            if mf[0:len(currMF)]==currMF:
                chunk_index=i

        cleanqueen = np.squeeze(cleanqueen_file[args.foreground][:])
        
        if issubclass(cleanqueen.dtype.type, np.floating):
            warnings.warn('Casting cleanqueen to int. ' \
                    'This could be dangerous.')
            cleanqueen = cleanqueen.astype('uint32')
       
        segmentation_file = h5py.File(currMF, 'r')
        segmentation = segmentation_file['segmentation']
        num_frames=len(segmentation)
        num_labels = np.max(cleanqueen, axis=(0, 1))
        all_num_labels = int(np.sum(num_labels))
        
        segmentation_file.close()
        stitching_file.close()
        cleanqueen_file.close()
        
        small_maps = stitching_column[chunk_index]
        enlarge_t = np.array([[stitching_grid_size, 0, 0], \
                          [0, stitching_grid_size, 0]], 'float32')
        maps = cv2.warpAffine(small_maps, enlarge_t,  cleanqueen.shape[::-1])
    else:
        cleanqueen = None
        maps = None
        registration_grid_sizes = None
        num_frames = None
        num_labels = None
        
     # Bcast information all ranks need to know
    cleanqueen, maps, num_labels = \
            comm.bcast((cleanqueen, maps, num_labels))
   # 
    if comm.rank == 0:
   
        qf_height, qf_width =cleanqueen.shape
        def input_iter():
            segmentation_file = h5py.File(currMF, 'r')
     
            with buffer_handle.Reader( \
                    segmentation_file['segmentation'], \
                    segmentation_file['segmentation'].chunks[0]) \
                    as segmentation_reader:
                    
                    while not segmentation_reader.done():
                
                        seg= segmentation_reader.read()
                        seg=np.squeeze(seg[:,:,0])
                        [segH, segW ] =seg.shape
                 
                        if (segH, segW)!=(qf_height, qf_width):
                            tmpZ= np.zeros((qf_height, qf_width),dtype='uint8')
                            tmpZ[0:segH, 0:segW]=seg
                            seg=tmpZ
                        
                        yield chunk_index, seg, ref_chunk
               
            segmentation_file.close()
        iter_segmentation = input_iter()
        func = None
                  
  
        # Prepare output file
        output_dirname = os.path.dirname(args.output)
        output_basename = os.path.basename(args.output)
        suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                for _ in range(4))
        tmp_output_basename = '.' + output_basename + '.' + suffix
        tmp_output = os.path.join(output_dirname, tmp_output_basename)
        areas_file = h5py.File(tmp_output, 'w')
        chunksize = 1#min(num_frames, max(1, BLOCKSIZE_MB * 1024 * 1024 / all_num_labels / 4)).astype('int')
    
        areas = areas_file.create_dataset('areas', \
                shape=(num_frames, all_num_labels), \
                dtype='uint32', \
                compression='gzip', \
                shuffle=True, \
                chunks=(chunksize, all_num_labels))
        chunk_indexes = areas_file.create_dataset('chunk_indexes', \
                shape=(num_frames, ), \
                dtype='uint16', \
                compression='gzip', \
                shuffle=True, \
                chunks=(chunksize, ))
        areas_file.attrs.create('cleanqueen', \
                os.path.relpath(args.cleanqueen, \
                    os.path.dirname(args.output)),
                dtype=h5py.special_dtype(vlen=str))
        
        areas_file.attrs.create('stitching', \
                os.path.relpath(stitching_filename, \
                    os.path.dirname(args.output)),
                dtype=h5py.special_dtype(vlen=str))
    
    else:
        iter_segmentation = None
    

    func = lambda job:  extract_areas(job, \
                    cleanqueen=cleanqueen, \
                    maps=maps, \
                    num_labels=num_labels)
  
    result_iter = parallel_map(comm, func, iter_segmentation)

    if comm.rank == 0:
        with buffer_handle.Writer(areas, chunksize) \
                 as areas_writer, \
             buffer_handle.Writer(chunk_indexes, chunksize) \
                 as chunk_indexes_writer:
            for chunk_index, sizes in tqdm(result_iter, total=num_frames, \
                    desc='Extracting areas'):
              
                areas_writer.write(sizes)
                chunk_indexes_writer.write(chunk_index)

        areas_file.close()
        os.rename(tmp_output, args.output)

