#!/usr/bin/env python2

import argparse
import h5py
import cv2
import buffertools
from tqdm import tqdm
import os
import numpy as np
import matplotlib.cm
import matplotlib.colors

def extract_areas(masterframe_file, foreground_label, debug_imshow):
    registration_filename = os.path.join( \
            os.path.dirname(masterframe_file.filename), \
            masterframe_file.attrs['registration'])
    segmentation_filename = os.path.join( \
            os.path.dirname(masterframe_file.filename), \
            masterframe_file.attrs['segmentation'])

    with h5py.File(segmentation_filename, 'r') \
            as segmentation_file, \
         h5py.File(registration_filename, 'r') \
            as registration_file:
        segmentation = segmentation_file['segmentation']
        registration = registration_file['maps']
        grid_size = registration.attrs['grid_size']
        enlarge_t = np.array([[grid_size, 0, 0], \
                              [0, grid_size, 0]], 'float32')
        
        if segmentation.dtype == 'bool':
            segmentation_labels = \
                    {segmentation.attrs['enum'][0]: 0,
                     segmentation.attrs['enum'][1]: 1}
        else:
            segmentation_labels = \
                    h5py.check_dtype(enum=segmentation.dtype)

        with buffertools.Reader(segmentation, segmentation.chunks[0]) \
                as segmentation_reader, \
             buffertools.Reader(registration, registration.chunks[0]) \
                as registration_reader:

            cm = matplotlib.colors.ListedColormap(np.random.rand(256, 3))
            while not segmentation_reader.done():
                maps = cv2.warpAffine(registration_reader.read(), \
                        enlarge_t, \
                        (segmentation.shape[2], segmentation.shape[1]))
                foreground = segmentation_reader.read() \
                        == segmentation_labels[foreground_label]

                warped = cv2.remap( \
                        foreground.astype('uint8'), \
                        maps, None, \
                        interpolation = cv2.INTER_LINEAR).astype('bool')
                labelled = labels * warped
                norm = matplotlib.colors.Normalize(vmin=1, vmax=labels.max())
                image = cm(norm(labelled))
                if debug_imshow:
                    cv2.imshow('f', image)
                    cv2.waitKey(1)
                yield np.bincount(labelled.flatten(), minlength=labels.max() + 1)[1:]

if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('--foreground', required=True)
    p.add_argument('masterframe')
    p.add_argument('mask')
    p.add_argument('output')
    p.add_argument('--debug-imshow', action='store_true')
    args = p.parse_args()
    
    mask = cv2.imread(args.mask)
    if mask is None:
        raise RuntimeError('Cannot read mask')
    mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)

    with h5py.File(args.masterframe, 'r') as masterframe_file:
        masterframe = masterframe_file[args.foreground].value
        num_frames = masterframe_file.attrs['num_frames']

        background = mask.astype(np.uint8)
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5,5))
        background = cv2.dilate((background).astype('uint8'), kernel)
        dist_t, labels = cv2.distanceTransformWithLabels( \
                background, cv2.DIST_L2, 1)
        
        if os.path.exists(args.output):
            os.remove(args.output)
        with h5py.File(args.output, 'w') as areas_file:
            areas = areas_file.create_dataset('areas', \
                    shape=(num_frames, labels.max()), \
                    dtype='uint32', \
                    chunks=(32, labels.max()))
            areas_file.create_dataset('labels', data=labels)
            with buffertools.Writer(areas, areas.chunks[0]) as areas_writer:
                for frame_areas in tqdm( \
                        extract_areas(masterframe_file, \
                            args.foreground, args.debug_imshow), \
                        total=num_frames):
                    areas_writer.write(frame_areas)

