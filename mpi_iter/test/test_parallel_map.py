"""Tests for mpi parallel mapping tool.

Can be run in either mode.
- mpiexec -n 2 pytest ./mpi_iter
- pytest ./mpi_iter
"""

from mpi4py import MPI
import mpi_iter
from tqdm import tqdm

def test_scatter_gather():

    comm = MPI.COMM_WORLD

    def func(job):
        return job*2

    input_list = [0, 1, 2]

    # Set up generator for parallelisation
    input_iter = iter(input_list)
    result_iter = mpi_iter.parallel_map(comm, func, input_iter)

    if comm.rank == 0:
        # Iterate through generator in root to call parallel map in workers
        results = [result for result in result_iter]
        assert results == list(map(func, input_list))
    else:
        assert result_iter is None


def test_distribute_write_to_file(capsys, tmpdir):
    comm = MPI.COMM_WORLD

    def multiply(x):
        return str(x*2)

    input_list = [0, 1, 2]

    # Broadcast tmpfile from root. (Outside of the test suite, in normal use
    # case, define output file directly outside of if-clause.)
    output_file = None
    if comm.rank == 0:
        output_file = tmpdir.mkdir("sub").join("output.txt")
    output_file_handle = comm.bcast(output_file, root=0)

    # Write-to-file function
    def remote_func(job):
        output_file_handle.write(multiply(job), mode='a')

    # Set up generator for parallelisation
    input_iter = iter(input_list)
    result_iter = mpi_iter.parallel_map(comm, remote_func, input_iter)

    if comm.rank == 0:
        # Iterate through generator in root to call parallel map in workers
        for result in result_iter:
            pass
        
        # Check parallel output file against serial version
        for result in map(multiply, input_list):
            assert result in output_file.read()

def test_multiple_calls():
    comm = MPI.COMM_WORLD

    def func(job):
        return job*2

    input_list = [0, 1, 2]

    # Set up generator for 1st parallelisation
    input_iter = iter(input_list)
    result_iter = mpi_iter.parallel_map(comm, func, input_iter)        

    # Set up generator for 2nd parallelisation
    input_iter = iter(input_list)  # Must re-initiate iterator
    result_iter2 = mpi_iter.parallel_map(comm, func, input_iter)

    # Iterate through generators in root to call parallel map in workers
    if comm.rank == 0:
        results = [result for result in result_iter]
        assert results == list(map(func, input_list))

        results2 = [result for result in result_iter2]
        assert results2 == list(map(func, input_list))

def test_tqdm_integration(capsys):
    comm = MPI.COMM_WORLD

    def func(job):
        return job*2

    input_list = [0, 1, 2]

    # Set up generator for parallelisation
    input_iter = iter(input_list)
    result_iter = mpi_iter.parallel_map(comm, func, input_iter)

    if comm.rank == 0:
        # Iterate through generator in root to call parallel map in workers
        # and print process bar in root
        _ = [result for result in tqdm(\
            result_iter, total=len(input_list), desc='Multiplying')]

        captured = capsys.readouterr()
        assert 'Multiplying' in captured.err
    else:
        captured = capsys.readouterr()
        assert 'Multiplying' not in captured.err
        
    print(f'Rank {comm.rank+1}/{comm.size}')