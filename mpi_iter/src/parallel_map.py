#!/usr/bin/env python

TAG_JOB = 1
TAG_RESULT = 2

def parallel_map_master(comm, iterable):
    # Presend jobs
    num_workers_active = 0
    for slave_rank in range(1, comm.size):
        try:
            job = next(iterable)
        except StopIteration:
            break

        # Send one job
        comm.send(job, dest=slave_rank, tag=TAG_JOB)

        num_workers_active += 1

    # Send jobs, receive results
    slave_rank = 1
    while True:
        try:
            job = next(iterable)
        except StopIteration:
            break

        # Reveive result, send new job in background
        send_req = comm.isend(job, dest=slave_rank, tag=TAG_JOB)
        result = comm.recv(source=slave_rank, tag=TAG_RESULT)
        yield result
        send_req.wait()

        slave_rank = slave_rank % num_workers_active + 1

    # Receive remaining jobs
    for _ in range(num_workers_active):
        result = comm.recv(source=slave_rank, tag=TAG_RESULT)
        yield result

        slave_rank = slave_rank % num_workers_active + 1

    # Send terminating pivot element
    for dst in range(1, comm.size):
        comm.send(None, dest=dst, tag=TAG_JOB)

def parallel_map_slave(comm, func):
  
    while True:
        job = comm.recv(tag=TAG_JOB)
        if job is None:
            break
  
        comm.send(func(job), 0, tag=TAG_RESULT)
    return

def parallel_map(comm, func, iterable):
    '''
    Apply func to every item in iterable, distributing tasks using the MPI
    comm.

    Parameters:
    ===========
    comm: MPI comm, rank 0 distributes tasks and receives them
    func: function for mapping, needed for root
    iterable: an iterable, needed for all ranks that are not root

    Returns:
    ========
    a generator for that returns the result of func(elem) for elem in iterable
    This will be returned for rank 0, for all other ranks, this function
    will return None *after* the mapping is done.
    '''
    
    if comm.size <= 1:
        return (func(item) for item in iterable)
    else:
        if comm.rank == 0:
            return parallel_map_master(comm, iterable)
        else:
            return parallel_map_slave(comm, func)

