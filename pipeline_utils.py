#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 29 15:50:17 2020

@author: sam
"""

import socket
import os
import subprocess
import time
import numpy as np
import json
import scipy.io
import pandas as pd
import h5py
import cv2
from skimage import morphology
from ssh_tools import expand_path, retry_ssh, retry_rsync

def submit_job(job_args, slurm_args=None, mpicmd='srun',
               max_queued=None, wait_time=None, partition=None, 
               sshcmd=None,
               debug=False):
    """Submit job to SLURM using srun and return jobid.
    job_args: list
    slurm_args: list
    """

    # Default to parsed command-line arguments
    if max_queued is None:
        max_queued = 300
    if wait_time is None:
        wait_time = 100
    if (partition is None) or (partition == ' '):
        partition = '' 
    if sshcmd is None:
        hostname = socket.gethostname()
        sshcmd = 'ssh {}'.format(hostname)

    # Check current queue length
    user_queue = retry_ssh(\
        sshcmd, 'squeue --user {}'.format(os.path.expandvars('$USER')))

    if len(user_queue.split('\n')) > max_queued:
        print('Maximum number of queued jobs of {} reached, waiting for {} s.'.format(
            max_queued, wait_time))
        time.sleep(wait_time)
        
    # Submit job using --wrap
    cmd = 'sbatch --parsable'  # So jobid can be parsed
    if slurm_args is not None:
        cmd += " " + " ".join(slurm_args)
    if partition is not '':
        cmd += " " + f'--partition {partition}'  # No default value available
    cmd += f' --wrap="{mpicmd} {" ".join(job_args)}"' 
    
    if debug:
        print(cmd)
    
    # Retrieve jobid
    jobid = retry_ssh(sshcmd, cmd)

    return jobid




def check_filedeps(inputFileList, outputFile, rerun):
    
    nonExistantFiles=[]
    nonExistantFiles.append(0)
    for file in inputFileList:
        if os.path.isfile(file)==False:
            nonExistantFiles.append(1)
    
    outputExists=0
    if os.path.isfile(outputFile):
        outputExists=1
    if rerun:
        outputExists=0
    
    if outputExists == 0 and np.amax(np.array(nonExistantFiles)) == 0:
        return 1
    else:
        return 0
        


def check_QueueLength(queueSize, wait_time=None):

    keepWaiting=1
    while keepWaiting==1:
        user_queue = subprocess.check_output(['squeue', '--user', os.path.expandvars('$USER')])
        queue_str=user_queue.decode()
        if (len(queue_str.split('\n'))-2) > queueSize:
            print('waiting for the queue to reduce  before continuing...')
            time.sleep(5)
    #import pdb; pdb.set_trace()
        else:
            keepWaiting=0
            
            
            
def checkRelCamExistance(directory, chunk, array_params, pano_params, relCamsFile):
    df =  pd.read_pickle(array_params)
    with open(relCamsFile, 'r') as fr:
        relCams=fr.read(); 
    relCams = relCams.strip('[]\n').split()
    for ind in range(len(relCams)):
       relCams[ind] = relCams[ind].strip(',') 

    registration_files=[]
    goodRanks=[]
    goodRankInds=[]
    for img in range(len(relCams)):
        registration_fileName = (directory + '/' + df['names'][int(relCams[img])] + '-' + chunk + '.reg')
        if os.path.isfile(registration_fileName):
            registration_files.append(registration_fileName)  
            goodRanks.append(int(relCams[img]))
            goodRankInds.append(img)
            
    f = open(relCamsFile, 'w')
    json.dump(goodRanks, f)
    f.close()
    print('there are ' + str(len(goodRankInds)) + ' good files, and ' + str(len(relCams)) + ' relcams')
    #adjust panorama if a registration file drops out
    if len(goodRankInds) != len(relCams):
        print('about to modify the mat file')
        mFile=scipy.io.loadmat(pano_params) 
        uStruct=mFile['uStruct'] 
        vStruct=mFile['vStruct'] 
        m_v0_=mFile['m_v0_'].astype('int32')
        m_v1_=mFile['m_v1_'].astype('int32')
        m_u0_=mFile['m_u0_'].astype('int32')
        m_u1_=mFile['m_u1_'].astype('int32')
        mosaich=np.asscalar(mFile['mosaich'])
        mosaicw=np.asscalar(mFile['mosaicw'])
        
        uStruct = uStruct[0,goodRankInds]
        vStruct = vStruct[0,goodRankInds]
        m_v0_ =  m_v0_[goodRankInds]
        m_v1_ =  m_v1_[goodRankInds]
        m_u0_ =  m_u0_[goodRankInds]
        m_u1_ =  m_u1_[goodRankInds]
                
        scipy.io.savemat(pano_params, mdict={'uStruct': uStruct, 'vStruct': vStruct, \
                                             'm_v0_': m_v0_, \
                                             'm_v1_': m_v1_, \
                                             'm_u0_': m_u0_, \
                                             'm_u1_': m_u1_, \
                                             'mosaich':mosaich, \
                                             'mosaicw':mosaicw})
    
def get_masterframe_info(mf_file):

    num_frames = mf_file.attrs['num_frames']

    # Get masterframe
    if 'masterframe' in mf_file:
        # New format for array
        labels = mf_file['masterframe'][0]
        h, w, k = labels.shape
        labels = labels.reshape(k, h, w)
    else:
        # Old format
        labels = np.array( \
                [mf_file[label][:] \
            for label in (key.replace('/', '-') \
            for key in mf_file.attrs['labels'])], 'float32')
    
    forground_probs = np.float32(labels) / np.float32(num_frames)
    mf = (1. - np.product(1. - forground_probs, axis=0))

    # Get cuttlefish mask
    if 'chunkaverage_mask' in mf_file:
        # New format for array
        mask = mf_file['chunkaverage_mask'][:]
    else:
        # Old format
        mask = cuttlefish_mask(mf)

    return mf, mask, num_frames

def cuttlefish_mask(masterframe):
    gSigma1=2
    gSize1=15
    gSigma2=1.5
    gSize2=15
    gSigma3=30
    gSize3=int(2*(2*gSigma3)+1)
    minSize=1000000
    blur1 = cv2.GaussianBlur(masterframe, (gSize1,gSize1), gSigma1)
    blur2 = cv2.GaussianBlur(masterframe, (gSize2,gSize2), gSigma2)
    blur3=blur2-blur1
    mask = np.zeros_like(blur3, dtype=bool)
    mask[blur3>0.1] = True
    blur3 = cv2.GaussianBlur(blur3*mask, (gSize3,gSize3), gSigma3)
    mask = np.zeros_like(blur3, dtype=bool)
    mask[blur3>0] = True
    cleaned = morphology.remove_small_objects(mask, minSize, 2)
    labels, num_labels = scipy.ndimage.label(cleaned)
    if num_labels==0:
        cmask = np.ones_like(labels, dtype='bool_')
        return cmask
        # return None
    else:
        cuttlefish_label = np.argmax(np.bincount(labels.flatten())[1 : ]) + 1
        return labels == cuttlefish_label
