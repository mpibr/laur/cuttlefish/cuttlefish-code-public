#!/usr/bin/env python3

from mpi4py import MPI

import argparse
import cv2
import glob
import numpy as np
from tqdm import tqdm
import os
import h5py
import random
import os.path
import string
import pandas as pd
 
def calcFocus(img):
    redFrame=np.single(img[:,:,2])
    gSigma1=2
    gSize1=15
    gSigma2=1
    gSize2=15
    blur1 = cv2.GaussianBlur(redFrame, (gSize1,gSize1), gSigma1)
    blur2 = cv2.GaussianBlur(redFrame, (gSize2,gSize2), gSigma2)
    blur3=blur1-blur2
    mask = np.zeros_like(blur3, dtype=bool)
    mask[blur3>4] = True
    focusV=np.sum(mask[:])
    return focusV

if __name__ == '__main__':
    
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank=comm.Get_rank()
    p = argparse.ArgumentParser('calculate focus')
    p.add_argument('--dir', required=True, help='directory with panorama videos')
    p.add_argument('--array_params', required=True, help='pickle file with camera list')
    p.add_argument('output', help='Output focus file')
    args = p.parse_args()
  
    if comm.rank == 0:
        allVids=np.array(glob.glob(args.dir + '/*.avi'))
        cap = cv2.VideoCapture(allVids[0])
        num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        recvbuf = np.zeros([size,num_frames], dtype='int')
      #  df =  pd.read_pickle(args.array_params)
        
        # first make global pano
     #   goodVids=[]
     #   for img in range(df.shape[0]):
      #      strMatch=np.array([fname.find(df['names'][img]) for fname in allVids])
      #      goodVids.append(allVids[strMatch>-1][0])
    #    allVids=goodVids
#
    else:
        allVids = None
        num_frames = None
        recvbuf = None
    
    vids = comm.scatter(allVids, root=0)
    num_frames = comm.bcast(num_frames, root=0)
    rank=comm.Get_rank()
    cap = cv2.VideoCapture(vids)
    focus = np.zeros(num_frames,dtype='int')
    

    for frame in tqdm(range(num_frames), \
           total=num_frames, \
           desc='calculating focus'): 
        
        succ, img = cap.read()
        
        if succ:
            focus[frame] = calcFocus(img)
        if not succ:
            print('ERRRRRRROR!')
            break
    
    comm.Gather(focus, recvbuf, root=0)
   
    if comm.rank == 0:
       
        
        output_dirname = os.path.dirname(args.output)
        output_basename = os.path.basename(args.output)
        suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                for _ in range(4))
        tmp_output_basename = '.' + output_basename + '.' + suffix
        tmp_output = os.path.join(output_dirname, tmp_output_basename)
        focus_file = h5py.File(tmp_output, 'w')
        
        focus_dset = focus_file.create_dataset( \
                    'focus', \
                    shape=[size, num_frames], \
                    dtype='int')
        focus_dset[...] = recvbuf
        
        #calculate chunks from sum of focus stat
        minChunkLength=500
        thresh=1.5e6
        splitThresh=6000
        
        threshFocus=np.sum(recvbuf,0)>thresh
        threshCrosses=np.nonzero(np.diff(threshFocus))
        onsetLogical=threshFocus[threshCrosses]==False
        offsetLogical=threshFocus[threshCrosses]==True
        tC=np.asarray(threshCrosses)
        onsets=tC[0,onsetLogical]+1
        offsets=tC[0,offsetLogical] 
    
        if len(onsets) == 0:
            onsets=np.array([0])
    	    
        if len(offsets) ==0:
            offsets=np.array([len(threshFocus)])
    	 
     	
        if offsets[0] < onsets[0]:
            onsets=np.append([0], onsets)
            
     	    
        if offsets[len(offsets)-1] < onsets[len(onsets)-1]:
            offsets=np.append(offsets,[len(threshFocus)-1])
            
     #   if len(offsets)>len(onsets):
       #     offsets=offsets[:len(onsets)]  
    
        chunkDur = offsets-onsets
        goodChunks=chunkDur>minChunkLength
        onsets=onsets[goodChunks]
        offsets=offsets[goodChunks]
        chunkDur=chunkDur[goodChunks]
        onsets=onsets+25 #add a second to make sure it's in focus
        offsets=offsets-25
        chunkDur = offsets-onsets #recalculate
        
        
        for y in range(50):
            for x in range(len(onsets)):
                currDur = offsets[x]-onsets[x]
                if currDur>splitThresh:
                    onsetsH1=np.append(onsets[:(x+1)], [onsets[x]+np.round(currDur/2)+1])
                    onsets=np.append([onsetsH1], onsets[(x+1):len(onsets)])
                   
                    if x == 0:
                        offsetsH1= [np.round(currDur/2)]
                    else:
                        offsetsH1=np.append(offsets[:x], [offsets[x]-np.round(currDur/2)])
                    offsets=np.append([offsetsH1], offsets[x:len(offsets)])
                
        chunkTimes=np.zeros((len(onsets), 2),dtype='int')
        chunkTimes[:,0]=onsets
        chunkTimes[:,1]=offsets
        
        
        
        
        
#        import matplotlib.pyplot as plt
#        plt.figure()
#        plt.plot(np.sum(recvbuf,0))
#        for x in range(len(onsets)):
#            plt.plot([chunkTimes[x,0], chunkTimes[x,1]], [  thresh,  thresh])
            
    #    for x in range(len(fabricOn)):
         #  plt.plot([fabricOn[x], fabricOff[x]], [  thresh*2,  thresh*2])
            
       # import scipy.signal
       # mf=scipy.signal.medfilt(np.sum(recvbuf,0),311)
    		       
         
                
        video_names = focus_file.create_dataset(\
                    'videoNames', \
                    shape=[size], \
                    dtype=h5py.special_dtype(vlen=str))
        video_names[...] = allVids
        
        	
        chunkList = focus_file.create_dataset(\
                    'chunkTimes', \
                    shape=[chunkTimes.shape[0],chunkTimes.shape[1]], \
                    dtype='int')
        chunkList[...] = chunkTimes
          
        
        focus_file.close()
        os.rename(tmp_output, args.output)
