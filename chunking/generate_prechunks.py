#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
from mpi4py import MPI
import cv2
from tqdm import tqdm
import h5py
import itertools
import numpy as np
import mpi_iter
import random
import string
from scipy.ndimage import convolve

CURR_CHUNK_VERS = 2

BLOCKSIZE_MB = 512

def video_iter(cap):
    '''
    Makes OpenCV's VideoCapture to an iterable.
    '''
    while True:
        succ, img = cap.read()
        if not succ:
            break
        yield img

def chunk(iterable, chunksize):
    '''
    Returns chunks from an iterable.
    '''
    iterable = iter(iterable)
    while True:
        buffer = []
        try:
            for _ in range(chunksize):
                buffer.append(next(iterable))
        except StopIteration:
            if buffer:
                yield buffer
            break
        yield buffer

class Logger:

    def __init__(self, comm, filename, level):
        self._comm = MPI.COMM_WORLD
        if filename is not None:
            mode = MPI.MODE_WRONLY | MPI.MODE_CREATE
            self._fh = MPI.File.Open(comm, filename, mode)
            self._fh.Set_atomicity(True)
        else:
            self._fh = None
        self._level = level

    def info(self, str):
        if self._fh is not None and self._level >= 20:
            msg = '%d: %s\n' % (self._comm.rank, str)
            self._fh.Write_shared(msg)
            self._fh.Sync()

    def close(self):
        if self._fh is not None:
            self._fh.Sync()
            self._fh.Close()


def fspecial_log(hsize=5, sigma=0.5):
    '''Equivalent to fspecial('log') in MATLAB'''
    
    lin = np.round(np.linspace(-np.floor(hsize/2), np.floor(hsize/2), hsize))

    [X,Y] = np.meshgrid(lin, lin)
    
    hg = np.array([np.exp(-(xi**2 + yi**2)/(2*(sigma**2))) for xi, yi in zip(X,Y)])
    kernel_t = np.array([hgi*(xi**2 + yi**2-2*sigma**2)/(sigma**4*np.sum(hg)) for hgi, xi, yi \
                in zip(hg, X, Y)])

    log = kernel_t - np.sum(kernel_t)/hsize**2
    
    return log

def calc_focus_stat(img, LoG):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    # imfilter using LoG
    img = convolve(img.astype('float'), LoG, mode='nearest')
    img = np.round(img.clip(0,255)).astype("uint8")

    return np.sum(img).reshape(1,)

def gen_chunk_start_end(d):
    # set threshold
    d[d<args.thresh] = 0
    borders = np.diff((d!=0).astype("int32"), axis=0)
    onsetTimes = (borders==1).nonzero()[0] + 1
    offsetTimes = (borders==-1).nonzero()[0] 

    # match lengths
    if len(onsetTimes)==0: 
        onsetTimes = np.append(onsetTimes, 0)
    if len(offsetTimes)==0: 
        offsetTimes = np.append(offsetTimes, len(d))
    if offsetTimes[0] <= onsetTimes[0]:
        onsetTimes = np.insert(onsetTimes, 0, 0)
    if offsetTimes[-1] <= onsetTimes[-1]:
        offsetTimes = np.append(offsetTimes, len(d))

    # remove short chunks
    chunkDur = offsetTimes - onsetTimes
    badChunks = np.zeros(len(chunkDur), dtype="bool")
    badChunks[chunkDur < args.min_chunk_length] = True
    chunkVector = np.stack((onsetTimes, offsetTimes))
    chunkVector = chunkVector[:,~badChunks]
    
    return chunkVector

if __name__ == '__main__':
    comm = MPI.COMM_WORLD
    
    p = argparse.ArgumentParser('Prechunk video')
    p.add_argument('--video', required=True, help='Video to process')
    p.add_argument('output', help='Output prechunks file')
    p.add_argument('--thresh', type=int, default=10000)
    p.add_argument('--min-chunk-length', type=int, default=180)
    p.add_argument('--ksize', type=int, default=15)
    p.add_argument('--sigma', type=int, default=2)
    p.add_argument('--video-start', type=float, help='Video starting time', \
                   default=0.000000)
    p.add_argument('--start', type=float, help='Start video at')
    p.add_argument('--end', type=float, help='End video at')
    p.add_argument('--log-level', type=int, default=0)
    p.add_argument('--log')
    p.add_argument('--threads', type=int, default=1)
    args = p.parse_args()

    logger = Logger(comm, args.log, args.log_level)
    
    # Build filter
    if comm.rank == 0:
        logger.info('Build Laplacian of Gaussian (LoG) filter')
        LoG = fspecial_log(args.ksize, args.sigma)
    else:
        LoG = None

    # Bcast filter
    logger.info('Broadcasting filter')
    LoG = comm.bcast(LoG)
    
    # Open video
    err = False
    if comm.rank == 0:
        logger.info('Open video')
        cap = cv2.VideoCapture(args.video)
        if not cap.isOpened():
            err = True
    err = comm.bcast(err)
    if err:
        raise RuntimeError('Video cannot be opened')

    # Master rank prepares input, output
    logger.info('Preparing worker-consumer-pattern')
    frame_iter = None
    func = None
    if comm.rank == 0:
        # Get video info
        if int(cv2.__version__.split('.')[0]) >= 3:
            fps = float(cap.get(cv2.CAP_PROP_FPS))
            height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
            width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
            num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        else:
            fps = float(cap.get(cv2.cv.CV_CAP_PROP_FPS))
            height = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
            width = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
            num_frames = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
        logger.info('Video at %dfps, length %d, %dx%d' \
                % (fps, num_frames, width, height))

        # Setup start and end times
        if args.start is None:
            args.start = args.video_start
        if args.end is None:
            args.end = args.video_start + num_frames / fps
        num_frames = int((args.end - args.start) * fps)

        # Goto the video start
        logger.info('Cutting video at %d-%d' % (args.start, args.end))
        if int(cv2.__version__.split('.')[0]) >= 3:
            prop_pos = cv2.CAP_PROP_POS_MSEC
        else:
            prop_pos = cv2.cv.CV_CAP_PROP_POS_MSEC
        chunkoffset = 0
        while cap.get(prop_pos) / 1000. + args.video_start < args.start:
            cap.grab()
            chunkoffset += 1
        
        # Setup frame iterator
        logger.info('Setting up iterators')
        frame_iter = video_iter(cap)
        frame_iter = itertools.islice(frame_iter, 0, num_frames)
        
        # Open up output
        output_dirname = os.path.dirname(args.output)
        output_basename = os.path.basename(args.output)
        suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                for _ in range(4))
        tmp_output_basename = '.' + output_basename + '.' + suffix
        tmp_output = os.path.join(output_dirname, tmp_output_basename)
        logger.info('Opening output')
        writer = h5py.File(tmp_output, 'w')
        logger.info('Creating attribute video')
        writer.attrs.create('/video', os.path.relpath(args.video, \
                os.path.dirname(args.output)), \
                dtype=h5py.special_dtype(vlen=str))
        logger.info('Creating attribute thresh')
        writer.attrs.create('/thresh', args.thresh, dtype='int32')
        logger.info('Creating attribute minChunkLength')
        writer.attrs.create('/minChunkLength', args.min_chunk_length, dtype='uint64')
        logger.info('Creating attribute log_ksize')
        writer.attrs.create('/ksize', args.ksize, dtype='uint8')
        logger.info('Creating attribute log_sigma')
        writer.attrs.create('/sigma', args.sigma, dtype='uint8')
        logger.info('Creating attribute currChunkVers')
        writer.attrs.create('/version', CURR_CHUNK_VERS, dtype='uint8')

        chunksize = max(1, BLOCKSIZE_MB * 1024 * 1024 / height / width)
        logger.info('Creating dataset chunks (focus stat) at size ' + str(chunksize))
        focus_stat = writer.create_dataset('chunks', \
                shape=(num_frames, 1), chunks=(chunksize,1), dtype='float32') 
                # TODO: potentially save space switching to int32
    
    # Function to be called
    logger.info('Setting up function call')
    func = lambda img: calc_focus_stat(img, LoG)

    # Process stuff in a worker consumer way
    # Root rank reads frames, slave ranks process them
    logger.info('Processing frames')
    result_iter = mpi_iter.parallel_map(comm, func, frame_iter)
    if comm.rank == 0:
        chunksize = focus_stat.chunks[0]
        for num, focus_chunk in \
                enumerate(chunk(tqdm(result_iter, \
                total=num_frames, desc='Calculating focus statistic'), chunksize)):
            start = num * chunksize
            end = start + len(focus_chunk)
            focus_stat[start : end] = focus_chunk
            
        # calculate preliminary chunks    
        chunk_ends = gen_chunk_start_end(focus_stat[:])
        writer.create_dataset('chunkTimes', data=chunk_ends, dtype='uint64')
        
        writer.close()
        os.rename(tmp_output, args.output)
        cap.release()
    logger.info('Done')    