#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import tempfile
import argparse
import h5py
import subprocess
from tqdm import tqdm
import cv2

# Old versions of x264 lacking metadata
# https://superuser.com/a/1371105
BROKEN_ENCODERS = ['Lavc57.89.100']

def process_chunks(video, chunktimes, start_frames, end_frames, add_metadata=False, 
                   recode=False, write_png=False):
    
    outdir = os.path.dirname(chunktimes)
    prefix = os.path.splitext(chunktimes)[0]
    basename = os.path.basename(prefix)
    _, chunktimes_tmp = tempfile.mkstemp(prefix=chunktimes+'.', dir='.')
    cmd_args = ['ffprobe', \
        '-loglevel', 'error', \
        '-select_streams', 'v:0', \
        '-show_entries', 'stream=r_frame_rate', \
        '-print_format', 'csv=print_section=0', \
        video]
    fps_str = subprocess.check_output(cmd_args).strip().decode()

    try:
        fps = float(fps_str)
    except ValueError:
        numerator, denominator = fps_str.split('/')
        fps = float(numerator)/float(denominator)
        
    if recode:
        prefix, ext = os.path.splitext(video)
        video_recoded = prefix + '_recoded' + ext
        ffmpeg_cmd = f'ffmpeg \
            -y \
            -hide_banner -loglevel quiet -stats \
            -i {video} \
            -c:v h264 \
            -copyts \
            {video_recoded}'
        print('Recode video.')
        os.system(ffmpeg_cmd)
        video = video_recoded

    for start_f, end_f in tqdm(zip(start_frames, end_frames), desc='Chunking'):
        chunkname = '{}-{}-{}'.format(basename, start_f, end_f)
        chunk = os.path.join(outdir, chunkname) + '.mp4'
        
        start_secs = '{:0.3f}'.format(start_f / fps)
        end_secs = '{:0.3f}'.format(end_f / fps)

        if not os.path.isfile(chunk) or \
            (os.path.getctime(chunk) < os.path.getctime(video)):
            ffmpeg_cmd = f'ffmpeg \
                -y \
                -ss {start_secs} \
                -i {video} \
                -t {end_secs} \
                -codec copy \
                -copyts \
                -metadata comment="{start_secs}:{end_secs}:{video}" \
                -loglevel error'
            if add_metadata:
                ffmpeg_cmd += ' -avoid_negative_ts 1 -bsf:v h264_metadata="{}"'.format(
                    'sei_user_data=dc45e9bde6d948b7962cd820d923eeef+x264 - core 150'
                )
            ffmpeg_cmd += ' ' + chunk
            
            os.system(ffmpeg_cmd)
        else:
            print('Chunk already exists: ' + chunk)
        
        cmd_args = ['ffprobe', \
            '-loglevel', 'error', \
            '-select_streams', 'v:0', \
            '-show_entries', 'stream=start_time', \
            '-print_format', 'csv=print_section=0', \
            chunk]
        chunk_start = subprocess.check_output(cmd_args).decode().strip()
        
        with open(chunktimes_tmp, 'a') as f:
            chunk_info = '{} {} {} {} {}\n'.format(
                start_f, end_f, chunk_start, start_secs, end_secs)
            f.write(chunk_info)
            
        if write_png:
            frame = os.path.join(outdir, chunkname) + '.png'
            if not os.path.isfile(frame):
            
                cap = cv2.VideoCapture(chunk)
                
                # prop_pos = cv2.CAP_PROP_POS_MSEC
                # while cap.get(prop_pos) / 1000. + float(chunk_start) < float(start_secs):
                    # cap.grab()
                    
                # HACK: because for some reason cap.grab() doesn't work
                prop_pos = cv2.CAP_PROP_POS_FRAMES
                fps = cap.get(cv2.CAP_PROP_FPS)
                while cap.get(prop_pos) + float(chunk_start)/fps < float(start_secs)/fps:
                    _ = cap.read()
                    
                succ, img = cap.read()
        
                cv2.imwrite(frame, img)

    os.rename(chunktimes_tmp, chunktimes)

if __name__ == '__main__':
    p = argparse.ArgumentParser('Chunking video')
    p.add_argument('--video', required=True, help='Video file')
    p.add_argument('--chunks', help='Chunks file')
    p.add_argument('--write-png', action='store_true', help='Write first frame as png.')
    p.add_argument('--array', action='store_true', help='Different chunks file format.')
    p.add_argument('output', help='Chunktimes file')
    args = p.parse_args()

    video = args.video
    prefix = os.path.splitext(video)[0]
    if args.chunks is None:
        args.chunks = prefix + '.chunks'

    # Check video encoder version to determine if missing metadata
    add_metadata = False
    recode = False
    encoder = subprocess.check_output([
        'ffprobe', video,
        '-select_streams', 'v:0',
        '-show_entries', 'stream_tags=encoder',
        '-print_format', 'csv=print_section=0']).decode().split(' ')[0]
    if encoder in BROKEN_ENCODERS:
        add_metadata = False
        recode = True
        print('Video encoded without appropriate x264 metadata.')
    
    # Get chunking times
    with h5py.File(args.chunks, 'r') as hf:
        if args.array:
            start_times, end_times = hf['chunkTimes'][:].astype('uint64').T
        else:
            start_times, end_times = hf['chunkTimes'][:].astype('uint64')

    process_chunks(video, args.output, start_times, end_times, add_metadata, recode, args.write_png)
