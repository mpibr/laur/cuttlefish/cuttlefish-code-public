#!/usr/bin/env python2

from mpi4py import MPI

import argparse
import cv2
import glob
import mpi_iter
import itertools
import numpy as np
from tqdm import tqdm
import os
import h5py
import random
import os.path
import string

 
def calcFocus(img):
    redFrame=np.single(img[:,:,2])
    gSigma1=2
    gSize1=15
    gSigma2=1
    gSize2=15
    blur1 = cv2.GaussianBlur(redFrame, (gSize1,gSize1), gSigma1)
    blur2 = cv2.GaussianBlur(redFrame, (gSize2,gSize2), gSigma2)
    blur3=blur1-blur2
    mask = np.zeros_like(blur3, dtype=bool)
    mask[blur3>4] = True
    focusV=np.sum(mask[:])
    return focusV



if __name__ == '__main__':
    
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank=comm.Get_rank()
    p = argparse.ArgumentParser('calculate focus')
    p.add_argument('--dir', required=True, help='directory with panorama videos')
    p.add_argument('output', help='Output focus file')
    args = p.parse_args()
  
    if comm.rank == 0:
        allVids=glob.glob(args.dir + '/*.avi')
        cap = cv2.VideoCapture(allVids[0])
        num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        recvbuf = np.zeros([size,num_frames], dtype='int')
        
    else:
        allVids = None
        num_frames = None
        recvbuf = None
    
    vids = comm.scatter(allVids, root=0)
    num_frames = comm.bcast(num_frames, root=0)
        
    rank=comm.Get_rank()
    cap = cv2.VideoCapture(vids)
    focus = np.zeros(num_frames,dtype='int')
    

    for frame in tqdm(range(num_frames), \
           total=num_frames, \
           desc='calculating focus'): 
        
        succ, img = cap.read()
        
        if succ:
            focus[frame] = calcFocus(img)
        if not succ:
            print('ERRRRRRROR!')
            break
    
    comm.Gather(focus, recvbuf, root=0)
   
    if comm.rank == 0:
       
        output_dirname = os.path.dirname(args.output)
        output_basename = os.path.basename(args.output)
        suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                    for _ in range(4))
        tmp_output_basename = '.' + output_basename + '.' + suffix
        tmp_output = os.path.join(output_dirname, tmp_output_basename)
        focus_file = h5py.File(tmp_output, 'w')
    
        focus_dset = focus_file.create_dataset( \
                        'focus', \
                        shape=[size, num_frames], \
                        dtype='int')
        focus_dset[...] = recvbuf
        
        video_names = focus_file.create_dataset(\
                        'videoNames', \
                        shape=[size], \
                        dtype=h5py.special_dtype(vlen=unicode))
        video_names[...] = allVids
        focus_file.close()
        os.rename(tmp_output, args.output)