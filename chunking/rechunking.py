import numpy as np
import h5py
import glob

dir = '/Volumes/laur/sepia_compressed/sepia206-20191216-001/'
chunks= dir + '2019-12-16-10-56-57.chunks'
minChunkLength = 500
thresh = 1.5e6
splitThresh = 6000

focus_file = h5py.File(chunks, 'r')
recvbuf = focus_file['focus']
focus_file['chunkTimes']


threshFocus = np.sum(recvbuf, 0) > thresh
threshCrosses = np.nonzero(np.diff(threshFocus))
onsetLogical = threshFocus[threshCrosses] == False
offsetLogical = threshFocus[threshCrosses] == True
tC = np.asarray(threshCrosses)
onsets = tC[0, onsetLogical] + 1
offsets = tC[0, offsetLogical]

if len(onsets) == 0:
    onsets = np.array([0])

if len(offsets) == 0:
    offsets = np.array([len(threshFocus)])

if offsets[0] < onsets[0]:
    onsets = np.append([0], onsets)

if offsets[len(offsets) - 1] < onsets[len(onsets) - 1]:
    offsets = np.append(offsets, [len(threshFocus) - 1])

chunkDur = offsets - onsets
goodChunks = chunkDur > minChunkLength
onsets = onsets[goodChunks]
offsets = offsets[goodChunks]
chunkDur = chunkDur[goodChunks]

for x in range(len(onsets)):
    currDur = offsets[x] - onsets[x]
    if currDur > splitThresh:
        onsetsH1 = np.append(onsets[:(x + 1)], [onsets[x] + np.round(currDur / 2) + 1])
        onsets = np.append([onsetsH1], onsets[(x + 1):len(onsets)])

        if x == 0:
            offsetsH1 = [np.round(currDur / 2)]
        else:
            offsetsH1 = np.append(offsets[:x], [offsets[x - 1] + np.round(currDur / 2)])
        offsets = np.append([offsetsH1], offsets[x:len(offsets)])

chunkTimes=np.zeros((len(onsets), 2),dtype='int')
chunkTimes[:,0]=onsets+25 #add a second to make sure it's in focus
chunkTimes[:,1]=offsets

chunk2='/Volumes/laur/sepia_compressed/sepia206-20191216-001/2019-12-16-10-56-57.chunk2'

focus_file2 = h5py.File(chunk2, 'w')
# focus_file2=focus_file
chunkList = focus_file2.create_dataset( \
    'chunkTimes', \
    shape=[chunkTimes.shape[0], chunkTimes.shape[1]], \
    dtype='int')
chunkList[...] = chunkTimes
focus_file2.create_dataset("focus", data=focus_file['focus'])
focus_file2.create_dataset("videoNames", data=focus_file['videoNames'])

vids = glob.glob(dir + '/*.avi')

video_names = focus_file2.create_dataset( \
    'videoNames', \
    shape=[18], \
    dtype=h5py.special_dtype(vlen=unicode))
video_names[...] = vids


focus_file.close()
focus_file2.close()

# focus_file2 = h5py.File(chunk2, 'r')
# focus_file2['chunkTimes']
# focus_file2.close()

