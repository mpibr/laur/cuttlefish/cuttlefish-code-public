#!/usr/bin/env python2

import argparse
import numpy as np
from tqdm import tqdm
import os.path
import h5py
import buffertools
import cv2
import itertools

def iter_buffered(dataset):
    '''
    Iterates through a hdf5 dataset using a local buffer for efficient reading.
    '''

    length = len(dataset)
    chunksize = dataset.chunks[0]
    chunk_ranges = [(s, min(length, s + chunksize)) \
            for s in range(0, length, chunksize)]
    for start, end in chunk_ranges:
        chunk = dataset[start : end]
        for item in chunk:
            yield item

if __name__ == '__main__':
    p = argparse.ArgumentParser('Play side by side vid')
    p.add_argument('--crop')
    p.add_argument('--scale', type=float)
    p.add_argument('--output')
    p.add_argument('--codec', default="MP4V")
    p.add_argument('--max-frames', type=int)
    p.add_argument('registration', help='Registration hd5 file')
   
    args = p.parse_args()
    
    label_colours = np.array([[0, 0, 0],[ 255, 0, 255],[0, 255, 0]])

    # Read the registration file
    registration = h5py.File(args.registration, 'r')
    maps_dset = registration['maps']
    reg_num_frames = maps_dset.shape[0]
    if args.max_frames is not None:
        reg_num_frames = args.max_frames
    fps = registration.attrs['fps']
    rel_video_filename = registration.attrs['video']
    rel_segmentation_filename = registration.attrs['segmentation']
    chunkoffset = registration.attrs['chunkoffset']

    # Transformation to scale maps up
    scale_maps_t = np.array([[maps_dset.attrs['grid_size'], 0., 0.], \
                             [0., maps_dset.attrs['grid_size'], 0.]], 'float32')
    
    # Open video
    video_filename = os.path.join( \
            os.path.dirname(args.registration), \
            rel_video_filename)
    video = cv2.VideoCapture(video_filename)
    if cv2.__version__.split('.')[0] == '3':
        fps = float(video.get(cv2.CAP_PROP_FPS))
        height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
        width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    else:
        fps = float(video.get(cv2.cv.CV_CAP_PROP_FPS))
        height = int(video.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
        width = int(video.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))

    # Open up the segmentation
    segmentation_filename = os.path.join( \
            os.path.dirname(args.registration), \
            rel_segmentation_filename)
    segmentation_file = h5py.File(segmentation_filename, 'r')

    num_frames, height, width = segmentation_file['segmentation'].shape
    # label_colours = segmentation_file.attrs['label_colours']
    segmentation_iter = iter_buffered(segmentation_file['segmentation'])

    # Goto chunk start
    for _ in range(chunkoffset):
        video.grab()
    def create_video_iter():
        for _ in range(reg_num_frames):
            succ, img = video.read()
            if not succ:
                raise RuntimeError('Video finished too early')
            yield img
    video_iter = create_video_iter()
    def create_iter_maps():
        with buffertools.Reader(maps_dset, maps_dset.chunks[0]) \
                as maps_reader:
            while not maps_reader.done():
                yield maps_reader.read()
    maps_iter = create_iter_maps()
    
    # Calculate output video frame size
    output_framesize = (width, height)
    if args.crop is not None:
        x, y, width, height = (int(s) for s in args.crop.split(','))
        output_framesize = (width, height)
    if args.scale is not None:
        output_framesize = tuple(int(s * args.scale + .5) \
                for s in output_framesize)
    output_canvas_size = 2*output_framesize[0], output_framesize[1]
    
    # Open output video
    if args.output is not None:
        if cv2.__version__.split('.')[0] == '3':
            fourcc = cv2.VideoWriter_fourcc(*args.codec)
        else:
            fourcc = cv2.cv.FOURCC(*args.codec)
        output_video_file = cv2.VideoWriter(args.output, fourcc, \
                fps, output_canvas_size)
        if not output_video_file.isOpened():
            raise RuntimeError("Cannot write video")
    else:
        cv2.namedWindow('side_by_side',cv2.WINDOW_NORMAL)
        cv2.resizeWindow('side_by_side', output_canvas_size[0], output_canvas_size[1])
    
    # Play registered video
    for frame, maps, segmentation in tqdm(itertools.izip(video_iter, maps_iter, segmentation_iter), \
            total=reg_num_frames, \
            desc='Playing side-by-side'): 

        img = np.ones(segmentation.shape + (3,), 'uint8')
                
        for label, colour in enumerate(label_colours):
            img[segmentation == label] = colour[::-1]

        # Warp frame and segmentation
        maps = cv2.warpAffine(maps, scale_maps_t, frame.shape[1::-1])
        frame = cv2.remap(frame, maps, None, \
                interpolation=cv2.INTER_LINEAR)
        img = cv2.remap(img, maps, None, \
                interpolation=cv2.INTER_LINEAR)
        
        # Crop and scale
        if args.crop is not None:
            x, y, width, height = (int(s) for s in args.crop.split(','))
            frame = frame[y : y + height, x : x + width]
            img = img[y : y + height, x : x + width]
        if args.scale is not None:
            frame = cv2.resize(frame, output_framesize)
            img = cv2.resize(img, output_framesize)

        (hA, wA) = frame.shape[:2]
        (hB, wB) = img.shape[:2]
        vis = np.zeros((output_canvas_size[1], output_canvas_size[0], 3), dtype="uint8")
        vis[:hA, :wA] = frame
        vis[:hB, wA:] = img
        
        if args.output is None:
            cv2.imshow('side_by_side', vis)
            cv2.waitKey(1)
        else:
            output_video_file.write(vis)

    if args.output is not None:
        output_video_file.release()

    video.release()
    registration.close()