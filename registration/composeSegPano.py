#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 09:46:48 2020

@author: sam
"""


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 10:33:44 2019

@author: reiters
"""
import os
import numpy as np
import cv2
import h5py
import argparse
import scipy.io
import pandas as pd
import random
import string
#from skimage import morphology
import scipy.ndimage
from scipy import spatial
import moving_least_squares
from skimage import morphology
from tqdm import tqdm
from dipy.align import VerbosityLevels
from dipy.align.imwarp import SymmetricDiffeomorphicRegistration
from dipy.align.metrics import CCMetric
import matplotlib.pyplot as plt


def cuttlefish_mask(masterframe):
  #edge effects from the nn
    masterframe[:,1:32]=0
    masterframe[1:32,:]=0
    masterframe[:,-32:]=0
    masterframe[-32:,:]=0
    masterframe=np.float32(masterframe)
    
    gSigma1=1
    gSize1=15
    gSigma2=10
    gSize2=25
    gSigma3=5
    gSize3=int(2*(2*gSigma3)+1)
    minSize=100000
    blur1 = cv2.GaussianBlur(masterframe, (gSize1,gSize1), gSigma1)
    blur2 = cv2.GaussianBlur(masterframe, (gSize2,gSize2), gSigma2)
    blur3=blur1-blur2
    mask = np.zeros_like(blur3, dtype=bool)
    mask[blur3>0.9] = True
    blur3 = cv2.GaussianBlur(blur3*mask, (gSize3,gSize3), gSigma3)
    mask = np.zeros_like(blur3, dtype=bool)
    mask[blur3>0.3] = True
    
    
    cleaned = morphology.remove_small_objects(mask, minSize, 2)
    labels, num_labels = scipy.ndimage.label(cleaned)
    if num_labels==0:
        return None
    else:
        cuttlefish_label = np.argmax(np.bincount(labels.flatten())[1 : ]) + 1
        return labels == cuttlefish_label



def chunk(iterable, chunksize):
    '''
    Returns chunks from an iterable.
    '''
    iterable = iter(iterable)
    while True:
        buffer = []
        try:
            for _ in range(chunksize):
                buffer.append(next(iterable))
        except StopIteration:
            if buffer:
                yield buffer
            break
        yield buffer
        
def iter_buffered(dataset):
    '''
    Iterates through a hdf5 dataset using a local buffer for efficient reading.
    '''

    length = len(dataset)
    chunksize = dataset.chunks[0]
    chunk_ranges = [(s, min(length, s + chunksize)) \
            for s in range(0, length, chunksize)]
    for start, end in chunk_ranges:
        chunk = dataset[start : end]
        for item in chunk:
            yield item


def detect_points(segmentation, det_min_points, det_max_points, pad, \
        circularity_thresh):


    contours, hierarchy = cv2.findContours(segmentation, \
            cv2.RETR_CCOMP, cv2.CHAIN_APPROX_TC89_L1)[-2:]

    # Get outer most contours
    contour_f = []
    index = 0
    while index >= 0:
        contour = contours[index]
        m = cv2.moments(contour, False)
        contour_f.append((m, cv2.arcLength(contour, True)))
        index = hierarchy[0, index, 0]

    # Filter no arc length
    contour_f = list(filter(lambda cf : cf[1]>0 , contour_f))

    # Filter out non circular
    contour_f = list(filter(lambda m: 4 * np.pi * m[0]['m00'] / m[1] / m[1] \
            > circularity_thresh, contour_f))
    contour_f = list(filter(lambda m: \
               pad < m[0]['m10'] / m[0]['m00'] \
            and m[0]['m10'] / m[0]['m00'] < segmentation.shape[1] - pad \
            and pad < m[0]['m01'] / m[0]['m00'] \
            and m[0]['m01'] / m[0]['m00'] < segmentation.shape[0] - pad, \
            contour_f))

    # get centres
    points = np.asarray(list(map(lambda m: \
            (m[0]['m10'] / m[0]['m00'], m[0]['m01'] / m[0]['m00']), \
        contour_f)), dtype='float32')

    if len(points) < det_min_points:
            raise RuntimeError('cataclysmic error!')
    # find ~evenly spaced centers
   # import pdb; pdb.set_trace()
    minXY = np.amin(points,axis=0).astype(int)
    maxXY = np.amax(points,axis=0).astype(int)
    gridPts=np.sqrt(min(det_max_points, points.shape[0])).astype(int)
    xgrid = np.linspace(minXY[0], maxXY[0], gridPts)
    ygrid = np.linspace(minXY[1], maxXY[1], gridPts)
   # import pdb; pdb.set_trace()
    xv, yv = np.meshgrid(xgrid, ygrid)
    tree = spatial.KDTree(points)
    distance, index = tree.query(list(zip(xv.ravel(), yv.ravel())),k=1)
    points = np.unique(points[index],axis=0)
    return points

flatten = lambda l: [item for sublist in l for item in sublist]

if __name__ == '__main__':
    
  
    p = argparse.ArgumentParser('compose panorama')
    p.add_argument('--directory', required=True)
    p.add_argument('--array-params', help='for making a panorama')
    p.add_argument('--data-dir', required=True)
    p.add_argument('--chunk', required=True)
    
    # Warping parameters
    p.add_argument('--mls-alpha', type=float, default=7., \
            help='MLS smoothing factor')
    p.add_argument('--mls-grid', type=int, default='16', \
            help='Grid point distance for evaluating MLS')

    # Tracking parameters
    p.add_argument('--winsize', default=128, type=int, \
            help='Window size around the tracked object')
    
    # Detector algorithm
    p.add_argument('--det-max-points', default=40000, type=int, \
            help='Number of tracking points')
    p.add_argument('--det-min-points', default=400, type=int, \
            help='Minimal number of tracking points')
    p.add_argument('--det-circularity-thresh', type=float, default=0.1)

    # Symmetric Diffeomorphic Registration
    p.add_argument('--sdr-sigma-diff', default=7, type=float, \
            help='Standard deviation of the Gaussian smoothing kernel.')
    p.add_argument('--sdr-radius', default=32, type=int, \
            help='Radius of neighborhood to compute cross correlation.')
    p.add_argument('--sdr-level-iters', default='50,25,5', type=str, \
            help='Number of iterations at each pyramid level')
    
    p.add_argument('--max-frames', type=int)
    p.add_argument('--crop', help='x,y,w,h')
    p.add_argument('--scale', type=float)
    p.add_argument('--seg-video-output', help='"True" to use default extension')
    p.add_argument('--pano-video-output', help='"True" to use default extension')
    p.add_argument('--codec', default="H264")
    p.add_argument('--no-segPano', action='store_true')
    p.add_argument('--foreground', default='Chromatophore')
    p.add_argument('output', help='output segPano')
    args = p.parse_args()

    
    
    # directory='/home/sam/flash/1cc92bea-d556-11ea-baff-db0f47e23c31'
    # data_dir='/home/sam/bucket/sepia_processed/sepia209-20200607-001'
    # chunkName='2020-06-07-14-32-04-152751-160574'
    # output='testOut'
    
    directory = args.directory
    data_dir=args.data_dir
    chunkName = args.chunk
    output=args.output
    
    if args.seg_video_output == 'True':
        args.seg_video_output = os.path.splitext(output)[0] + '_segPano.mp4'
    
    if args.pano_video_output == 'True':
        args.pano_video_output = os.path.splitext(output)[0] + '_pano.mp4'
        
    det_max_points = args.det_max_points
    det_min_points = args.det_min_points
    det_circularity_thresh = args.det_circularity_thresh
    winsize = args.winsize
    mls_alpha = args.mls_alpha
    grid_size = args.mls_grid

    def callback_CC(sdr, status):
        STATUS_DICT = {
            0: 'INIT_START',
            1: 'INIT_END',
            2: 'OPT_START',
            3: 'OPT_END',
            4: 'SCALE_START',
            5: 'SCALE_END',
            6: 'ITER_START',
            7: 'ITER_END'
        }
        status_string = STATUS_DICT[status]
        print(f'SDR status: {status} - {status_string}')
            
    metric = CCMetric(dim=2, sigma_diff=args.sdr_sigma_diff, radius=args.sdr_radius)
    level_iters = [int(el) for el in args.sdr_level_iters.split(',')]
    sdr = SymmetricDiffeomorphicRegistration(metric, level_iters)
    sdr.callback = callback_CC
    # sdr.verbosity = VerbosityLevels.DEBUG
    print(f'SDR params: Sigma={args.sdr_sigma_diff}, radius={args.sdr_radius}, pyramids={level_iters}')
                    
    workingFolder = directory + '/' + chunkName
    # array_params = directory + '/array_config'
    array_params = args.array_params
    relCamsFile = workingFolder + '_relCams'
    pano_params = workingFolder + '_panoParams.mat'
    matStr = workingFolder + '_.mat'
    
    #first need to set the order
    try:
        df =  pd.read_csv(array_params)
    except:
        df =  pd.read_pickle(array_params)

    with open(relCamsFile, 'r') as fr:
        relCams=fr.read(); 
    relCams = relCams.strip('[]\n').split()
    for ind in range(len(relCams)):
       relCams[ind] = relCams[ind].strip(',') 
   
    segmentation_files=[]
    brightness_norm_list = []
    for img in range(len(relCams)):
        # segmentation_files.append(data_dir + '/' + df['names'][int(relCams[img])] + '-' + chunkName + '.seg')
        segmentation_files.append(os.path.join(data_dir, df['names'][int(relCams[img])] + chunkName + '.seg'))
        brightness_norm_list.append(df['brightnessNorm'][int(relCams[img])])
    
    mFile=scipy.io.loadmat(pano_params, squeeze_me=True) 
    uStruct=mFile['uStruct'] 
    vStruct=mFile['vStruct'] 
    m_v0_=mFile['m_v0_'].astype('int32')
    m_v1_=mFile['m_v1_'].astype('int32')
    m_u0_=mFile['m_u0_'].astype('int32')
    m_u1_=mFile['m_u1_'].astype('int32')
    height=mFile['mosaich']
    width=mFile['mosaicw']
    
    kernel = np.ones((3,3),np.uint8) 
    BLOCKSIZE_MB = 32
    chunksize = 1
      
    scale_maps_t = np.array([[grid_size, 0., 0.], \
                          [0., grid_size, 0.]], 'float32')
        
    identity_maps = np.dstack(np.meshgrid( \
                    *tuple(np.arange(0, s + grid_size, grid_size) \
                            for s in (width, height)))).astype('float32')
    coords = identity_maps.reshape((-1, 2))
    
    
    segmentation_iter=[]
    cap_list=[]
    u_im_=[]
    v_im_=[]
    for cam in range(len(segmentation_files)):
        segmentation_file = segmentation_files[cam]
        currCam=segmentation_file.split('/')[-1].split('-')[0]
                
        u_im_.append(np.float32(np.squeeze(uStruct)[cam]))
        v_im_.append(np.float32(np.squeeze(vStruct)[cam]))
    
        segmentation = h5py.File(segmentation_file,'r')    
        num_frames = segmentation['segmentation'].shape[0]
        segmentation_iter.append(iter_buffered(segmentation['segmentation']))

        fps = segmentation.attrs['fps']
        chunkoffset = segmentation.attrs['chunkoffset']

        rel_video_filename = str(segmentation.attrs['video'])
        video_filename = os.path.join(os.path.dirname(segmentation_file), rel_video_filename)

        if args.max_frames is not None:
                num_frames = args.max_frames

        cap = cv2.VideoCapture(video_filename)
        
        for _ in range(chunkoffset):
            # cap.grab()
            # HACK: because for some reason cap.grab() doesn't work
            _ = cap.read()
            
        cap_list.append(cap)
        
        if 'labels' in list(segmentation.attrs.keys()):
            labels = segmentation.attrs['labels']
            label_colours = segmentation.attrs['label_colours']
        else:

            num_classes=segmentation['segmentation'].shape[3]+1
            
            if num_classes == 3:
                label_colours = [(0, 0, 255),( 0, 255, 0),( 255, 0, 0)]
                labels = ['Dark','Light','Background']
            else:
                label_colours = [(0, 0, 255),( 0, 0, 255)]
                labels = ['Chromatophore','Background']
            
    video_iter=[] 
    def create_video_iter():
        for _ in range(num_frames):
            img_list = []
            for i in range(len(cap_list)):
                succ, img = cap_list[i].read()
                if not succ:
                    raise RuntimeError('Video finished too early')
                img_list.append(img)
            yield img_list
    video_iter = create_video_iter()
    
    # Open up output
    if not args.no_segPano:
        output_dirname = os.path.dirname(output)
        output_basename = os.path.basename(output)
        suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                for _ in range(4))
        tmp_output_basename = '.' + output_basename + '.' + suffix
        tmp_output = os.path.join(output_dirname, tmp_output_basename)
        writer = h5py.File(tmp_output, 'w')
        writer.attrs.create('label_colours', label_colours,
            dtype='uint8')
        writer.attrs.create('segmentation_files', segmentation_files, \
                dtype=h5py.special_dtype(vlen=str)) 
        

        writer.attrs.create('fps', fps, dtype='float32')
        writer.attrs.create('num_frames', num_frames, \
            dtype='uint64')

    
        segmentation_data = writer.create_dataset('segmentation', \
            shape=(num_frames, height, width,len(labels)-1), \
            chunks=(chunksize, height, width,len(labels)-1), \
            dtype='uint8')

        # labels remove last dimension to match segmentation
        writer.attrs.create('labels', labels[:-1],  \
                dtype=h5py.special_dtype(vlen=str))

    # Open output video
    if (args.seg_video_output is not None) or (args.pano_video_output is not None):
        # Calculate output video frame size
        output_framesize = (width, height)
        if args.crop is not None:
            x, y, crop_width, crop_height = (int(s) for s in args.crop.split(','))
            output_framesize = (crop_width, crop_height)
        if args.scale is not None:
            output_framesize = tuple(int(s * args.scale + .5) \
                    for s in output_framesize)
    
        if int(cv2.__version__.split('.')[0]) >= 3:
            fourcc = cv2.VideoWriter_fourcc(*args.codec)
        else:
            fourcc = cv2.cv.FOURCC(*args.codec)
        
    if args.seg_video_output is not None:
        seg_vid_output_dirname = os.path.dirname(args.seg_video_output)
        seg_vid_output_basename = os.path.basename(args.seg_video_output)
        tmp_seg_vid_output_basename = '.' + seg_vid_output_basename
        tmp_seg_vid_output = os.path.join(seg_vid_output_dirname, tmp_seg_vid_output_basename)
        output_seg_video_file = cv2.VideoWriter(tmp_seg_vid_output, fourcc, \
                fps, output_framesize)
        if not output_seg_video_file.isOpened():
            raise RuntimeError("Cannot write video")
        
    if args.pano_video_output is not None:
        pano_vid_output_dirname = os.path.dirname(args.pano_video_output)
        pano_vid_output_basename = os.path.basename(args.pano_video_output)
        tmp_pano_vid_output_basename = '.' + pano_vid_output_basename
        tmp_pano_vid_output = os.path.join(pano_vid_output_dirname, tmp_pano_vid_output_basename)
        output_pano_video_file = cv2.VideoWriter(tmp_pano_vid_output, fourcc, \
                fps, output_framesize)
        if not output_pano_video_file.isOpened():
            raise RuntimeError("Cannot write video")
 
    for img in tqdm(range(num_frames),total=num_frames, desc='Assembling segmented panorama'):
        
        maskbuf=[]
        im_p=[]    
        maskbuf_mapped=[]
        im_p_mapped=[]  
         
        if args.pano_video_output is not None:
            video_frame_list = next(video_iter)
            video_im_p=[] 

        for cam in range(len(segmentation_iter)):
            
            frame = np.squeeze(next(segmentation_iter[cam]))
            if len(frame.shape) == 3:
                frame = frame[:,:,labels==args.foreground]
            
            currUmap=u_im_[cam]
            currVmap=v_im_[cam]
            
            mask=np.ones([frame.shape[0],frame.shape[1]])
            mass = np.zeros([height, width]).astype('bool')
            mosaic = np.zeros([height, width], np.dtype('uint8'))
            #warp frame to pano
            mosaic_img=cv2.remap(frame, currUmap, currVmap, cv2.INTER_NEAREST)
            mosaic_mask=cv2.remap(mask, currUmap, currVmap, cv2.INTER_NEAREST).astype('bool')
            
            mass[m_v0_[cam].item():m_v1_[cam].item(), m_u0_[cam].item():m_u1_[cam].item()] +=  mosaic_mask
            mosaic[m_v0_[cam].item():m_v1_[cam].item(), m_u0_[cam].item():m_u1_[cam].item()] +=  mosaic_img

            maskbuf.append(mass.astype(bool))
            im_p.append(mosaic)

            if args.pano_video_output is not None:
                video_frame = video_frame_list[cam]
                video_frame = video_frame / brightness_norm_list[cam]*2

                video_mosaic = np.zeros([height, width, 3], np.dtype('float32'))
                video_mosaic_img=cv2.remap(video_frame, currUmap, currVmap, cv2.INTER_NEAREST)
                video_mosaic[m_v0_[cam].item():m_v1_[cam].item(), m_u0_[cam].item():m_u1_[cam].item()] +=  video_mosaic_img
                video_mosaic = np.clip(video_mosaic, 0, 255)
                video_mosaic = video_mosaic.astype('uint8')
                
                video_im_p.append(video_mosaic)
                        
        mass = np.zeros([height, width]).astype('bool')
        mosaic = np.zeros([height, width], np.dtype('uint8'))
        
        if args.pano_video_output is not None:
            video_mosaic = np.zeros([height, width, 3], np.dtype('float32'))
      
        for i in range(len(segmentation_iter)):
            currMask=maskbuf[i]
            currImg= im_p[i]
            unused = mass==0
            currImg=currImg*unused
            currMask=currMask*unused
            mass+=  currMask
            mosaic += currImg
            
            if args.pano_video_output is not None:
                currVidImg = video_im_p[i]
                # currVidImg[unused==0] = 0
                video_mosaic += currVidImg
        

        seg_chunk=mosaic.astype('uint8') 
        seg_chunk_thresh=seg_chunk.copy()
        seg_chunk_thresh[seg_chunk_thresh<100]=0
        
        if args.pano_video_output is not None:
            maskbuf_sum = np.array(maskbuf).sum(axis=0)
            video_mosaic[maskbuf_sum>0,0] = video_mosaic[maskbuf_sum>0,0]/maskbuf_sum[maskbuf_sum>0]
            video_mosaic[maskbuf_sum>0,1] = video_mosaic[maskbuf_sum>0,1]/maskbuf_sum[maskbuf_sum>0]
            video_mosaic[maskbuf_sum>0,2] = video_mosaic[maskbuf_sum>0,2]/maskbuf_sum[maskbuf_sum>0]
            vid_chunk = video_mosaic.astype('uint8') 
        
            
        if img>0: #warp

            next_points = np.empty_like(mosaic_points)
            curr_status = np.empty((len(mosaic_points), ), 'uint8')
            
            cv2.calcOpticalFlowPyrLK(prev_chunk, seg_chunk, mosaic_points, \
                                    next_points, curr_status, winSize=(winsize, winsize))
        
        #     dist = np.sum(np.square(mosaic_points-next_points),1)
        #     winThresh=winsize*5
        #   curr_status[dist>winThresh]=0
            curr_status = curr_status.astype('bool')
                                
            # Filter out bad tracks
            next_points = next_points[curr_status]
            orig_points = orig_points[curr_status]
                                    
            curr_mapped_coords = moving_least_squares.similarity(orig_points,next_points, \
                                                                coords, alpha=mls_alpha)
        
            maps = curr_mapped_coords.reshape(identity_maps.shape)
        
            maps = cv2.warpAffine(maps, scale_maps_t, \
                                                mosaic.shape[1::-1])
            seg_chunk_mapped = cv2.remap(mosaic, maps, None, \
                                            interpolation=cv2.INTER_LINEAR)
        
            prevImg=img-1
            if prevImg % 500 == 0:
                nonlinear_mapping = sdr.optimize(orig_chunk, seg_chunk_mapped)
                print('nonlinear warping!')
            seg_chunk_mapped = nonlinear_mapping.transform(seg_chunk_mapped, 'linear')
            
            seg_chunk_mapped_big  = np.expand_dims(seg_chunk_mapped  , axis=0)
            
            if not args.no_segPano:
                segmentation_data[img] = np.expand_dims(seg_chunk_mapped_big, axis=3)

            if args.seg_video_output is not None:
                vid_frame = seg_chunk_mapped_big.squeeze().copy()
                vid_frame = plt.cm.viridis(vid_frame/255.)
                vid_frame = np.array(vid_frame * 255, dtype='uint8')
                vid_frame = cv2.cvtColor(vid_frame, cv2.COLOR_RGB2BGR)
                
            if args.pano_video_output is not None:
                vid_chunk_mapped = cv2.remap(vid_chunk, maps, None, \
                    interpolation=cv2.INTER_LINEAR)
                vid_chunk_mapped[...,0] = nonlinear_mapping.transform(vid_chunk_mapped[...,0], 'linear')
                vid_chunk_mapped[...,1] = nonlinear_mapping.transform(vid_chunk_mapped[...,1], 'linear')
                vid_chunk_mapped[...,2] = nonlinear_mapping.transform(vid_chunk_mapped[...,2], 'linear')

            # Crop and scale
            if args.crop is not None:
                x, y, crop_width, crop_height = (int(s) for s in args.crop.split(','))
                if args.seg_video_output is not None:
                    vid_frame = vid_frame[y : y + crop_height, x : x + crop_width]
                if args.pano_video_output is not None:
                    vid_chunk_mapped = vid_chunk_mapped[y : y + crop_height, x : x + crop_width]
                    
            if (args.scale is not None) and (args.seg_video_output is not None):
                vid_frame = cv2.resize(vid_frame, output_framesize)
            if (args.scale is not None) and (args.pano_video_output is not None):
                vid_chunk_mapped = cv2.resize(vid_chunk_mapped, output_framesize)
            
            if args.seg_video_output is not None:    
                output_seg_video_file.write(vid_frame)
                
            if args.pano_video_output is not None:    
                output_pano_video_file.write(vid_chunk_mapped)


              
        if img==0:
            chunk_mask=cuttlefish_mask(seg_chunk)
            prev_chunk_thresh=seg_chunk_thresh*chunk_mask
            prev_chunk=seg_chunk
            orig_chunk=seg_chunk.copy()
            orig_points = detect_points(prev_chunk_thresh, \
                                  det_min_points,det_max_points, winsize, det_circularity_thresh)
            mosaic_points=orig_points
            
            mf=np.expand_dims(orig_chunk,axis=2)
            mf=np.expand_dims(mf,axis=0) #backwards compatability thing
            
            if not args.no_segPano:
                segmentation_data[0:1] = mf
            

            cv2.imwrite(output +'_mf.png',orig_chunk)
            
            if not args.no_segPano:
                writer.create_dataset('masterframe', \
                data = mf,  \
                dtype='uint8', \
                compression='gzip')
                
        
                writer.create_dataset( \
                    'chunkaverage_mask', \
                    data = chunk_mask, \
                    dtype='bool')

            if args.seg_video_output is not None:
                vid_frame = mf.squeeze().copy()
                vid_frame = plt.cm.viridis(vid_frame/255.)
                vid_frame = np.array(vid_frame * 255, dtype='uint8')
                vid_frame = cv2.cvtColor(vid_frame, cv2.COLOR_RGB2BGR)

            if args.pano_video_output is not None:
                vid_chunk_mapped = vid_chunk
                cv2.imwrite(output +'_cavg.png',vid_chunk_mapped)
                
            # Crop and scale
            if args.crop is not None:
                x, y, crop_width, crop_height = (int(s) for s in args.crop.split(','))
                if args.seg_video_output is not None:
                    vid_frame = vid_frame[y : y + crop_height, x : x + crop_width]
                if args.pano_video_output is not None:
                    vid_chunk_mapped = vid_chunk_mapped[y : y + crop_height, x : x + crop_width]
                    
            if (args.scale is not None) and (args.seg_video_output is not None):
                vid_frame = cv2.resize(vid_frame, output_framesize)
            if (args.scale is not None) and (args.pano_video_output is not None):
                vid_chunk_mapped = cv2.resize(vid_chunk_mapped, output_framesize)
            
            if args.seg_video_output is not None:    
                output_seg_video_file.write(vid_frame)
                
            if args.pano_video_output is not None:    
                output_pano_video_file.write(vid_chunk_mapped)
            
        else:
            prev_chunk=seg_chunk
            prev_chunk_thresh=seg_chunk_thresh
            mosaic_points=next_points
    
    if not args.no_segPano:        
        writer.close()
        os.rename(tmp_output, args.output)
    if args.seg_video_output is not None:
        output_seg_video_file.release()
        os.rename(tmp_seg_vid_output, args.seg_video_output)
    if args.pano_video_output is not None:
        output_pano_video_file.release()
        os.rename(tmp_pano_vid_output, args.pano_video_output)