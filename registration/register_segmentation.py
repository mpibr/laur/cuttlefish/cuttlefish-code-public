#!/usr/bin/env python3

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import cv2
import numpy as np
import moving_least_squares
import h5py
from scipy import spatial
import os.path
from tqdm import tqdm
#import itertools
import buffertools
import random
import os
import string
from mpi4py import MPI
import time


def chunk_at(l, n, num):
    chunk_len = len(l) // num
    start = n * chunk_len
    end = start + chunk_len if n != num - 1 else len(l)
    return l[start : end]

def chunks(l, num):
    for n in range(num):
        yield chunk_at(l, n, num)

def detect_points(segmentation, det_max_points, pad, \
        circularity_thresh):

    mask = segmentation != background_index

    contours, hierarchy = cv2.findContours(mask.astype('uint8'), \
            cv2.RETR_CCOMP, cv2.CHAIN_APPROX_TC89_L1)[-2:]

    # Get outer most contours
    contour_f = []
    index = 0
    while index >= 0:
        contour = contours[index]
        m = cv2.moments(contour, False)
        contour_f.append((m, cv2.arcLength(contour, True)))
        index = hierarchy[0, index, 0]


    # Filter no arc length, on python3 tuple can not be passed to lambda directly,
    # therefore the tuple is in m_cf and the second item is passed by m_cf[1]
    contour_f = filter(lambda m_cf: m_cf[1] > 0, contour_f)

    # Filter out non circular
    contour_f = filter(lambda m_cf: 4 * np.pi * m_cf[0]['m00'] / m_cf[1] / m_cf[1] \
            > circularity_thresh, contour_f)
    contour_f = filter(lambda m_cf: \
               pad < m_cf[0]['m10'] / m_cf[0]['m00'] \
            and m_cf[0]['m10'] / m_cf[0]['m00'] < segmentation.shape[1] - pad \
            and pad < m_cf[0]['m01'] / m_cf[0]['m00'] \
            and m_cf[0]['m01'] / m_cf[0]['m00'] < segmentation.shape[0] - pad, \
            contour_f)

    # get centres
    points = np.asarray(list(map(lambda m_cf: \
            (m_cf[0]['m10'] / m_cf[0]['m00'], m_cf[0]['m01'] / m_cf[0]['m00']), \
        contour_f)), dtype='float32')

    
    # find ~evenly spaced centers
    minXY = np.amin(points,axis=0).astype(int)
    maxXY = np.amax(points,axis=0).astype(int)
    gridPts=np.sqrt(min(det_max_points, points.shape[0])).astype(int)
    xgrid = np.linspace(minXY[0], maxXY[0], gridPts)
    ygrid = np.linspace(minXY[1], maxXY[1], gridPts)
    xv, yv = np.meshgrid(xgrid, ygrid)
    tree = spatial.KDTree(points)
    distance, index = tree.query(list(zip(xv.ravel(), yv.ravel())),k=1)
    points = np.unique(points[index],axis=0)
    return points



def process_video(comm, video_iter, segmentation_iter, **kwargs):
    alpha = kwargs['mls_alpha']
    grid_size = kwargs['mls_grid']
    winsize = kwargs['winsize']
    max_points = kwargs['det_max_points']
    min_points = kwargs['det_min_points']
    circularity_thresh= kwargs['det_circularity_thresh']
    debug_time = kwargs['debug_time']

    first_frame = True
    prev_gray = None
    for img, segmentation in zip(video_iter, segmentation_iter):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        if len(segmentation.shape) == 3:
            segmentation = np.argmax(segmentation, axis=-1)

        # Do the tracking
        if first_frame:

            # Detect points
            first_frame = False
            points = detect_points(segmentation, \
                    max_points, winsize, circularity_thresh) \
                     if comm.rank == 0 else None
            points = comm.bcast(points)
            original_points = points
            num_detected_points = len(points)
            if num_detected_points < min_points:
                raise RuntimeError(\
                    'Only found {} points in first frame, need at least {}.'.format(\
                        num_detected_points, min_points))
            
            # Use a subset of to be tracked points for this rank
            scattered_points = chunk_at(points, comm.rank, comm.size)
            scattered_original_points = chunk_at(points, comm.rank, comm.size)

            # Generate interpolation grid, scatter it
            height, width = segmentation.shape[:2]
            identity_maps = np.dstack(np.meshgrid( \
                    *tuple(np.arange(0, s + grid_size, grid_size) \
                           for s in (width, height)))).astype('float32')
            coords = identity_maps.reshape((-1, 2))
            
            scattered_coords = comm.scatter(list(chunks(coords, comm.size)))
            
            if comm.rank == 0:
                yield original_points, points, img, \
                        identity_maps, identity_maps
            else:
                yield None, None, None, \
                        None, None
            
        else:
            # Track points for this rank
            if debug_time:
                comm.barrier()
            if comm.rank == 0 and debug_time:
                start = time.time()
            scattered_status = np.empty((len(scattered_points), ), 'uint8')
            next_scattered_points = np.empty_like(scattered_points)
            cv2.calcOpticalFlowPyrLK(prev_gray, gray, \
                    scattered_points, \
                    next_scattered_points, \
                    scattered_status,
                    winSize=(winsize, winsize))
            scattered_status = scattered_status.astype('bool')
            scattered_points = next_scattered_points

            # Filter out bad tracks
            scattered_points = \
                    scattered_points[scattered_status]
            scattered_original_points = \
                    scattered_original_points[scattered_status]
            if comm.rank == 0 and debug_time:
                end = time.time()
                tqdm.write('OptFlow %f' % (end - start))

            # All gather points
            if debug_time:
                comm.barrier()
            if comm.rank == 0 and debug_time:
                start = time.time()
            points = \
                    np.vstack(comm.allgather(scattered_points))
            original_points = \
                    np.vstack(comm.allgather(scattered_original_points))
            if comm.rank == 0 and debug_time:
                end = time.time()
                tqdm.write('Gather points %f' % (end - start))
            if len(points) < min_points:
                if comm.rank == 0:
                    tqdm.write(\
                        'RuntimeWarning: Only found {} of {} original points, '.format(\
                                len(points), num_detected_points) \
                            + 'need at least {}, but continuing'.format(min_points))
            
            # Interpolate at the scattered_coords
            if debug_time:
                comm.barrier()
            if comm.rank == 0 and debug_time:
                start = time.time()
            scattered_inv_mapped_coords = moving_least_squares.similarity( \
                    points, original_points, scattered_coords, alpha=alpha)
            scattered_mapped_coords = moving_least_squares.similarity( \
                    original_points, points, scattered_coords, alpha=alpha)
            if comm.rank == 0 and debug_time:
                end = time.time()
                tqdm.write('MLS %f' % (end - start))

            # Gather interpolated grid
            if debug_time:
                comm.barrier()
            if comm.rank == 0 and debug_time:
                start = time.time()
            inv_map_coords = comm.gather(scattered_inv_mapped_coords)
            mapped_coords = comm.gather(scattered_mapped_coords)
            if comm.rank == 0 and debug_time:
                end = time.time()
                tqdm.write('Gather maps %f' % (end - start))
            if comm.rank == 0:
                inv_mapped_coords = np.vstack(inv_map_coords)
                mapped_coords = np.vstack(mapped_coords)
                inv_maps = inv_mapped_coords.reshape(identity_maps.shape)
                maps = mapped_coords.reshape(identity_maps.shape)
        
            if comm.rank == 0:
                yield original_points, points, img, \
                        maps, inv_maps
            else:
                yield None, None, None, \
                        None, None
        
        prev_gray = gray

if __name__ == '__main__':
    comm = MPI.COMM_WORLD

    p = argparse.ArgumentParser('Register segmentation')

    # Input/output
    p.add_argument('--segmentation', required=True, \
            help='Segmentation hd5 file')
    p.add_argument('output', help='Save reg contours to this file')
    p.add_argument('--output-video', help='Save reg video')

    # Background; needed for the detector
    p.add_argument('--background', default='Background')

    # Warping parameters
    p.add_argument('--mls-alpha', type=float, default=7., \
            help='MLS smoothing factor')
    p.add_argument('--mls-grid', type=int, default='16', \
            help='Grid point distance for evaluating MLS')

    # Tracking parameters
    p.add_argument('--winsize', default=128, type=float, \
            help='Window size around the tracked object')
    
    # Detector algorithm
    p.add_argument('--det-max-points', default=900, type=int, \
            help='Number of tracking points')
    p.add_argument('--det-min-points', default=400, type=int, \
            help='Minimal number of tracking points')
    p.add_argument('--det-circularity-thresh', type=float, default=.85)

    # Debug output
    p.add_argument('--debug-imshow', action='store_true', \
            help='Show videos while processing')
    p.add_argument('--debug-remapped', action='store_true')
    p.add_argument('--debug-time', action='store_true')

    p.add_argument('--max-frames', type=int)
    args = p.parse_args()
    
    # Open up segmentation
    segmentation_file = h5py.File(args.segmentation, 'r')
    segmentation = segmentation_file['segmentation']
    segmentation_num_frames = segmentation.shape[0]
    label_colours = segmentation_file.attrs['label_colours']
    fps = segmentation_file.attrs['fps']
    if (segmentation.dtype == 'bool') or (len(segmentation.shape) == 4):
        label_indexes = {segmentation.attrs['enum'][0]: 0, \
                         segmentation.attrs['enum'][1]: 1}
    else:
        label_indexes = h5py.check_dtype(enum=segmentation.dtype)
    background_index = label_indexes[args.background]



    chunkoffset = segmentation_file.attrs['chunkoffset']
    
    # Open up video
    rel_video_filename = segmentation_file.attrs['video']
    video_filename = os.path.join(os.path.dirname(args.segmentation), \
        rel_video_filename)
    cap = cv2.VideoCapture(video_filename)
    if not cap.isOpened():
        raise RuntimeError('Video \'%s\' cannot be openend' % video_filename)
    if cv2.__version__.split('.')[0] >= '3':
        fps = float(cap.get(cv2.CAP_PROP_FPS))
        height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    else:
        fps = float(cap.get(cv2.cv.CV_CAP_PROP_FPS))
        height = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
        width = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
        num_frames = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
    
    # Move to start of chunk
    for _ in range(chunkoffset):
        cap.grab()

    if comm.rank == 0:
        # Open up output
        output_dirname = os.path.dirname(args.output)
        output_basename = os.path.basename(args.output)
        suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                for _ in range(4))
        tmp_output_basename = '.' + output_basename + '.' + suffix
        tmp_output = os.path.join(output_dirname, tmp_output_basename)
        registration_file = h5py.File(tmp_output, 'w')
        registration_file.attrs.create('label_colours', label_colours, \
                dtype='uint8')
        registration_file.attrs.create('video', \
                os.path.relpath(video_filename, \
                        os.path.dirname(args.output)), \
                dtype=h5py.special_dtype(vlen=str))
        registration_file.attrs.create('segmentation', \
                os.path.relpath(args.segmentation, \
                        os.path.dirname(args.output)), \
                dtype=h5py.special_dtype(vlen=str))
        registration_file.attrs.create('fps', fps, dtype='float32')
        registration_file.attrs.create('chunkoffset', chunkoffset,
                                       dtype='uint64')
        maps_shape = ((height + 2 * args.mls_grid - 1) // args.mls_grid,
                      (width + 2 * args.mls_grid - 1) // args.mls_grid,
                      2)

        maps_dset = registration_file.create_dataset('maps', \
                                                     shape=segmentation.shape[:1] + maps_shape, \
                                                     chunks=segmentation.chunks[:1] + maps_shape, \
                                                     dtype='float32', \
                                                     compression=segmentation.compression, \
                                                     compression_opts=segmentation.compression_opts, \
                                                     shuffle=segmentation.shuffle)

        maps_dset.attrs.create('grid_size', args.mls_grid, dtype='uint16')
        inv_maps_dset = registration_file.create_dataset('inv_maps', \
                shape=segmentation.shape[:1] + maps_shape, \
                chunks=segmentation.chunks[:1] + maps_shape, \
                dtype='float32', \
                compression=segmentation.compression, \
                compression_opts=segmentation.compression_opts, \
                shuffle=segmentation.shuffle)
        inv_maps_dset.attrs.create('grid_size', args.mls_grid, dtype='uint16')

    # Setup frame iterator
    def create_video_iter():
        for _ in range(segmentation_num_frames):
            if comm.rank == 0 and args.debug_time:
                start = time.time()
            succ, img = cap.read()
            if comm.rank == 0 and args.debug_time:
                end = time.time()
                tqdm.write('Read vid %f' % (end - start))
            if not succ:
                raise RuntimeError('Video stream aborted too early')
            yield img
    video_iter = create_video_iter()
    
    # Setup segmentation iterator
    def create_segmentation_iter(segmentation):
        with buffertools.Reader(segmentation, segmentation.chunks[0]) \
                as segmentation_reader:
            for _ in range(segmentation_num_frames):
                if comm.rank == 0 and args.debug_time:
                    start = time.time()
                seg = segmentation_reader.read()
                if comm.rank == 0 and args.debug_time:
                    end = time.time()
                    tqdm.write('Read seg %f' % (end - start))
                yield seg
    segmentation_iter = create_segmentation_iter(segmentation)
    
    # Output iterator
    result_iter = process_video(comm, video_iter, segmentation_iter, \
            mls_alpha=args.mls_alpha, mls_grid=args.mls_grid, \
            winsize=args.winsize, \
            det_max_points=args.det_max_points, \
            det_min_points=args.det_min_points, \
            debug_time=args.debug_time,
            det_circularity_thresh=args.det_circularity_thresh)
   

    if comm.rank == 0:
        result_iter = tqdm(result_iter, total=segmentation_num_frames,
            desc='Registering')
    
    scale_maps_t = np.array([[args.mls_grid, 0., 0.], \
                             [0., args.mls_grid, 0.]], 'float32')
    
    # Write every frame
    if comm.rank == 0:
        with buffertools.Writer(maps_dset, maps_dset.chunks[0]) \
                as maps_writer, \
             buffertools.Writer(inv_maps_dset, inv_maps_dset.chunks[0]) \
                as inv_maps_writer:
            for original_points, points, frame, maps, inv_maps in result_iter:
                if args.debug_time:
                    start = time.time()
                maps_writer.write(maps)
                inv_maps_writer.write(inv_maps)
                if args.debug_time:
                    end = time.time()
                    tqdm.write('write %f' % (end - start))

                if args.debug_imshow:
                    if args.debug_remapped:
                        # Convert maps to maps
                        maps = cv2.warpAffine(maps, scale_maps_t, \
                                frame.shape[1::-1])

                        # Warp frame in chunk
                        frame = cv2.remap(frame, maps, None, \
                                interpolation=cv2.INTER_LINEAR)

                    for x, y in points:
                        if int(cv2.__version__.split('.')[0]) >= 3:
                            cv2.circle(frame, (x, y), 8, (255, 0, 0), 1, \
                                    cv2.LINE_AA)
                        else:
                            cv2.circle(frame, (x, y), 8, (255, 0, 0), 1, \
                                    cv2.LINE_AA)
                    for x, y in original_points:
                        if int(cv2.__version__.split('.')[0]) >= 3:
                            cv2.circle(frame, (x, y), 8, (0, 0, 255), 1, \
                                    cv2.LINE_AA)
                        else:
                            cv2.circle(frame, (x, y), 8, (0, 0, 255), 1, \
                                    cv2.LINE_AA)
                    
                    cv2.namedWindow('image',cv2.WINDOW_NORMAL)
                    cv2.resizeWindow('image',1800,900)
                    cv2.imshow('image', frame)
                    cv2.waitKey(1)
    else:
        for _ in result_iter:
            pass

    cap.release()
    segmentation_file.close()
    
    if comm.rank == 0:
        registration_file.close()
        os.rename(tmp_output, args.output)

