import os
import pandas as pd
import cv2
from tqdm import tqdm
import subprocess
import numpy as np
import configparser as ConfigParser
import io
import h5py
import joblib


exp = 'sepia209-20200415-001'
video_basename='2020-04-15-15-15-45'


dirname='/gpfs/laur/sepia_compressed/' + exp
outdir = '/gpfs/laur/sepia_processed/' + exp + '/transitions/'
local_code_dir = '/home/liangx/cuttlefish-code/'



args_config = dirname + '/' + video_basename + '.cfg'
config = ConfigParser.ConfigParser(allow_no_value=True)
config.read([args_config])
array_config = config.get('sepia', 'array_config')
df = pd.read_pickle(array_config)


try:
    os.mkdir(outdir)
except:
    pass

chunk_overlap_file = dirname + '/' + exp + '.chunk_overlap'
chunk_overlap = joblib.load(chunk_overlap_file)

start_frames = chunk_overlap[1,]
end_frames = chunk_overlap[2,]

for start_f, end_f in tqdm(zip(start_frames, end_frames), desc='registration'):
    chunk_prefix = ['{}-{}-{}'.format(video_basename, start_f, end_f)]
    relCams = [outdir+cp + '_relCams' for cp in chunk_prefix]

    with open(relCams[0], 'r') as fr:
        rc = fr.read()
        rc = rc.strip('[]\n').split()
        for ind in range(len(rc)):
            ind = rc[ind].strip(',')
    fps=25
    start_secs = '{:0.3f}'.format(start_f / fps)
    end_secs = '{:0.3f}'.format(end_f / fps)
    print(rc)
    for i in rc:
        cam = df['names'][int(i)]
        chunkname = '{}-{}-{}-{}'.format(cam, video_basename, start_f, end_f)
        # print(chunkname)
        chunk = os.path.join(outdir, chunkname) + '.mp4'
        print(chunk)
        cmd_args = ['ffprobe', \
                    '-loglevel', 'error', \
                    '-select_streams', 'v:0', \
                    '-show_entries', 'stream=start_time', \
                    '-print_format', 'csv=print_section=0', \
                    chunk]
        chunk_start = subprocess.check_output(cmd_args).decode().strip()
        print(chunk_start)
        if float(start_secs)<float(chunk_start):
            start_secs = chunk_start
        output = os.path.join(outdir, chunkname) + '.reg'
        if not os.path.exists(output):
            job_args = [os.path.join(local_code_dir, 'registration', 'register_segmentation_array.py'), \
                        '--video', chunk, \
                        '--video-start', chunk_start, \
                        '--start', start_secs, \
                        '--end', end_secs, \
                        '--mls-grid', '32', \
                        '--det-max-points' , '3000',\
                        #'--det-circularity-thresh', '0.85', \
                        '--det-min-points', '5', \
                        output]
            subprocess.call(job_args)

    n = len(rc)
    output_seg = os.path.join(outdir,chunk_prefix[0]+'.seg')
    if not os.path.exists(output_seg):
        job_args = ['mpirun', '-n', str(n), \
                    os.path.join(local_code_dir, 'registration', 'composePanoVid.py'), \
                    '--directory', outdir, \
                    '--chunk', chunk_prefix[0], \
                    '--pano_params', os.path.join(outdir,chunk_prefix[0]+'_panoParams.mat'), \
                    '--relCams', os.path.join(outdir,chunk_prefix[0]+'_relCams'), \
                    '--array_params', array_config, \
                    '--classifier', os.path.join("/gpfs/laur/sepia_tools/",'model-fabricRoll-XL.h5'), \
                    output_seg]
        subprocess.call(job_args)
    else:
        print(output_seg,'exit!')

# home = os.environ['HOME']
# output = home+"/Dropbox/lab2/sepia/2020-04-30-1/"
# outdir = '/gpfs/laur/sepia_processed/sepia210-20200430-001/t22-1/'
#
# seg = os.path.join(outdir, '2020-04-30-14-05-25-166000-166500') + '.seg'
#
# cq = os.path.join(outdir, '2020-04-30-14-05-25-166000-166500') + '.cleanqueen'
#
# cq_file = h5py.File(cq, 'r')
# cleanqueen = cq_file['Chromatophore']
#
#
# seg_file = h5py.File(seg, 'r')
# masterframe = seg_file['masterframe']
#
# segmentation = seg_file['segmentation']
# mf = (masterframe[0,:,:,0]/500).astype(np.uint8)
#
# from PIL import Image
#
# result = Image.fromarray(mf)
# result.save(output + 'masterframe.png')
#
# plt.savefig(output+'cleanqueen.png')
#
#
# mf = Image.open(output + 'masterframe.png')
# mf = np.array(mf)
