import os
import pandas as pd
import cv2
from tqdm import tqdm
import subprocess
import numpy as np
import configparser as ConfigParser
import io
import joblib
import pandas as pd

exp = 'sepia209-20200326-001'
video_basename='2020-03-26-16-41-48'


dirname='/gpfs/laur/sepia_compressed/' + exp
outdir = '/gpfs/laur/sepia_processed/' + exp + '/transitions/'
local_code_dir = '/home/liangx/cuttlefish-code/'



args_config = dirname + '/' + video_basename + '.cfg'
config = ConfigParser.ConfigParser(allow_no_value=True)
config.read([args_config])
array_config = config.get('sepia', 'array_config')
df = pd.read_pickle(array_config)


try:
    os.mkdir(outdir)
except:
    pass

chunk_overlap_file = dirname + '/' + exp + '.chunk_overlap'
chunk_overlap = joblib.load(chunk_overlap_file)



start_frames = chunk_overlap[1,]
end_frames = chunk_overlap[2,]



for start_f, end_f in tqdm(zip(start_frames, end_frames), desc='panorama'):
    print(start_f,'-',end_f)
    subprocess.call(['python3', local_code_dir + '/panoramaConfig.py',
                     '--directory', outdir, '--name', video_basename, '--chunk', str(start_f) + '-' + str(end_f),
                     '--arrayConfig', array_config])
