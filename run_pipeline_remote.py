#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import argparse
import time
import datetime
import numpy as np
from pipeline_utils import submit_job

if __name__ == '__main__':

    # Arguments used in run_pipeline
    p = argparse.ArgumentParser('Running pipeline on remote', add_help=False)
    p.add_argument('--background', help='Class name of background', type=str)
    p.add_argument('--foreground', help='Class name of chromatophores', type=str)
    p.add_argument('--filedeps-slurm', default="")
    p.add_argument('--segmentation-slurm', default="")
    p.add_argument('--segmentation-args', default="")
    p.add_argument('--registration-slurm', default="")
    p.add_argument('--registration-args', default="")
    p.add_argument('--masterframe-slurm', default="")
    p.add_argument('--masterframe-args', default="")
    p.add_argument('--chunkaverage-slurm', default="")
    p.add_argument('--chunkaverage-args', default="")
    p.add_argument('--chunk-stitching-slurm', default="")
    p.add_argument('--chunk-stitching-args', default="")
    p.add_argument('--stitching-slurm', default="")
    p.add_argument('--stitching-args', default="", help='Arguments for stitching')
    p.add_argument('--queenframe-slurm', default="", help='SLURM options for queenframe')
    p.add_argument('--queenframe-args', default="", help='Arguments for queenframe')
    p.add_argument('--average-slurm', default="", help='SLURM options for average')
    p.add_argument('--average-args', default="", help='Arguments for average')
    p.add_argument('--colourlabels-slurm', default="", help='SLURM options for colourlabels')
    p.add_argument('--colourlabels-args', default="", help='Arguments for colourlabels')
    p.add_argument('--cleanqueen-slurm', default="", help='SLURM options for cleanqueen')
    p.add_argument('--cleanqueen-args', default="", help='Arguments for cleanqueen')
    p.add_argument('--areas-slurm', default="", help='SLURM options for areas')
    p.add_argument('--areas-args', default="", help='Arguments for areas')
    p.add_argument('--mpicmd', default='mpiexec', \
        help='Command to run jobs in parallel using MPI')
    p.add_argument('--partition', help='SLURM partition to submit to')
    p.add_argument('--sshcmd', help='SSH command for command outside of shell.')
    p.add_argument('--debug', action='store_true')
    p.add_argument('--rerun-registration', action='store_true')
    p.add_argument('--rerun-masterframe', action='store_true')
    p.add_argument('--rerun-stitching', action='store_true')
    p.add_argument('--rerun-average', action='store_true')
    p.add_argument('--rerun-colourlabels', action='store_true')
    p.add_argument('--rerun-queenframe', action='store_true')
    p.add_argument('--rerun-cleanqueen', action='store_true')
    p.add_argument('--skip-chunks', action='store_true', help='Start from chunk stitching')
    p.add_argument('--skip-stitching', action='store_true', help='Stop before chunk stitching')
    p.add_argument('--max-queued', default=299)
    p.add_argument('--sleep-between-submits', type=float, default=0.5)  # new
    p.add_argument('--wait-for-queue', default=900, help='In seconds')   
    p.add_argument('--check-file-patience', default=40, \
        help='Max seceonds to wait when checking input file dependencies.')
    p.add_argument('prefix', help='Input video')
    args = p.parse_args()

    # Setup file names
    code_dir = os.path.dirname(os.path.abspath(__file__))
    args.prefix = args.prefix
    dirname = os.path.dirname(args.prefix)
    chunktimes = args.prefix + '.chunktimes'
    stitching = args.prefix + '.stitching'
    stitching_log = args.prefix + '.stitching.log'
    queenframe = args.prefix + '.queenframe'
    queenframe_log = args.prefix + '.queenframe.log'
    average = args.prefix + '.average'
    average_log = args.prefix + '.average.log'
    colourlabels = args.prefix + '.colourlabels'
    colourlabels_log = args.prefix + '.colourlabels.log'
    cleanqueen = args.prefix + '.cleanqueen'
    cleanqueen_log = args.prefix + '.cleanqueen.log'
    areas = args.prefix + '.areas'
    areas_log = args.prefix + '.areas.log'
    classifier = os.path.join(dirname, 'classifier.clf')
    colourica = os.path.join(dirname, 'colourica.ica')
    filedeps_log = args.prefix + '.filedeps.log'
    
    now = datetime.datetime.now()
    # Add horizontal separator to file dependency log
    with open(filedeps_log, 'a') as f:
        f.write('======{}======\n'.format(now.strftime("%Y-%m-%d %H:%M:%S")))

    # Sort chunks by chunksize if chunk-parallel steps are not skipped
    with open(chunktimes, 'r') as f:
        size_list = []
        line_list = []
        for line in f:
            line = line.strip('\n').split(' ')
            start_f, end_f, _, _, _ = line
            chunk_video = '{}-{}-{}.mp4'.format(args.prefix, start_f, end_f)
            size_list.append(os.path.getsize(chunk_video))
            line_list.append(line)
        
    if not args.skip_chunks:
        sorted_chunktimes = np.array(line_list)[np.argsort(size_list)]
    else:
        sorted_chunktimes = np.array(line_list)

    # Get all chunk filenames
    chunk_prefix = ['{}-{}-{}'.format(args.prefix, start_f, end_f) for 
        (start_f, end_f, _, _, _) in sorted_chunktimes]
    chunk = [cp + '.mp4' for cp in chunk_prefix]
    segmentation = [cp + '.seg' for cp in chunk_prefix]
    segmentation_log = [cp + '.seg.log' for cp in chunk_prefix]
    segmentation_jobid = []
    registration = [cp + '.reg' for cp in chunk_prefix]
    registration_log = [cp + '.reg.log' for cp in chunk_prefix]
    registration_jobid = []
    masterframe = [cp + '.masterframe' for cp in chunk_prefix]
    masterframe_log = [cp + '.masterframe.log' for cp in chunk_prefix]
    masterframe_jobid = []
    chunkaverage = [cp + '.chunkaverage' for cp in chunk_prefix]
    chunkaverage_log = [cp + '.chunkaverage.log' for cp in chunk_prefix]
    chunkaverage_jobid = []
    chunk_stitching = [cp + '.cstitching' for cp in chunk_prefix]
    chunk_stitching_log = [cp + '.cstitching.log' for cp in chunk_prefix]
    chunk_stitching_jobid = []
    _, _, chunk_video_start, chunk_start_secs, chunk_end_secs = sorted_chunktimes.T
    nchunks = len(chunk)
    
    # Job submission parameters
    submit_params = {
        'mpicmd': args.mpicmd,
        'max_queued': args.max_queued,
        'wait_time': args.wait_for_queue,
        'partition': args.partition,
        'sshcmd': args.sshcmd,
        'debug': args.debug,
    }
    
    all_jobid = []
        
    if not args.skip_chunks:
        # Submit all segmentation jobs for the chunks
        for chunkidx, output in enumerate(segmentation):

            slurm_args = ['--job-name', 'check-segmentation', \
                          '--output', '/dev/null', \
                          '--error', filedeps_log, \
                          '--open-mode', 'append']
            if args.filedeps_slurm is not '':
                slurm_args += args.filedeps_slurm.split(' ')
            job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                        '--required', classifier, chunk[chunkidx], \
                        '--output', output, \
                        '--patience', str(args.check_file_patience)]
            check_segmentation_jobid = submit_job(job_args, slurm_args, **submit_params)
            
            slurm_args = ['--dependency', 'afterok:{}'.format(check_segmentation_jobid), \
                          '--output', segmentation_log[chunkidx], \
                          '--kill-on-invalid-dep=yes', \
                          '--job-name', 'segmentation']
            if args.segmentation_slurm is not '':
                slurm_args += args.segmentation_slurm.split(' ')
            job_args = ['chromas-classify-video', \
                        '--video', chunk[chunkidx], \
                        '--classifier', classifier, \
                        '--video-start', chunk_video_start[chunkidx], \
                        '--start', chunk_start_secs[chunkidx], \
                        '--end', chunk_end_secs[chunkidx], \
                        '--log-level', '30', \
                        '--log', segmentation_log[chunkidx], \
                        output]
            
            job_args = [os.path.join(code_dir, 'neuralNetSegmentation', 'classify_videoNN.py'), \
                        '--video', chunk[chunkidx], \
                        '--classifier', classifier, \
                        '--video-start', chunk_video_start[chunkidx], \
                        '--start', chunk_start_secs[chunkidx], \
                        '--end', chunk_end_secs[chunkidx], \
                        output]
            
            if args.segmentation_args is not '':
                job_args += args.segmentation_args.split(' ')
            segmentation_jobid.append(submit_job(job_args, slurm_args, **submit_params))

            print('>>> Submitted {}: {}'.format(segmentation_jobid[chunkidx], output))
            
            if args.sleep_between_submits != 0:
                time.sleep(args.sleep_between_submits)
                
        all_jobid += segmentation_jobid

        # Submit all registration jobs for the chunks
        for chunkidx, output in enumerate(registration):
            
            slurm_args = ['--dependency', 'afterany:{}'.format(segmentation_jobid[chunkidx]), \
                          '--kill-on-invalid-dep=yes', \
                          '--job-name', 'check-registration', \
                          '--output', '/dev/null', \
                          '--error', filedeps_log, \
                          '--open-mode', 'append']
            if args.filedeps_slurm is not '':
                slurm_args += args.filedeps_slurm.split(' ')
            job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                        '--required', segmentation[chunkidx], \
                        '--output', output, \
                        '--patience', str(args.check_file_patience)]
            if args.rerun_registration:
                job_args += ['--skip-mtime']
            check_registration_jobid = submit_job(job_args, slurm_args, **submit_params)           
            
            slurm_args = ['--dependency', 'afterok:{}'.format(check_registration_jobid), \
                          '--output', registration_log[chunkidx], \
                          '--kill-on-invalid-dep=yes', \
                          '--job-name', 'registration']
            if args.registration_slurm is not '':
                slurm_args += args.registration_slurm.split(' ')
            job_args = [os.path.join(code_dir, 'registration', 'register_segmentation.py'), \
                        '--segmentation', segmentation[chunkidx], \
                        '--background', args.background, \
                        output]
            if args.registration_args is not '':
                job_args += args.registration_args.split(' ')
            registration_jobid.append(submit_job(job_args, slurm_args, **submit_params))

            print('>>> Submitted {}: {}'.format(registration_jobid[chunkidx], output))
            
            time.sleep(args.sleep_between_submits)
            
        all_jobid += registration_jobid

        # Submit all masterframe jobs for the chunks
        for chunkidx, output in enumerate(masterframe):
            
            slurm_args = ['--dependency', 'afterany:' + registration_jobid[chunkidx] + ':' 
                                                      + segmentation_jobid[chunkidx], \
                          '--kill-on-invalid-dep=yes', \
                          '--job-name', 'check-masterframe', \
                          '--output', '/dev/null', \
                          '--error', filedeps_log, \
                          '--open-mode', 'append']
            if args.filedeps_slurm is not '':
                slurm_args += args.filedeps_slurm.split(' ')
            job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                        '--required', registration[chunkidx], segmentation[chunkidx], \
                        '--output', output, \
                        '--patience', str(args.check_file_patience)]
            if args.rerun_masterframe:
                job_args += ['--skip-mtime']
            check_masterframe_jobid = submit_job(job_args, slurm_args, **submit_params)

            slurm_args = ['--dependency', 'afterok:' + check_masterframe_jobid, \
                          '--output', masterframe_log[chunkidx], \
                          '--kill-on-invalid-dep=yes', \
                          '--job-name', 'masterframe']
            if args.masterframe_slurm is not '':
                slurm_args += args.masterframe_slurm.split(' ')
            job_args = [os.path.join(code_dir, 'master_frame', 'generate_master_frame.py'), \
                        '--registration', registration[chunkidx], \
                        '--background', args.background, \
                        output]
            if args.masterframe_args is not '':
                job_args += args.masterframe_args.split(' ')
            masterframe_jobid.append(submit_job(job_args, slurm_args, **submit_params))
            
            print('>>> Submitted {}: {}'.format(masterframe_jobid[chunkidx], output))
            
            time.sleep(args.sleep_between_submits)
            
        all_jobid += masterframe_jobid
        
        # Submit all chunkaverage jobs for the chunks
        for chunkidx, output in enumerate(chunkaverage):
            
            slurm_args = ['--dependency', 'afterany:' + registration_jobid[chunkidx], \
                          '--kill-on-invalid-dep=yes', \
                          '--job-name', 'check-chunkaverage', \
                          '--output', '/dev/null', \
                          '--error', filedeps_log, \
                          '--open-mode', 'append']
            if args.filedeps_slurm is not '':
                slurm_args += args.filedeps_slurm.split(' ')
            job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                        '--required', registration[chunkidx], \
                        '--output', output, \
                        '--patience', str(args.check_file_patience)] 
            check_chunkaverage_jobid = submit_job(job_args, slurm_args, **submit_params)          
            
            slurm_args = ['--dependency', 'afterok:' + check_chunkaverage_jobid, \
                          '--output', chunkaverage_log[chunkidx], \
                          '--kill-on-invalid-dep=yes', \
                          '--job-name', 'chunkaverage']
            if args.chunkaverage_slurm is not '':
                slurm_args += args.chunkaverage_slurm.split(' ')
            job_args = [os.path.join(code_dir, 'master_frame', 'generate_chunkaverage.py'), \
                        '--registration', registration[chunkidx], \
                        output]
            if args.chunkaverage_args is not '':
                job_args += args.chunkaverage_args.split(' ')
            chunkaverage_jobid.append(submit_job(job_args, slurm_args, **submit_params))
            
            print('>>> Submitted {}: {}'.format(chunkaverage_jobid[chunkidx], output))
            
            time.sleep(args.sleep_between_submits)
            
        all_jobid += chunkaverage_jobid

    # Run stitching on individual chunks
    if not args.skip_stitching:

        # Submit all chunk-stitching jobs for the chunks
        for chunkidx, output in enumerate(chunk_stitching):
            print ('args_skip_chunks---------------------')
            print (args.skip_chunks)            
            if args.skip_chunks:
                slurm_args = ['--kill-on-invalid-dep=yes', \
                          '--job-name', 'check-chunk-stitching', \
                          '--output', '/dev/null', \
                          '--error', filedeps_log, \
                          '--open-mode', 'append']
            
            else:
                chunk_stitching_deps = ':'.join(masterframe_jobid)
                slurm_args = ['--dependency', 'afterany:' + chunk_stitching_deps, \
                          '--kill-on-invalid-dep=yes', \
                          '--job-name', 'check-chunk-stitching', \
                          '--output', '/dev/null', \
                          '--error', filedeps_log, \
                          '--open-mode', 'append']
            if args.filedeps_slurm is not '':
                slurm_args += args.filedeps_slurm.split(' ')
                
            job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                        '--required', masterframe[chunkidx], \
                        '--output', output, \
                        '--patience', str(args.check_file_patience)] 
            if args.rerun_stitching:
                job_args += ['--skip-mtime']
            check_chunk_stitching_jobid = submit_job(job_args, slurm_args, **submit_params)
            
            slurm_args = ['--dependency', 'afterok:' + check_chunk_stitching_jobid, \
                          '--output', chunk_stitching_log[chunkidx], \
                          '--kill-on-invalid-dep=yes', \
                          '--job-name', 'chunk_stitching']
            if args.chunk_stitching_slurm is not '':
                slurm_args += args.chunk_stitching_slurm.split(' ')
            job_args = [os.path.join(code_dir, 'stitching', 'stitch_chunk.py'), \
                        masterframe[chunkidx]] \
                        + masterframe \
                        + [output]
            if args.chunk_stitching_args is not '':
                job_args += args.chunk_stitching_args.split(' ')
            chunk_stitching_jobid.append(submit_job(job_args, slurm_args, **submit_params))
            
            print('>>> Submitted {}: {}'.format(chunk_stitching_jobid[chunkidx], output))

            time.sleep(args.sleep_between_submits)
            
        all_jobid += chunk_stitching_jobid

        # Submit stitching job
        output = stitching
        slurm_args = ['--dependency', 'afterany:' + ':'.join(chunk_stitching_jobid), \
                      '--kill-on-invalid-dep=yes', \
                      '--job-name', 'check-stitching', \
                      '--output', '/dev/null', \
                      '--error', filedeps_log, \
                      '--open-mode', 'append']
        if args.filedeps_slurm is not '':
            slurm_args += args.filedeps_slurm.split(' ')
        #### why optional???
        job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                    '--optional'] + chunk_stitching \
                    + ['--output', output, \
                    '--patience', str(args.check_file_patience)] 
        check_stitching_jobid = submit_job(job_args, slurm_args, **submit_params)          
        
        slurm_args = ['--dependency', 'afterok:' + check_stitching_jobid, \
                      '--output', stitching_log, \
                      '--kill-on-invalid-dep=yes', \
                      '--job-name', 'stitching']
        if args.stitching_slurm is not '':
            slurm_args += args.stitching_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'stitching', 'stitch_all_chunks.py')] \
                    + chunk_stitching \
                    + [output]
        if args.stitching_args is not '':
            job_args += args.stitching_args.split(' ')
        stitching_jobid = submit_job(job_args, slurm_args, **submit_params)
        
        print('>>> Submitted {}: {}'.format(stitching_jobid, output))
        all_jobid += [stitching_jobid]

        time.sleep(args.sleep_between_submits)

        # Submit queenframe job
        output = queenframe
        slurm_args = ['--dependency', 'afterany:' + stitching_jobid, \
                      '--kill-on-invalid-dep=yes', \
                      '--job-name', 'check-queenframe', \
                      '--output', '/dev/null', \
                      '--error', filedeps_log, \
                      '--open-mode', 'append']
        if args.filedeps_slurm is not '':
            slurm_args += args.filedeps_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                    '--required', stitching, \
                    '--output', output, \
                    '--patience', str(args.check_file_patience)] 
        if args.rerun_queenframe:
            job_args += ['--skip-mtime']
        check_queenframe_jobid = submit_job(job_args, slurm_args, **submit_params)         
        
        slurm_args = ['--dependency', 'afterok:' + check_queenframe_jobid, \
                        '--output', queenframe_log, \
                        '--kill-on-invalid-dep=yes', \
                        '--job-name', 'queenframe']
        if args.queenframe_slurm is not '':
            slurm_args += args.queenframe_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'stitching', 'generate_queen_frame.py'), \
                    stitching, \
                    output]
        if args.queenframe_args is not '':
            job_args += args.queenframe_args.split(' ')
        queenframe_jobid = submit_job(job_args, slurm_args, **submit_params)
        
        print('>>> Submitted {}: {}'.format(queenframe_jobid, output))
        all_jobid += [queenframe_jobid]

        time.sleep(args.sleep_between_submits)

        # Submit average job
        output = average

        if args.skip_chunks:
            average_deps = stitching_jobid
        else:
            average_deps = ':'.join([stitching_jobid] + chunkaverage_jobid)

        slurm_args = ['--dependency', 'afterany:' + average_deps, \
                      '--kill-on-invalid-dep=yes', \
                      '--job-name', 'check-average', \
                      '--output', '/dev/null', \
                      '--error', filedeps_log, \
                      '--open-mode', 'append']
        if args.filedeps_slurm is not '':
            slurm_args += args.filedeps_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                    stitching, \
                    '--optional'] + chunkaverage \
                    + ['--output', output, \
                    '--patience', str(args.check_file_patience)] 
        if args.rerun_average:
            job_args += ['--skip-mtime']
        check_average_jobid = submit_job(job_args, slurm_args, **submit_params)           
        
        slurm_args = ['--dependency', 'afterok:' + check_average_jobid, \
                        '--output', average_log, \
                        '--kill-on-invalid-dep=yes', \
                        '--job-name', 'average']
        if args.average_slurm is not '':
            slurm_args += args.average_slurm.split(' ')
        # one place to store chunkaverage-ext??
        job_args = [os.path.join(code_dir, 'stitching', 'generate_average.py'), \
                    stitching, \
                    output, \
                    '--chunkaverage-ext', 'chunkaverage']
        if args.average_args is not '':
            job_args += args.average_args.split(' ')
        average_jobid = submit_job(job_args, slurm_args, **submit_params)
        
        print('>>> Submitted {}: {}'.format(average_jobid, output))
        all_jobid += [average_jobid]

        time.sleep(args.sleep_between_submits)

        # Submit colourlabels job
        output = colourlabels

        slurm_args = ['--dependency', 'afterany:' + average_jobid, \
                      '--kill-on-invalid-dep=yes', \
                      '--job-name', 'check-colourlabels', \
                      '--output', '/dev/null', \
                      '--error', filedeps_log, \
                      '--open-mode', 'append']
        if args.filedeps_slurm is not '':
            slurm_args += args.filedeps_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                    '--required', average, colourica, \
                    '--output', output, \
                    '--patience', str(args.check_file_patience)] 
        if args.rerun_colourlabels:
            job_args += ['--skip-mtime']
        check_colourlabels_jobid = submit_job(job_args, slurm_args, **submit_params)        
        
        slurm_args = ['--dependency', 'afterok:' + check_colourlabels_jobid, \
                        '--output', colourlabels_log, \
                        '--kill-on-invalid-dep=yes', \
                        '--job-name', 'colourlabels']
        if args.colourlabels_slurm is not '':
            slurm_args += args.colourlabels_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'stitching', 'generate_colourlabels.py'), \
                    average, \
                    '--ica', colourica, \
                    output]
        if args.colourlabels_args is not '':
            job_args += args.colourlabels_args.split(' ')
        colourlabels_jobid = submit_job(job_args, slurm_args, **submit_params)
        
        print('>>> Submitted {}: {}'.format(colourlabels_jobid, output))
        all_jobid += [colourlabels_jobid]

        time.sleep(args.sleep_between_submits)

        # Submit cleanqueen job
        output = cleanqueen

        slurm_args = ['--dependency', 'afterany:' + queenframe_jobid, \
                      '--kill-on-invalid-dep=yes', \
                      '--job-name', 'check-cleanqueen', \
                      '--output', '/dev/null', \
                      '--error', filedeps_log, \
                      '--open-mode', 'append']
        if args.filedeps_slurm is not '':
            slurm_args += args.filedeps_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                    '--required', queenframe, \
                    '--output', output, \
                    '--patience', str(args.check_file_patience)] 
        if args.rerun_cleanqueen:
            job_args += ['--skip-mtime']
        check_cleanqueen_jobid = submit_job(job_args, slurm_args, **submit_params)         
        
        slurm_args = ['--dependency', 'afterok:' + check_cleanqueen_jobid, \
                        '--output', cleanqueen_log, \
                        '--kill-on-invalid-dep=yes', \
                        '--job-name', 'cleanqueen']
        if args.cleanqueen_slurm is not '':
            slurm_args += args.cleanqueen_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'stitching', 'generate_cleanqueen.py'), \
                    queenframe, \
                    output]
        if args.cleanqueen_args is not '':
            job_args += args.cleanqueen_args.split(' ')
        cleanqueen_jobid = submit_job(job_args, slurm_args, **submit_params)
        
        print('>>> Submitted {}: {}'.format(cleanqueen_jobid, output))
        all_jobid += [cleanqueen_jobid]

        time.sleep(args.sleep_between_submits)

        # Submit area job
        output = areas

        slurm_args = ['--dependency', 'afterany:' + cleanqueen_jobid, \
                      '--kill-on-invalid-dep=yes', \
                      '--job-name', 'check-areas', \
                      '--output', '/dev/null', \
                      '--error', filedeps_log, \
                      '--open-mode', 'append']
        if args.filedeps_slurm is not '':
            slurm_args += args.filedeps_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                    '--required', cleanqueen, \
                    '--output', output, \
                    '--patience', str(args.check_file_patience)] 
        check_areas_jobid = submit_job(job_args, slurm_args, **submit_params)      
        
        slurm_args = ['--dependency', 'afterok:' + check_areas_jobid, \
                        '--output', areas_log, \
                        '--kill-on-invalid-dep=yes', \
                        '--job-name', 'areas']
        if args.areas_slurm is not '':
            slurm_args += args.areas_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'extract_chromatophores', 'extract_chromatophores.py'), \
                    '--foreground', args.foreground,
                    cleanqueen, \
                    output]
        if args.areas_args is not '':
            job_args += args.areas_args.split(' ')

        areas_jobid = submit_job(job_args, slurm_args, **submit_params)
        
        print('>>> Submitted {}: {}'.format(areas_jobid, output))
        all_jobid += [areas_jobid]

        time.sleep(args.sleep_between_submits)

    # Submit final job
    slurm_args = ['--dependency', 'afterany:' \
                    + ':'.join(all_jobid), \
                    '--kill-on-invalid-dep=yes', \
                    '--job-name', 'final']
    if args.filedeps_slurm is not '':
            slurm_args += args.filedeps_slurm.split(' ')
    job_args = ['hostname']
    
    final_jobid = submit_job(job_args, slurm_args, **submit_params)

    print('FINAL_JOBID=', final_jobid)