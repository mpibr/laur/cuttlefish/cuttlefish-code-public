#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import os
import shutil
import subprocess
import pytest
import time

CODE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

@pytest.fixture(scope="session")
def gen_tmp_files(tmpdir_factory):
    
    # Old input and output
    old_input = str(tmpdir_factory.mktemp('old').join('input.txt'))
    with open(old_input, 'w') as f:
        f.write('abc')
    time.sleep(1)
    old_output = str(tmpdir_factory.mktemp('old').join('output.txt'))
    with open(old_output, 'w') as f:
        f.write('abc')
    time.sleep(1)

    # Python copy that preserves timestamp information
    old_input_copied = str(tmpdir_factory.mktemp('old_copied').join('input.txt'))
    shutil.copy2(old_input, old_input_copied)
    time.sleep(1)

    # Rsync flags that preserve timestamp information
    old_input_rsynced = str(tmpdir_factory.mktemp('old_copied').join('input.txt'))
    os.system('rsync -ap "{}" "{}"'.format(old_input, old_input_rsynced))
    time.sleep(1)

    # New input (simulating updated input)
    new_input = str(tmpdir_factory.mktemp('new').join('input.txt'))
    with open(new_input, 'w') as f:
        f.write('abc')
    time.sleep(1)

    # Non-existent output file path
    new_output = str(tmpdir_factory.mktemp('new').join('output.txt'))

    return old_input, old_output, old_input_copied, old_input_rsynced, new_input, new_output

def test_return_zero_if_file_not_exists(gen_tmp_files):
    old_input, _, _, _, _, new_output = gen_tmp_files

    os.chdir(CODE_DIR)

    cmd_args = ['./check_filedeps.py', '--required', old_input, '--output', new_output]
    
    p = subprocess.Popen(cmd_args,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    _ = p.communicate()

    assert p.returncode == 0

def test_nonzero_exit_if_file_exists(gen_tmp_files):
    old_input, old_output, _, _, _, _ = gen_tmp_files

    os.chdir(CODE_DIR)

    cmd_args = ['./check_filedeps.py', '--required', old_input, '--output', old_output]
    
    p = subprocess.Popen(cmd_args,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    _ = p.communicate()

    assert p.returncode != 0

def test_return_zero_if_forced_even_if_file_exists(gen_tmp_files):
    """Used for forced-rerun of certain steps."""
    old_input, old_output, _, _, _, _ = gen_tmp_files

    os.chdir(CODE_DIR)

    cmd_args = ['./check_filedeps.py', '--required', old_input, '--output', old_output, \
        '--skip-mtime']

    p = subprocess.Popen(cmd_args,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    _ = p.communicate()

    assert p.returncode == 0

def test_nonzero_exit_if_file_exists_even_if_old_file_copied(gen_tmp_files):
    _, old_output, old_input_copied, _, _, _ = gen_tmp_files

    os.chdir(CODE_DIR)

    cmd_args = ['./check_filedeps.py', '--required', old_input_copied, '--output', old_output]
    
    p = subprocess.Popen(cmd_args,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    _ = p.communicate()

    assert p.returncode != 0

def test_nonzero_exit_if_file_exists_even_if_old_file_rsynced(gen_tmp_files):
    _, old_output, _, old_input_rsynced, _, _ = gen_tmp_files

    os.chdir(CODE_DIR)

    cmd_args = ['./check_filedeps.py', '--required', old_input_rsynced, '--output', old_output]
    
    p = subprocess.Popen(cmd_args,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    _ = p.communicate()

    assert p.returncode != 0

def test_return_zero_if_input_newer_than_output(gen_tmp_files):
    _, old_output, _, _, new_input, _ = gen_tmp_files

    os.chdir(CODE_DIR)

    cmd_args = ['./check_filedeps.py', '--required', new_input, '--output', old_output]
    
    p = subprocess.Popen(cmd_args,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    _ = p.communicate()

    assert p.returncode == 0
