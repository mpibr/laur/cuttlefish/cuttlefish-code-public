#!/usr/bin/env python2
# -*- encoding: utf-8 -*-

"""Test stitching with fake data.
Scripts of concern:
./stitching/stitch_chunk.py
./stitching/stitch_all_chunks.py
./stitching/mapped_area.py
"""

import os
import cv2
import shutil
import glob
import pytest
import numpy as np
import h5py
import scipy.fftpack

CODE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def make_fake_masterframes(param):
    '''Generate fake masterframes with different formats.'''

    NUM_FRAMES = 2000

    def lay_chromatophores():
        WIDTH = 4096
        HEIGHT = 2160
        RADIUS_MIN = 2
        RADIUS_MAX = 20

        blank = np.zeros((HEIGHT, WIDTH), dtype='float32')
        for _ in range(7500):
            x = np.random.randint(WIDTH)
            y = np.random.randint(HEIGHT)
            radius = np.random.randint(RADIUS_MIN, RADIUS_MAX)
            intensity = np.random.rand()*0.8 + 0.8
            blank = cv2.circle(blank, (x,y), radius, intensity, -1)
        return blank

    def cuttlefish_mask(image):
        center = tuple(np.array(image.shape[2::-1], dtype=int)//2)  # Weird inconsistent integer behaviour in certain versions of OpenCV 4
        mask = np.zeros(image.shape[:2], dtype='uint8')
        mask = cv2.ellipse(mask,center,(1000,500),0,0,360,1,-1)
        return mask==1

    def mask_background(image):
        mask = cuttlefish_mask(image)
        image[~mask] = 0
        return image

    def rotate_cuttlefish(image):
        ANGLE = 20

        center = tuple((int(image.shape[1]/2), int(image.shape[0]/2)))  # Weird inconsistent integer behaviour in certain versions of OpenCV 4
        rot_mat = cv2.getRotationMatrix2D(center, ANGLE, 1.0)
        image = cv2.warpAffine(image, rot_mat, image.shape[2::-1], flags=cv2.INTER_LINEAR)
        return image

    mf = lay_chromatophores()
    mfs = [mask_background(mf)]
    if param['k_classes'] == 3:
        mfs_k2 = [mask_background(mf)]

    for i in range(param['num_masterframes']):
        mf_tf = rotate_cuttlefish(mfs[0])

        if param['diff_shapes']:
            mf_tf = mf_tf[:,150*i:]

        if param['k_classes'] == 3:
            mf_tf_k2 = mf_tf.copy()
            mf_tf[mf_tf % 2 == 0] = 0
            mf_tf_k2[mf_tf_k2 % 2 == 1] = 0
            mfs_k2.append(mf_tf_k2)

        mfs.append(mf_tf)

    mfs = [(mf_tf*NUM_FRAMES).astype('uint32') for mf_tf in mfs]

    if param['k_classes'] == 3:
        mfs_k2 = [(mf_tf*NUM_FRAMES).astype('uint32') for mf_tf in mfs_k2]
        mfs = zip(mfs, mfs_k2)

    for masterfile, mf in zip(param['masterframes'], mfs):
        if param['k_classes'] == 3:
            mf, mf_k2 = mf

        with h5py.File( masterfile, 'w') as hf:
            if param['array']:
                mf = mf.reshape((1,) + mf.shape + (1,))
                hf.create_dataset('masterframe', data=mf)
                hf.attrs['labels'] = ['Chromatophore', 'Background']
            else:
                if param['k_classes'] == 3:
                    hf.create_dataset('Dark', data=mf)
                    hf.create_dataset('Light', data=mf_k2)
                    hf.attrs['labels'] = ['Dark', 'Light']
                else:
                    hf.create_dataset('Chromatophore', data=mf)
                    hf.attrs['labels'] = ['Chromatophore']

            hf.attrs['num_frames'] = int(NUM_FRAMES)


@pytest.fixture(scope="module")
def run_stitching(tmpdir_factory, request):
    '''Generate masterframes in tmp paths and run stitching.'''

    if 'k_classes' not in request.param.keys():
        request.param['k_classes'] = 2
    if 'num_masterframes' not in request.param.keys():
        request.param['num_masterframes'] = 2
    if 'array' not in request.param.keys():
        request.param['array'] = False
    if 'diff_shapes' not in request.param.keys():
        request.param['diff_shapes'] = False
    if 'grid_size' not in request.param.keys():
        request.param['grid_size'] = 500
    if 'search_space' not in request.param.keys():
        request.param['search_space'] = '4:2,0.2:0.1,:,:'

    # Generate masterframes
    masterframes = []
    for i in range(request.param['num_masterframes']):
        tmpfile = str(tmpdir_factory.mktemp('mf').join('mf{}.masterframe'.format(i)))
        masterframes.append(tmpfile)
            
    stitchfile = str(tmpdir_factory.mktemp('mf').join('output.stitching'))

    request.param['masterframes'] = masterframes
    make_fake_masterframes(request.param)

    # Make sure the first is not the largest
    if request.param['diff_shapes']:
        masterframes = masterframes[1:] + masterframes[:1] 

    # Run stitching
    os.chdir(CODE_DIR)
    cstitchfiles = []
    for mf in masterframes:
        cstitchfile = os.path.splitext(mf)[0] + '.cstitching'
        cstitchfiles.append(cstitchfile)

        # --allow-run-as-root because ENV not supported in OpenMPI 2
        cmd = '''mpiexec  --allow-run-as-root -n 2 ./stitching/stitch_chunk.py \
            {} {} {} \
            --coarse-grid-size 512 --grid-size {} \
            --patch-size 64 --search-space {}\
            '''.format(mf, ' '.join(masterframes), cstitchfile, \
                request.param['grid_size'], request.param['search_space'])

        os.system(cmd)

    cmd = '''./stitching/stitch_all_chunks.py \
        {} {}'''.format(' '.join(cstitchfiles), stitchfile)

    os.system(cmd)

    return masterframes, cstitchfiles, stitchfile

@pytest.mark.parametrize('run_stitching', [\
    {'k_classes': 2, 'array': False}, \
    {'k_classes': 3}, \
    {'array': True}, \
    {'array': True, 'diff_shapes': True}, \
    ], \
    indirect=True)
def test_stitch_all_chunks(run_stitching):
    _, _, stitchfile = run_stitching
    
    with h5py.File(stitchfile, 'r') as hf:
        stitch_mat = hf['stitching_matrix'][:]

    assert not np.isnan(stitch_mat).any()

@pytest.mark.parametrize('run_stitching', [\
    {'search_space': '500:200,0.4:0.05,:,:'}, \
    ], \
    indirect=True)
def test_stitch_mapping_direction(run_stitching):
    '''Making sure that [src, trg] direction is maintained after changing of
    parallelisation logic (by target -> by source).
    '''

    masterframes, _, stitchfile = run_stitching

    with h5py.File(stitchfile, 'r') as hf:
        stitch_mat = hf['stitching_matrix'][:]
        grid_size = hf['stitching_matrix'].attrs['grid_size']

    with h5py.File(masterframes[0], 'r') as hf:
        mf0 = hf[hf.attrs['labels'][0]][:].astype('float32')
    with h5py.File(masterframes[1], 'r') as hf:
        mf1 = hf[hf.attrs['labels'][0]][:].astype('float32')

    scale_maps_t = np.array([[grid_size, 0., 0.], \
                             [0., grid_size, 0.]], 'float32')

    maps = cv2.warpAffine(stitch_mat[0,1], scale_maps_t, mf1.shape[1::-1])
    maps_wrong = cv2.warpAffine(stitch_mat[1,0], scale_maps_t, mf1.shape[1::-1])

    mf0_warped = cv2.remap(mf0, maps, None, interpolation=cv2.INTER_LINEAR)
    mf0_warped_wrong = cv2.remap(mf0, maps_wrong, None, interpolation=cv2.INTER_LINEAR)

    corr_warped = scipy.fftpack.fft2(np.vstack((mf0_warped, mf1))).real.max()
    corr_warped_wrong = scipy.fftpack.fft2(np.vstack((mf0_warped_wrong, mf1))).real.max()
    
    error_margin = 0.015

    assert corr_warped > corr_warped_wrong * (1 - error_margin)
