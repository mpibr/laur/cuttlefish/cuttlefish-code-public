#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from mpi4py import MPI
import pytest

pytestmark = [pytest.mark.mpi]

def test_comm_size(cmdopt_comm_size, capsys):
    comm = MPI.COMM_WORLD
    
    assert comm.size == int(cmdopt_comm_size)