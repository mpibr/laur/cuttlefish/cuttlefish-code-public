#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import cv2
import numpy as np
import pytest

def test_SURF():
    detector = cv2.xfeatures2d.SURF_create()

def test_video_writer(tmp_path):
    vid_file = os.path.join(tmp_path, 'test.mp4')
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    wr = cv2.VideoWriter(vid_file, fourcc, 30, (100,100))

    assert wr.isOpened()

    frame = np.ones((100,100,3), dtype='float32')
    frame[1,1] = 0.5
    frame[2,2] = 0
    frame *= 255
    frame = frame.astype('uint8')

    wr.write(frame)
    wr.release()

    cap = cv2.VideoCapture(vid_file)
    assert cap.isOpened()
    
    _, im = cap.read()

    # Check because certain failed builds return flat gray
    assert len(np.unique(im)) > 1
    assert np.mean(im) > 200

    cap = cv2.VideoCapture(vid_file)
    assert cap.grab()  # In broken version returns False

