# !/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""Tests for input-output compatibility between components of the pipeline.
Main focus: pointers and paths.
Estimated time to complete: 40 s with pytest-xdist.
Requirement: 
    mpiexec
    ...
    See TEST_*
"""

import os
import glob
from shutil import copyfile
import subprocess
import ast
import h5py
import numpy as np
import pytest
from uuid import uuid4
import pytest
from pytest import approx
import configparser
from scipy.io import loadmat
import cv2

CODE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.chdir(CODE_DIR)

TEST_VIDFILE = os.path.join(
    CODE_DIR, 'test/input_dir/path0/input.mp4')
TEST_VIDFILE_ARRAY = os.path.join(
    CODE_DIR, 'test/input_dir/path0/input_3000.avi')
TEST_CLFFILE = os.path.join(
    CODE_DIR, 'test/classifiers/random-forest_2-classes.clf')
TEST_CLFFILE_KERAS = os.path.join(
    CODE_DIR, 'test/classifiers/unet_3-classes.clf')
TEST_KERASFILE = os.path.join(
    CODE_DIR, 'test/classifiers/unet_2-classes.keras')
TEST_SEGFILE_BACKGOUND = 'Background'
TEST_SEGFILE_FOREGOUND = 'Dark'
TEST_SEGFILE_FOREGOUND_ARRAY = 'Chromatophore'
TEST_CLEANQUEENFILE = os.path.join(
    CODE_DIR, 'test/input_dir/path0/bool.cleanqueen')
TEST_CLEANQUEENFILE_PROB = os.path.join(
    CODE_DIR, 'test/input_dir/path0/prob.cleanqueen')
TEST_CLEANQUEENFILE_ARRAY = os.path.join(
    CODE_DIR, 'test/input_dir/path0/prob_array.cleanqueen')

EXPECTED_PANO_PARAMS = os.path.join(
    CODE_DIR, 'test/input_dir/path1/array_expected_panoParams.mat')

@pytest.fixture(scope="module")
def run_panorama(tmpdir_factory):
    # Contains rec*_cam*-start-end.png
    IN_DIR = os.path.join(CODE_DIR, 'test/input_dir/array_png/')
    
    CHUNK_NAME = '2021-01-13-16-12-14-29119-30680'
    
    # Contains camera calibration
    ARRAY_CONFIG = os.path.join(CODE_DIR, 'test/4x4cameraConfig_2ms_dim.pkl')
    
    out_dir = str(tmpdir_factory.mktemp('path'))
    mat_string = os.path.join(out_dir, CHUNK_NAME + '_.mat')
    relevant_cams = os.path.join(out_dir, CHUNK_NAME + '_relCams')
    pano_params = os.path.join(out_dir, CHUNK_NAME + '_panoParams.mat')
    panorama = os.path.join(out_dir, CHUNK_NAME + '_pano.png')

    # Always been Python    
    
    cmd_args = [CODE_DIR + '/panorama/panoramaConfig.py',
        '--directory', out_dir, 
        '--data-dir', IN_DIR,
        '--chunk', CHUNK_NAME,
        '--arrayConfig', ARRAY_CONFIG]
    
    proc = subprocess.Popen(cmd_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    captured = proc.communicate()
    
    if not os.path.isfile(mat_string):
        stdout, stderr = captured
        raise RuntimeError(stderr.decode('utf-8'))
    
    # Originally MATLAB    

    cmd_args = [CODE_DIR + '/panorama/panorama_stitching.py',
        '--directory', out_dir,
        '--data-dir', IN_DIR, 
        '--panorama-config', mat_string]
    
    print(' '.join(cmd_args))
    
    proc = subprocess.Popen(cmd_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    captured = proc.communicate()
    
    if not os.path.isfile(pano_params):
        stdout, stderr = captured
        raise RuntimeError(stderr.decode('utf-8'))
    
    return mat_string, relevant_cams, pano_params, panorama, out_dir

def test_pano_config(run_panorama):
    
    mat_string, relevant_cams, _, _, _ = run_panorama
    
    assert os.path.isfile(mat_string)
    assert os.path.isfile(relevant_cams)
    
    matStr = loadmat(mat_string)
    assert np.all(matStr['el_relative'] == np.array([
        [0, 1], [0, 2], [0, 3], [1, 2], [1, 3], [2, 3]]))
    assert np.all(matStr['relCams'] == np.array([[4, 5, 8, 9]]))
    
def test_pano_params(run_panorama):
    
    _, _, pano_params, panorama, _ = run_panorama
    
    assert os.path.isfile(pano_params)
    assert os.path.isfile(panorama)
    
    pano_matStr = loadmat(pano_params)

    assert np.in1d(['relCams', 'mosaicw', 'mosaich', 
                'm_u0_', 'm_u1_', 'm_v0_', 'm_v1_', 
                'uStruct', 'vStruct'], 
                list(pano_matStr.keys())).all()
    
    pano_img = cv2.imread(panorama)
    
    assert pano_img.shape[:2] == approx((5612, 5497), rel=1e-2)
    
@pytest.fixture(scope="module")
def compose_panorama(run_panorama):
    
    IN_DIR = os.path.join(CODE_DIR, 'test/input_dir/array_seg/')
    segfiles = glob.glob(os.path.join(IN_DIR, '*.seg'))
    
    CHUNK_NAME = '2021-01-13-16-12-14-29119-30680'
    
    # Contains camera calibration
    ARRAY_CONFIG = os.path.join(CODE_DIR, 'test/4x4cameraConfig_2ms_dim.pkl')
    
    _, _, _, _, out_dir = run_panorama
    segPano_file = os.path.join(out_dir, CHUNK_NAME + '.segPano')
    seg_vid = os.path.join(out_dir, CHUNK_NAME + '_segPano.mp4')
    segPano_img = os.path.join(out_dir, CHUNK_NAME + '.segPano_mf.png')
    
    # Compose segPano

    cmd_args = [CODE_DIR + '/registration/composeSegPano.py',
        '--directory', out_dir, 
        '--data-dir', IN_DIR,
        '--chunk', CHUNK_NAME,
        '--array-params', ARRAY_CONFIG,
        '--seg-video-output', seg_vid,
        '--max-frames', '3',
        '--det-min-points', '100',
        '--det-max-points', '200',
        '--sdr-level-iters', '2',
        '--sdr-radius', '3',
        '--sdr-sigma-diff', '0.1',
        segPano_file]
    
    print(' '.join(cmd_args))
    
    proc = subprocess.Popen(cmd_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    captured = proc.communicate()
    
    if os.path.isfile(segPano_file):
        segPano = h5py.File(segPano_file, 'r')
    else:
        stdout, stderr = captured
        raise RuntimeError(stderr.decode('utf-8'))
    
    if not os.path.isfile(seg_vid):
        stdout, stderr = captured
        raise RuntimeError(stderr.decode('utf-8'))
    
    if not os.path.isfile(segPano_img):
        stdout, stderr = captured
        raise RuntimeError(stderr.decode('utf-8'))
    
    return segPano, seg_vid, segPano_img, segfiles
    
def test_compose_segPano(compose_panorama):
    """
    Generate segPano using supplied .seg and processed pano in the test run.
    """
    
    segPano, seg_vid, segPano_img, segfiles = compose_panorama
    
    # segpano
    
    segfiles_pointers = [os.path.join(os.path.dirname(segPano.filename), f) \
        for f in segPano.attrs['segmentation_files']]
    
    assert np.all(np.sort([os.path.abspath(pointer) for pointer in segfiles_pointers]) \
        == np.sort([os.path.abspath(f) for f in segfiles]))
        
    assert segPano['masterframe'].shape[1:3] == approx((5612, 5497), rel=1e-2)
    assert segPano['segmentation'].shape[1:3] == approx((5612, 5497), rel=1e-2)

    # segpano vid
    
    assert os.path.isfile(seg_vid)
    
    cap = cv2.VideoCapture(seg_vid)
    _, im = cap.read()
    
    assert im.shape[:2] == approx((5612, 5497), rel=1e-2)
    
    # segpano img
    ## In some versions of the code, images were used for stitching, not masterframe.
    
    assert os.path.isfile(segPano_img)
    
    im = cv2.imread(segPano_img)
    
    assert im.shape[:2] == approx((5612, 5497), rel=1e-2)

def test_chunk_video(tmpdir):
    """
    Test specific videos encoded with old x264 missing SEI metadata. 
    Reference: https://trac.ffmpeg.org/ticket/6717
    """
    
    VIDFILE = os.path.join(
        CODE_DIR, 'test/input_dir/sepia043-20180517-001/20180517-001.mov')
    chunktimes = str(tmpdir.mkdir('output').join('output.chunktimes'))

    subprocess.call([CODE_DIR + '/chunking/chunk_video.py', \
        '--video', VIDFILE, \
        chunktimes])

    outdir = os.path.dirname(chunktimes)
    chunk_vid = glob.glob(os.path.join(outdir,'*.mp4'))[0]

    python_cmds = 'import cv2;' \
        + 'import logging;' \
        + 'logging.getLogger("my_logger").setLevel(logging.CRITICAL);' \
        + 'cap = cv2.VideoCapture("{}");'.format(chunk_vid) \
        + 'state, im = cap.read();' \
        + 'print(im[:5,:5,0])'
        
    proc = subprocess.Popen(['python', '-c', python_cmds],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, _ = proc.communicate()
    arr = np.array(ast.literal_eval(out.decode().replace(' ', ',')))

    assert not np.all(arr == 130) 
    
    ## Tear down
    
    recoded_vid = os.path.splitext(VIDFILE)[0] \
        + '_recoded' + os.path.splitext(VIDFILE)[1]
        
    if os.path.isfile(recoded_vid):
        os.remove(recoded_vid)
        
    assert not os.path.isfile(recoded_vid)

@pytest.fixture(scope="module")
def segfile(tmpdir_factory):
    return str(tmpdir_factory.mktemp('path').join(str(uuid4()) + '.seg'))

@pytest.fixture(scope="module")
def regfile(tmpdir_factory):
    return str(tmpdir_factory.mktemp('path').join(str(uuid4()) + '.reg'))

@pytest.fixture(scope="module")
def masterfile(tmpdir_factory):
    return str(tmpdir_factory.mktemp('path').join(str(uuid4()) + '.masterframe'))

@pytest.fixture(scope="module")
def chunkaveragefile(tmpdir_factory):
    return str(tmpdir_factory.mktemp('path').join(str(uuid4()) + '.chunkaverage'))

@pytest.fixture(scope="module")
def areasfile(tmpdir_factory):
    return str(tmpdir_factory.mktemp('path').join(str(uuid4()) + '.areas'))

@pytest.fixture(scope="module")
def segment_video(segfile, request):
    
    if request.param['chromas']:
        cmd_args = ['chromas-classify-video', \
                    '--video', TEST_VIDFILE, \
                    '--classifier', request.param['clf_file'], \
                    '--video-start', '0.000', \
                    '--start', '0.001', \
                    '--end', '0.080', \
                    '--threads', '2', \
                    segfile]

        if request.param['is_keras']:
            cmd_args += ['--use-keras']
        if request.param['return_prob']:
            cmd_args += ['--return-prob']

    else:
        cmd_args = ['./neuralNetSegmentation/classify_videoNN.py', \
            '--video', TEST_VIDFILE_ARRAY, \
            '--classifier', TEST_KERASFILE, \
            '--video-start', '0.000', \
            '--start', '0.001', \
            '--end', '0.160', \
            '--threads', '2', \
            # '--num-classes', '2', \
            '--log-level', '30', \
            segfile]

    proc = subprocess.Popen(['bash', '-c', '{}'.format(' '.join(cmd_args))],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    captured = proc.communicate()

    if os.path.isfile(segfile):
        seg = h5py.File(segfile, 'r')
    else:
        stdout, stderr = captured
        print(stdout.decode('utf-8'))
        raise RuntimeError(stderr.decode('utf-8'))

    return seg, captured, request

@pytest.fixture(scope="module")
def register_segmentation(regfile, segfile, segment_video):

    cmd_args = [os.path.join(CODE_DIR, 'registration', 'register_segmentation.py'), \
                     '--segmentation', segfile, \
                     '--background', TEST_SEGFILE_BACKGOUND, \
                     '--det-min-points', '100', \
                     '--det-max-points', '200', \
                     regfile]
    proc = subprocess.Popen(cmd_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    captured = proc.communicate()

    if os.path.isfile(regfile):
        reg = h5py.File(regfile, 'r')
    else:
        stdout, stderr = captured
        raise RuntimeError(stderr.decode('utf-8'))

    return (reg, captured), segment_video

@pytest.fixture(scope="module")
def gen_masterframe(masterfile, regfile, register_segmentation):

    cmd_args = [os.path.join(CODE_DIR, 'master_frame', 'generate_master_frame.py'), \
                    '--registration', regfile, \
                    '--background', TEST_SEGFILE_BACKGOUND, \
                    masterfile]
    proc = subprocess.Popen(cmd_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    captured = proc.communicate()

    if os.path.isfile(masterfile):
        mf = h5py.File(masterfile, 'r')
    else:
        stdout, stderr = captured
        raise RuntimeError(stderr.decode('utf-8'))

    return mf, captured

@pytest.fixture(scope="module")
def gen_chunkaverage(chunkaveragefile, regfile, register_segmentation):

    cmd_args = [os.path.join(CODE_DIR, 'master_frame', 'generate_chunkaverage.py'), \
                    '--registration', regfile, \
                    chunkaveragefile]
    proc = subprocess.Popen(cmd_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    captured = proc.communicate()

    cavg = h5py.File(chunkaveragefile, 'r')

    return cavg, captured

@pytest.fixture(scope="module")
def gen_areas(areasfile, request):
    
    if not request.param['array']:
        cmd_args = [os.path.join(CODE_DIR, 
                    'extract_chromatophores', 'extract_chromatophores.py'), \
                    '--foreground', request.param['foreground'],
                     request.param['cq_file'], \
                     areasfile]
    else:

        cq = h5py.File(request.param['cq_file'], 'r')
        stitching_file_pointer = os.path.join(os.path.dirname(cq.filename), 
                                        cq.attrs['stitching'])
        chunk_mask = np.array(cq.attrs['chunk_mask'])
        cq.close()
        
        stitching = h5py.File(stitching_file_pointer, 'r')
        chunks = np.array([os.path.join(os.path.dirname(stitching.filename), c) \
                    for c in stitching.attrs['chunks']])
        stitching.close()
        
        masterframes = chunks[chunk_mask]
        existing_masterframes = masterframes[
            np.array([os.path.isfile(mf) for mf in masterframes])]
        
        careasfiles = []
        for mf in existing_masterframes:
            careasfile = os.path.join(
                os.path.dirname(areasfile),
                os.path.split(os.path.splitext(mf)[0])[1] + '.careas')
            careasfiles.append(careasfile)
            
            cmd_args = [os.path.join(CODE_DIR, 
                'extract_chromatophores', 'extract_chromatophores_array_chunk.py'), \
                '--chunk', careasfile, \
                '--foreground', request.param['foreground'], \
                '--cleanqueen', request.param['cq_file'], \
                careasfile]
            
            proc = subprocess.Popen(cmd_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            captured = proc.communicate()
            stdout, stderr = captured
            print(stdout.decode('utf-8'))
            
            if os.path.isfile(careasfile):
                areas = h5py.File(careasfile, 'r')
            else:
                
                raise RuntimeError(stderr.decode('utf-8'))
        
        cmd_args = [os.path.join(CODE_DIR, 
                    'extract_chromatophores', 'combine_area_chunks.py')] \
                    + careasfiles + [areasfile]

    proc = subprocess.Popen(cmd_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    captured = proc.communicate()

    if os.path.isfile(areasfile):
        areas = h5py.File(areasfile, 'r')
    else:
        stdout, stderr = captured
        raise RuntimeError(stderr.decode('utf-8'))
        
    return areas, captured, request

# TODO: chromas temp removed for py2 issue
@pytest.mark.parametrize('segment_video', [\
    # {'chromas': True, 'clf_file': TEST_CLFFILE, 'is_keras': False, 'return_prob': False}, \
    # {'chromas': True, 'clf_file': TEST_CLFFILE_KERAS, 'is_keras': True, 'return_prob': False}, \
    # {'chromas': True, 'clf_file': TEST_CLFFILE_KERAS, 'is_keras': True, 'return_prob': True}, \
    {'chromas': False, 'clf_file': TEST_KERASFILE}, \
    ], \
    indirect=True)
def test_segmentation(gen_chunkaverage, gen_masterframe, register_segmentation, \
    segfile, regfile, masterfile):

    # test segmentation
    _, (seg, captured, request) = register_segmentation
    print(captured)
    vidfile_pointer = os.path.join(os.path.dirname(seg.filename), 
                            seg.attrs['video'])
    if request.param['chromas']:
        assert os.path.abspath(vidfile_pointer) == os.path.abspath(TEST_VIDFILE)
    else:
        assert os.path.abspath(vidfile_pointer) == os.path.abspath(TEST_VIDFILE_ARRAY)

    n_classes = len(seg.attrs['label_colours'])
    if not request.param['chromas'] or request.param['return_prob']:
        assert seg['segmentation'].shape[-1] == n_classes

    num_frames = seg['segmentation'].shape[0]

    _, err = captured
    assert 'Segmenting: 100%' in err.decode()

    # test registration
    (reg, captured), _ = register_segmentation
    print(captured)
    segfile_pointer = os.path.join(os.path.dirname(reg.filename), 
                                    reg.attrs['segmentation'])
    assert os.path.abspath(segfile_pointer) == os.path.abspath(segfile)

    assert reg['maps'].shape[0] == num_frames

    _, err = captured
    assert 'Registering: 100%' in err.decode()

    # test masterframe
    mf, captured = gen_masterframe
    regfile_pointer = os.path.join(os.path.dirname(mf.filename), 
                                    mf.attrs['registration'])
    assert os.path.abspath(regfile_pointer) == os.path.abspath(regfile)

    n_non_background_classess = n_classes - 1
    assert len(mf.keys()) == n_non_background_classess

    for k in mf.keys():
        assert np.max(mf[k][:]) <= num_frames

    _, err = captured
    assert 'Computing master frame: 100%' in err.decode()

    # test chunkaverage
    cavg, captured = gen_chunkaverage
    regfile_pointer = os.path.join(os.path.dirname(cavg.filename), 
                                    mf.attrs['registration'])
    assert os.path.abspath(regfile_pointer) == os.path.abspath(regfile)

@pytest.mark.parametrize('gen_areas', [\
    {'cq_file': TEST_CLEANQUEENFILE, 'foreground': TEST_SEGFILE_FOREGOUND, 'array': False}, \
    {'cq_file': TEST_CLEANQUEENFILE_PROB, 'foreground': TEST_SEGFILE_FOREGOUND, 'array': False}, \
    {'cq_file': TEST_CLEANQUEENFILE_ARRAY, 'foreground': TEST_SEGFILE_FOREGOUND_ARRAY, 'array': False}, \
    {'cq_file': TEST_CLEANQUEENFILE_ARRAY, 'foreground': TEST_SEGFILE_FOREGOUND_ARRAY, 'array': True}, \
    ], \
    indirect=True)
def test_extract_chromatophores(gen_areas):

    areas, captured, request = gen_areas

    stdout, err = captured
    print(stdout.decode('utf-8'))
    if request.param['array']:
        assert 'Combining area files: 100%' in err.decode(), err.decode()
    else:
        assert 'Extracting areas: 100%' in err.decode(), err.decode()

    cleanqueen_pointer = os.path.join(os.path.dirname(areas.filename), 
                                areas.attrs['cleanqueen'])
    assert os.path.abspath(cleanqueen_pointer) == os.path.abspath(request.param['cq_file'])

    
