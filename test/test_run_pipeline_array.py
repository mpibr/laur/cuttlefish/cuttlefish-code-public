#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""Test for submission.
Estimated time to complete: ~3 min. #FIX
Requirement: 
    test/input_dir/sepia213-20201123-001/rec*_cam*_2020-11-23-14-53-15.avi (18 files)
    test/input_dir/sepia213-20201123-001/2020-11-23-14-53-15.cfg
    test/input_dir/sepia213-20201123-001/2020-11-23-14-53-15.chunks
    # FIX
    test/classifiers/random-forest_2-classes.clf
    test/classifiers/invivo.ica
    test/.cuttleline.cfg
    SLRUM on host
"""

import os
import shutil
import glob
import pytest
import numpy as np
import h5py
import datetime
import scipy.io

pytestmark = [pytest.mark.pipeline, pytest.mark.slurm, pytest.mark.array]
CODE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

@pytest.fixture(scope="session")
def run_pipeline(cmdopt_cfg, cmdopt_hostname):
    os.chdir(CODE_DIR)

    if os.path.isdir('test/output_dir/sepia213-20201123-001'):
        shutil.rmtree('test/output_dir/sepia213-20201123-001')
        
    steps = ['--skip-queenframe', 
             '--skip-chunks --skip-stitching']    
    
    for cmdpots in steps:
        cmd = '''./run_pipeline_array.py \
            2020-11-23-14-53-15 \
            --inputFolder test/input_dir/sepia213-20201123-001 \
            --cluster-config {} \
            --sleep-between-sync 40 \
            --check-file-patience 100 \
            --max-queued 8 \
            --debug'''.format(cmdopt_cfg)
            
        if cmdopt_hostname is not None:
            cmd += ' --hostname {}'.format(cmdopt_hostname)
            cmd += ' --hostname_gpu {}'.format(cmdopt_hostname)
            #FIX: gpu
            
        cmd += f' {cmdpots}'
        
        os.system(cmd)
        
    yield True

    print('Teardown output')
    if os.path.isdir('test/output_dir/sepia213-20201123-001'):
        shutil.rmtree('test/output_dir/sepia213-20201123-001')

def test_no_job_cancelled_due_to_dependency_failure(run_pipeline):
    with open('test/output_dir/sepia213-20201123-001/2020-11-23-14-53-15.filedeps.log', 'r') as f:
        filedeps_content = str(f.read())
    for line in filedeps_content.strip().split('\n')[1:]:
        assert (line[:6] == 'PASSED') or (line[:6] == '======'), line

def test_log_files_generation(run_pipeline):
    for file_ext in ['stitching.log', 'queenframe.log', 'cleanqueen.log', 'areas.log']:
        f = 'test/output_dir/sepia213-20201123-001/2020-11-23-14-53-15.' + file_ext
        assert os.path.isfile(f), 'Missing log file: ' + file_ext
                
def test_outputs_generation(run_pipeline):
    for file_ext in ['chunktimes', 'stitching', 'queenframe', 'cleanqueen', 'areas']:
        f = 'test/output_dir/sepia213-20201123-001/2020-11-23-14-53-15.' + file_ext
        assert os.path.isfile(f), 'Missing {}, log: \n'.format(file_ext) \
            + open(f + '.log', 'r').read()

def test_chunk_level_files_generation(run_pipeline):
    chunks = ['2020-11-23-14-53-15-10-20','2020-11-23-14-53-15-30-40']
    for c in chunks:
        prefix = 'test/output_dir/sepia213-20201123-001/' + c
        
        for file_ext in ['_.mat', '_relCams', '_panoParams.mat', '_pano.png', 
                         '.segPano', '_pano.mp4', '.cstitching']:
            f = prefix + file_ext
            assert os.path.isfile(f), 'Missing {}, log: \n'.format(f) \
                + open(f + '.log', 'r').read()

def test_cam_level_files_generation(run_pipeline):
    panoConfig_files = glob.glob(
        'test/output_dir/sepia213-20201123-001/2020-11-23-14-53-15-*_.mat')
    
    for pconfig in panoConfig_files:
        matStr = scipy.io.loadmat(pconfig)
        file_list = np.array([os.path.join(
                'test/output_dir/sepia213-20201123-001', 
                os.path.split(f)[1]) \
            for f in matStr['file_list']])
        relevant_files = file_list[matStr['relImageInd'][0]]
        relevant_prefix = [os.path.splitext(f) for f in relevant_files]
        
        for p in relevant_prefix:
            for file_ext in ['seg']:
                files = glob.glob(f'{p}.{file_ext}')
                for f in files:
                    assert os.path.isfile(f), 'Missing {}, log: \n'.format(f) \
                        + open(f + '.log', 'r').read()  
