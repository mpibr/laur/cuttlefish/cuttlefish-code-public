import pytest

def pytest_addoption(parser):
    parser.addoption(
        "--hostname", action="store", help="hostname"
    )
    parser.addoption(
        "--localhost", action="store", help="localhost"
    )
    parser.addoption(
        "--remote-tmpdir", action="store", help="overwrite $TMPDIR in remote host"
    )
    parser.addoption(
        "--partition", action="store", help="SLURM partition."
    )
    parser.addoption(
        "--cfg", action="store", help="config file"
    )
    parser.addoption(
        "--comm-size", default=1, action="store", help="expected comm size"
    )

@pytest.fixture(scope="session")
def cmdopt_hostname(request):
    return request.config.getoption("--hostname")

@pytest.fixture(scope="session")
def cmdopt_localhost(request):
    return request.config.getoption("--localhost")

@pytest.fixture(scope="session")
def cmdopt_remote_tmpdir(request):
    return request.config.getoption("--remote-tmpdir")

@pytest.fixture(scope="session")
def cmdopt_partition(request):
    return request.config.getoption("--partition")

@pytest.fixture(scope="session")
def cmdopt_cfg(request):
    return request.config.getoption("--cfg")

@pytest.fixture(scope="session")
def cmdopt_comm_size(request):
    return request.config.getoption("--comm-size")

def pytest_configure(config):
    config.addinivalue_line(
        "markers", "array: for array data."
    )
    config.addinivalue_line(
        "markers", "pipeline: mark tests for end-to-end pipeline."
    )
    config.addinivalue_line(
        "markers", "slurm: mark tests that require SLURM to run."
    )
    config.addinivalue_line(
        "markers", "ssh: mark tests that require ssh to run."
    )
    config.addinivalue_line(
        "markers", "py2: mark tests that require python 2."
    )
    config.addinivalue_line(
        "markers", "mpi: mark tests that require MPI."
    )