#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import pytest
import time
from ssh_tools import retry_ssh

# marking slurm and ssh requirement
pytestmark = [pytest.mark.slurm, pytest.mark.ssh]
CODE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

cwd = os.getcwd()
os.chdir(CODE_DIR)
from pipeline_utils import submit_job
os.chdir(cwd)

def test_submit_job(cmdopt_partition, cmdopt_hostname):
    
    job_args = [os.path.join(CODE_DIR, 'test/ssh_tools_script.py'), 'abc']

    slurm_args = ['--job-name', 'test-submit-job', \
        '--output', '/dev/null']
    slurm_args += [
        '-n', '1', \
        '--time', '00:00:10', \
        '--mem', '8M']

    sshcmd = 'ssh '+ cmdopt_hostname
    
    jobid = submit_job(
        job_args, slurm_args, 
        partition=cmdopt_partition, 
        sshcmd=sshcmd,
        debug=True)

    print('Waiting for job:', jobid)
    job_running = True
    while job_running:
        stdout = retry_ssh(sshcmd, f'scontrol show job {jobid} --oneliner')
        status = stdout.split('JobState=')[-1].split(' ')[0]

        if status == 'PENDING':
            time.sleep(10)
        else:
            job_running = False
            
    assert not job_running
