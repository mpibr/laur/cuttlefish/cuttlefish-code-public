#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import os
import subprocess
import socket
import pytest

pytestmark = pytest.mark.ssh  # marking ssh requirement
CODE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

cwd = os.getcwd()
os.chdir(CODE_DIR)
from ssh_tools import expand_path, retry_ssh, retry_rsync
os.chdir(cwd)

@pytest.fixture
def sshcmd_localhost(cmdopt_localhost):
    if cmdopt_localhost is None:
        hostname = socket.gethostname()
    else:
        hostname = cmdopt_localhost
    
    return 'ssh {}'.format(hostname)

def test_expand_relative_path():
    test_dir = os.path.dirname(__file__)
    os.chdir(test_dir)
    assert expand_path('./file.txt') == os.path.join(test_dir, 'file.txt')
    assert expand_path('../file.txt') == os.path.join(CODE_DIR, 'file.txt')
    os.chdir(CODE_DIR)
    assert expand_path('./file.txt') == os.path.join(CODE_DIR, 'file.txt')

def test_expand_home():
    home_dir = os.environ['HOME']
    assert expand_path('~/file.txt') == os.path.join(home_dir, 'file.txt')

def test_expand_relative_and_home():
    home_dir = os.environ['HOME']
    assert expand_path('~/p0/../p1/file.txt') == os.path.join(home_dir, 'p1/file.txt')

def test_expand_relative_ssh(sshcmd_localhost):
    assert expand_path('/p0/../p1/file.txt', sshcmd_localhost) == '/p1/file.txt'

def test_expand_home_ssh(sshcmd_localhost):
    remote_environs = retry_ssh(sshcmd_localhost, 'env')
    remote_home = remote_environs.split('\nHOME=')[1].split('\n')[0]

    assert expand_path('~/file.txt', sshcmd_localhost)\
         == os.path.join(remote_home, 'file.txt')

def test_expand_relative_and_home_ssh(sshcmd_localhost):
    remote_environs = retry_ssh(sshcmd_localhost, 'env')
    remote_home = remote_environs.split('\nHOME=')[1].split('\n')[0]
    assert expand_path('~/p0/../p1/file.txt', sshcmd_localhost) == os.path.join(remote_home, 'p1/file.txt')

def test_expand_relative_and_home_ssh_real_remote(cmdopt_hostname):
    sshcmd = 'ssh '+ cmdopt_hostname
    remote_home = retry_ssh(sshcmd, "echo $HOME")
    # remote_environs = remote_environs.decode("utf-8") 
    # remote_home = remote_environs.split('HOME=')[1].split('\n')[0]
    # print(os.path.join(remote_home, 'p1/file.txt'))
    assert expand_path('~/p0/../p1/file.txt', sshcmd) == os.path.join(remote_home, 'p1/file.txt')

def test_ssh_localhost(sshcmd_localhost):
    assert retry_ssh(sshcmd_localhost, 'echo abc') == 'abc'

def test_passing_space_containing_argument_to_python_script(sshcmd_localhost):
    python_script = os.path.join(CODE_DIR, 'test/ssh_tools_script.py')
    assert retry_ssh(sshcmd_localhost, \
        '{} "a b"'.format(python_script)) == "a b"

def test_passing_space_containing_argument_to_python_script_via_compound_ssh(sshcmd_localhost):
    python_script = os.path.join(CODE_DIR, 'test/ssh_tools_script.py')
    for _ in range(2,5):
        sshcmd_localhost += ' ' + sshcmd_localhost
        assert retry_ssh(sshcmd_localhost, \
            '{} "a b"'.format(python_script)) == "a b", \
                'Failed to pass under compound sshcmd: ' + sshcmd_localhost

def test_ssh_real_remote(cmdopt_hostname):
    assert cmdopt_hostname is not None, 'Provide command option --hostname=HOSTNAME'
    assert retry_ssh('ssh '+ cmdopt_hostname, 'echo abc') == 'abc'

def test_ssh_error_handling(sshcmd_localhost):
    with pytest.raises(RuntimeError) as excinfo:
        retry_ssh(sshcmd_localhost, 'ls -a abc')
    assert excinfo.value.args[0][:3] == 'ls:'

def test_ssh_connection_error_retry():
    with pytest.raises(RuntimeError) as excinfo:
        retry_ssh('ssh notahost', 'ls -a abc', connection_retries=1)
    assert excinfo.value.args[0][:6] == 'Failed'

def test_ssh_connection_error_retry_with_flush():
    with pytest.raises(RuntimeError) as excinfo:
        retry_ssh('ssh notahost', 'ls -a abc', connection_retries=1, flush=True)
    assert excinfo.value.args[0][:6] == 'Failed'

def test_ssh_localhost_with_flush(capfd, sshcmd_localhost):
    retry_ssh(sshcmd_localhost, 'echo abc', flush=True)
    out, _ = capfd.readouterr()
    lines = out.split('\n')
    while lines and not lines[-1].strip():
        lines.pop()
    assert lines[-1][:6] == '>> ssh'

def test_ssh_semicolon_warning(sshcmd_localhost):
    with pytest.warns(UserWarning):
        retry_ssh(sshcmd_localhost, 'echo abc; echo abc')

def test_rsync(tmpdir, sshcmd_localhost):
    source_file = str(tmpdir.mkdir('src').join('source.txt'))
    with open(source_file, 'w') as f:
        f.write('abc')
    target_file = str(tmpdir.mkdir('trg').join('target.txt'))
    retry_rsync(sshcmd_localhost, '"{}" :"{}"'.format(source_file, target_file))
    assert os.path.isfile(target_file)

@pytest.fixture
def get_remote_dir(cmdopt_hostname, cmdopt_remote_tmpdir):

    def check_remote_writable(path):
        try:
            cmd = 'find {} -maxdepth 0 -readable -writable'.format(path)
            writable = retry_ssh('ssh ' + cmdopt_hostname, cmd)
            if writable == path:
                return True
            else:
                return False
        except RuntimeError:
            return False

    if (cmdopt_remote_tmpdir is not None) and check_remote_writable(cmdopt_remote_tmpdir):
        remote_tmpdir = cmdopt_remote_tmpdir

    elif check_remote_writable("'$TMPDIR'"):
        remote_tmpdir = "'$TMPDIR'"

    elif check_remote_writable('/tmp'):
        remote_tmpdir = '/tmp'

    else:
        check_remote_writable(cmdopt_remote_tmpdir)
        raise RuntimeError('Please provide remote tmpdir for testing rsync, '
            + 'either with command option or as an environment variable')

    remote_dir = os.path.join(remote_tmpdir, 'path2')

    return remote_dir

def test_rsync_real_remote(tmpdir, cmdopt_hostname, get_remote_dir):
    source_file = str(tmpdir.mkdir('src').join('source.txt'))
    remote_dir = get_remote_dir

    with open(source_file, 'w') as f:
        f.write('abc')
    retry_ssh('ssh ' + cmdopt_hostname, 'mkdir -p {}'.format(remote_dir))
    retry_ssh('ssh ' + cmdopt_hostname, 'rm -f {}/target.txt'.format(remote_dir))
    target_file = '{}/target.txt'.format(remote_dir)
    os.system('rm -f /tmp/path/return.txt')
    return_file = '/tmp/path/return.txt'
    os.system('rm -f {}'.format(return_file))
    if not os.path.isdir('/tmp/path/'):
        os.mkdir('/tmp/path/')

    retry_rsync('ssh ' + cmdopt_hostname, '"{}" :"{}"'.format(source_file, target_file))
    retry_rsync('ssh ' + cmdopt_hostname, ':"{}" "{}"'.format(target_file, return_file))

    assert os.path.isfile(return_file)
    os.system('rm -f /tmp/path/return.txt')
    retry_ssh('ssh ' + cmdopt_hostname, 'rm -f {}/target.txt'.format(remote_dir))

def test_rsync_include(tmpdir, cmdopt_hostname, get_remote_dir):
    local_dir = str(tmpdir.mkdir('local'))
    remote_dir = get_remote_dir
    
    retry_ssh('ssh ' + cmdopt_hostname, 'rm -f -R {}'.format(remote_dir))
    retry_ssh('ssh ' + cmdopt_hostname, 'mkdir -p {}'.format(remote_dir))
    retry_ssh('ssh ' + cmdopt_hostname, 'touch {}/remote.txt'.format(remote_dir))
    retry_ssh('ssh ' + cmdopt_hostname, 'touch {}/remote2.txt'.format(remote_dir))

    retry_rsync('ssh ' + cmdopt_hostname, 
        '-r --include="*.txt" :"{}"/ "{}"'.format(remote_dir, local_dir))
    assert os.path.isfile('{}/remote.txt'.format(local_dir))
    assert os.path.isfile('{}/remote2.txt'.format(local_dir))
    retry_ssh('ssh ' + cmdopt_hostname, 'rm -f {}/remote.txt'.format(remote_dir))
    retry_ssh('ssh ' + cmdopt_hostname, 'rm -f {}/remote2.txt'.format(remote_dir))
