#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""Test for submission.
Estimated time to complete: ~3 min.
Requirement: 
    test/input_dir/sepia030-20170704-011/20170704-011.mov
    test/input_dir/sepia030-20170704-011/20170704-011.chunks
    test/input_dir/sepia030-20170704-011/20170704-011.cfg
    test/classifiers/random-forest_2-classes.clf
    test/classifiers/invivo.ica
    test/.cuttleline.cfg
    SLRUM on host
"""

import os
import shutil
import glob
import pytest
import numpy as np
import h5py
import pytest

# marking slurm requirement and as pipeline
pytestmark = [pytest.mark.slurm, pytest.mark.pipeline]
CODE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

@pytest.fixture(scope="session")
def run_pipeline(cmdopt_cfg, cmdopt_hostname):
    os.chdir(CODE_DIR)

    if os.path.isdir('test/output_dir/sepia030-20170704-011'):
        shutil.rmtree('test/output_dir/sepia030-20170704-011')
    
    if cmdopt_hostname is None:
        cmd = '''./run_pipeline.py \
            test/input_dir/sepia030-20170704-011/20170704-011.mov \
            --host-config {} \
            --local-config {} \
            --sleep-between-sync 40 \
            --check-file-patience 100 \
            --debug'''.format(cmdopt_cfg, cmdopt_cfg)
    else:
        cmd = '''./run_pipeline.py \
            test/input_dir/sepia030-20170704-011/20170704-011.mov \
            --host-config {} \
            --local-config {} \
            --sleep-between-sync 40 \
            --check-file-patience 100 \
            --hostname {} \
            --debug'''.format(cmdopt_cfg, cmdopt_cfg, cmdopt_hostname)
    
    os.system(cmd)
    yield True

    print('Teardown output')
    if os.path.isdir('test/output_dir/sepia030-20170704-011'):
        shutil.rmtree('test/output_dir/sepia030-20170704-011')

def test_no_job_cancelled_due_to_dependency_failure(run_pipeline):
    with open('test/output_dir/sepia030-20170704-011/20170704-011.filedeps.log', 'r') as f:
        filedeps_content = str(f.read())
    for line in filedeps_content.strip().split('\n')[1:]:
        assert line[:6] == 'PASSED', line

def test_log_files_generation(run_pipeline):
    for file_ext in ['stitching.log', 'queenframe.log', 'average.log', \
                     'colourlabels.log', 'cleanqueen.log', 'areas.log']:
        f = 'test/output_dir/sepia030-20170704-011/20170704-011.' + file_ext
        assert os.path.isfile(f), 'Missing log file: ' + file_ext
                
def test_outputs_generation(run_pipeline):
    for file_ext in ['chunktimes', 'stitching', 'queenframe', 'average', \
                     'cleanqueen', 'areas', 'colourlabels']:
        f = 'test/output_dir/sepia030-20170704-011/20170704-011.' + file_ext
        assert os.path.isfile(f), 'Missing {}, log: \n'.format(file_ext) \
            + open(f + '.log', 'r').read()

def test_chunk_level_files_generation(run_pipeline):
    for file_ext in ['mp4', 'seg', 'reg', 'masterframe', 'chunkaverage', \
                     'cstitching']:
        files = glob.glob(
            'test/output_dir/sepia030-20170704-011/20170704-011-*.' + file_ext)
        for f in files:
            assert os.path.isfile(f), 'Missing {}, log: \n'.format(f) \
                + open(f + '.log', 'r').read()

@pytest.mark.xfail(reason='will be fixed with stitching feature branch')
def test_stitching_order(run_pipeline):
    with h5py.File('test/output_dir/sepia030-20170704-011/20170704-011.stitching', 'r') as hf:
        chunks = hf.attrs['chunks']
    chunk_start = np.array([int(x.split('-')[2]) for x in chunks])
    with h5py.File('test/input_dir/sepia030-20170704-011/20170704-011.chunks', 'r') as hf:
        chunk_start_input = hf['chunkTimes'][0]
    assert (chunk_start == chunk_start_input).all()