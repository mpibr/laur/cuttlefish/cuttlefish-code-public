#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Used in test_ssh_tools.py, test_pipeline_utils.py"""

import argparse

p = argparse.ArgumentParser()
p.add_argument('input')
args = p.parse_args()

print(args.input)