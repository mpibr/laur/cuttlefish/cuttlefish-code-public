# cuttlefish-code

Extracting chromatophore sizes using from high-resolution imaging of cuttlefish.

Code for extracting skin-pattern representation from low-resolution imaging data can be found at ![https://gitlab.mpcdf.mpg.de/mpibr/laur/cuttlefish/texture-code](https://gitlab.mpcdf.mpg.de/mpibr/laur/cuttlefish/texture-code).

## Publications

> Reiter, S., Hülsdunk, P., Woo, T. et al. Elucidating the control and development of skin patterning in cuttlefish. Nature 562, 361–366 (2018). https://doi.org/10.1038/s41586-018-0591-3 (![Analysis code](../../tree/v1.0.0))

> Woo, T.\*, Liang, X.\*, Evans, D.A. et al. The dynamics of pattern matching in camouflaging cuttlefish. Nature (2023). https://doi.org/10.1038/s41586-023-06259-2 (![Analysis code](https://gitlab.mpcdf.mpg.de/mpibr/laur/cuttlefish/camo-pattern-dynamics))

## Getting started

You need a hpc cluster for computations, login must be password-less:

```shell
ssh-keygen
ssh-copy-id hpc
```

### Requirements

- Python 3.7+ installed
- Clone this repository
- Install Python requirements
- Create a local config file for the HPC

```shell
git clone git@gitlab.mpcdf.mpg.de:mpibr/laur/cuttlefish/cuttlefish-code-public.git cuttlefish-code
cd cuttlefish-code
```

There are two ways to install the Python requirements:

- ![Method 1: Use pre-built and tested docker container](#method-1-use-pre-built-and-tested-docker-container)
- ![Method 2: Use conda/mamba and build OpenCV](#method-2-use-condamamba-and-build-opencv)

#### Method 1: Use pre-built and tested docker container

The simplest way to start running the pipeline is to take advantage of our the pre-built and tested docker container. The container has all requirements pre-installed (![Dockerfile](./Dockerfile)) and is automatically built with GitLab continuous integration. Additionally, the test suite has been triggered inside the container. (See ![.gitlab-ci.yml](./.gitlab-ci.yml) for details.) The image is hosted on the container registry of this repository.

 [![pipeline status](https://gitlab.mpcdf.mpg.de/mpibr/laur/cuttlefish/cuttlefish-code-public/badges/master/pipeline.svg?ignore_skipped=true)](https://gitlab.mpcdf.mpg.de/mpibr/laur/cuttlefish/cuttlefish-code-public/-/commits/master)

Here is an example of how to run the container using udocker (![Installation Manual](https://indigo-dc.github.io/udocker/installation_manual.html)) in the user environment without root privileges:

```shell
udocker pull gitlab-registry.mpcdf.mpg.de/mpibr/laur/cuttlefish/cuttlefish-code-public:latest  # Download the container image
udocker -D create --name=pipeline gitlab-registry.mpcdf.mpg.de/mpibr/laur/cuttlefish/cuttlefish-code-public:latest  # Create the container on the local machine
udocker -q run -v $PWD -w $PWD -u $USER pipeline  # Enter the container
```

##### Additional udocker options for specific HPC configurations

To enable the sharing of SSH configurations, add the option `-v $HOME/.ssh:/.ssh` when entering the container.

If the HPC features a gateway machine and uses Kerberos authentication, to enable the use of nested SSH from within the container to access SLURM commands outside the container, add the following settings to `/etc/ssh/ssh_config` *inside* the container:

```
PasswordAuthentication no
GSSAPIDelegateCredentials yes

```

To make the Kerberos credentials available, pass the required environmental variable using `-e KRB5CCNAME=$KRB5CCNAME` when entering the container. Check the path with `echo $KRB5CCNAME` and make sure the path is also accessible by mounting the directory, e.g. `-v /tmp`.

To run the test suite inside the local container, make sure the environmental variable `$TMPDIR` is set (e.g. to `/ptmp/$USER`), and that it is accessible from within the container by mounting using the option `-v $TMPDIR -e TMPDIR=$TMPDIR` when entering the container.

For example, to use all of the above options, enter the container as follows:

```shell
udocker -q run -v $PWD -w $PWD -v $HOME/.ssh:/.ssh -e KRB5CCNAME=$KRB5CCNAME -v /tmp -v $TMPDIR -e TMPDIR=$TMPDIR -u $USER pipeline
```

#### Method 2: Use conda/mamba and build OpenCV

##### Step 1: conda install

Most of the python packages can be installed with in a conda virtual environment. The requirements are defined in ![environment.yml](./environment.yml). The same file is used in the building of the docker container (![Dockerfile](./Dockerfile)) described above.

To install the environment with conda (replace `conda` with `mamba` or `micromamba` when appropriate):

```shell
conda install -f environment.yml
```

This installs the software packages into the default virtual environment (`base`), which avoid the activation overhead before running each script. If an environment called `base` already exists, you may choose one of the following alternatives:
1. Overwrite the default environemnt.
    1. Back up the existing default environment: `conda create --name base_backup --clone base`
    2. Install the software packages: `conda env update -f environment.yml --prune`
2. Install the software packages into a non-default virtual environemnt:
    1. Install the software packages: `conda env create -f environment.yml --name new_env`
    2. Add `conda activate new_env` into the `~/.bashrc`.

Make sure `~/.local/bin` is in the `$PATH` environment variable, both locally and on the hpc

```shell
echo $PATH
ssh hpc 'echo $PATH'
```
If not present, add `export PATH=$HOME/.local/bin:$PATH` into the `~/.bashrc` of the local or hpc machine, respectively.


##### Step 2: OpenCV

Most image processing is done with the OpenCV package. The most time-consuming step is the building of the non-free version of OpenCV. The non-free version is required because the SURF algorithm is used in panorama stitching ([panorama/panorama_stitching.py](./panorama/panorama_stitching.py)). Until SURF has been fully integrated into OpenCV (like SIFT, since v4.3.0 and v3.4.10), it is unlikely that pre-built wheels containing SURF will be available (see ![this issue](https://github.com/opencv/opencv-python/issues/126)). 

```shell
CMAKE_ARGS="-DWITH_FFMPEG=ON \
    -DOPENCV_ENABLE_NONFREE=ON \
    -DCMAKE_BUILD_PARALLEL_LEVEL=$(nproc)" \
    pip install --no-binary=opencv-contrib-python \
    opencv-contrib-python --verbose --user
```

If the building of OpenCV fails, the docker container method detailed above is recommended.

If panorama stitching is not required, OpenCV can be quite simply installed with:

```shell
pip install opencv-contrib-python --user
```

Successful install can be tested with:

```shell
pytest test/test_cv2.py
```

##### Step 3: mpi4py

Parallel processing in python is handled with mpi4py. To install mpi4py that communicates with the MPI on an HPC, MPICC must be specified during installation, e.g.

```
MPICC=/usr/bin/mpicc pip install mpi4py --user
```

Successful install can be tested with:

```shell
export COMM_SIZE=2
mpiexec -n=$COMM_SIZE pytest -m mpi --comm-size=$COMM_SIZE
```

##### Step 4: custom packages

Custom packages required can be installed to the local environment using pip.

```
cd mpi_iter
pip install . --user
cd ..

cd buffertools
pip install . --user
cd ..

cd libreg
pip install . --user
cd ..

cd moving_least_squares
pip install . --user
cd ..
```

#### Local config file

Locally create a cluster config file with the contents of where to find the data, where to write the data, code location, slurm parameters. There are separate fields for segmentation and other pipeline steps to allow for a GPU cluster to be used for segmentation. Here is an example for running `.hpc.cfg`:

```ini
[host]
data_dir=~/cuttlefish-code/test/out_dir
working_dir=$TMPDIR
code_dir=~/cuttlefish-code
partition=express
hostname=hpc
mpicmd=srun --mpi=openmpi
shell=udocker -q run -v ~/cuttlefish-code -v $HOME/.ssh:/.ssh -e KRB5CCNAME=$KRB5CCNAME -v /tmp -v $TMPDIR -e TMPDIR=$TMPDIR -u $USER pipeline

[seg_host]
working_dir=$TMPDIR
code_dir=~/cuttlefish-code
partition=gpu
hostname=hpc
mpicmd=srun --mpi=openmpi
shell=udocker -q run -v ~/cuttlefish-code -v $HOME/.ssh:/.ssh -e KRB5CCNAME=$KRB5CCNAME -v /tmp -v $TMPDIR -e TMPDIR=$TMPDIR -u $USER pipeline

[local]
sepia_dir=./test/input_dir
```

An example used in the test can be found at: ![test/.cuttleline3.cfg](test/.cuttleline3.cfg) 

## Testing the environment

To test the environment, run pytest:

```shell
pytest -m "not pipeline"
```

Add the options `--hostname=hpc --localhost=hpc` if the cluster names are dynamically resolved on the HPC. Add `--remote-tmpdir=$TMPDIR` for unconventional tmpdir path for rsync functionality to the hpc.

Components that do not require SLURM can be tested with:

```shell
pytest -m "not slurm"
```

Add `-n $(nproc)` for parallel test running. Refer to `pytest --help` for more options.

All of these have been run in the GitLab continuous integration pipeline. (See ![.gitlab-ci.yml](./.gitlab-ci.yml) for details.)

## Demo data

A few sets of test data are provided in this repository to demonstrate the use of the pipeline (![test/input_dir](./test/input_dir)). To test run the whole pipeline with the created config file:

```shell
pytest -m "pipeline" --cfg=test/.hpc.cfg
```

Add the options `--hostname=hpc --localhost=hpc` the cluster names are dynamically resolved on the HPC.

All of these have been run in the GitLab continuous integration pipeline. (See ![.gitlab-ci.yml](./.gitlab-ci.yml) for references to the test scripts used.)

## Running the pipeline

The best way to learn how to run the pipeline is to refer to the test scripts referenced in the demo above, namely ![.gitlab-ci.yml](./.gitlab-ci.yml) and ![test/test_run_pipeline_array.py](./test/test_run_pipeline_array.py). A brief explanation is provided below.

### Prior to processing a video

To process a dataset, a dataset configuration file has to be created. This contains slurm parameters for running on the hpc, and the location of an array configuration file and a classifier file. An example used in the test can be found at: ![test/input_dir/sepia213-20201123-001/2020-11-23-14-53-15.cfg](test/input_dir/sepia213-20201123-001/2020-11-23-14-53-15.cfg) 

Creating an array configuration file
The array configuration file tells the pipeline the relative position of the cameras in 2d, for tracking cuttlefish over multiple cameras. Video naming convention is recX_camY_date.avi, with rec referring to recording PC (0-N), cam referring to camera on a given PC (0-N), and the date linking all videos in a recording. An example used in the test can be found at: ![test/4x4cameraConfig_2ms_dim.pkl](test/4x4cameraConfig_2ms_dim.pkl)

If camera apertures are not set exactly the same across the array, first run panoramaBrightness.py, using a uniform image across all cameras, to normalize their brightness levels. 
Next, run ![arrayConfig.py](arrayConfig.py), adjusting the layout of the camera array (camList) to match what was used. This will use SIFT features to estimate the affine transformation mapping any camera in the array to any other camera. The script outputs an array configuration file.

Creating a classifier file. We use a custom gui (![chromas](chromas)) for semantic segmentation of small patches (64x64 pixels) of cuttlefish skin. Any other program will work as well for generating training data. Using chromas, images and labels are saved in an hdf5 file. 

Once data is labeled, a U-net classifier (Keras) can be trained by running the script ![neuralNetSegmentation/genUNet_python3.py](neuralNetSegmentation/genUNet_python3.py). An example used in the test can be found at: ![classifiers/unet_2-classes.keras](classifiers/unet_2-classes.keras)

### Chunking

Tracking of chromatophore areas over time is done by processing segments ('chunks') of data where the animal is in view and in focus. The first step of the pipeline is to establish these chunks:  

```shell
chunking/chunking.py
```
This will run through all videos of the camera array, calculating the focus statistic (difference of Gaussians) to determine in-focus chunks, and save the start and end frames of these chunks. 

Alternatively, chunks can be defined manually. An example output used in the test can be found at ![test/input_dir/sepia213-20201123-001/2020-11-23-14-53-15.chunks](test/input_dir/sepia213-20201123-001/2020-11-23-14-53-15.chunks)

### Triggering the pipeline

With all the necessary files in place, the whole pipeline can be triggered by 

```shell
run_pipeline_array.py
```

After setting up paths, this first uses ffmpeg to cut out video clips corresponding to chunk times, saving these in the working directory. 
Data is then sent to the HPC systems for processing.

No manual intervention is *necessary* beyond this point. But the pipeline script also provides useful options (See `--help` for details) for breaking up the pipeline into smaller blocks for a more interactive run. 

As demonstrated in ![test/test_run_pipeline_array.py](./test/test_run_pipeline_array.py), a common use case is to break the pipeline at stitching. The steps of the pipeline are explained below, following this structure.

#### Up to stitching

The first steps up to and including stitching is triggered by:

```shell
run_pipeline_array.py --skip-queenframe
```

##### Panorama stitching

For each chunk, the program generates an image panorama for the subset of cameras where the cuttlefish is in view, using nonlinear warping to deal with parallax effects. Nonlinear warping uses the technique of Li et al. 2018:

J. Li, Z. Wang, S. Lai, Y. Zhai and M. Zhang, "Parallax-Tolerant Image Stitching Based on Robust Elastic Warping," in IEEE Transactions on Multimedia, vol. 20, no. 7, pp. 1672-1687, July 2018, doi: 10.1109/TMM.2017.2777461.

##### Segmentation

The cluster set up for segmentation is sent videos and a classifier file, and uses slurm to segment all frames in all chunked videos.

##### SegPano

The cluster set up for all other operations (CPU cluster). It first combines segmented videos into aligned segmented panoramas (![registration/composeSegPano.py](registration/composeSegPano.py)). Segmented images from different array cameras are tracked over time using Lukas Kanade optic flow, warped into the reference frame of the first image in the chunk, and then warped into a panorama using parameters previously calculated for the chunk.

##### Stitching videos

The next step is to align the images across different chunks. For this, the first panorama image for every chunk is aligned with that of every other chunk (![stitching/stich_chunk.py](stitching/stich_chunk.py), followed by ![stitching/stitch_all_chunks.py](stitching/stitch_all_chunks.py)). 

#### Refine stitching

The resulting stitching file can be manually examined and adjusted using the interactive script ![analysis/refineStitching.py](analysis/refineStitching.py). 

#### Extracting chromatophore areas

After refinement, the rest of the pipeline can be triggered with:

```shell
run_pipeline_array.py --skip-chunks --skip-stitching
```

This calculates a single averaged aligned panorama image for the dataset ('queenframe'), which is partitioned int chromatophore regions ('cleanqueen'). Finally, calculated segmented panoramas, stitching parameters, and chromatophore regions to calculate the number of pixels within each chromatophore region classified as a chromatophore (the chromatophore's area). This area matrix is the data object used in downstream analysis. 
