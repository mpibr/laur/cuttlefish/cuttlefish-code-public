#!/usr/bin/env

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import cv2
import numpy as np

if int(cv2.__version__.split('.')[0]) == 2:
    CV_AA = cv2.CV_AA
elif int(cv2.__version__.split('.')[0]) == 3:
    CV_AA = cv2.LINE_AA

scale=2.
speed=1.
def show_image(img, pos=None, sleep=0, \
        winsize=(640, 320), minisize=(128, 64)):
    global scale
    global speed 

    if pos is None:
        pos = np.array(img.shape[:2][::-1]) / 2.
    else:
        pos = pos.copy()
    mid = np.array(winsize) / 2.

    miniscale = min(minisize[0] / float(img.shape[1]), \
                minisize[1] / float(img.shape[0]))
    mini_t = np.array([[1, 0, 0], [0, 1, 0,]], 'float32') * miniscale
    fit_minisize = (int(miniscale * img.shape[1]), int(miniscale * img.shape[0]))
    mini = cv2.warpAffine(img, mini_t, fit_minisize)

    while True:
        # Create normal view
        t = np.array([[scale, 0, scale * (mid[0] / scale - pos[0])], \
                      [0, scale, scale * (mid[1] / scale - pos[1])]], 'float32')
        view = cv2.warpAffine(img, t, winsize, flags=cv2.INTER_NEAREST)
        cv2.line(view, (int(winsize[0] / 2. * 16), 0), \
                       (int(winsize[0] / 2. * 16), (winsize[1] - 1) * 16), \
                 0, 1, CV_AA, 4)
        cv2.line(view, (0, int(winsize[1] / 2. * 16)), \
                       ((winsize[0] - 1) * 16, int(winsize[1] / 2. * 16)), \
                 0, 1, CV_AA, 4)

        # Create coarse view
        upper_left = (pos[0] - mid[0] / scale, pos[1] - mid[1] / scale)
        bottom_right = (pos[0] + mid[0] / scale, pos[1] + mid[1] / scale)
        upper_left = tuple((16 * np.dot(mini_t[:, : 2], upper_left) \
                + mini_t[:, 2]).astype(int))
        bottom_right = tuple((16 * np.dot(mini_t[:, : 2], bottom_right) \
                + mini_t[:, 2]).astype(int))
        nav_mini = mini.copy()
        cv2.rectangle(nav_mini, upper_left, bottom_right, 0, 1, CV_AA, 4)

        view[ - mini.shape[0] : , - mini.shape[1] : ] = nav_mini

        cv2.imshow('Tracker', view)
        key = cv2.waitKey(sleep) % 256
        if sleep > 0:
            return
        if key == ord(' '):
            return pos
        elif key == 27:
            return
        elif key == ord('b'):
            return np.array([np.nan, np.nan], 'float32')
        elif key == ord('a'):
            pos[0] -= speed / scale
        elif key == ord('d'):
            pos[0] += speed / scale
        elif key == ord('w'):
            pos[1] -= speed / scale
        elif key == ord('s'):
            pos[1] += speed / scale
        elif key == ord(','):
            speed /= 2
        elif key == ord('.'):
            speed *= 2
        elif key == ord('+'):
            scale *= 1.25
        elif key == ord('-'):
            scale /= 1.25

