#!/usr/bin/env python

import argparse
import cv2
import os.path
from . import gui
import numpy as np

def main():
    p = argparse.ArgumentParser('Tracker')
    p.add_argument('csv')
    args = p.parse_args()

    with open(args.csv) as csv_file:
        video_comment = csv_file.readline()
        if not video_comment.startswith('# '):
            raise RuntimeError('Wrong csv file format')
        rel_video_filename = video_comment[2:].rstrip('\n\r')
        video_filename = os.path.normpath( \
                os.path.join(os.path.dirname(args.csv), \
                    rel_video_filename))
        user_track = np.loadtxt(csv_file)

    cap = cv2.VideoCapture(video_filename)
    if not cap.isOpened():
        raise RuntimeError('Video \'%s\' cannot be openend' % video_filename)

    for fn, x, y in user_track:
        point = np.array([x, y], 'float32')
        if np.any(np.isnan(point)):
            point = None
        while int(cap.get(cv2.CAP_PROP_POS_FRAMES)) < fn:
            succ, img = cap.read()
            gui.show_image(img, point, 1)
        succ, img = cap.read()
        gui.show_image(img, point, 0)

if __name__ == '__main__':
    main()

