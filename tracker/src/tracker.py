#!/usr/bin/env python

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

import cv2
import numpy as np
import argparse
import os.path
from . import gui

def get_tracking_data(cap, every=30, lk_winsize=21):
    '''
    Returns manual tracking offset.
    '''
    frame_number = 0
    point = None
    while True:
        succ, img = cap.read()
        if not succ:
            return
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        if point is None or np.any(np.isnan(point)):
            if frame_number % every == 0:
                point = gui.show_image(img)
                if point is None:
                    raise ValueError('Aborted by user')
                yield frame_number, point
            else:
                gui.show_image(img, None, 1)
        else:
            prev_point_arr = np.array([[prev_point]], 'float32')
            point_arr = np.empty((1, 1, 2), 'float32')
            point_arr, _, _ = cv2.calcOpticalFlowPyrLK(prev_gray, gray, \
                    prev_point_arr, nextPts=point_arr, \
                    winSize=(lk_winsize, lk_winsize))
            point = point_arr[0, 0]
            if frame_number % every == 0:
                corrected_point = gui.show_image(img, point)
                if corrected_point is None:
                    raise ValueError('Aborted by user')
                point = corrected_point
                yield frame_number, point
            else:
                gui.show_image(img, point, 1)
        prev_point = point
        prev_gray = gray
        frame_number += 1

def main():
    p = argparse.ArgumentParser('Tracker')
    p.add_argument('video')
    p.add_argument('csv')
    p.add_argument('--lk-winsize', default=21, type=int)
    args = p.parse_args()

    cap = cv2.VideoCapture(args.video)
    if not cap.isOpened():
        raise RuntimeError('Video cannot be opened')
    track = [(fn, x, y) for fn, (x, y) in get_tracking_data(cap)]
    cap.release()

    rel_video = os.path.relpath(args.video, os.path.dirname(args.csv))
    np.savetxt(args.csv, track, header='%s\nframe-number x y' % rel_video, \
            fmt=('%d', '%f', '%f'))

if __name__ == '__main__':
    main()

