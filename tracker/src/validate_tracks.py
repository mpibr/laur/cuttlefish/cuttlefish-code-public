#!/usr/bin/env python

import argparse
import cv2
import os.path
from . import gui
import numpy as np

class LKTracker():
    def __init__(self):
        self.prev_gray = None
        self.w = None
        self.h = None
        self.point = None
        self.status = None

    def init(self, img, box):
        self.prev_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        self.w, self.h = box[2], box[3]
        self.point = np.array([box[0] + box[2] / 2., box[1] + box[3] / 2.],
                'float32')
        self.status = True

    def update(self, img):
        if not self.status:
            raise RuntimeError('Track lost')
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        point_arr = np.empty((1, 1, 2), 'float32')
        status_arr = np.empty((1, 1), 'uint8')
        cv2.calcOpticalFlowPyrLK(self.prev_gray, gray, \
                self.point[None, None], nextPts=point_arr, \
                status=status_arr, \
                winSize=(self.w, self.h))
        self.point = point_arr[0, 0]
        self.prev_gray = gray
        self.status = status_arr[0, 0] > 0
        return self.status, (self.point[0] - self.w / 2., \
                self.point[1] - self.h / 2., \
                self.w, self.h)

def get_tracking_data(cap, point, winsize=21, algo='MEDIANFLOW'):
    '''
    Returns manual tracking offset.
    '''
    tracker = None
    frame_number = 0
    while True:
        succ, img = cap.read()
        if not succ:
            return
        if tracker is None:
            if algo == 'LK':
                tracker = LKTracker()
            else:
                tracker = cv2.Tracker_create(algo)
            box = (point[0] - winsize / 2., point[1] - winsize / 2., \
                    winsize, winsize)
            tracker.init(img, box)
            gui.show_image(img, point, 1)
            yield frame_number, point
        else:
            succ, (x, y, w, h) = tracker.update(img)
            point = np.array([x + w / 2., y + w / 2.], 'float32')
            gui.show_image(img, point, 1)
            yield frame_number, point
        frame_number += 1

def main():
    p = argparse.ArgumentParser('Tracker')
    p.add_argument('csv')
    p.add_argument('out')
    p.add_argument('--winsize', default=21, type=int)
    p.add_argument('--algo', default='MEDIANFLOW')
    args = p.parse_args()

    with open(args.csv) as csv_file:
        video_comment = csv_file.readline()
        if not video_comment.startswith('# '):
            raise RuntimeError('Wrong csv file format')
        rel_video_filename = video_comment[2:].rstrip('\n\r')
        video_filename = os.path.normpath( \
                os.path.join(os.path.dirname(args.csv), \
                    rel_video_filename))
        user_track = np.loadtxt(csv_file)

    cap = cv2.VideoCapture(video_filename)
    if not cap.isOpened():
        raise RuntimeError('Video \'%s\' cannot be openend' % video_filename)
    
    for fn, x, y in user_track:
        if np.any(np.isnan([x, y])):
            continue
        else:
            point = np.array([x, y], 'float32')
            for _ in range(int(fn)):
                cap.grab()
            break
    else:
        raise RuntimeError('No starting point found')
    track = [(fn, x, y) for fn, (x, y) in get_tracking_data(cap, point, \
            args.winsize, args.algo)]
    track = np.array(track)

    rel_csv = os.path.relpath(args.csv, os.path.dirname(args.out))
    np.savetxt(args.out, track, header='%s\nx y' % rel_csv, \
            fmt=('%d', '%f', '%f'))

if __name__ == '__main__':
    main()

