#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 14:20:18 2019

@author: reiters

"""
import numpy as np
import glob
from PIL import Image
import cv2
from scipy import ndimage
import pandas as pd
from numpy.linalg import inv
from skimage import morphology
import scipy.ndimage
import scipy.io
import scipy.ndimage
import argparse
import matplotlib.pyplot as plt

def detect_points(gray):
    #import pdb; pdb.set_trace()
    print('detecting points')
    pad=64
    circThresh=0.75
    
    filteredImg=np.zeros(gray.shape, dtype=scipy.float32)
    scipy.ndimage.filters.gaussian_laplace(gray, 1.5, filteredImg)    #maybe change this filtering for different datasets
    mask = np.zeros_like(filteredImg, dtype=bool)
    mask[filteredImg>2] = True  #I used 5 for some older datasets
    #import pdb; pdb.set_trace()
    contours, hierarchy = cv2.findContours(mask.astype('uint8'), \
            cv2.RETR_CCOMP, cv2.CHAIN_APPROX_TC89_L1)[-2:]

    # Get outer most contours
    if len(contours) > 0:
        
       contour_f = []
       index = 0
       while index >= 0:
           contour = contours[index]
           m = cv2.moments(contour, False)
           contour_f.append((m, cv2.arcLength(contour, True)))
           index = hierarchy[0, index, 0]

        # Filter no arc length
       contour_f = list(filter(lambda cf : cf[1]>0 , contour_f))

       # Filter out non circular
       contour_f = list(filter(lambda m: 4 * np.pi * m[0]['m00'] / m[1] / m[1] \
                               > circThresh, contour_f))
       contour_f = list(filter(lambda m: \
                               pad < m[0]['m10'] / m[0]['m00'] \
                               and m[0]['m10'] / m[0]['m00'] < gray.shape[1] - pad \
                               and pad < m[0]['m01'] / m[0]['m00'] \
                               and m[0]['m01'] / m[0]['m00'] < gray.shape[0] - pad, \
                               contour_f))

        # get centres
       points = np.asarray(list(map(lambda m: \
                                    (m[0]['m10'] / m[0]['m00'], m[0]['m01'] / m[0]['m00']), \
                                    contour_f)), dtype='float32')
    
       numPoints=len(points)
    else:
        numPoints=0
    return numPoints



if __name__ == '__main__':
    
#    comm = MPI.COMM_WORLD
  #  size = comm.Get_size()
  #  rank=comm.Get_rank()
    p = argparse.ArgumentParser('calculate focus')
    p.add_argument('--directory', required=True)
    p.add_argument('--data-dir', required=True)
    p.add_argument('--chunk', required=True)
    p.add_argument('--arrayConfig', required=True)
    p.add_argument('--output')
    args = p.parse_args()

    gSigma1=1
    gSize1=15
    gSigma2=5
    gSize2=15
 #   if comm.rank == 0:
        
    try:
        df =  pd.read_csv(args.arrayConfig,dtype='float32')
    except:
        df =  pd.read_pickle(args.arrayConfig)
    
    bNorm=df['brightnessNorm'].values
    
    centralCam=6
    
    file_list = np.array(sorted(glob.glob(args.data_dir + '/*' +  args.chunk + '.png'))) 

    # first make global pano
    gray_imgs=np.empty(df.shape[0], dtype=object)

    for img in range(df.shape[0]):
      #  import pdb; pdb.set_trace()
        strMatch=np.array([fname.find(df['names'].iloc[img]) for fname in file_list])
        gray_imgs[img] = np.array(Image.open(file_list[strMatch>-1][0]).convert('L'))
        
    #filter the images for relCams check
    filt_imgs=np.empty(df.shape[0], dtype=object)
    for img in range(df.shape[0]):
        blur1 = cv2.GaussianBlur(gray_imgs[img].astype('float32'), (gSize1,gSize1), gSigma1)
        blur2 = cv2.GaussianBlur(gray_imgs[img].astype('float32'), (gSize2,gSize2), gSigma2)
        blur3 = blur2-blur1
        mask = np.zeros_like(blur3, dtype=bool)
        mask[blur3>10] = True
        filt_imgs[img] = mask

  #  import pdb; pdb.set_trace()
    imsize = gray_imgs[0].shape
    im_n = df.shape[0]
    Hall = df[str(centralCam)] #global to the first image
    
  
    #make big array to find the animal
    ubox=[]
    vbox=[]
    
    ubox.extend(range(imsize[0]))
    ubox.extend(range(imsize[0]))
    ubox.extend([0]*imsize[0])
    ubox.extend([imsize[1]-1]*imsize[0])
    ubox=np.array([ubox])
    vbox.extend([0]*imsize[1])
    vbox.extend([imsize[0]-1]*imsize[1])
    vbox.extend(range(imsize[1]))
    vbox.extend(range(imsize[1]))
    vbox=np.array([vbox])
        
    ubox_=[]
    vbox_=[]
    ubox_all_ = [];
    vbox_all_ = [];
    
    for i in range(im_n):
        H = inv(Hall[i])
        ubox_ = H[0,0]*ubox + H[0,1]*vbox +H[0,2]
        vbox_ = H[1,0]*ubox + H[1,1]*vbox +H[1,2]
        ubox_all_.append(ubox_)
        vbox_all_.append(vbox_)
    ubox_all_=np.squeeze(np.array([ubox_all_]))
    vbox_all_=np.squeeze(np.array([vbox_all_]))
    
    u0 = np.ceil(np.amin(ubox_all_))
    u1 = np.floor(np.amax(ubox_all_))
    ur = np.arange(u0,[u1+1])
    v0 = np.ceil(np.amin(vbox_all_))
    v1 = np.floor(np.amax(vbox_all_))
    vr = np.arange(v0,[v1+1])
    mosaicw = len(ur)
    mosaich = len(vr)
    
    m_u0_ = np.zeros([im_n,1]).astype('int32')
    m_u1_ = np.zeros([im_n,1]).astype('int32')
    m_v0_ = np.zeros([im_n,1]).astype('int32')
    m_v1_ = np.zeros([im_n,1]).astype('int32')
    imw_ =  np.zeros([im_n,1]).astype('int32')
    imh_ =  np.zeros([im_n,1]).astype('int32')
    
    for i in range(im_n):
        #margin = 0.2* imsize[0]
        margin=0
        u0_im_ = np.maximum(np.amin(ubox_all_[i]) - margin, u0 )
        u1_im_ = np.minimum(np.amax(ubox_all_[i]) + margin, u1 )
        v0_im_ = np.maximum(np.amin(vbox_all_[i]) - margin, v0 )
        v1_im_ = np.minimum(np.amax(vbox_all_[i]) + margin, v1 )
        m_u0_[i] = np.int32(np.ceil([u0_im_ - u0 ]))
        m_u1_[i] = np.floor([u1_im_ - u0 ])
        m_v0_[i] = np.ceil([v0_im_ - v0 ])
        m_v1_[i] = np.floor([v1_im_ - v0 ])
        imw_[i] = np.floor([m_u1_[i] - m_u0_[i] ])
        imh_[i] = np.floor([m_v1_[i] - m_v0_[i] ])
    
    
    #global mosaic
    u, v = np.meshgrid(ur,vr)
    mass = np.zeros([mosaich, mosaicw])
    mosaic = np.zeros([mosaich, mosaicw])
    imgNum= np.zeros([mosaich, mosaicw]).astype('int32')
    
    #easy to parallelize this step
    for i in range(im_n):
        print('global pano frame ' + str(i))
    
        u_im = u[np.asscalar(m_v0_[i]):np.asscalar(m_v1_[i]),
                      np.asscalar(m_u0_[i]):np.asscalar(m_u1_[i])]
        v_im = v[np.asscalar(m_v0_[i]):np.asscalar(m_v1_[i]),
                      np.asscalar(m_u0_[i]):np.asscalar(m_u1_[i])]
        H = Hall[i]
        u_im_ = H[0,0]*u_im + H[0,1]*u_im +H[0,2]
        v_im_ = H[1,0]*v_im + H[1,1]*v_im +H[1,2]
        
    
        im_p=ndimage.map_coordinates(filt_imgs[i].astype('float64'), [v_im_.ravel(), u_im_.ravel()], mode='constant',cval=0).reshape(u_im_.shape)

        mosaic[np.asscalar(m_v0_[i]):np.asscalar(m_v1_[i]),
                      np.asscalar(m_u0_[i]):np.asscalar(m_u1_[i])] \
                      =  im_p
                              
        imgNum[np.asscalar(m_v0_[i]):np.asscalar(m_v1_[i]),
                      np.asscalar(m_u0_[i]):np.asscalar(m_u1_[i])] \
                      = np.array([im_p>-1])*[i+1]

    gSigma3=25
    gSize3=int(2*(2*gSigma3)+1)
    minSize=100000
    blur3 = cv2.GaussianBlur(mosaic, (gSize3,gSize3), gSigma3)
    mask = np.zeros_like(blur3, dtype=bool)
    mask[blur3>0.01] = True #lowered for low snr data
    cleaned = morphology.remove_small_objects(mask, minSize, 2)
    labels, num_labels = scipy.ndimage.label(cleaned)
    cuttlefish_label = np.argmax(np.bincount(labels.flatten())[1 : ]) + 1

    cuttleMask =  labels==cuttlefish_label
    
    relCams=np.unique(imgNum[cuttleMask])-1
    relCams=np.array([x for x in relCams if x >= 0 ])
  #  import pdb; pdb.set_trace()
    c=0
    fileRange=np.arange(file_list.shape[0])
    relImageInd=np.zeros(relCams.shape[0], dtype='int8')
    numPts=np.zeros(relCams.shape[0], dtype='int64')
    for img in relCams:
        
        strMatch=np.array([fname.find(df['names'][img]) for fname in file_list])
        gray_img = np.array(Image.open(file_list[strMatch>-1][0]).convert('L'))
        numPts[c] = detect_points(gray_img)
        relImageInd[c] = fileRange[strMatch>-1]
        c+=1
    #import pdb; pdb.set_trace()
      #remove bad images
    goodImgs=numPts>10
    relCams=relCams[goodImgs]
    relImageInd=relImageInd[goodImgs]
    
    
    centralCam=relCams[0]
    Hall=df[str(centralCam)].values 
    im_n=len(relCams)
    params_init=[]
    edgeList=[]
    neighbors=[]
    
    if len(relCams) > 1: # if there is only one good cam then the array becomes easier 
        for c in relCams:
            H = Hall[c]
            n=df['neighbors'][c].ravel()
            n=n[np.isin(n, relCams)]
            n=n[np.logical_not(np.isin(n, c))]
            n2=np.ones([len(n),2], dtype='int8')*c
            n2[:,1]=n
            neighbors.append([n2])
            params_init.extend([H[0,0],H[0,1],H[0,2],H[1,2]])
        
        params_init=np.array(params_init)
        params_init=params_init[4:]
        
        neighbors=np.array(neighbors)
        neighbors=np.vstack(neighbors[:,0]) 
        tmp = np.array([neighbors[i,neighbors[i,:].argsort()] for i in range(neighbors.shape[0])])
        edge_list=np.unique(tmp,axis=0)
    
    
        el_relative=np.zeros(edge_list.shape,dtype='int8')
        for v in range(len(relCams)):
            currCam=relCams[v]
            el_relative[edge_list==currCam]=v
    
    else:
        el_relative=0
        
    
      # from here to matlab
    H_pair = np.empty((im_n,im_n), dtype=object)
    for i in range(im_n):
        cam1= relCams[i]
        for ii in range(im_n):
            cam2= relCams[ii]    
            H_pair[i,ii]= df[str(cam1)][cam2]
            H_pair[ii,i]= inv(H_pair[i,ii])
    
    
    relCamsFile= args.directory + '/'  + args.chunk + '_relCams' 
    fs = open(relCamsFile, 'w')
    fs.write(str(relCams))
    fs.close()
    
        
    matStr = args.directory + '/' + args.chunk + '_.mat'
    optionalFiles=[]
    
    if args.output =='':
         args.output=args.chunk
         
    scipy.io.savemat(matStr, mdict={'H_pair': H_pair, 'file_list': file_list, 'el_relative': el_relative,
                                                                  'relImageInd': relImageInd, 'directory':args.directory, 
                                                                  'chunk':args.chunk,'relCams': relCams, 'bNorm':bNorm})   
    
     
   
