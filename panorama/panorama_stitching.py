#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Python implementation of panoStitching.m
modified from https://github.com/mahaveer0suthar/Parallax-Tolerant-Image-Stitching/blob/master/multiple_views/REW_mosaic.m
"""

import argparse
import os
import scipy.io
import numpy as np
import cv2
from sklearn.linear_model import RANSACRegressor, LinearRegression
from scipy.optimize import least_squares

def make_Hpair(n_images, params):
    params = params.reshape(-1,4)
    H_pair = np.zeros((n_images, n_images, 3, 3))
    for i in range(n_images):
        H_pair[i, i] = np.identity(3)
        
    for i in range(1, n_images):
        curr_params = params[i-1]
        
        S = np.array([
            [curr_params[0],curr_params[1],curr_params[2]],
            [curr_params[1],curr_params[0],curr_params[3]],
            [0,0,1]
        ])
        H_pair[0,i] = S
        H_pair[i,0] = np.linalg.inv(S)
        
    for i in range(1, n_images-1):
        for j in range(i+1, n_images):
            H_pair[i, j] = np.dot(H_pair[0, j], H_pair[i, 0])
            H_pair[j, i] = np.linalg.inv(H_pair[i, j])
            
    return H_pair

def residual_all_robust_similarity(params, X_src, X_trg, edge_list, sigma):
    # parameretes sigma indicates the distance scope for inliers with homopraphy matrix, in pixels
    
    indexes = np.unique(edge_list)
    n_images = len(indexes)
    
    H_pair = make_Hpair(n_images, params)
    
    err = np.array([])
    for ei, (i, j) in enumerate(edge_list):
        i = np.where(indexes==i)[0][0]
        j = np.where(indexes==j)[0][0]

        X1_p2 = np.dot(H_pair[i,j], X_src[ei].T)
        
        X1_p2[0] = X1_p2[0] / X1_p2[2]
        X1_p2[1] = X1_p2[1] / X1_p2[2]
        errH1_2 = X_trg[ei] - X1_p2.T
        
        X2_p1, _, _, _ = np.linalg.lstsq(H_pair[i,j], X_trg[ei].T, rcond=None)
        X2_p1[0] = X2_p1[0] / X2_p1[2]
        X2_p1[1] = X2_p1[1] / X2_p1[2]
        errH2_1 = X_src[ei] - X2_p1.T

        err = np.concatenate([err, errH1_2[:,0], errH1_2[:,1], errH2_1[:,0], errH2_1[:,1]])
        
    outlier = np.abs(err) > sigma
    
    err[outlier] = np.sign(err[outlier]) * (sigma + sigma * np.log(np.abs(err[outlier]/sigma)))
    
    return err


if __name__ == '__main__':
  
    p = argparse.ArgumentParser('Stitch panorama')
    p.add_argument('--data-dir', required=True)
    p.add_argument('--panorama-config', required=True, help='Output .mat of config')
    p.add_argument('--SURF-thresh', default=200, type=float, 
        help='Hessian threshold of the SURF feature detector. Reduce for more points.')
    p.add_argument('--matcher', default='affine', choices=['affine', 'nearest', 'range'])
    p.add_argument('--match-conf', default=0.6, type=float, help='Matching confidence')
    p.add_argument('--min-matches', default=10, type=int,
        help='Minimal number of match points in a tile for it to be included.')
    p.add_argument('--ransac-tolerance', default=1, type=float,
        help='Threshold for RANSAC outlier removal.')
    p.add_argument('--smoothing-factor', default=0.001, type=float, 
        help='Lambda of smoothing.')
    p.add_argument('--deform-grid', default=50, type=int, 
        help='Interval in pixels for the computing of deformation functions.')
    p.add_argument('--smoothing-k', default=5, type=float, 
        help='Number of iterations to optimise smoothing.')
    p.add_argument('--directory', required=True, help='Output directory')
    args = p.parse_args()

    matStr = scipy.io.loadmat(args.panorama_config)
    
    file_list = np.array([os.path.join(args.data_dir, os.path.split(f)[1]) \
        for f in matStr['file_list']])
    
    output_file = os.path.join(args.directory, matStr['chunk'][0]) + '_panoParams.mat'

    # Get relevant cameras
    relevant_files = file_list[matStr['relImageInd'][0]]
    relevant_brightness = matStr['bNorm'][0][matStr['relCams'][0]]

    n_images = len(relevant_files)
        
    im_list = []
    im_size = []
    for f, bright in zip(relevant_files, relevant_brightness):
        im = cv2.imread(f) / bright * 2
        im = im.astype('uint8')
        im_list.append(im)
        im_size.append(im.shape)
    im_size = np.array(im_size)

    n_channels = im.shape[-1]

    edge_list = matStr['el_relative']
    n_edges = len(edge_list)
    
    # Parameters
    # weighting parameter to balance the fitting term and the smoothing term
    lambd = args.smoothing_factor * im_size[0,0] * im_size[0,1]
    # interval in pixels for the computing of deformation functions
    intv_mesh = args.deform_grid
    # the smooth transition width in the non-overlapping region is set to 
    # K_smooth times of the maximum bias.
    # K_smooth = 5 
    bgcolor = 0

    if (n_edges > 1) or (edge_list[0,0] != 0):
        # feature detection and matching
        im_gray = []
        features = []

        # detection
        for i, im in enumerate(im_list):
            if n_channels ==3 :
                im = cv2.cvtColor(im, cv2.COLOR_RGB2GRAY)
                # dim images get better feature detection if you increase contrast
            im_gray.append(im)

            if n_channels == 3:
                detector = cv2.xfeatures2d.SURF_create(
                    hessianThreshold=args.SURF_thresh,
                    nOctaves=3,
                    nOctaveLayers=4-2,
                    extended=False, upright=True)
            else:
                detector = cv2.xfeatures2d.SURF_create(
                    hessianThreshold=args.SURF_thresh,
                    nOctaves=1,
                    nOctaveLayers=6-2)
            
            ft = cv2.detail.computeImageFeatures2(detector, im)
            features.append(ft)
                
        # matching
        X_src = []
        X_trg = []
        n_matches = []

        for ei, (i, j) in enumerate(edge_list):
            # affine
            if args.matcher == 'affine':
                matcher = cv2.detail_AffineBestOf2NearestMatcher(
                    full_affine=False, try_use_gpu=False, 
                    match_conf=args.match_conf, num_matches_thresh1=args.min_matches)
            elif args.matcher == 'nearest':
                matcher = cv2.detail_BestOf2NearestMatcher(
                    try_use_gpu=False, match_conf=args.match_conf, 
                    num_matches_thresh1=args.min_matches, 
                    num_matches_thresh2=args.min_matches)
            elif args.matcher == 'range':
                matcher = cv2.detail_BestOf2NearestRangeMatcher(
                    range_width=5, try_use_gpu=False, match_conf=args.match_conf,
                    num_matches_thresh1=args.min_matches, 
                    num_matches_thresh2=args.min_matches)
            matches = matcher.apply2([features[i], features[j]])   # this can be used in cv2.drawMatches
        #     matcher.collectGarbage()
            
            # not required, but use for debugging
            matched_points_idx_1 = np.array([m.queryIdx for m in matches[1].getMatches()])
            matched_points_idx_2 = np.array([m.trainIdx for m in matches[1].getMatches()])
            matched_points_1 = np.array(features[i].getKeypoints())[matched_points_idx_1]
            matched_points_2 = np.array(features[j].getKeypoints())[matched_points_idx_2]
            matched_points_1 = np.array([kp.pt for kp in matched_points_1])
            matched_points_2 = np.array([kp.pt for kp in matched_points_2])
            
            # RANSAC
            median_absolute_deviation = np.median(
                np.abs(matched_points_1 - np.median(matched_points_1)))
            res_thresh = args.ransac_tolerance * median_absolute_deviation
            # res_thresh = 30  # how to set maxDistance = 30? 
            affine_model = RANSACRegressor( \
                LinearRegression(), max_trials=1000, \
                residual_threshold=res_thresh, \
                loss='squared_loss')
            affine_model.fit(matched_points_2, matched_points_1)

            matched_points_1I = np.ascontiguousarray( \
                    matched_points_1[affine_model.inlier_mask_], 'float32')
            matched_points_2I = np.ascontiguousarray( \
                    matched_points_2[affine_model.inlier_mask_], 'float32')

            matched_points_1I = np.hstack([matched_points_1I, np.ones((len(matched_points_1I),1))])
            matched_points_2I = np.hstack([matched_points_2I, np.ones((len(matched_points_2I),1))])    

            X_src.append(matched_points_1I)
            X_trg.append(matched_points_2I)
            n_matches.append(len(matched_points_1I))

        X_src = np.array(X_src, dtype=object)
        X_trg = np.array(X_trg, dtype=object)
        n_matches = np.array(n_matches)    
            
        X_src_original = X_src.copy()
        X_trg_original = X_trg.copy()
        X_src = X_src[n_matches >= args.min_matches]
        X_trg = X_trg[n_matches >= args.min_matches]
        edge_list = edge_list[n_matches >= args.min_matches]
        
        # Bundle adjustment
        Hall_init = matStr['H_pair'][0]  # from global pano
        paras_init = []
        for h in Hall_init[1:]:
            paras_init.append([h[0,0], h[0,1], h[0,2], h[1,2]])
        paras_init = np.array(paras_init).reshape(-1)
        
        # Optimise params for edges
        for sigma in [1000,100,10]:
            res = least_squares(residual_all_robust_similarity, paras_init, args=(X_src, X_trg, edge_list, sigma))
            if res.success:
                paras_init = res.x

        paras_final = res.x

        H_pair = make_Hpair(n_images, paras_final.reshape(-1))
        Hall = H_pair[0].copy()
        
        # Compute mosaic params
        ubox = np.empty((n_images), dtype=object)
        vbox = np.empty((n_images), dtype=object)
        ubox_ = np.empty((n_images), dtype=object)
        vbox_ = np.empty((n_images), dtype=object)
        ubox_all_ = np.empty(0)
        vbox_all_ = []
        uStruct = np.empty((n_images), dtype=object)
        vStruct = np.empty((n_images), dtype=object)

        for i in range(n_images):
            ubox[i] = np.concatenate([
                np.arange(im_size[i,1]),
                np.arange(im_size[i,1]), 
                np.ones(im_size[i,0]), 
                np.ones(im_size[i,0]) * im_size[i,1]
            ])
            
            vbox[i] = np.concatenate([
                np.ones(im_size[i,1]), 
                np.ones(im_size[i,1]) * im_size[i,0],
                np.arange(im_size[i,0]),
                np.arange(im_size[i,0])
            ])
            
            H = np.linalg.inv(Hall[i])
            z1_ = H[2,0] * ubox[i] + H[2,1] * vbox[i] + H[2,2]
            ubox_[i] = (H[0,0] * ubox[i] + H[0,1] * vbox[i] + H[0,2]) / z1_
            vbox_[i] = (H[1,0] * ubox[i] + H[1,1] * vbox[i] + H[1,2]) / z1_

            ubox_all_ = np.concatenate([ubox_all_, ubox_[i]])
            vbox_all_ = np.concatenate([vbox_all_, vbox_[i]])
            
        u0 = min(ubox_all_)
        u1 = max(ubox_all_)
        ur = np.arange(u0, u1)
        v0 = min(vbox_all_)
        v1 = max(vbox_all_)
        vr = np.arange(v0, v1)
        mosaicw = len(ur)
        mosaich = len(vr)

        m_u0_ = np.zeros(n_images, dtype=int)
        m_u1_ = np.zeros(n_images, dtype=int)
        m_v0_ = np.zeros(n_images, dtype=int)
        m_v1_ = np.zeros(n_images, dtype=int)
        imw_ = np.zeros(n_images, dtype=int)
        imh_ = np.zeros(n_images, dtype=int)

        # align the sub coordinates with the mosaic coordinates
        for i in range(n_images):
            
            # additional margin of the reprojected image region considering the possilbe deformation
            margin = 0.2 * np.min(im_size[:,:2])
            
            u0_im_ = max(min(ubox_[i]) - margin, u0)
            u1_im_ = min(max(ubox_[i]) + margin, u1)
            v0_im_ = max(min(vbox_[i]) - margin, v0)
            v1_im_ = min(max(vbox_[i]) + margin, v1)
            m_u0_[i] = int(np.ceil(u0_im_ - u0))
            m_u1_[i] = int(np.floor(u1_im_ - u0))
            m_v0_[i] = int(np.ceil(v0_im_ - v0))
            m_v1_[i] = int(np.floor(v1_im_ - v0))
            imw_[i] = int(np.floor(m_u1_[i] - m_u0_[i]))
            imh_[i] = int(np.floor(m_v1_[i] - m_v0_[i]))
            
            
        # elastic local alignment and mosaiking
        ## adjacent matrix describing the topological relationship of the input images,
        ## the elements of Adj indicate the edge indexes between the two overlapping images, 
        ## the 0 elements indicate not overlapped
        adj = np.zeros((n_images, n_images), dtype=int) 

        for ei, (i, j) in enumerate(edge_list):
            adj[i,j] = ei+1
            adj[j,i] = ei+1
            
        u, v = np.meshgrid(ur, vr)

        im_p = np.empty((n_images), dtype=object)
        mask = np.empty((n_images), dtype=object)
        mass = np.zeros((mosaich, mosaicw, n_channels))
        mosaic = np.zeros((mosaich, mosaicw, n_channels))

        refi=1

        for ki in range(n_images):
            i = (ki + refi - 1) % n_images
            
            # align image i
            u_im = u[m_v0_[i]:m_v1_[i], m_u0_[i]:m_u1_[i]]
            v_im = v[m_v0_[i]:m_v1_[i], m_u0_[i]:m_u1_[i]]

            H = Hall[i]
            z1_ = H[2,0] * u_im + H[2,1] * v_im + H[2,2]
            u_im_ = (H[0,0] * u_im + H[0,1]  * v_im + H[0,2]) / z1_
            v_im_ = (H[1,0] * u_im + H[1,1]  * v_im + H[1,2]) / z1_
            
            need_deform = False
            sub_u0_ = []
            sub_u1_ = []
            sub_v0_ = []
            sub_v1_ = []
            Pi = np.empty((0,2))
            Pi_ = np.empty((0,2))
                
            # for every image that has already been aligned
            for kj in range(ki):
                j = (kj + refi - 1) % n_images
                
                if adj[i,j] > 0:
                    need_deform = True
                    
                    H = H_pair[j,i]
                    
                    ubox_ji = H[0,0] * ubox[j] + H[0,1] * vbox[j] + H[0,2]
                    vbox_ji = H[1,0] * ubox[j] + H[1,1] * vbox[j] + H[1,2]
                    
                    sub_u0_.append(max([1, min(ubox_ji)]))
                    sub_u1_.append(max([im_size[i, 1], max(ubox_ji)]))
                    sub_v0_.append(max([1, min(vbox_ji)]))
                    sub_v1_.append(max([im_size[i, 0], max(vbox_ji)]))
                    
                    ei = adj[i,j] -1
                    
                    if (i == edge_list[ei,0]) and (j == edge_list[ei,1]):
                        Xi = X_src[ei]
                        Xj = X_trg[ei]
                    else:
                        Xi = X_trg[ei]
                        Xj = X_src[ei]
                        
                    xj_i = H[0,0] * Xj[:,0] + H[0,1] * Xj[:,1] + H[0,2]
                    yj_i = H[1,0] * Xj[:,0] + H[1,1] * Xj[:,1] + H[1,2]
                    
                    Pi = np.concatenate([Pi, Xi[:,:2]])
                    Pi_ = np.concatenate([Pi_, np.stack([xj_i, yj_i], axis=1)])
  
            if need_deform:
                sub_u0_ = min(sub_u0_)
                sub_u1_ = max(sub_u1_)
                sub_v0_ = min(sub_v0_)
                sub_v1_ = max(sub_v1_)
                
                # merge the coincided points
                ok_Pi = np.zeros(len(Pi), dtype='bool')
                _, idx_Pi = np.unique(np.round(Pi), return_index=True, axis=0)
                ok_Pi[idx_Pi] = True
                ok_Pi_ = np.zeros(len(Pi_), dtype='bool')
                _, idx_Pi_ = np.unique(np.round(Pi_), return_index=True, axis=0)
                ok_Pi_[idx_Pi_] = True
                ok_nd = ok_Pi & ok_Pi_
                Pi_nd = Pi[ok_nd]
                Pi_nd_ = Pi_[ok_nd]
                
                # form the linear system
                xi = Pi_nd[:,0]
                yi = Pi_nd[:,1]
                xj_ = Pi_nd_[:,0]
                yj_ = Pi_nd_[:,1]
                gxn = xj_ - xi
                hyn = yj_ - yi
                
                n = len(xj_)
                xx = np.repeat([xj_], n, axis=0)
                yy = np.repeat([yj_], n, axis=0)
                dist2 = (xx - xx.T)**2 + (yy - yy.T)**2
                np.fill_diagonal(dist2, 1)
                K = 0.5 * dist2 * np.log(dist2)
                np.fill_diagonal(K, lambd * 8*np.pi)
                
                K_ = np.zeros((n+3, n+3))
                K_[:n,:n] = K
                K_[n,:n] = xj_
                K_[n+1,:n] = yj_
                K_[n+2,:n] = np.ones(n)
                K_[:n,n] = xj_
                K_[:n,n+1] = yj_
                K_[:n,n+2] = np.ones(n)

                G_ = np.zeros((n+3,2))
                G_[:n,0] = gxn
                G_[:n,1] = hyn

                # solve the linear system
                W_ = np.dot(np.linalg.pinv(K_), G_)
                wx = W_[:n,0]
                wy = W_[:n,1]
                a = W_[n:,0]
                b = W_[n:,1]

                # remove outliers based on the distribution of weights
                outlier = (np.abs(wx)>3 * np.std(wx) ) | (np.abs(wy)>3 * np.std(wy) )
                
                inlier_idx = np.arange(len(xi))
                
                for kiter in range(10):
                    if sum(outlier) < 0.0027*n:
                        break

                    ok = ~outlier
                    inlier_idx = inlier_idx[ok]
                    K_ = K_[np.concatenate([ok, np.ones(3, dtype=bool)])][:,np.concatenate([ok, np.ones(3, dtype=bool)])]
                    G_ = G_[np.concatenate([ok, np.ones(3, dtype=bool)])]
                    W_ = np.dot(np.linalg.pinv(K_), G_)
                    n = len(inlier_idx)
                    wx = W_[:n,0]
                    wy = W_[:n,1]
                    a = W_[n:,0]
                    b = W_[n:,1]
                    outlier = (np.abs(wx)>3 * np.std(wx) ) | (np.abs(wy)>3 * np.std(wy) )

                ok = np.zeros(len(xj_), dtype=bool)
                ok[inlier_idx] = True
                xj_ = xj_[ok]
                yj_ = yj_[ok]
                gxn = gxn[ok]
                hyn = hyn[ok]
                
                eta_d0 = 0 # lower boundary for smooth transition area
                eta_d1 = args.smoothing_k * max(np.abs(np.concatenate([gxn, hyn]))) # higher boundary for smooth transition area
                sub_u0_ = sub_u0_ + min(gxn)
                sub_u1_ = sub_u1_ + max(gxn)
                sub_v0_ = sub_v0_ + min(hyn)
                sub_v1_ = sub_v1_ + max(hyn)

                u_mesh_ = u_im_[:imh_[i]:intv_mesh][:,:imw_[i]:intv_mesh]
                v_mesh_ = v_im_[:imh_[i]:intv_mesh][:,:imw_[i]:intv_mesh]
                gx_mesh_ = np.zeros((int(np.ceil(imh_[i]/intv_mesh)), int(np.ceil(imw_[i]/intv_mesh))))
                hy_mesh_ = np.zeros((int(np.ceil(imh_[i]/intv_mesh)), int(np.ceil(imw_[i]/intv_mesh))))

                for kf in range(n):
                    dist2 = (u_mesh_ - xj_[kf]) **2 + (v_mesh_ - yj_[kf]) **2
                    rbf = 0.5 * dist2 * np.log(dist2)
                    gx_mesh_ = gx_mesh_ + wx[kf] * rbf
                    hy_mesh_ = hy_mesh_ + wy[kf] * rbf 

                gx_mesh_ = gx_mesh_ + a[0] * u_mesh_ + a[1] * v_mesh_ + a[2]
                hy_mesh_ = hy_mesh_ + b[0] * u_mesh_ + b[1] * v_mesh_ + b[2]
                gx_im_ = cv2.resize(gx_mesh_, (imw_[i], imh_[i]))
                hy_im_ = cv2.resize(hy_mesh_, (imw_[i], imh_[i]))

                # smooth tansition to global transform
                dist_horizontal = np.max([sub_u0_-u_im_, u_im_-sub_u1_], axis=0)
                dist_vertical = np.max([sub_v0_-v_im_, v_im_-sub_v1_], axis=0)
                dist_sub = np.max([dist_horizontal, dist_vertical], axis=0)
                dist_sub = np.max([np.zeros(dist_sub.shape), dist_sub], axis=0)
                eta = (eta_d1 - dist_sub) / (eta_d1 - eta_d0)
                eta[dist_sub < eta_d0] = 1
                eta[dist_sub > eta_d1] = 0
                gx_im_ = gx_im_ * eta
                hy_im_ = hy_im_ * eta
                
                u_im_ = u_im_ - gx_im_
                v_im_ = v_im_ - hy_im_

                # update the feature locations
                for kj in range(ki+1, n_images):   # for every image that has not been aligned
                    j = (kj + refi - 1) % n_images
                    
                    if adj[i,j] > 0:
                        ei = adj[i,j] - 1
                        if (i == edge_list[ei,0]) and (j == edge_list[ei,1]):
                            Xi = X_src[ei]
                        else:
                            Xi = X_trg[ei]
                            
                        newXi = Xi.copy()

                        # Iterative solver
                        for kiter in range(20):
                            u_f = newXi[:,0]
                            v_f = newXi[:,1]
                            gx_f = np.zeros(len(newXi))
                            hy_f = np.zeros(len(newXi))

                            for kf in range(n):
                                dist2 = (u_f - xj_[kf]) **2 + (v_f - yj_[kf]) **2
                                rbf = 0.5 * dist2 * np.log(dist2)
                                gx_f = gx_f + wx[kf] * rbf
                                hy_f = hy_f + wy[kf] * rbf
                                
                            gx_f = gx_f + a[0] * u_f + a[1] * v_f + a[2]
                            hy_f = hy_f + b[0] * u_f + b[1] * v_f + b[2]
                            dist_horizontal_f = np.max([sub_u0_-u_f, u_f-sub_u1_], axis=0)
                            dist_vertical_f = np.max([sub_v0_-v_f, v_f-sub_v1_], axis=0)
                            dist_sub_f = np.max([dist_horizontal_f, dist_vertical_f], axis=0)
                            dist_sub_f = np.max([np.zeros(dist_sub_f.shape), dist_sub_f], axis=0)
                            eta_f = (eta_d1 - dist_sub_f) / (eta_d1 - eta_d0)
                            eta_f[dist_sub_f < eta_d0] = 1
                            eta_f[dist_sub_f > eta_d1] = 0
                            gx_f = gx_f * eta_f
                            hy_f = hy_f * eta_f
                            
                            newXi[:,0] = Xi[:,0] + gx_f
                            newXi[:,1] = Xi[:,1] + hy_f
                            
                        if (i == edge_list[ei,0]) and (j == edge_list[ei,1]):
                            X_src[ei] = newXi
                        else:
                            X_trg[ei] = newXi

            im_p[i] = cv2.remap((im_list[i]/255).astype('float32'), 
                                u_im_.astype('float32'), v_im_.astype('float32'), 
                                cv2.INTER_LINEAR, borderValue=(np.nan, np.nan, np.nan))*255

            mask[i] = ~np.isnan(im_p[i])
            mass[m_v0_[i]:m_v1_[i], m_u0_[i]:m_u1_[i], :] = mass[m_v0_[i]:m_v1_[i], m_u0_[i]:m_u1_[i], :] + mask[i]

            im_p[i][np.isnan(im_p[i])] = 0
            mosaic[m_v0_[i]:m_v1_[i], m_u0_[i]:m_u1_[i], :] = mosaic[m_v0_[i]:m_v1_[i], m_u0_[i]:m_u1_[i], :] + im_p[i]
            
            uStruct[i] = u_im_
            vStruct[i] = v_im_   
            
        mosaic = mosaic / mass
        mosaic = mosaic.astype('uint8')
        
        scipy.io.savemat(
            output_file, 
            mdict={'uStruct': uStruct, 'vStruct': vStruct, 'm_v0_': m_v0_, 'm_v1_': m_v1_,
                   'm_u0_': m_u0_, 'm_u1_': m_u1_, 'mosaicw': mosaicw, 'mosaich': mosaich,
                    'relCams': matStr['relCams']}, do_compression=True)
           
        panorama_output_file = os.path.join(args.directory, matStr['chunk'][0]) + '_pano.png'    
        cv2.imwrite(panorama_output_file, mosaic)

    else:
        scipy.io.savemat(output_file, mdict={'relCams': matStr['relCams']})