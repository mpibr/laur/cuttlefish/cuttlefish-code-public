#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import subprocess
import argparse
import numpy as np
import os
import glob

FONTFILE = '/usr/share/fonts/truetype/freefont/FreeSans.ttf'
CAMERA_ARRAY = np.array([\
        [[np.nan, np.nan], [0, 4], [0, 2], [0, 3], [0, 1]],
        [[0, 7], [0, 0], [0, 5], [0, 6], [1, 6]],
        [[1, 4], [1, 0], [1, 2], [1, 7], [1, 5]],
        [[1, 3], [1, 1], [2, 1], [2, 0], [2, 7]],
        [[2, 6], [2, 3], [2, 5], [2, 2], [2, 4]]
    ])

def get_ffmpeg_args(input_dir, scale, no_text=False, \
                    start_sec=None, rate=1, timecode=True, crop=None):
    """ 
    Returns :
        inputs (str) : input streams for ffmpeg, '-i FILENAME -i FILENAME ...'
        filters (str) : ffmpeg filter complex, 
                        '[INPUT]FILTER[OUTPUT];[INTPUT]FILTER[OUTPUT];...'
    """

    camera_array = CAMERA_ARRAY[:]

    # Fill empty camera with blank
    first_vid = glob.glob(os.path.join(input_dir, 'rec0_cam0_*-*-*-*-*-*.avi'))[0]
    basename = first_vid.split('_')[-1]
    dimensions = subprocess.check_output( ['ffprobe', \
            '-loglevel', 'error', \
            '-select_streams', 'v:0', \
            '-show_entries', 'stream=width,height', \
            '-print_format', 'csv=s=x:print_section=0', \
            first_vid]).strip()

    # Only process videos within the crop region
    if crop is not None:
        tile_w, tile_h = map(float, dimensions.split('x'))
        w, h, x, y = map(float, crop.split(':'))
        col_start = int(np.floor( x / tile_w ))
        col_end = int(np.ceil( (x+w) / tile_w ))
        row_start = int(np.floor( y / tile_h ))
        row_end = int(np.ceil( (y+h) / tile_h ))

        camera_mask = np.zeros(camera_array.shape[:2], dtype='bool')
        camera_mask[row_start:row_end, col_start:col_end] = True
        num_row = row_end - row_start
        num_col = col_end - col_start

        camera_array = camera_array[camera_mask].reshape(num_row, num_col, 2)
    else:
        num_row, num_col = camera_array.shape[:2]

    camera_list = camera_array.reshape(-1, 2)

    # Define input streams
    input_args = []
    input_streams = []
    blank_filter = []
    for rec_id, cam_id in camera_list:
        if np.isnan(rec_id):
            blank_filter += ['color=black:{}[blank]'.format(dimensions)]
            input_stream = '[blank]'
        else:
            if start_sec is None:
                input_stream = '[{}:v]'.format(len(input_args)/2)
            else:
                input_stream = '[{}:v]'.format(len(input_args)/4)
            
            cam_prefix = 'rec{}_cam{}_'.format(int(rec_id), int(cam_id))
            vid_name = os.path.join(input_dir, cam_prefix + basename)

            # Skip to position
            if start_sec is not None:
                input_args.append('-ss')
                input_args.append(start_sec)
            input_args.append('-i')
            input_args.append(vid_name)
            
        input_streams.append(input_stream)
    
    # Filters to rescale videos
    scale_filters = blank_filter
    scale_outputs = []
    for idx, input_stream in enumerate(input_streams):
        output_handle = '[v{}]'.format(idx)
        scale_filters.append(input_stream \
            + 'scale=iw*{}:ih*{}'.format(scale, scale) + output_handle)
        scale_outputs.append(output_handle)
    
    # Filters to label source videos
    if no_text:
        mosaic_input_mat = np.array(scale_outputs).reshape(num_row, num_col)
    else:
        drawtext_filters = []
        drawtext_outputs = []
        for idx, input_handle in enumerate(scale_outputs):
            if np.isnan(camera_list[idx,0]):
                text = ''
            else:
                text = 'rec{}_cam{}'.format(int(camera_list[idx,0]), int(camera_list[idx,1]))
            output_handle = '[t{}]'.format(idx)
            drawtext_filters.append(input_handle \
                + 'drawtext=fontfile={}:text={}:fontsize=12:fontcolor=white:x=0:y=0'.format(
                    FONTFILE, text)\
                + output_handle)
            drawtext_outputs.append(output_handle)
        mosaic_input_mat = np.array(drawtext_outputs).reshape(num_row, num_col)

    # Filters to make mosaic
    hstack_filters = []
    hstack_outputs = []
    for i, row in enumerate(mosaic_input_mat):
        input_handles = ''.join(row)
        output_handle = '[r{}]'.format(i)
        hstack_filters.append(input_handles + 'hstack=inputs=' + str(num_col) + output_handle)
        hstack_outputs.append(output_handle)
    vstack_filters = [''.join(hstack_outputs) + 'vstack=inputs=' + str(num_row) + '[v]']

    # Join all filters
    all_filters = scale_filters
    if not no_text:
        all_filters += drawtext_filters
    all_filters += hstack_filters + vstack_filters

    # Crop video
    if crop is not None:
        crop = [w, h, x - tile_w * col_start, y - tile_h * row_start]
        crop = map(lambda x: str(int(x * scale)), crop)
        crop = ':'.join(crop)
        all_filters += ['[v]crop={}[v]'.format(crop)]
    
    # Print timecode at top-right corner
    if timecode:
        fps = subprocess.check_output(['ffprobe', \
                                        '-loglevel', 'error', \
                                        '-select_streams', 'v:0', \
                                        '-show_entries', 'stream=r_frame_rate', \
                                        '-print_format', 'csv=print_section=0', \
                                        first_vid]).strip()
        all_filters += [
            '[v]drawtext=fontfile={}\
            :timecode=\'00\:00\:00\:00\':r={}\
            :fontsize=12:fontcolor=white:x=0:y=0[v]'.format(FONTFILE, fps)]

    # Alter playback rate
    if rate != 1:
        all_filters += ['[v]setpts={:0.3f}*PTS[v]'.format(1./rate)]

    return ' '.join(input_args), ';'.join(all_filters)

if __name__ == '__main__':
    p = argparse.ArgumentParser('Generate array overview mosaic')
    p.add_argument('--input-dir', help='Directory containing individual videos')
    p.add_argument('--start-sec', help='Starting timestamp or sec')
    p.add_argument('--max-sec', type=float, help='Maximum duration')
    p.add_argument('--crop', help='w:h:x:y')
    p.add_argument('--scale', default=0.05, type=float, help='Scale of output')
    p.add_argument('--rate', type=float, default=1, help='Playback rate')
    p.add_argument('--no-text', action='store_true', help='Do not show camera info')
    p.add_argument('--timecode', action='store_true', help='Show timecode')
    p.add_argument('output', default='/dev/null', help='Output video')
    args = p.parse_args()

    # Remove old file, so that metadata can be written
    if os.path.isfile(args.output):
        overwrite = raw_input('File already exists. Overwrite ? [y/N] ')
        if overwrite == 'y':
            os.remove(args.output)
        else:
            print('File not overwritten.')

    if not os.path.isfile(args.output):
        inputs, all_filters = get_ffmpeg_args(args.input_dir, args.scale, args.no_text, 
                                                args.start_sec, args.rate, args.timecode,
                                                args.crop)
        # Trim video
        if args.max_sec is not None:
            inputs += ' -t {}'.format(args.max_sec/args.rate)
        
        cmd = 'ffmpeg {} -filter_complex \"{}\" -map [v] -codec h264 \
                -metadata comment=\"start={} max={} crop={} scale={} rate={}\" {}'.format(
                    inputs, all_filters, args.start_sec, \
                    args.max_sec, args.crop, args.scale, args.rate, args.output)

        os.system(cmd)