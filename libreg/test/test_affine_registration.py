# import pytest
# from libreg import affine_registration
# import scipy.misc
# import numpy as np
# import cv2

# #
# # (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# # (C) Copyright 2016 Max-Planck Institute for Brain Research
# #
# # Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
# #
# # Redistribution and use in source and binary forms, with or without
# # modification, are permitted provided that the following conditions are met:
# #
# #     * Redistributions of source code must retain the above copyright notice,
# #       this list of conditions and the following disclaimer.
# #     * Redistributions in binary form must reproduce the above copyright
# #       notice, this list of conditions and the following disclaimer in the
# #       documentation and/or other materials provided with the distribution.
# #     * Neither the name of the author nor the names of its contributors
# #       may be used to endorse or promote products derived from this software
# #       without specific prior written permission.
# #
# # THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# # "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# # LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# # A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# # CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# # EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# #     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# #     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# # LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# #     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# # SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# #

# def old_lena():
#     """Get old test image depricated from scipy."""
#     import pickle
#     import os
#     fname = os.path.join(os.path.dirname(__file__),'lena.dat')
#     f = open(fname,'rb')
#     lena = np.array(pickle.load(f))
#     f.close()
#     return lena

# class TestAffineRegistration():

#     @pytest.mark.xfail(reason="same numerical discrepency in py2, reason unknown, fails only occasionally")
#     def test_cross_correlation_fft(self):
#         lena = np.float32(old_lena())
#         template = lena[64:-64, 64:-64]

#         for _ in range(32):
#             shift = np.random.normal(scale=25, size=2)
#             true_t = cv2.getRotationMatrix2D( \
#                     tuple(s // 2 for s in lena.shape[::-1]), 0, 1)
#             true_t[:, 2] += shift
            
#             # Warp lena
#             true_lena_t = cv2.warpAffine(lena, true_t, lena.shape[::-1], \
#                     flags=cv2.INTER_LINEAR)

#             # Find template
#             F = np.fft.fft2(template, true_lena_t.shape)
#             G = np.fft.fft2(true_lena_t, true_lena_t.shape)
#             pred_shift, _ = affine_registration.cross_correlation_fft(F, G, 16)
#             pred_shift -= [64, 64]
            
#             assert np.linalg.norm(shift - pred_shift) < 0.3

#     def test_phase_correlation(self):
#         lena = np.float32(old_lena())
#         template = lena[64:-64, 64:-64]

#         lena *= cv2.createHanningWindow(lena.shape[::-1], cv2.CV_32F)
#         template *= cv2.createHanningWindow(template.shape[::-1], cv2.CV_32F)

#         for _ in range(32):
#             shift = np.random.normal(scale=25, size=2)
#             true_t = cv2.getRotationMatrix2D( \
#                     tuple(s // 2 for s in lena.shape[::-1]), 0, 1)
#             true_t[:, 2] += shift
            
#             # Warp lena
#             true_lena_t = cv2.warpAffine(lena, true_t, lena.shape[::-1], \
#                     flags=cv2.INTER_LINEAR)

#             # Find template
#             F = np.fft.fft2(template, true_lena_t.shape)
#             G = np.fft.fft2(true_lena_t, true_lena_t.shape)
#             pred_shift, _ = affine_registration.phase_correlation(F, G, 16)
#             pred_shift -= [64, 64]
            
#             assert np.linalg.norm(shift - pred_shift) < 0.3

#     @pytest.mark.xfail(reason="same numerical discrepency in py2, reason unknown")
#     def test_fourier_mellin_angles(self):
#         lena = np.float32(old_lena())
#         template = lena[64:-64, 64:-64]

#         lena *= cv2.createHanningWindow(lena.shape[::-1], cv2.CV_32F)
#         template *= cv2.createHanningWindow(template.shape[::-1], cv2.CV_32F)
        
#         # Test angles
#         for angle in range(-180, 180, 10):
#             # Generate random rotation matrix
#             true_t = cv2.getRotationMatrix2D( \
#                     tuple(s // 2 for s in lena.shape[::-1]), angle, 1)

#             # Warp lena
#             true_lena_t = cv2.warpAffine(lena, true_t, lena.shape[::-1], \
#                     flags=cv2.INTER_LINEAR)

#             # Find template
#             pred_t, _ = affine_registration.match_template_fm( \
#                     template, np.fft.fft2(true_lena_t), 16)

#             # Warp lena
#             pred_lena_t = cv2.warpAffine(template, pred_t, lena.shape[::-1], \
#                     flags=cv2.INTER_LINEAR)
#             # Warp a mask
#             mask = np.ones_like(template, 'uint8')
#             pred_mask = cv2.warpAffine(mask, pred_t, lena.shape[::-1], \
#                     flags=cv2.INTER_LINEAR).astype(bool)

#             # Check distance pred_lenta_t and true_lena_t
#             diff = pred_lena_t - true_lena_t
#             diff[~pred_mask] = np.nan
#             assert np.abs(np.nanmean(diff)) < 0.3

#     @pytest.mark.xfail(reason="same numerical discrepency in py2, reason unknown")
#     def test_fourier_mellin_random(self):
#         lena = np.float32(old_lena())
#         template = lena[64:-64, 64:-64]
        
#         lena *= cv2.createHanningWindow(lena.shape[::-1], cv2.CV_32F)
#         template *= cv2.createHanningWindow(template.shape[::-1], cv2.CV_32F)

#         # Test angle and shifts randomly
#         for _ in range(32):
#             # Generate random rotation matrix
#             angle = np.random.randint(-180, 180)
#             shift = np.random.normal(scale=25, size=2)
#             true_t = cv2.getRotationMatrix2D( \
#                     tuple(s // 2 for s in lena.shape[::-1]), angle, 1)
#             true_t[:, 2] += shift

#             # Warp lena
#             true_lena_t = cv2.warpAffine(lena, true_t, lena.shape[::-1], \
#                     flags=cv2.INTER_LINEAR)

#             # Find template
#             pred_t, _ = affine_registration.match_template_fm( \
#                     template, np.fft.fft2(true_lena_t), \
#                     upsample_factor=10)

#             # Warp lena
#             pred_lena_t = cv2.warpAffine(template, pred_t, lena.shape[::-1], \
#                     flags=cv2.INTER_LINEAR)
#             # Warp a mask
#             mask = np.ones_like(template, 'uint8')
#             pred_mask = cv2.warpAffine(mask, pred_t, lena.shape[::-1], \
#                     flags=cv2.INTER_LINEAR).astype(bool)

#             # Check distance pred_lenta_t and true_lena_t
#             diff = pred_lena_t - true_lena_t
#             diff[~pred_mask] = np.nan
#             assert np.abs(np.nanmean(diff)) < 0.3