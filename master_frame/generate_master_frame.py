#!/usr/bin/env python3

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import numpy as np
from tqdm import tqdm
import os.path
import h5py
import buffertools
from mpi4py import MPI
import cv2
import os
import random
import string

if __name__ == '__main__':
    comm = MPI.COMM_WORLD

    p = argparse.ArgumentParser('Compute the master frame')
    p.add_argument('--registration', required=True, \
            help='Registration hd5 file')
    p.add_argument('output', help='Save master freame to this file')

    p.add_argument('--background', default='Background')
    args = p.parse_args()
    
    # Read the registration file
    reader = h5py.File(args.registration, 'r')
    maps = reader['maps']
    label_colours = reader.attrs['label_colours']
    fps = reader.attrs['fps']
    rel_video_filename = reader.attrs['video']
    rel_segmentation_filename = reader.attrs['segmentation']
    chunkoffset = reader.attrs['chunkoffset']

    # Transformation to scale maps up
    scale_maps_t = np.array([[maps.attrs['grid_size'], 0., 0.], \
                             [0., maps.attrs['grid_size'], 0.]], 'float32')
    
    # Setup filenames
    video_filename = os.path.join( \
            os.path.dirname(args.registration), \
            rel_video_filename)
    segmentation_filename = os.path.join( \
            os.path.dirname(args.registration), \
            rel_segmentation_filename)

    # Read the segmentation file
    segmentation_file = h5py.File(segmentation_filename, 'r')
    segmentation = segmentation_file['segmentation']

    # Find out foreground and background indexes
    if (segmentation.dtype == 'bool') or (len(segmentation.shape) == 4):
        label_indexes = {segmentation.attrs['enum'][0]: 0, \
                         segmentation.attrs['enum'][1]: 1}
    else:
        label_indexes = h5py.check_dtype(enum=segmentation.dtype)
    background_index = label_indexes[args.background]

    # Generate a dict of foreground indexes
    foreground_indexes = {label: index \
            for (label, index) in label_indexes.items() \
            if label != args.background}

    # Compute the master frames
    foreground_cnt = {label: np.zeros(segmentation.shape[1 : 3], 'float32') \
            for label in foreground_indexes}

    # Find out start and ends to process for this rank
    chunksize = len(segmentation) / comm.size
    start = chunksize * comm.rank
    end = min(chunksize * (comm.rank + 1), len(segmentation))

    with buffertools.Reader(maps, maps.chunks[0], start, end) \
            as maps_reader, \
         buffertools.Reader(segmentation, segmentation.chunks[0], start, end) \
            as segmentation_reader:
        it = list(range(int(start), int(end)))
        if comm.rank == 0:
            it = tqdm(it, total=end - start, desc='Computing master frame')
        for _ in it:
            small_maps = maps_reader.read()
            frame = segmentation_reader.read()

            # Convert maps to maps
            maps = cv2.warpAffine(small_maps, scale_maps_t, frame.shape[1::-1])

            # Warp frame in chunk
            if frame.dtype == 'bool':
                frame = cv2.remap(frame.astype('uint8'), maps, None, \
                        interpolation=cv2.INTER_NEAREST, \
                        borderMode=cv2.BORDER_CONSTANT, \
                        borderValue=background_index).astype('bool')
            else:
                frame = cv2.remap(frame, maps, None, \
                        interpolation=cv2.INTER_NEAREST, \
                        borderMode=cv2.BORDER_CONSTANT, \
                        borderValue=background_index)
            
            # Do the counting for every label
            for label, label_index in foreground_indexes.items():
                if len(frame.shape) == 2:
                    foreground_cnt[label] += frame == label_index
                else:
                    foreground_cnt[label] += frame[..., label_index] / 255

    # Sum across all ranks
    for label in foreground_cnt:
        foreground_cnt[label] = comm.reduce(foreground_cnt[label])

    reader.close()

    # rank 0 writes
    if comm.rank == 0:

        # Open up master frame file
        output_dirname = os.path.dirname(args.output)
        output_basename = os.path.basename(args.output)
        suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                for _ in range(4))
        tmp_output_basename = '.' + output_basename + '.' + suffix
        tmp_output = os.path.join(output_dirname, tmp_output_basename)
        master_file = h5py.File(tmp_output, 'w')
        master_file.attrs.create('fps', fps, dtype='float32')
        master_file.attrs.create('video', \
                os.path.relpath(video_filename, \
                   os.path.dirname(args.output)), \
                dtype=h5py.special_dtype(vlen=str))
        master_file.attrs.create('segmentation', \
                os.path.relpath(segmentation_filename, \
                    os.path.dirname(args.output)), \
                dtype=h5py.special_dtype(vlen=str))
        master_file.attrs.create('registration', \
                os.path.relpath(args.registration, \
                    os.path.dirname(args.output)), \
                dtype=h5py.special_dtype(vlen=str))
        master_file.attrs.create('chunkoffset', chunkoffset, dtype='uint64')
        master_file.attrs.create('num_frames', len(segmentation), \
                dtype='uint64')
        master_file.attrs.create('labels',list(foreground_cnt.keys()), \
                dtype=h5py.special_dtype(vlen=str))

        # Write master frames
        for key, value in foreground_cnt.items():
            master_file.create_dataset(key.replace('/', '-'), data=value, \
                    chunks=value.shape, compression='gzip')

        master_file.close()
        os.rename(tmp_output, args.output)

