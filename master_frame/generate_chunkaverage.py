#!/usr/bin/env python3

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import numpy as np
from tqdm import tqdm
import os.path
import h5py
import buffertools
import cv2
import os
import random
import string

if __name__ == '__main__':
    p = argparse.ArgumentParser('Compute the chunk average')
    p.add_argument('--registration', required=True, \
            help='Registration hd5 file')
    p.add_argument('output', help='Save chunk average to this file')
    args = p.parse_args()
    
    # Read the registration file
    reader = h5py.File(args.registration, 'r')
    maps = reader['maps']
    label_colours = reader.attrs['label_colours']
    fps = reader.attrs['fps']
    rel_video_filename = reader.attrs['video']
    rel_segmentation_filename = reader.attrs['segmentation']
    chunkoffset = reader.attrs['chunkoffset']

    # Transformation to scale maps up
    scale_maps_t = np.array([[maps.attrs['grid_size'], 0., 0.], \
                             [0., maps.attrs['grid_size'], 0.]], 'float32')
    
    # Setup filenames
    video_filename = os.path.join( \
            os.path.dirname(args.registration), \
            rel_video_filename)
    segmentation_filename = os.path.join( \
            os.path.dirname(args.registration), \
            rel_segmentation_filename)

    # Read the segmentation file
    segmentation_file = h5py.File(segmentation_filename, 'r')
    segmentation = segmentation_file['segmentation']
    num_frames = len(segmentation)

    # Open up the video
    video = cv2.VideoCapture(video_filename)

    # Move to chunk start
    for _ in range(chunkoffset):
        video.grab()

    # Compute the average frame
    chunkaverage = np.zeros(segmentation.shape[1 : 3] + (3, ), 'uint64')
    with buffertools.Reader(maps, maps.chunks[0]) as maps_reader:
        for _ in tqdm(range(num_frames), \
                total=num_frames, \
                desc='Computing master frame'):
            small_maps = maps_reader.read()
            succ, frame = video.read()

            # Convert maps to maps
            maps = cv2.warpAffine(small_maps, scale_maps_t, \
                    frame.shape[:2][::-1])

            # Warp frame in chunk
            frame = cv2.remap(frame, maps, None, \
                    interpolation=cv2.INTER_LINEAR)

            chunkaverage += frame

    reader.close()
    segmentation_file.close()
    video.release()

    # Open up master frame file
    output_dirname = os.path.dirname(args.output)
    output_basename = os.path.basename(args.output)
    suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
            for _ in range(4))
    tmp_output_basename = '.' + output_basename + '.' + suffix
    tmp_output = os.path.join(output_dirname, tmp_output_basename)
    chunkaverage_file = h5py.File(tmp_output, 'w')
    chunkaverage_file.attrs.create('fps', fps, dtype='float32')
    chunkaverage_file.attrs.create('video', \
            os.path.relpath(video_filename, \
               os.path.dirname(args.output)), \
            dtype=h5py.special_dtype(vlen=str))
    chunkaverage_file.attrs.create('segmentation', \
            os.path.relpath(segmentation_filename, \
                os.path.dirname(args.output)), \
            dtype=h5py.special_dtype(vlen=str))
    chunkaverage_file.attrs.create('registration', \
            os.path.relpath(args.registration, \
                os.path.dirname(args.output)), \
            dtype=h5py.special_dtype(vlen=str))
    chunkaverage_file.attrs.create('chunkoffset', chunkoffset, dtype='uint64')
    chunkaverage_file.attrs.create('num_frames', num_frames, \
            dtype='uint64')
    chunkaverage_file.create_dataset('frame', \
            data=chunkaverage.astype('float32') / num_frames / 255, \
            dtype='float32', \
            compression='gzip')

    chunkaverage_file.close()
    os.rename(tmp_output, args.output)

