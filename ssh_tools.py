#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import subprocess
import time
import warnings

CODE_DIR = os.path.dirname(os.path.abspath(__file__))

def retry_ssh(sshcmd, cmd, flush=False, connection_retries=100, connection_retrysleep=10):
    if ';' in cmd:
        warnings.warn(\
            "Commands with semicolons (;) may have unexpected behaviour."
            "Break the command into multiple retry_ssh calls.")
    num_ssh_layers = sshcmd.count('ssh ')
    cmd = cmd.replace('"', '\"')
    for i in list(range(2, num_ssh_layers+1)):
        cmd = cmd.replace('"', '\\'*(2**(i-2)) + '"')
    cmd_args = sshcmd.split(' ') + [cmd]
    n = 0
    while n < connection_retries:
        try:
            if flush:
                with subprocess.Popen(\
                        cmd_args, \
                        stdout=subprocess.PIPE, \
                        bufsize=1, \
                        universal_newlines=True) as p:
                    for line in p.stdout:
                        print('>> {}: {}'.format(sshcmd, line))
                        
                if p.returncode != 0:
                    raise subprocess.CalledProcessError(p.returncode, p.args)
                return True
            else:
                stdout = subprocess.check_output(cmd_args, stderr=subprocess.STDOUT).rstrip()
                return stdout.decode()
        except subprocess.CalledProcessError as err:
            if err.returncode == 255:
                print('Retrying ssh ...')
                time.sleep(connection_retrysleep)
                n += 1
            else:
                stderr = err.output
                print('Cannot perform SSH operation:', err.returncode, stderr.decode())
                raise RuntimeError(stderr.decode())
    raise RuntimeError('Failed to connect via SSH after {} tries'.format(n))

def retry_rsync(sshcmd, cmd, flush=False, connection_retries=30, connection_retrysleep=5):
    cmd_args = ['rsync', '-e'] + [sshcmd] + cmd.replace('"', '').split(' ')
    n = 0
    while n < connection_retries:
        try:
            if flush:
                with subprocess.Popen(\
                        cmd_args, \
                        stdout=subprocess.PIPE, \
                        bufsize=1, \
                        universal_newlines=True) as p:
                    for line in p.stdout:
                        print('>> rsync: {}'.format(line))
                        
                if p.returncode != 0:
                    raise subprocess.CalledProcessError(p.returncode, p.args)
                return True
            else:
                stdout = subprocess.check_output(cmd_args, stderr=subprocess.STDOUT).rstrip()
                return stdout.decode()
        except subprocess.CalledProcessError as err:
            if (err.returncode == 255) or (err.returncode == 23) or (err.returncode == 12):
                print('Retrying rsync ...')
                time.sleep(connection_retrysleep)
            else:
                stderr = err.output
                print('Cannot perform rsync operation:', err.returncode)
                if stderr is not None:
                    stderr = stderr.decode()
                raise RuntimeError(stderr)
            n += 1

def expand_path(x, sshcmd=None):
    """
    Relative path makes no sense in an ssh environment. But expansion does.
    """
    if sshcmd is None:
        return os.path.abspath(os.path.expandvars(os.path.expanduser(x)))
    else:
        cmd = "python3 << EOF\nimport os\nprint(os.path.abspath(os.path.expandvars(os.path.expanduser('{}'))))\nEOF".format(x)
        return retry_ssh(sshcmd, cmd)