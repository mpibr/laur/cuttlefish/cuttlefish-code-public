#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import argparse
import time
import sys

sys.tracebacklimit = 0

def output_is_outdated(input_list, output):
    """Check if rerun is necessary based on last-modified time."""

    for input in input_list:
        if os.path.getmtime(input) > os.path.getmtime(output):
            return True
    
    return False

if __name__ == '__main__':

    p = argparse.ArgumentParser('Check file dependency', add_help=False)
    p.add_argument('input', nargs='*', type=str)
    p.add_argument('--output', type=str)
    g = p.add_mutually_exclusive_group()
    g.add_argument('--optional', nargs='*', type=str)
    g.add_argument('--required', nargs='*', type=str)
    p.add_argument('--skip-mtime', action='store_true', help='')
    p.add_argument('--patience', default=40, type=float, \
        help='Max sec to wait before declare file non-existent.')
    args = p.parse_args()

    # Check that all required inputs exist
    inputs = []
    if args.input is not None:
        inputs += args.input
    if args.required is not None:
        inputs += args.required
    for f in inputs:
        slept_time = 0
        while not os.path.isfile(f):
            if slept_time < args.patience:
                time.sleep(1)
                slept_time += 1
            else:
                raise IOError('Required input "{}" for output "{}" does not exist.'.format(f, args.output))

    # If output already exists, check that rerun is necessary
    if os.path.isfile(args.output) and not args.skip_mtime:
        all_inputs = inputs
        if args.optional is not None:
            all_inputs += args.optional
        if not output_is_outdated(all_inputs, args.output):
            raise RuntimeError('Output "{}" is already up-to-date.'.format(args.output))

    print('PASSED: Dependencies check for output "{}"'.format(args.output), file=sys.stderr)
