#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 22 14:38:47 2019

@author: reiters
"""


import random
import numpy as np
from keras.layers import Input
from keras.layers.core import Dropout, Lambda
from keras.layers.convolutional import Conv2D, Conv2DTranspose
from keras.layers.pooling import MaxPooling2D
from keras.layers.merge import concatenate
from keras.callbacks import EarlyStopping, ModelCheckpoint
import h5py
import imgaug.augmenters as iaa
from imgaug.augmentables.segmaps import SegmentationMapOnImage   
    
def preprocessing(img, mask, crop_size):
    
    
    #crop
    height, width = img.shape[0], img.shape[1]
    dy, dx = [crop_size, crop_size]
    x = np.random.randint(0, width - dx)
    y = random.randint(0, height - dy)
    crop_img = img[y:(y+dy), x:(x+dx), :]
    crop_mask = mask[y:(y+dy), x:(x+dx)]
    
    return( crop_img, crop_mask)
    


def image_aug(imgs,masks, batch_size=16, crop_size=32, numClasses=2):
    
      # augmentation from package
      seq = iaa.Sequential([
      iaa.Sometimes(0.5,iaa.Affine(rotate=(-45, 45))),  # rotate by -45 to 45 degrees (affects segmaps)
      iaa.Fliplr(0.5),
      iaa.Flipud(0.5),
      iaa.Sometimes(0.2, iaa.GaussianBlur(sigma=(0.0, 1.0))),
      iaa.Sometimes(0.2,iaa.PiecewiseAffine(scale=(0.01, 0.05))),
      iaa.Multiply((0.5, 1.5))], random_order=True)    
    
      # Select files (paths/indices) for the batch
      randSelection = np.random.choice(len(imgs), 
                                     size = batch_size)
      currImgs=imgs[randSelection]
      currMasks=masks[randSelection]
      currMasks=currMasks.astype(np.int32)-1
      currMasks=1-currMasks
      
     # perform preprocessing and get labels
      batch_x = np.zeros((batch_size,crop_size,crop_size,3),dtype=np.uint8)
      batch_y = np.zeros((batch_size,crop_size,crop_size,1),dtype=np.int)
    
      for i in range(batch_size):
            outImg, outMask = preprocessing(np.squeeze(currImgs[i]),np.squeeze(currMasks[i]), crop_size)
            outMask = SegmentationMapOnImage(outMask, shape=outMask.shape, nb_classes=3)
            augImg, augMask = seq(image=outImg, segmentation_maps=outMask)
            batch_x[i,:,:,:]=augImg
            batch_y[i,:,:,0]=augMask.get_arr_int()
            print(i)          
  
      return( batch_x, batch_y)
      
      
      
def build_unet(IMG_CHANNELS):
    # Build U-Net model
    inputs = Input((None, None, IMG_CHANNELS))
    s = Lambda(lambda x: x / 255) (inputs)
    
    c1 = Conv2D(8, (3, 3), activation='elu',kernel_initializer='he_normal', padding='same') (s)
    c1 = Dropout(0.1) (c1)
    c1 = Conv2D(8, (3, 3), activation='elu',kernel_initializer='he_normal', padding='same') (c1)
    p1 = MaxPooling2D((2, 2)) (c1)
    
    c2 = Conv2D(16, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (p1)
    c2 = Dropout(0.1) (c2)
    c2 = Conv2D(16, (3, 3), activation='elu', kernel_initializer='he_normal',  padding='same') (c2)
    p2 = MaxPooling2D((2, 2)) (c2)
    
    c3 = Conv2D(32, (3, 3), activation='elu', kernel_initializer='he_normal',  padding='same') (p2)
    c3 = Dropout(0.2) (c3)
    c3 = Conv2D(32, (3, 3), activation='elu', kernel_initializer='he_normal',  padding='same') (c3)
    p3 = MaxPooling2D((2, 2)) (c3)
    
    c4 = Conv2D(64, (3, 3), activation='elu', kernel_initializer='he_normal',  padding='same') (p3)
    c4 = Dropout(0.2) (c4)
    c4 = Conv2D(64, (3, 3), activation='elu', kernel_initializer='he_normal',  padding='same') (c4)
    p4 = MaxPooling2D(pool_size=(2, 2)) (c4)
    
    c5 = Conv2D(128, (3, 3), activation='elu', kernel_initializer='he_normal',  padding='same') (p4)
    c5 = Dropout(0.3) (c5)
    c5 = Conv2D(128, (3, 3), activation='elu', kernel_initializer='he_normal',  padding='same') (c5)
    
    u6 = Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same') (c5)
    u6 = concatenate([u6, c4])
    c6 = Conv2D(64, (3, 3), activation='elu', kernel_initializer='he_normal',  padding='same') (u6)
    c6 = Dropout(0.2) (c6)
    c6 = Conv2D(64, (3, 3), activation='elu', kernel_initializer='he_normal',  padding='same') (c6)
    
    u7 = Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same') (c6)
    u7 = concatenate([u7, c3])
    c7 = Conv2D(32, (3, 3), activation='elu', kernel_initializer='he_normal',  padding='same') (u7)
    c7 = Dropout(0.2) (c7)
    c7 = Conv2D(32, (3, 3), activation='elu', kernel_initializer='he_normal',  padding='same') (c7)
    
    u8 = Conv2DTranspose(16, (2, 2), strides=(2, 2), padding='same') (c7)
    u8 = concatenate([u8, c2])
    c8 = Conv2D(16, (3, 3), activation='elu', kernel_initializer='he_normal',  padding='same') (u8)
    c8 = Dropout(0.1) (c8)
    c8 = Conv2D(16, (3, 3), activation='elu', kernel_initializer='he_normal',  padding='same') (c8)
    
    u9 = Conv2DTranspose(8, (2, 2), strides=(2, 2), padding='same') (c8)
    u9 = concatenate([u9, c1], axis=3)
    c9 = Conv2D(8, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (u9)
    c9 = Dropout(0.1) (c9)
    c9 = Conv2D(8, (3, 3), activation='elu', kernel_initializer='he_normal',  padding='same') (c9)
    
    outputs = Conv2D(NUM_CLASSES, (1, 1), activation='softmax') (c9)
    
    model = Model(inputs=[inputs], outputs=[outputs])
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['categorical_accuracy'])
    
    return model      



# Set some parameters
IMG_WIDTH = 32
IMG_HEIGHT = 32
IMG_CHANNELS = 3
NUM_CLASSES = 2
#dataDir='/home/reiters/classifiers/'
dataDir='/home/liangx/Dropbox/lab2/'
chromasFileName = 'sepia210_classifier.hd5'
#datasetName='fabricRoll'

#load info from chromas file
chromasFile=h5py.File(dataDir + chromasFileName,'r')
chromasData=chromasFile['data']
chromasEntries=chromasData.attrs['entries']
numEntries=chromasEntries.shape[0]

allImgs=[]
allMasks=[]
allNames=[]
for entry in range(numEntries):
    img=chromasData[chromasEntries[entry]]['image'][()]
    labels=chromasData[chromasEntries[entry]]['labels'][()]
    
    if np.min(labels)>0: #all labeled
       #img=img[...,::-1] # bgr to rgb
       imgName=chromasData[chromasEntries[entry]].attrs['name'].decode()
       
       allImgs.append(img)
       allMasks.append(labels)
       allNames.append(imgName)

allImgs=np.array(allImgs)
allMasks=np.array(allMasks) #switch labels 1 being chromatophore
numTrain=round(len(allImgs)*trainTestSplit)

trainImgs=allImgs[0:numTrain,:,:,:]
trainMasks=allMasks[0:numTrain,:,:]
testImgs=allImgs[numTrain:,:,:,:]
testMasks=allMasks[numTrain:,:]


model = build_unet(IMG_CHANNELS)
stim=10000
X_train, Y_train = image_aug(trainImgs,trainMasks,batch_size=stim)
X_test, Y_test = image_aug(testImgs,testMasks,batch_size=round(stim*(1-trainTestSplit)))
one_hot_labels = to_categorical(Y_train, num_classes=NUM_CLASSES)[:,:,:,::-1]  #chromatophores on first channel
one_hot_labels_test = to_categorical(Y_test, num_classes=NUM_CLASSES)[:,:,:,::-1]

earlystopper = EarlyStopping(patience=12, verbose=1)
checkpointer = ModelCheckpoint('model-fabricRoll-2.h5', verbose=1, save_best_only=True)
results = model.fit(X_train, one_hot_labels,validation_split=0.1,epochs=8000,batch_size=16, callbacks=[earlystopper, checkpointer])


score, acc = model.evaluate(X_test, one_hot_labels_test)
print('Test score:', score)
print('Test accuracy:', acc)

chromasFile.close()


