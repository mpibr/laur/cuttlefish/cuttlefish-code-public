import argparse


import sys
import tensorflow as tf
from keras.models import Model, load_model
from keras.layers import Input
from keras.layers.core import Lambda
from keras.layers.convolutional import Conv2D, Conv2DTranspose
from keras.layers.pooling import MaxPooling2D
from keras.layers.merge import concatenate
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras import backend as K
import numpy as np
#import pandas as pd
from tqdm import tqdm
from itertools import chain

import cv2
#import sklearn.naive_bayes
#import sklearn.ensemble
#import sklearn.cluster
#import sklearn.naive_bayes
#import sklearn.svm

import os.path
#import cPickle as pickle

#import document
from tqdm import tqdm
import h5py
import itertools
import sys
import logging
#from . import feature
import os
import random
import string
import threading
import numpy as np
import matplotlib.pyplot as plt


def predict_img(img, clf):
    BigImg = np.expand_dims(img, axis=0)
    # save the full prediction as uint 8
    pred = np.uint8(np.squeeze(clf.predict(BigImg))[:, :, :-1] * 255)

    return pred


def chunk(iterable, chunksize):
    '''
    Returns chunks from an iterable.
    '''
    iterable = iter(iterable)
    while True:
        buffer = []
        try:
            for _ in range(chunksize):
                buffer.append(next(iterable))
        except StopIteration:
            if buffer:
                yield buffer
            break
        yield buffer


def video_iter(cap,clf):
    '''
    Makes OpenCV's VideoCapture to an iterable.
    '''
    while True:
        succ, img = cap.read()
        # img = np.array(img)

        pred = predict_img(img[..., ::-1], clf)
        if not succ:
            break
        yield pred


if __name__ == '__main__':

    p = argparse.ArgumentParser('Classify video')
    # p.add_argument('--classifier', required=True, help='Classifier')
    p.add_argument('--classifier')
    p.add_argument('--num_classes', type=int, default=2)
    p.add_argument('--window_size', type=int, default=32)
    p.add_argument('--video', required=True, help='Video to process')
    p.add_argument('output', help='Output segmentation file')
    p.add_argument('--video-start', type=float, required=True, \
                   help='Video starting time')
    p.add_argument('--start', type=float, help='Start video at')
    p.add_argument('--end', type=float, help='End video at')
    p.add_argument('--log-level', type=int, default=0)

    p.add_argument('--log')
    p.add_argument('--threads', type=int, default=1)
    args = p.parse_args()

# os.environ["CUDA_VISIBLE_DEVICES"] = '-1'

    clf = load_model(args.classifier)

    if args.num_classes == 3:
        label_colours = [(0, 0, 255), (0, 255, 0), (255, 0, 0)]
        labels = ['Background', 'Dark', 'Light']
    else:
        label_colours = [(0, 0, 255), (0, 0, 255)]
        labels = ['background', 'chromatophore']

    err = False
    cap = cv2.VideoCapture(args.video)
    if not cap.isOpened():
        err = True
    if err:
        raise RuntimeError('Video cannot be opened')

# classifier='/Users/xitongliang/Dropbox/PycharmProjects/cep_stim/model-EB2.h5'
# clf = load_model(classifier)
#
# video='/Users/xitongliang/Dropbox/lab2/stim/EB/cam0_2019-12-06-15-54-03.avi'
# output = '/Users/xitongliang/Dropbox/lab2/stim/EB/cam0_2019-12-06-15-54-03_processed'
#
# cap = cv2.VideoCapture(video)
# label_colours = [(0, 0, 255), (0, 0, 255)]
# labels = ['background', 'chromatophore']

    # Get video info
    fps = float(cap.get(cv2.CAP_PROP_FPS))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))


    # Setup start and end times
    if args.start is None:
        args.start = args.video_start
    if args.end is None:
        args.end = args.video_start + num_frames / fps

    num_frames = int((args.end - args.start) * fps)
    prop_pos = cv2.CAP_PROP_POS_MSEC
    chunkoffset = 0

    # Setup frame iterator
    frame_iter = video_iter(cap, clf)
    frame_iter = itertools.islice(frame_iter, 0, num_frames)

    # Open up output
    output_dirname = os.path.dirname(args.output)
    output_basename = os.path.basename(args.output)
    suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                     for _ in range(4))
    tmp_output_basename = '.' + output_basename + '.' + suffix
    tmp_output = os.path.join(output_dirname, tmp_output_basename)

    writer = h5py.File(tmp_output, 'w')
    writer.attrs.create('label_colours', label_colours,
                        dtype='uint8')
    writer.attrs.create('video', os.path.relpath(args.video, \
                                                 os.path.dirname(args.output)), \
                        dtype=h5py.special_dtype(vlen=str))
    writer.attrs.create('fps', fps, dtype='float32')
    writer.attrs.create('chunkoffset', chunkoffset, dtype='uint64')

    # chunksize = max(1, BLOCKSIZE_MB * 1024 * 1024 / height / width)
    chunksize = 1

    segmentation = writer.create_dataset('segmentation', \
                                         shape=(num_frames, height, width, len(labels) - 1), \
                                         chunks=(chunksize, height, width, len(labels) - 1), \
                                         dtype='uint8',
                                         compression='gzip')

    for num, seg_chunk in \
            enumerate(chunk(tqdm(frame_iter, \
                                 total=num_frames, desc='Segmenting'), 1)):
        start = num * chunksize
        end = start + len(seg_chunk)
        segmentation[start: end] = seg_chunk

    writer.close()
    os.rename(tmp_output, args.output)
