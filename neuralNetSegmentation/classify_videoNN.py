#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

"""
Created on Mon Feb 11 14:00:07 2019

@author: reiters
"""

from mpi4py import MPI

import argparse


import sys
from tensorflow.keras.models import load_model
import numpy as np
import pandas as pd
from tqdm import tqdm
from itertools import chain

import cv2
import os.path

#import document
from tqdm import tqdm
import h5py
import itertools
import sys
import logging
import os
import random
import string
import threading
import numpy as np
import mpi_iter


def cuttlefish_mask(masterframe):
    masterframe=np.float32(masterframe)
    gSigma1=1
    gSize1=15
    gSigma2=10
    gSize2=25
    gSigma3=50
    gSize3=int(2*(2*gSigma3)+1)
    minSize=100000
    blur1 = cv2.GaussianBlur(masterframe, (gSize1,gSize1), gSigma1)
    blur2 = cv2.GaussianBlur(masterframe, (gSize2,gSize2), gSigma2)
    blur3=blur1-blur2
    mask = np.zeros_like(blur3, dtype=bool)
    mask[blur3>0.5] = True
    blur3 = cv2.GaussianBlur(blur3*mask, (gSize3,gSize3), gSigma3)
    mask = np.zeros_like(blur3, dtype=bool)
    mask[blur3>0] = True


def predict_img(img, clf, windowSize):
    BigImg= np.expand_dims(img, axis=0)
    paddedW=(img.shape[0]//windowSize+1)*windowSize
    paddedH=(img.shape[1]//windowSize+1)*windowSize
    BigImg=np.zeros([1,paddedW,paddedH,3])
    BigImg[:,0:img.shape[0],0:img.shape[1],:]=img
    pred=clf.predict(BigImg)
    return pred[0,:img.shape[0],:img.shape[1]]

# def video_iter(cap):
#     '''
#     Makes OpenCV's VideoCapture to an iterable.
#     '''
#     while True:
#         succ, img = cap.read()
#         if not succ:
#             raise RuntimeError('Video finished too early')
#         yield img

def create_video_iter(cap):
    # for _ in range(num_frames):
    #for _ in range(5):
    while True:
        succ, img = cap.read()
        if not succ:
            raise RuntimeError('Video finished too early')
        yield img        

def chunk(iterable, chunksize):
    '''
    Returns chunks from an iterable.
    '''
    iterable = iter(iterable)
    while True:
        buffer = []
        try:
            for _ in range(chunksize):
                buffer.append(next(iterable))
        except StopIteration:
            if buffer:
                yield buffer
            break
        yield buffer
 
BLOCKSIZE_MB = 32

class Logger:

    def __init__(self, comm, filename, level):
        self._comm = MPI.COMM_WORLD
        if filename is not None:
            mode = MPI.MODE_WRONLY | MPI.MODE_CREATE
            self._fh = MPI.File.Open(comm, filename, mode)
            self._fh.Set_atomicity(True)
        else:
            self._fh = None
        self._level = level

    def info(self, str):
        if self._fh is not None and self._level >= 20:
            msg = '%d: %s\n' % (self._comm.rank, str)
            self._fh.Write_shared(msg)
            self._fh.Sync()
        if self._level >= 20:
            msg = '%d: %s\n' % (self._comm.rank, str)
            sys.stdout.write(msg)

    def close(self):
        if self._fh is not None:
            self._fh.Sync()
            self._fh.Close()


if __name__ == '__main__':
    comm = MPI.COMM_WORLD
    
    p = argparse.ArgumentParser('Classify video')
    p.add_argument('--classifier')
    p.add_argument('--array-params', help='for making a panorama')
    p.add_argument('--num-classes', type=int)
    p.add_argument('--window_size', type=int, default=32)
    p.add_argument('--video', required=True, help='Video to process')
    p.add_argument('output', help='Output segmentation file')
    p.add_argument('--video-start', type=float, required=True, \
            help='Video starting time')
    p.add_argument('--start', type=float, help='Start video at')
    p.add_argument('--end', type=float, help='End video at')
    p.add_argument('--log-level', type=int, default=0)
    p.add_argument('--log')
    p.add_argument('--threads', type=int, default=1)
    args = p.parse_args()

    logger = Logger(comm, args.log, args.log_level)
    
    if args.array_params is not None:
        #just need the brightness norm
        try:
            df =  pd.read_csv(args.array_params)
        except:
            df =  pd.read_pickle(args.array_params)

        strMatch=[]
        for img in range(df.shape[0]):
            strMatch.append(np.array([args.video.find(df['names'][img])]))
        currVid = np.where(np.array(strMatch)!=-1)[0]
        bNorm = df['brightnessNorm'].values[currVid]
    else:
        bNorm = 1
   
    if comm.rank == 0:
        logger.info('Open classifier')

        clf = load_model(args.classifier)
        logger.info('Parsing classifier')
        
        if args.num_classes is None:
            o_shape = clf.layers[-1].output_shape
            args.num_classes = o_shape[len(o_shape)-1]
            
        if args.num_classes == 3:
            label_colours = [(0, 0, 255),( 0, 255, 0),( 255, 0, 0)]
            labels = ['Background','Dark','Light']
        else:
            label_colours = [(0, 0, 255),( 0, 0, 255)]
            labels = ['Chromatophore','Background']

    else:
        labels = None
        label_colours = None

    # Open video
    err = False
    if comm.rank == 0:
        logger.info('Open video')
        cap = cv2.VideoCapture(args.video)
        if not cap.isOpened():
            err = True
    err = comm.bcast(err)
    if err:
        raise RuntimeError('Video cannot be opened')

    # Master rank prepares input, output
    logger.info('Preparing worker-consumer-pattern')
    frame_iter = None
    func = None
    if comm.rank == 0:
        # Get video info
        if cv2.__version__.split('.')[0] >= '3':
            fps = float(cap.get(cv2.CAP_PROP_FPS))
            height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
            width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
            num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        else:
            fps = float(cap.get(cv2.cv.CV_CAP_PROP_FPS))
            height = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
            width = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
            num_frames = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
        logger.info('Video at %dfps, length %d, %dx%d' \
                % (fps, num_frames, width, height))

        # Setup start and end times
        if args.start is None:
            args.start = args.video_start
        if args.end is None:
            args.end = args.video_start + num_frames / fps

        num_frames = int((args.end - args.start) * fps)

        # Goto the video start
        logger.info('Cutting video at %d-%d' % (args.start, args.end))
        if cv2.__version__.split('.')[0] >= '3':
            prop_pos = cv2.CAP_PROP_POS_MSEC
        else:
            prop_pos = cv2.cv.CV_CAP_PROP_POS_MSEC
        chunkoffset = 0
        while cap.get(prop_pos) / 1000. + args.video_start < args.start:
            cap.grab()
            chunkoffset += 1
        
        # Setup frame iterator
        logger.info('Setting up iterators')
        frame_iter = create_video_iter(cap)
        frame_iter = itertools.islice(frame_iter, 0, num_frames)
        
        # Open up output
        output_dirname = os.path.dirname(args.output)
        output_basename = os.path.basename(args.output)
        suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                for _ in range(4))
        tmp_output_basename = '.' + output_basename + '.' + suffix
        tmp_output = os.path.join(output_dirname, tmp_output_basename)
        logger.info('Opening output')
        writer = h5py.File(tmp_output, 'w')
        logger.info('Creating attribute label_colours')
        writer.attrs.create('label_colours', label_colours,
                dtype='uint8')
        logger.info('Creating attribute video')
        writer.attrs.create('video', os.path.relpath(args.video, \
                os.path.dirname(args.output)), \
                dtype=h5py.special_dtype(vlen=str))
        logger.info('Creating attribute fps')
        writer.attrs.create('fps', fps, dtype='float32')
        logger.info('Creating attribute chunkoffset')
        writer.attrs.create('chunkoffset', chunkoffset, dtype='uint64')
        writer.attrs.create('num_frames', num_frames, dtype='uint64')

        chunksize = int(max(1, BLOCKSIZE_MB * 1024 * 1024 / height / width))
        # if len(labels) == 2:
        #     segmentation_dtype = 'bool'
        # else:
        # segmentation_dtype = h5py.special_dtype( \
        #         enum=('int8', \
        #             {label: index for index, label in enumerate(labels)}))
        logger.info('Creating dataset segmentation')
        segmentation = writer.create_dataset('segmentation', \
                shape=(num_frames, height, width, len(labels)), \
                chunks=(chunksize, height, width, len(labels)), \
                dtype="uint8",
                compression='gzip')

        if len(labels) == 2:
            segmentation.attrs.create('enum', data=labels, \
                    dtype=h5py.special_dtype(vlen=str))

    # Process stuff in a worker consumer way
    # Root rank reads frames, slave ranks process them
    logger.info('Processing frames')
    func = lambda img: predict_img(img/bNorm, clf, args.window_size)

    result_iter = mpi_iter.parallel_map(comm, func, frame_iter)     
    if comm.rank == 0:
        writer.attrs.create('labels', labels,  \
             dtype=h5py.special_dtype(vlen=str))
        
        chunksize = segmentation.chunks[0]
        for num, seg_chunk in \
                enumerate(chunk(tqdm(result_iter, \
                total=num_frames, desc='Segmenting'), chunksize)):
            start = num * chunksize
            end = start + len(seg_chunk)
            # if len(labels) == 2:
            #     seg_chunk = [a.astype('bool') for a in seg_chunk]
            segmentation[start : end] = (np.array(seg_chunk) * 255).astype('uint8')
            if num == 0:
                mf = np.sum(seg_chunk, axis=0, keepdims=True)
                cv2.imwrite(args.output + '.png', np.uint8(np.squeeze(mf[0,...,0])))
            else:
                mf = np.sum([mf, np.uint64(seg_chunk)], axis=0) 
        
        mf = np.sum(seg_chunk, axis=0, keepdims=True)
        writer.create_dataset('masterframe', \
            shape=(1, height, width, len(labels) -1), \
            data=mf[...,:-1]/255.0,  \
            dtype='uint64', \
            compression='gzip')        
        
        logger.info('calculating chunk cuttlefish mask')
        mfMask = cuttlefish_mask(np.float32(np.squeeze(mf[0,:,:,0])/num_frames/255.0))
    
        writer.create_dataset( \
            'chunkaverage_mask', \
            data = mfMask, \
            dtype='bool')

        writer.close()
        os.rename(tmp_output, args.output)
    logger.info('Done')
