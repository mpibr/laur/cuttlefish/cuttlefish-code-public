## Stage 1: Build custom Python packages into /root/.local
# Official Miniconda (Debian) base image
FROM continuumio/miniconda3:4.6.14 AS builder

# Install build dependencies for custom modules
# They will not be in the final image to keep it slim.
# FFMPEG version synced with environment.yml
RUN echo "deb http://archive.debian.org/debian stretch main" > /etc/apt/sources.list && \
    apt-get update && \
    apt-get install -y build-essential && \
    conda config --set channel_priority strict && \
    conda install -c conda-forge -c default python=3.7 cython numpy git pkgconfig ffmpeg=5

COPY moving_least_squares /tmp/moving_least_squares
RUN pip install /tmp/moving_least_squares --user --no-deps

COPY buffertools /tmp/buffertools
RUN pip install /tmp/buffertools --user --no-deps

COPY mpi_iter /tmp/mpi_iter
RUN pip install /tmp/mpi_iter --user --no-deps

COPY libreg /tmp/libreg
RUN pip install /tmp/libreg --user --no-deps

### Build OpenCV to get non-free algorithms, e.g. SURF
### while making sure ffmpeg is discovered
RUN CMAKE_ARGS="-DWITH_FFMPEG=ON -DOPENCV_FFMPEG_SKIP_BUILD_CHECK=ON -DOPENCV_ENABLE_NONFREE=ON \
        -DCMAKE_BUILD_PARALLEL_LEVEL=$(nproc)" \
        pip install --no-binary=opencv-contrib-python opencv-contrib-python \
        --verbose --user --no-deps

######

## Stage 2: Build pipeline container
# Official Miniconda base image
# Micromamba activate did not work with SSH test
FROM continuumio/miniconda3:4.6.14

# Install system packages
# This includes libraries that cannot be managed through
# Miniconda, e.g. libGL which is required by OpenCV 4.0
# OpenMPI installed separately so it is in the right path.
RUN echo "deb http://archive.debian.org/debian stretch main" > /etc/apt/sources.list && \
    apt-get update && \
    apt-get install -y libgl1-mesa-glx && \
    apt-get install -y libopenmpi-dev openmpi-bin && \
    apt-get install -y libsm6 libxext6 libxrender-dev && \
    apt-get autoremove && apt-get autoclean && \
    rm -rf /var/lib/apt/lists/* && \
    echo 'export PS1="[\u@docker]\w$ "' >> /root/.bashrc

# Install Mamba using Miniconda and install Python packages and the associated libraries using Mamba
ADD environment.yml /tmp/environment.yml
RUN conda install mamba -n base -c conda-forge && \
    mamba env update -n base -f /tmp/environment.yml && \
    mamba clean --all --yes && \
    mamba list && \
    rm /tmp/environment.yml && \
    MPICC=/usr/bin/mpicc pip install mpi4py --user

# Copy custom packages from building stage
COPY --from=builder /root/.local /root/.local

# Add packages from building stage to path
RUN echo 'export PATH=/root/.local/bin:$PATH' >> /root/.bashrc

# Make sure scipy can use the new GLIBCXX
RUN echo 'export LD_LIBRARY_PATH=/opt/conda/lib/:$LD_LIBRARY_PATH' >> /root/.bashrc

# Ensure /root/.bashrc is sourced
ENV BASH_ENV "/root/.bashrc"
ENV TINI_SUBREAPER ""
ENV HOME "/root"
ENV TMPDIR "/tmp"
