#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pandas as pd
import argparse
import subprocess
import time
import datetime
from tqdm import tqdm
from uuid import uuid1
import io
import configparser as ConfigParser
from ssh_tools import expand_path, retry_ssh, retry_rsync
import h5py
import tempfile
import cv2
import numpy as np

def delete_old_chunks(chunktimes):
    prefix = os.path.splitext(chunktimes)[0]
    with open(chunktimes, 'r') as f:
        for line in f:
            start_f, end_f, _, _, _ = line.strip('\n').split(' ')
            for file_ext in ['mp4', 'seg', 'seg.log', \
                'masterframe', 'masterframe.log', 'chunkaverage', 'chunkaverage.log', \
                'cstitching', 'cstitching.log']:
                old_file = '{}-{}-{}.{}'.format(prefix, start_f, end_f, file_ext)
                if os.path.isfile(old_file):
                    os.remove(old_file)
    os.remove(chunktimes)

def generate_syncfiles_cpu(df, chunktimes, syncfiles):
    prefix = os.path.splitext(chunktimes)[0]
    basename = os.path.basename(prefix)
    if os.path.isfile(syncfiles):
        os.remove(syncfiles)
    files_to_sync = ''
    with open(syncfiles, 'a') as fs:

        with open(chunktimes, 'r') as fc:
            for line in fc:
                start_f, end_f, _, _, _ = line.strip('\n').split(' ')
                
                for c in range(df.shape[0]):
                    cam = df['names'][c]
                        
                    for file_ext in ['.mp4', '.png', '.seg', '.seg.log']:
                        files_to_sync += '+ {}{}-{}-{}{}\n'.format(cam, basename, start_f, end_f, file_ext)
                            
  
                for file_ext in ['_relCams', '_.mat', '_.mat.log', \
                                 '_panoParams.mat', '_panoParams.mat.log', '_pano.png', \
                                 '.segPano','.segPano.log', '.segPano_mf.png', '.segPano_cvg.png',\
                                 '_segPano.mp4', '_pano.mp4', \
                                 '.cstitching','.cstitching.log', \
                                 '.careas','.careas.log']:
                    files_to_sync += '+ {}-{}-{}{}\n'.format(basename, start_f, end_f, file_ext)
                    
        for file_ext in ['chunktimes', 'stitching', 'stitching.log', \
            'queenframe', 'queenframe.log',  \
            'cleanqueen', 'cleanqueen.log', \
            'areas', 'areas.log']:
            files_to_sync += '+ {}.{}\n'.format(basename, file_ext)
            
        if args.debug:
            files_to_sync += '+ {}.{}\n'.format(basename, 'filedeps.log')
        files_to_sync += '- *'
        fs.write(files_to_sync)
        
def generate_syncfiles_gpu(df, chunktimes, syncfiles):
    prefix = os.path.splitext(chunktimes)[0]
    basename = os.path.basename(prefix)
    if os.path.isfile(syncfiles):
        os.remove(syncfiles)
    files_to_sync = ''
    with open(syncfiles, 'a') as fs:

        with open(chunktimes, 'r') as fc:
            for line in fc:
                start_f, end_f, _, _, _ = line.strip('\n').split(' ')

                for c in range(df.shape[0]):
                    cam=df['names'][c]
                        
                    for file_ext in ['.mp4','.seg', '.seg.log']:
                        files_to_sync += '+ {}{}-{}-{}{}\n'.format(cam, basename, start_f, end_f, file_ext)
                            
                for file_ext in ['_relCams','.chunktimes']:
                    files_to_sync += '+ {}-{}-{}{}\n'.format(basename, start_f, end_f, file_ext)
                    
        for file_ext in ['chunktimes']:
            files_to_sync += '+ {}.{}\n'.format(basename, file_ext)
        if args.debug:
            files_to_sync += '+ {}.{}\n'.format(basename, 'filedeps.log')
        files_to_sync += '- *'
        fs.write(files_to_sync)

if __name__ == '__main__':

    pipeline_start_time = time.time()

    # Arguments used in run_pipeline
    p = argparse.ArgumentParser('Running pipeline', add_help=False)
    p.add_argument('--cluster-config', default='~/.cuttleline.cfg', type=str)
    p.add_argument('--hostname', type=str, help='e.g. localhost')
    p.add_argument('--hostname_gpu', type=str, help='e.g. localhost')
    p.add_argument('--sshcmd', type=str, help='If specified, override hostname')
    p.add_argument('--sshcmd_gpu', type=str, help='If specified, override hostname_gpu')
    p.add_argument('--uuid', help='UUID (8-4-4-4-12) of the output tmp folder')
    p.add_argument('--debug', action='store_true')
    p.add_argument('--keepfiles', action='store_true')
    p.add_argument('--config', help='Default to video_basename.cfg')
    p.add_argument('--sleep-between-sync', default=10, type=int)
    p.add_argument('--inputFolder', help='directory containing videos')
    p.add_argument('inputVideo', help='video basename')
    p.add_argument('--PullRemoteFirst', action='store_true')
    p.add_argument('--overview-chunk', help='overview prefix to add to chunking')
     
    p.add_argument('--skip-panorama', action='store_true') 
    p.add_argument('--skip-initialSync', action='store_true')
    p.add_argument('--skip-segPano', action='store_true')
    
    args, remaining_argv = p.parse_known_args()
    
    # Arguments passed directly to run_pipeline_remote
    # Included to display in --help
    p_remote = argparse.ArgumentParser(parents=[p])
    p_remote.add_argument('--rerun-panorama', action='store_true')
    p_remote.add_argument('--rerun-segmentation', action='store_true')
    p_remote.add_argument('--rerun-segPano', action='store_true')
    p_remote.add_argument('--rerun-chunk-stitching', action='store_true')
    p_remote.add_argument('--rerun-stitching', action='store_true')
    p_remote.add_argument('--skip-segmentation', action='store_true')
    p_remote.add_argument('--skip-chunks', action='store_true', help='Start from chunk stitching')
    p_remote.add_argument('--skip-stitching', action='store_true', help='Skip stitching')
    p_remote.add_argument('--skip-queenframe', action='store_true', 
                          help='Start from queenframe, proceed to areas')
    p_remote.add_argument('--max-queued', help='default=299, confirm in run_pipeline_remote')
    p_remote.add_argument('--check-file-patience', help='default=40, patience (s) for check_filedeps.')
    _ = p_remote.parse_args()
    
    # Set up basic blocks
    dirname = args.inputFolder 
    basepath = os.path.basename(dirname)
    video_basename = args.inputVideo
    prefix = os.path.join(dirname, video_basename)
    local_code_dir = os.path.dirname(os.path.abspath(__file__))
    if args.uuid is None:
        args.uuid = str(uuid1())
    print('UUID: ' + args.uuid)
    
    # Expand addresses to read ~, $HOME, etc.
    args.cluster_config = expand_path(args.cluster_config)
    if args.config is None:
        args.config = prefix + '.cfg'
    else:
        args.config = expand_path(args.config)

    # Parse dataset config, more options under 'submitting jobs'
    config = ConfigParser.ConfigParser(allow_no_value=True)
    try:
        config.read([args.config])
    except ConfigParser.MissingSectionHeaderError:
        # Backward compatibility for sectionless config
        cmd = 'cat '+ args.config
        string = '[sepia]\n' + subprocess.check_output(cmd).decode("utf-8")
        config.readfp(io.StringIO(string))

    classifier = expand_path(os.path.join(dirname, config.get('sepia', 'classifier')))
    array_config = expand_path(os.path.join(dirname, config.get('sepia', 'array_config')))

    #backwards compatability beuase cluster can't updated pandas to read new pkl files
    try:
        df =  pd.read_csv(array_config)
    except:
        df =  pd.read_pickle(array_config)
      
    if args.hostname is None:
        args.hostname = config.get('sepia', 'hostname')
        args.hostname = config.get('sepia', 'hostname_gpu')

    # Parse pipeline config (local)
    cluster_config = ConfigParser.ConfigParser()
    cluster_config.read(args.cluster_config)
    # Sectionless config support no longer supported, require [local] and [host] tags

    sepia_dir = cluster_config.get('local', 'sepia_dir')
    outdir = expand_path(os.path.join(cluster_config.get('local', 'outdir'), basepath))
    subprocess.call(['mkdir', '-p', outdir])  # make output folder with the same name
    if args.debug:
        print('local local_code_dir: '+ local_code_dir)
        print('local sepia_dir: '+ sepia_dir)
        print('local outdir: '+ outdir)

    if args.sshcmd is None:
        args.sshcmd = 'ssh ' + args.hostname  
        
    if args.sshcmd_gpu is None:
        args.sshcmd_gpu = 'ssh ' + args.hostname_gpu  
        
    working_dir = os.path.join(expand_path(cluster_config.get('host', 'working_dir'), \
        sshcmd=args.sshcmd), args.uuid)
    working_dir_gpu = os.path.join(expand_path(cluster_config.get('seg_host', 'working_dir'), \
        sshcmd=args.sshcmd), args.uuid)
    
    try:
        seg_partition = cluster_config.get('seg_host', 'partition')
    except ConfigParser.NoOptionError:
        seg_partition = config.get('sepia', 'segmentation_partition')
    
    code_dir = expand_path(cluster_config.get('host', 'code_dir'), sshcmd=args.sshcmd)
    # data_dir = expand_path(os.path.join(cluster_config.get('host', 'data_dir'), basepath))
    data_dir = os.path.join(expand_path(cluster_config.get('host', 'data_dir'), \
        sshcmd=args.sshcmd), basepath)
 
    # Get shell executor, e.g. container command
    mpicmd = cluster_config.get('host', 'mpicmd')
    try:
        shell = cluster_config.get('host', 'shell')
        mpicmd = '{} {}'.format(mpicmd, shell)
    except ConfigParser.NoOptionError:
        shell = ''
    mpicmd = ' '.join([cluster_config.get('host', 'mpicmd'), shell])

    print('sshcmd: '+ args.sshcmd)
    print('remote working_dir: '+ working_dir)
    print('remote data_dir: '+ data_dir)
    print('remote code_dir: '+ code_dir)
    print(('remote shell: '+ shell))
    print(('full mpicmd: '+ mpicmd))
    print('seg partition: '+ seg_partition)

    # CHUNKING
    # Chunk if chunktimes does not exisit or if a new video or new chunks file is found
    chunktimes = os.path.join(outdir, video_basename) + '.chunktimes'
    chunks = prefix + '.chunks'
    
    if not os.path.isfile(chunktimes) or \
        (os.path.getctime(chunktimes) < os.path.getctime(chunks)):

        # Delete old chunks and associated files if exist
        if os.path.isfile(chunktimes):
            delete_old_chunks(chunktimes)

        # Local chunking
        firstVid = True
        cam_list = df['names'].values
        if args.overview_chunk is not None:
            cam_list = np.append(cam_list, args.overview_chunk)
        for cam in cam_list:
        # for cam in df['names'].values:
           # import pdb; pdb.set_trace()
            currVideo = os.path.join(dirname, cam + video_basename) + '.avi'
            if os.path.isfile(currVideo) or os.path.islink(currVideo):
                
                chunk_video_output = chunktimes.replace(
                    video_basename, cam + video_basename)
                    
                subprocess.call([local_code_dir + '/chunking/chunk_video.py', \
                    '--video', currVideo, \
                    '--chunks', chunks, \
                    '--write-png', \
                    '--array', \
                    chunk_video_output])
                
                if firstVid:
                    os.rename(chunk_video_output, chunktimes)
                else:
                    os.remove(chunk_video_output)
                
                firstVid = False
                
    # with open(chunktimes, 'r') as f:
    #     size_list = []
    #     line_list = []
    #     for line in f:
    #         line = line.strip('\n').split(' ')
    #         start_f, end_f, _, _, _ = line
    #         line_list.append(line)

    #     sorted_chunktimes = np.array(line_list)
          
    # chunk_prefix = ['{}-{}-{}'.format(args.prefix, start_f, end_f) for 
    #     (start_f, end_f, _, _, _) in sorted_chunktimes]
    # chunk_name = [os.path.split(cp)[1] for cp in chunk_prefix]
    # panoConfig = [cp + '_.mat' for cp in chunk_prefix]
        
    # for chunkidx, output in enumerate(panoConfig):
        
    #     cmd_args = [local_code_dir + '/panorama/panoramaConfig.py', \
    #         '--directory', dirname, 
    #         '--data-dir', dirname,
    #         '--chunk', chunk_name[chunkidx],
    #         '--arrayConfig', array_config]
    #     cmd_args = ' '.join(cmd_args)
    #     stdout = retry_ssh(args.sshcmd, cmd_args, flush=False)

    # # go through chunk times, calculate panorama  
    # # do it locally because of matlab chaos on the cluster
    # # move to remote!!
    # if args.skip_panorama is False:
    #     with open(chunktimes, 'r') as fc:
    #         for line in tqdm(fc, desc = 'calculating panoramas'):
    #             start_f, end_f, _, _, _ = line.strip('\n').split(' ')
    #             chunk_name = '{}-{}-{}'.format(video_basename, start_f, end_f)
    #             pano_params = os.path.join(outdir, chunk_name) + '_panoParams.mat'
                
    #             if not os.path.isfile(pano_params):
    #                 print('currently ' + start_f + ':' + end_f)
    #                 subprocess.call([
    #                     'python', 
    #                     os.path.join(local_code_dir, 'panorama', 'panoramaConfig.py'),
    #                     '--directory', outdir, '--data-dir', outdir,
    #                     '--chunk', chunk_name,
    #                     '--arrayConfig', array_config])
                    
    #                 subprocess.call([
    #                     'python', 
    #                     os.path.join(local_code_dir, 'panorama', 'panorama_stitching.py'),
    #                     '--directory', outdir, '--data-dir', outdir,
    #                     '--panorama-config', pano_params])
  
    
    # SYNCING. Go to both clusters
    
    # Determine files to be transferred
    syncfiles_cpu = prefix + '.syncfiles_cpu'
    syncfiles_gpu = prefix + '.syncfiles_gpu'
    generate_syncfiles_cpu(df, chunktimes, syncfiles_cpu)
    generate_syncfiles_gpu(df, chunktimes, syncfiles_gpu)
    
    if not args.skip_initialSync:
        if args.PullRemoteFirst:
            print('Syncing back files ...')
    
            retry_rsync(args.sshcmd, '-ap --include-from="{}" "{}"/ :"{}" --progress'.format(
                syncfiles_cpu, working_dir, outdir), flush=args.debug)
            
            retry_rsync(args.sshcmd, '-ap --include-from="{}" "{}"/ :"{}" --progress'.format(
                syncfiles_gpu, working_dir_gpu, outdir), flush=args.debug)
     
        # Make working dir on cpu remote, send video clips and files there
        retry_ssh(args.sshcmd_gpu, 'mkdir -p {}'.format(working_dir))
        
        print('Sending shared files')
        retry_rsync(args.sshcmd, '-ap "{}" :"{}"'.format(
            array_config, os.path.join(working_dir, 'array_config')), flush=args.debug)
        print('sent array config')
    
        print('Sending files')
        retry_rsync(args.sshcmd, '-ap --include-from="{}" "{}"/ :"{}" --progress'.format(
            syncfiles_cpu, outdir, working_dir), flush=args.debug)
        
        # Make working dir on gpu remote, send files
            
        retry_ssh(args.sshcmd_gpu, 'mkdir -p {}'.format(working_dir_gpu))
        
        print('Sending shared files gpu')
        retry_rsync(args.sshcmd_gpu, '-ap "{}" :"{}"'.format(
            classifier, os.path.join(working_dir_gpu, 'classifier.clf')), flush=args.debug)
        print('sent classifier gpu')
        retry_rsync(args.sshcmd_gpu, '-ap "{}" :"{}"'.format(
            array_config, os.path.join(working_dir_gpu, 'array_config')), flush=args.debug)
        print('sent array config gpu')
        
        print('Sending files gpu')
        retry_rsync(args.sshcmd_gpu, '-ap --include-from="{}" "{}"/ :"{}" --progress'.format(
            syncfiles_gpu, outdir, working_dir_gpu), flush=args.debug)


    # # Submit segmentation on GPU cluster
    # if not args.skip_segmentation:
    #     print('Submitting segmentation jobs')
    #     cmd_args = [os.path.join(code_dir, 'run_pipeline_remote_segmentation.py'), \
    #        '--segmentation-slurm "{}"'.format(config.get('sepia', 'segmentation_slurm')), \
    #        '--segmentation-args "{}"'.format(config.get('sepia', 'segmentation_args')), \
    #        '--segmentation-partition "{}"'.format(config.get('sepia', 'segmentation_partition')), \
    #       # '--mpicmd "{}"'.format(cluster_config.get('host', 'mpicmd')), \ #use the default mpiexec on saion
    #        '--partition "{}"'.format(cluster_config.get('seg_host', 'partition')), \
    #        '--rerun-segmentation "{}"'.format(args.rerun_segmentation), \
    #        '--data-dir "{}"'.format(working_dir_gpu), \
    #         os.path.join(working_dir_gpu, video_basename)] + remaining_argv
    #     cmd_args = ' '.join(cmd_args)
    #     print(cmd_args)
        
    #     stdout = retry_ssh(args.sshcmd_gpu, cmd_args, flush=False)
         
    #     print('Waiting for job')
    #     job_running = True
    #     while job_running:
    #         stdout = retry_ssh(args.sshcmd_gpu, 'scontrol show job --oneliner')
            
    #         try:
    #             status = stdout.decode('utf-8').split('JobState=')[-1].split()[0]
    #             if status == 'PENDING':
    #                 for _ in tqdm(range(args.sleep_between_sync), 
    #                               desc='Sleep between resync', disable=not(args.debug)):
    #                     time.sleep(1)
                    
    #                 print('Syncing back files ...')
    #                 retry_rsync(args.sshcmd_gpu, 
    #                             '-ap --include-from="{}" :"{}"/ "{}" --progress'.format(
    #                                 syncfiles_gpu, working_dir_gpu, outdir), flush=args.debug)
    #             else:
    #                 job_running = False
    #         except:
    #             job_running = False
                
  
    # print('Syncing back files ...')
    # retry_rsync(args.sshcmd_gpu, 
    #             '-ap --include-from="{}" :"{}"/ "{}" --progress'.format(
    #                 syncfiles_gpu, working_dir_gpu, outdir), flush=args.debug)
    
    # Submit segPano and later jobs to cpu cluster
    # if not args.skip_segmentation:
    
    print('Submitting segPano and further jobs')
    cmd_args = [os.path.join(code_dir, 'run_pipeline_remote_array.py'), \
        '--foreground', config.get('sepia', 'foreground'), \
        '--filedeps-slurm "{} "'.format(config.get('sepia', 'filedeps_slurm')), \
        '--panoConfig-slurm "{}"'.format(config.get('sepia', 'panoConfig_slurm')), \
        '--panoConfig-args "{} "'.format(config.get('sepia', 'panoConfig_args')), \
        '--panoParams-slurm "{}"'.format(config.get('sepia', 'panoParams_slurm')), \
        '--panoParams-args "{} "'.format(config.get('sepia', 'panoParams_args')), \
        '--segmentation-slurm "{}"'.format(config.get('sepia', 'segmentation_slurm')), \
        '--segmentation-args "{} "'.format(config.get('sepia', 'segmentation_args')), \
        '--segmentation-partition "{}"'.format(seg_partition), \
        '--segPano-slurm "{}"'.format(config.get('sepia', 'segPano_slurm')), \
        '--segPano-args "{} "'.format(config.get('sepia', 'segPano_args')), \
        '--segPano-partition "{}"'.format(config.get('sepia', 'segPano_partition')), \
        '--chunk-stitching-slurm "{}"'.format(config.get('sepia', 'chunk_stitching_slurm')), \
        '--chunk-stitching-args "{} "'.format(config.get('sepia', 'chunk_stitching_args')), \
        '--chunk-stitching-partition "{}"'.format(config.get('sepia', 'chunk_stitching_partition')), \
        '--stitching-slurm "{}"'.format(config.get('sepia', 'stitching_slurm')), \
        '--stitching-args "{} "'.format(config.get('sepia', 'stitching_args')), \
        '--stitching-partition "{}"'.format(config.get('sepia', 'stitching_partition')), \
        '--queenframe-slurm "{}"'.format(config.get('sepia', 'queenframe_slurm')), \
        '--queenframe-args "{} "'.format(config.get('sepia', 'queenframe_args')), \
        '--cleanqueen-slurm "{}"'.format(config.get('sepia', 'cleanqueen_slurm')), \
        '--cleanqueen-args "{} "'.format(config.get('sepia', 'cleanqueen_args')), \
        '--chunk-areas-slurm "{}"'.format(config.get('sepia', 'chunk_areas_slurm')), \
        '--chunk-areas-args "{} "'.format(config.get('sepia', 'chunk_areas_args')), \
        '--areas-slurm "{}"'.format(config.get('sepia', 'areas_slurm')), \
        '--areas-args "{} "'.format(config.get('sepia', 'areas_args')), \
        '--mpicmd "{}"'.format(mpicmd), \
        '--partition "{}"'.format(cluster_config.get('host', 'partition')), \
        '--sshcmd "{}"'.format(args.sshcmd), \
        '--data-dir "{}"'.format(data_dir), \
        os.path.join(working_dir, video_basename)] + remaining_argv
    
    if args.debug:
        cmd_args.append('--debug')
    
    cmd_args = ' '.join(cmd_args)
    cmd_args = '{} {}'.format(shell, cmd_args)
    print(cmd_args)
    stdout = retry_ssh(args.sshcmd, cmd_args, flush=False)
    final_jobid = stdout.split('FINAL_JOBID=')[-1].rstrip("')").lstrip("'").lstrip(", u'")
    
    print('Waiting for job')
    job_running = True
    while job_running:
        if args.debug:
            filedeps_file = os.path.join(outdir, video_basename + '.filedeps.log')
            if os.path.isfile(filedeps_file):
                with open(filedeps_file) as f:
                    print(''.join(f.readlines()))

        stdout = retry_ssh(args.sshcmd, 'scontrol show job {} --oneliner'.format(final_jobid))
        status = stdout.split('JobState=')[-1].split(' ')[0]

        if args.debug:
            disable_tqdm = None
        else:
            disable_tqdm = True
        if status == 'PENDING':

            for _ in tqdm(range(args.sleep_between_sync), 
                          desc='Sleep between resync', 
                          disable=disable_tqdm):
                time.sleep(1)
            
            print('Syncing back files ...')
            retry_rsync(args.sshcmd, 
                        '-ap --include-from="{}" :"{}"/ "{}" --delete --progress'.format(
                            syncfiles_cpu, working_dir, outdir), flush=args.debug)
        else:
            job_running = False
                
    print('Syncing back files ...')
    retry_rsync(args.sshcmd, 
            '-ap --include-from="{}" :"{}"/ "{}" --progress'.format(
                syncfiles_cpu, working_dir, outdir), flush=args.debug)
    
    os.remove(syncfiles_cpu)
    os.remove(syncfiles_gpu)
    
    # Remove working dir
    if not args.keepfiles:
        print('Cleaning up')
        cmd_args = '{} rm -rf {}'.format(shell, working_dir)
        retry_ssh(args.sshcmd, cmd_args)
        cmd_args = '{} rm -rf {}'.format(shell, working_dir_gpu)
        retry_ssh(args.sshcmd_gpu, cmd_args)

    # do stuff
    if args.debug:
        time_elapsed = time.time() - pipeline_start_time
        print(('Pipeline ran through in ' + str(datetime.timedelta(seconds=time_elapsed))))

        

