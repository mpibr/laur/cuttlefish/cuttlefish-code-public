#!/usr/bin/env python2
# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

from setuptools import setup, find_packages
from Cython.Build import cythonize
from distutils.extension import Extension
import numpy as np

exec(open('src/version.py').read())

try:
    import PyQt5
except ImportError:
    print('PyQt must be installed.')

try:
    import cv2
except ImportError:
    print('OpenCV must be installed.')

setup(name='chromas',
        version=__version__,
        description='Pixel-wise image segmentation',
        author='Philipp Hülsdunk',
        author_email='huelsdunk@fias.uni-frankfurt.de',
        packages=['chromas'] \
                + ['chromas.' + d for d in find_packages('./src')],
        package_dir={'chromas': 'src'},
        install_requires=['numpy', 'scikit-learn', \
                'pebble', 'h5py', 'hyperopt==0.2.5', 'pymongo', \
                'networkx', 'tqdm', 'mpi4py==3.0'],
        entry_points={
            'gui_scripts':
                ['chromas=chromas.mainwindow:main'],
            'console_scripts':
                ['chromas-classify-video=chromas.classify_video:main',
                 'chromas-play-classified=chromas.play_classified:main', \
                 'chromas-tune=chromas.find_best_classifier:main', \
                 'chromas-common-labels=chromas.common_labels:main', \
                 'chromas-generate-classifier-keras=chromas.generate_classifier_keras:main']})
