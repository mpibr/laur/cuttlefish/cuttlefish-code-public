#!/usr/bin/env python2
# coding: utf-8

# Chromas stuff
from . import feature
from . import document
from . import classifier

# Numeric
import numpy as np

# Validation
from sklearn.model_selection import cross_val_score, train_test_split

# Hyper parameter opt
from hyperopt import hp, tpe, fmin, STATUS_OK, STATUS_FAIL

# Pythonic imports
import itertools
import warnings
import argparse
import traceback

from tqdm import tqdm

def format_params(d, depth=0):
    s = ''
    for key, v in d.items():
        if isinstance(v, tuple):
            v, sub_d = v
            sub_s = format_params(sub_d, depth + 1)
        else:
            sub_s = ''
        s += 4 * depth * ' ' + '%s: %s\n' % (key, v)
        s += sub_s
    return s

def main():
    p = argparse.ArgumentParser('Optimize classification')
    p.add_argument('filename', help='Chromas file')
    p.add_argument('--hp-max-evals', type=int, default=64)
    args = p.parse_args()
    
    # Load document
    doc = document.load(args.filename)
    feature_space = feature.construct_feature_space()
    feature_space.load(doc['feature_params'])
    feature_params = feature_space.classifier_params()
    feature_strategies = feature.construct_features(feature_params)
    
    # Extract features
    f_maps = map(lambda (name, data, label): \
            feature.get_feature_maps(feature_strategies, data), \
            doc['data_labels'])

    # Pack X, y
    X = []
    y = []
    for f_map, (name, data, label) \
            in itertools.izip(f_maps, doc['data_labels']):
        X.extend(f_map[np.where(label)])
        y.extend(label[np.where(label)] - 1)
    X =  np.asarray(X)
    y =  np.asarray(y)
    X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=.5)
    
    # Load parameters
    classifier_space = classifier.construct_classifier_space()
    classifier_space.load(doc['clf_params'])

    # Progress bar
    best_clf = {'loss': 0, 'clf': None}
    pbar = tqdm(desc='Hyperparameter optimization', total=args.hp_max_evals)
    
    # Function to optimize
    def fit(params):
        '''
        Return the F1-Macro score of a cross validated fit over the parameters.
        '''
        try:
            clf = classifier.construct_classifier(params)
        except:
            print('Constructing clf failed:\n%s' % format_params(params))
            traceback.print_exc()
            return {'status': STATUS_FAIL, 'loss': 0}
        
        # Set threading
        n_jobs_params = {key: -1 for key, value in clf.get_params().items() \
                if key.endswith('n_jobs')}
        clf.set_params(**n_jobs_params)
        n_jobs = 1 if n_jobs_params else -1

        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            try:
                f1 = cross_val_score(clf, X_train, y_train, \
                        scoring='f1_macro', \
                        cv=4, \
                        n_jobs=n_jobs)
            except:
                print('Fitting clf failed:\n%s' % format_params(params))
                traceback.print_exc()
                return {'status': STATUS_FAIL, 'loss': 0}
        
        # Update progress
        if best_clf['loss'] < f1.mean():
            best_clf['loss'] = f1.mean()
            best_clf['clf'] = params
            pbar.write('Best score: %.3f\n%s' % \
                    (best_clf['loss'], format_params(best_clf['clf'])))
        pbar.update(1)

        return {'status': STATUS_OK, \
                'loss': -f1.mean(), 'loss_variance': f1.var()}
    
    best = fmin(fit, space=classifier_space.hp_space(), \
            algo=tpe.suggest, max_evals=args.hp_max_evals)
    
    # Close progress bar
    pbar.close()
    
    # Save classifier parameters
    classifier_space.load_values_hp_fmin(best)
    doc['clf_params'] = {key: value for key, value in classifier_space.dump().items()
                         if value is not None}
    
    # Save document
    document.dump(args.filename, doc)

