# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

import cv2
import numpy as np
import math

class Identity:

    name = 'Original'

    def apply(self, image):
        return image

class Gray:

    name = 'Gray'

    def apply(self, image):
        return cv2.cvtColor(cv2.cvtColor(image, cv2.COLOR_RGB2GRAY), \
                cv2.COLOR_GRAY2RGB)

class LUV:

    name = 'LUV'

    def apply(self, image):
        return cv2.cvtColor(image, cv2.COLOR_RGB2LUV)

class Red:

    name = 'Red channel'

    def apply(self, image):
        out = image.copy()
        out[..., 1] = out[..., 0]
        out[..., 2] = out[..., 0]
        return out

class Green:

    name = 'Green channel'

    def apply(self, image):
        out = image.copy()
        out[..., 0] = out[..., 1]
        out[..., 2] = out[..., 1]
        return out

class Blue:

    name = 'Blue channel'

    def apply(self, image):
        out = image.copy()
        out[..., 0] = out[..., 2]
        out[..., 1] = out[..., 2]
        return out

class RemoveLumi:
     
    name = 'Remove luminiscence'

    def apply(self, image):
        image = cv2.cvtColor(image, cv2.COLOR_RGB2YCR_CB)
        image[..., 0] = 127
        return cv2.cvtColor(image, cv2.COLOR_YCR_CB2RGB)

class StretchLumi:
     
    name = 'Stretch luminiscence'

    def apply(self, image):
        image = cv2.cvtColor(image, cv2.COLOR_RGB2YCR_CB)
        image[..., 0] = cv2.equalizeHist(image[..., 0])
        return cv2.cvtColor(image, cv2.COLOR_YCR_CB2RGB)

class Retinex:
    
    name = 'Retinex'

    def __init__(self, dynamic=4., min_sigma=5., max_sigma=10., \
            sigma_ratio=math.sqrt(2)):
        self.dynamic = dynamic
        self.min_sigma = min_sigma
        self.max_sigma = max_sigma
        self.sigma_ratio = sigma_ratio
        self.__num_sigmas = int(math.log(self.max_sigma / self.min_sigma) \
                / math.log(self.sigma_ratio) + 1)
        self.__sigmas = [self.min_sigma * self.sigma_ratio ** sigma_idx
                for sigma_idx in range(self.__num_sigmas)]

    def apply(self, image):
        image = image.astype('float32') + 1

        # Preset parameters
        weights = np.empty_like((len(self.__sigmas),), 'float32')
        weights[:] = 1. / len(self.__sigmas)
        gain = 1
        alpha = 128
        offset = 0
        
        # Apply multi scale retinex
        gaussians = np.array([cv2.GaussianBlur(image, (0,0), sigma) \
                for sigma in self.__sigmas])
        msr = np.sum(weights * (np.log(image)[None] - np.log(gaussians)), \
                axis=0)
        logl = np.log(np.sum(image, axis=2))
        output = gain * (np.log(alpha * image) - logl[:, :, None]) \
                * msr + offset
        
        # Normalize using mean and std
        mean = np.mean(output)
        std = np.std(output)
        vmin = mean - self.dynamic * std
        vmax = mean + self.dynamic * std
        r = vmax - vmin
        output = (output - vmin) / r
        output[np.where(output < 0)] = 0
        output[np.where(output > 1)] = 1
        return (255 * output).astype('uint8')

class HistEq:

    name = 'Histogram Equalization'

    def __init__(self):
        pass

    def apply(self, image):
        return np.dstack((cv2.equalizeHist(image[..., ch]) \
                for ch in range(image.shape[-1])))

class ContrastBrightness:

    name = 'Contrast and brightness'

    def __init__(self, contrast=1., brightness=0.):
        self.contrast = contrast
        self.brightness = brightness

    def apply(self, image):
        output = self.contrast * image + self.brightness
        return np.clip(output, 0, 255).astype('uint8')

class Normalize:

    name = 'Normalize'

    def __init__(self, std=1.):
        self.__std = std

    def apply(self, image):
        output = (image - np.mean(image, axis=(0,1))) \
                / self.__std / np.std(image, axis=(0,1))
        return np.clip(255 * output, 0, 255).astype('uint8')

