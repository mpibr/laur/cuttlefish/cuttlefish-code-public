# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from data_scene import DataScene
from image_graphics_item import ImageGraphicsItem

import numpy as np

class LabelsScene(DataScene):
    '''
    Scene that enables visualization of labels
    '''

    def __init__(self, labels_model, parent):
        DataScene.__init__(self, parent)

        self._labels_model = labels_model
        self._labels_model.rowsInserted.connect(self._labelsInserted)
        self._labels_model.rowsRemoved.connect(self._labelsRemoved)
        self._labels_model.colourChanged.connect(self._labelsColourChanged)

        self._labels = np.empty((0, 0), 'uint8') # User labels

        self._labels_item = ImageGraphicsItem()
        self.addItem(self._labels_item)

    def labelsModel(self):
        return self._labels_model
    
    def labels(self):
        return self._labels

    def setLabels(self, labels):
        self._labels = labels

        self.repaintLabels()

    def repaintLabels(self):
        q_image = QImage(self._labels.data,
                self._labels.shape[1], self._labels.shape[0],
                self._labels.strides[0],
                QImage.Format_Indexed8)
        color_table = [QColor(0, 0, 0, 0).rgba()] \
                + map(lambda row: \
                    QColor(*self._labels_model.colour(row)).rgba(), \
                range(self._labels_model.rowCount()))
        q_image.setColorTable(color_table)
        self._labels_item.setImage(q_image)

    def _labelsInserted(self, parent, start, end):
        self.repaintLabels()

    def _labelsRemoved(self, parent, start, end):
        self.repaintLabels()
    
    def _labelsColourChanged(self, row):
        self.repaintLabels()

