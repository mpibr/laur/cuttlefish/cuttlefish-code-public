# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

# GUI
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from labels_scene import LabelsScene

# Numeric tools
import cv2
import numpy as np

import tempfile
import time

class LabelsModifiedCommand(QUndoCommand):

    NUM_SEC_AFTER_IGNORE_MERGE = 5

    def __init__(self, text, data_index, previous_labels, \
            bounds, mask, label, scene, \
            parent=None):
        QUndoCommand.__init__(self, text, parent)
        
        self._data_index = data_index
        self._previous_labels = previous_labels.copy()
        self._bounds = bounds
        self._mask = mask
        self._label = label
        self._scene = scene
        self._timestamp = time.time()

    def id(self):
        return hash(LabelsModifiedCommand)

    def mergeWith(self, command):
        if not isinstance(command, LabelsModifiedCommand):
            return False
        if self._data_index != command._data_index:
            return False
        if self._label != command._label:
            return False
        if abs(command._timestamp - self._timestamp) \
                > self.NUM_SEC_AFTER_IGNORE_MERGE:
            return False
        
        ymin0, xmin0, ymax0, xmax0 = self._bounds
        ymin1, xmin1, ymax1, xmax1 = command._bounds
        ymin = min(ymin0, ymin1)
        xmin = min(xmin0, xmin1)
        ymax = max(ymax0, ymax1)
        xmax = max(xmax0, xmax1)

        mask = np.zeros((ymax - ymin, xmax - xmin), 'bool')
        mask[ymin0 - ymin : ymax0 - ymin, xmin0 - xmin : xmax0 - xmin] |= \
                self._mask
        mask[ymin1 - ymin : ymax1 - ymin, xmin1 - xmin : xmax1 - xmin] |= \
                command._mask

        self._bounds = ymin, xmin, ymax, xmax
        self._mask = mask

        return True

    def redo(self):
        self._scene._data_selection.select(self._data_index, \
                QItemSelectionModel.SelectCurrent)
        
        ymin, xmin, ymax, xmax = self._bounds
        self._scene._labels[ymin : ymax, xmin : xmax][self._mask] = self._label
        self._scene.repaintLabels()
        self._scene.labelsChanged.emit()

    def undo(self):
        self._scene._data_selection.select(self._data_index, \
                QItemSelectionModel.SelectCurrent)

        self._scene._labels[:] = self._previous_labels
        self._scene.repaintLabels()
        self._scene.labelsChanged.emit()

class EditableLabelsScene(LabelsScene):
    '''
    LabelsScene which enables editing of labels
    '''

    labelsChanged = pyqtSignal()

    def __init__(self, undo_stack, labels_model, data_selection, parent):
        LabelsScene.__init__(self, labels_model, parent)

        self.setItemIndexMethod(QGraphicsScene.NoIndex)
        
        self._undo_stack = undo_stack
        self._data_selection = data_selection
        self._editing_label = -1 # Current edited label
        self._tool_radius = 0.
        self._mouse_press_button = Qt.NoButton

    def setToolRadius(self, tool_radius):
        self._tool_radius = tool_radius
        
    def setEditingLabel(self, label_num):
        self._editing_label = label_num

    def mousePressEvent(self, event):
        self._mouse_press_button = event.button()

        if event.button() in [Qt.LeftButton, Qt.RightButton] \
                and self._editing_label >= 0:

            pos = event.scenePos()
            tuple_int_pos = (int(pos.x()), int(pos.y()))
            label = self._editing_label + 1
            data_index = self._data_selection.selectedIndexes()[0]
            
            # Generate change mask
            ymin = max(tuple_int_pos[1] - int(self._tool_radius), \
                    0)
            xmin = max(tuple_int_pos[0] - int(self._tool_radius), \
                    0)
            ymax = min(tuple_int_pos[1] + int(self._tool_radius) + 1, \
                    self._labels.shape[0])
            xmax = min(tuple_int_pos[0] + int(self._tool_radius) + 1, \
                    self._labels.shape[1])

            # Check if we draw outside of the canvas
            if ymin >= self._labels.shape[0] or ymax < 0 \
                    or xmin >= self._labels.shape[1] or xmax < 0:
                QGraphicsScene.mousePressEvent(self, event)
                return
            
            bounds = ymin, xmin, ymax, xmax
            mask = np.zeros((ymax - ymin,  xmax - xmin), 'uint8')
            cv2.circle(mask, \
                    (tuple_int_pos[0] - xmin, tuple_int_pos[1] - ymin), \
                    int(self._tool_radius), 255, -1)
            
            # Modify mask based on the shift modifier => only allow current
            # label to be edited, then push the change command
            if (event.button() == Qt.LeftButton and
                bool(event.modifiers() & Qt.AltModifier) == False or
                event.button() == Qt.RightButton and
                bool(event.modifiers() & Qt.AltModifier) == True) and \
                    bool(event.modifiers() & Qt.ShiftModifier) == False:
                command = LabelsModifiedCommand("Label added", data_index, \
                        self._labels, bounds, mask.astype('bool'), label, self)
                self._undo_stack.push(command)

            elif (event.button() == Qt.RightButton and
                bool(event.modifiers() & Qt.AltModifier) == False or
                event.button() == Qt.LeftButton and
                bool(event.modifiers() & Qt.AltModifier) == True) and \
                    bool(event.modifiers() & Qt.ShiftModifier) == False:
                command = LabelsModifiedCommand("Label erased", data_index, \
                        self._labels, bounds, mask.astype('bool'), 0, self)
                self._undo_stack.push(command)

            elif (event.button() == Qt.LeftButton and
                bool(event.modifiers() & Qt.AltModifier) == False or
                event.button() == Qt.RightButton and
                bool(event.modifiers() & Qt.AltModifier) == True) and \
                    bool(event.modifiers() & Qt.ShiftModifier) == True:
                mask[np.where(self._labels[ymin : ymax, xmin : xmax] \
                        != 0)] = 0

                command = LabelsModifiedCommand("Label added", data_index, \
                        self._labels, bounds, mask.astype('bool'), label, self)
                self._undo_stack.push(command)

            elif (event.button() == Qt.RightButton and
                bool(event.modifiers() & Qt.AltModifier) == False or
                event.button() == Qt.LeftButton and
                bool(event.modifiers() & Qt.AltModifier) == True) and \
                    bool(event.modifiers() & Qt.ShiftModifier) == True:
                mask[np.where(self._labels[ymin : ymax, xmin : xmax] \
                        != label)] = 0
                
                command = LabelsModifiedCommand("Label erased", data_index, \
                        self._labels, bounds, mask.astype('bool'), 0, self)
                self._undo_stack.push(command)

        QGraphicsScene.mousePressEvent(self, event)

    def mouseMoveEvent(self, event):
        if self._mouse_press_button in [Qt.LeftButton, Qt.RightButton] and \
                self._editing_label >= 0:

            lastPos = event.lastScenePos()
            pos = event.scenePos()
            last_tuple_int_pos = (int(lastPos.x()), int(lastPos.y()))
            tuple_int_pos = (int(pos.x()), int(pos.y()))
            label = self._editing_label + 1
            data_index = self._data_selection.selectedIndexes()[0]

            # Generate change mask
            ymin = max(min(last_tuple_int_pos[1], tuple_int_pos[1]) \
                    - int(self._tool_radius), \
                    0)
            xmin = max(min(last_tuple_int_pos[0], tuple_int_pos[0]) \
                    - int(self._tool_radius), \
                    0)
            ymax = min(max(last_tuple_int_pos[1], tuple_int_pos[1]) \
                    + int(self._tool_radius) + 1, \
                    self._labels.shape[0])
            xmax = min(max(last_tuple_int_pos[0], tuple_int_pos[0]) \
                    + int(self._tool_radius) + 1, \
                    self._labels.shape[1])

            # Check if we draw outside of the canvas
            if ymin >= self._labels.shape[0] or ymax < 0 \
                    or xmin >= self._labels.shape[1] or xmax < 0:
                QGraphicsScene.mouseMoveEvent(self, event)
                return

            bounds = ymin, xmin, ymax, xmax
            mask = np.zeros((ymax - ymin,  xmax - xmin), 'uint8')
            cv2.line(mask, \
                    (tuple_int_pos[0] - xmin, tuple_int_pos[1] - ymin), \
                    (last_tuple_int_pos[0] - xmin, last_tuple_int_pos[1] - ymin), \
                    255, int(self._tool_radius * 2))
            
            # Modify mask based on the shift modifier => only allow current
            # label to be edited, then push the change command
            if (self._mouse_press_button == Qt.LeftButton and
                bool(event.modifiers() & Qt.AltModifier) == False or
                self._mouse_press_button == Qt.RightButton and
                bool(event.modifiers() & Qt.AltModifier) == True) and \
                    bool(event.modifiers() & Qt.ShiftModifier) == False:
                command = LabelsModifiedCommand("Label added", data_index, \
                        self._labels, bounds, mask.astype('bool'), label, self)
                self._undo_stack.push(command)

            elif (self._mouse_press_button == Qt.RightButton and
                bool(event.modifiers() & Qt.AltModifier) == False or
                self._mouse_press_button == Qt.LeftButton and
                bool(event.modifiers() & Qt.AltModifier) == True) and \
                    bool(event.modifiers() & Qt.ShiftModifier) == False:
                command = LabelsModifiedCommand("Label erased", data_index, \
                        self._labels, bounds, mask.astype('bool'), 0, self)
                self._undo_stack.push(command)

            elif (self._mouse_press_button == Qt.LeftButton and
                bool(event.modifiers() & Qt.AltModifier) == False or
                self._mouse_press_button == Qt.RightButton and
                bool(event.modifiers() & Qt.AltModifier) == True) and \
                    bool(event.modifiers() & Qt.ShiftModifier) == True:
                mask[np.where(self._labels[ymin : ymax, xmin : xmax] \
                        != 0)] = 0

                command = LabelsModifiedCommand("Label added", data_index, \
                        self._labels, bounds, mask.astype('bool'), label, self)
                self._undo_stack.push(command)

            elif (self._mouse_press_button == Qt.RightButton and
                bool(event.modifiers() & Qt.AltModifier) == False or
                self._mouse_press_button == Qt.LeftButton and
                bool(event.modifiers() & Qt.AltModifier) == True) and \
                    bool(event.modifiers() & Qt.ShiftModifier) == True:
                mask[np.where(self._labels[ymin : ymax, xmin : xmax] \
                        != label)] = 0
                
                command = LabelsModifiedCommand("Label erased", data_index, \
                        self._labels, bounds, mask.astype('bool'), 0, self)
                self._undo_stack.push(command)

        QGraphicsScene.mouseMoveEvent(self, event)
    
    def mouseReleaseEvent(self, event):
        if self._mouse_press_button in [Qt.LeftButton, Qt.RightButton]:
            self._mouse_press_button = Qt.NoButton
            self.labelsChanged.emit()

        QGraphicsScene.mouseReleaseEvent(self, event)

