#!/usr/bin/env python2
# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import math
from hyperopt import hp

class Space(object):

    def __init__(self, parent=None):
        self._parent = parent

    def parent(self):
        return self._parent

    def set_parent(self, parent):
        self._parent = parent

    def children_count(self):
        return 0

    def children(self):
        return []

    def classifier_params(self):
        pass
    
    def dump(self):
        pass

    def load(self, values_dict):
        pass
    
    def hp_space(self):
        pass

    def load_values_hp_fmin(self, hp_fmin_res):
        pass
    
class RootSpace(Space):

    def __init__(self):
        Space.__init__(self, None)
        self._child_nodes = []

    def add_node(self, key, node):
        node.set_parent(self)
        self._child_nodes.append((key, node))
        return node

    def children_count(self):
        return len(self._child_nodes)

    def children(self):
        return [node for key, node in self._child_nodes]
    
    def classifier_params(self):
        return {key: node.classifier_params() \
                for key, node in self._child_nodes}
    
    def dump(self):
        return {value_key: value \
                for key, node in self._child_nodes \
                for value_key, value in node.dump().items()}
    
    def load(self, values_dict):
        for key, node in self._child_nodes:
            node.load(values_dict)
    
    def hp_space(self):
        return {key: node.hp_space() for key, node in self._child_nodes}
    
    def load_values_hp_fmin(self, hp_fmin_res):
        for key, node in self._child_nodes:
            node.load_values_hp_fmin(hp_fmin_res)

class ValuedSpace(Space):

    def __init__(self, name, text, value):
        Space.__init__(self)
        self._name = name
        self._text = text
        self._value = value

    def name(self):
        return self._name

    def text(self):
        return self._text

    def value(self):
        return self._value

    def set_value(self, value):
        self._value = value

    def classifier_params(self):
        return self._value
    
    def dump(self):
        return {self._name: self._value}
    
    def load(self, values_dict):
        self._value = values_dict[self._name] \
                if self._name in values_dict else \
                None
    
class ChoiceSpace(ValuedSpace):

    def __init__(self, name, text, choices, value=None):
        ValuedSpace.__init__(self, name, text, value)
        self._choices = [choice for choice, text in choices]
        self._choice_to_text = {choice: text for choice, text in choices}

        self._child_nodes = {choice: [] for choice, text in choices}

    def add_node(self, choice, key, node):
        node.set_parent(self)
        self._child_nodes[choice].append((key, node))
        return node

    def children_count(self):
        return 0 if self.value() is None \
                else len(self._child_nodes[self.value()])

    def children(self):
        return [] if self.value() is None else \
                [node for key, node in self._child_nodes[self.value()]]
    
    def classifier_params(self):
        return (None, {}) if self.value() is None else \
            (self.value(), {key: node.classifier_params() \
                for key, node in self._child_nodes[self.value()]})
    
    def dump(self):
        d0 = ValuedSpace.dump(self)
        d1 = {value_key: value for choice, nodes in self._child_nodes.items() \
                for key, node in nodes \
                for value_key, value in node.dump().items()}
        return {value_key: value \
                for value_key, value in d0.items() + d1.items()}
    
    def load(self, values_dict):
        ValuedSpace.load(self, values_dict)
        for choice, nodes in self._child_nodes.items():
            for key, node in nodes:
                node.load(values_dict)
    
    def hp_space(self):
        return hp.choice(self.name(), \
                [(choice, {key: node.hp_space() \
                           for key, node in self._child_nodes[choice]}) \
                for choice in self._choices])

    def load_values_hp_fmin(self, hp_fmin_res):
        self.set_value(self._choices[hp_fmin_res[self.name()]])
        for key, node in self._child_nodes[self.value()]:
            node.load_values_hp_fmin(hp_fmin_res)
    
    def createEditor(self, parent, option):
        editor = QComboBox(parent)
        editor.addItems(map(self._choice_to_text.__getitem__, self._choices))
        return editor

    def setEditorData(self, editor):
        current_index = -1 if self.value() is None else \
                self._choices.index(self.value())
        editor.setCurrentIndex(current_index)

    def getEditorData(self, editor):
        return None if editor.currentIndex() == -1 else \
                self._choices[editor.currentIndex()]

    def paint(self, painter, option):
        if self.value() is None:
            option.text = 'Default'
            option.font.setItalic(True)
        else:
            option.text = self._choice_to_text[self.value()]

        QApplication.style().drawControl( \
                QStyle.CE_ItemViewItem, option, painter)

class BoolSpace(ValuedSpace):

    def __init__(self, name, text, value=None):
        ValuedSpace.__init__(self, name, text, value)
        
        self._child_nodes = {True: [], False: []}
    
    def add_node(self, choice, key, node):
        node.set_parent(self)
        self._child_nodes[choice].append((key, node))
        return node

    def children_count(self):
        return 0 if self.value() is None else \
                len(self._child_nodes[self.value()])

    def children(self):
        return [] if self.value() is None else \
                [node for key, node in self._child_nodes[self.value()]]
    
    def classifier_params(self):
        return (None, {}) if self.value() is None else \
                (self.value(), {key: node.classifier_params() \
                    for key, node in self._child_nodes[self.value()]})
    
    def dump(self):
        d0 = ValuedSpace.dump(self)
        d1 = {value_key: value for choice, nodes in self._child_nodes.items() \
                for key, node in nodes \
                for value_key, value in node.dump().items()}
        return {value_key: value \
                for value_key, value in d0.items() + d1.items()}
    
    def load(self, values_dict):
        ValuedSpace.load(self, values_dict)
        for choice, nodes in self._child_nodes.items():
            for key, node in nodes:
                node.load(values_dict)
    
    def hp_space(self):
        return hp.choice(self.name(), \
                [(choice, {key: node.hp_space() \
                           for key, node in self._child_nodes[choice]}) \
                for choice in [True, False]])

    def load_values_hp_fmin(self, hp_fmin_res):
        self._value = [True, False][hp_fmin_res[self.name()]]
    
    def createEditor(self, parent, option):
        editor = QCheckBox(parent)
        return editor

    def setEditorData(self, editor):
        state = Qt.Checked if self.value() else Qt.Unchecked
        editor.setCheckState(state)

    def getEditorData(self, editor):
        state = editor.checkState()
        return True if state == Qt.Checked else \
                False if state == Qt.Unchecked else None

    def paint(self, painter, option):
        rect = option.rect
        option = QStyleOptionButton()
        option.rect = rect

        if self.value() is None:
            option.state = QStyle.State_None
        else:
            option.state = QStyle.State_On if self.value() \
                    else QStyle.State_Off

        QApplication.style().drawControl( \
                QStyle.CE_CheckBox, option, painter)
    
class UniformIntSpace(ValuedSpace):

    def __init__(self, name, text, low, high, value=None):
        ValuedSpace.__init__(self, name, text, value)
        self._low = low
        self._high = high

    def low(self):
        return self._low

    def high(self):
        return self._high
    
    def hp_space(self):
        return hp.quniform(self.name(), self._low, self._high, 1)
    
    def load_values_hp_fmin(self, hp_fmin_res):
        self._value = int(hp_fmin_res[self.name()])
    
    def createEditor(self, parent, option):
        editor = QSpinBox(parent)
        editor.setRange(self._low, self._high)
        return editor

    def setEditorData(self, editor):
        if self.value() is not None:
            editor.setValue(self.value())

    def getEditorData(self, editor):
        return editor.value()

    def paint(self, painter, option):
        if self.value() is None:
            option.text = 'Default'
            option.font.setItalic(True)
        else:
            option.text = str(self.value())

        QApplication.style().drawControl( \
                QStyle.CE_ItemViewItem, option, painter)
    
class UniformFloatSpace(ValuedSpace):

    def __init__(self, name, text, low, high, value=None):
        ValuedSpace.__init__(self, name, text, value)
        self._low = float(low)
        self._high = float(high)

    def hp_space(self):
        return hp.uniform(self.name(), self._low, self._high)

    def load_values_hp_fmin(self, hp_fmin_res):
        self._value = float(hp_fmin_res[self._name])

    def low(self):
        return self._low

    def high(self):
        return self._high

    def createEditor(self, parent, option):
        editor = QDoubleSpinBox(parent)
        editor.setRange(self._low, self._high)
        editor.setSingleStep((self._high - self._low) / 100)
        editor.setDecimals(2 - int(math.log10(self._high - self._low) + .5))
        return editor

    def setEditorData(self, editor):
        if self.value() is not None:
            editor.setValue(self.value())

    def getEditorData(self, editor):
        return editor.value()

    def paint(self, painter, option):
        if self.value() is None:
            option.text = 'Default'
            option.font.setItalic(True)
        else:
            option.text = str(self.value())

        QApplication.style().drawControl( \
                QStyle.CE_ItemViewItem, option, painter)

class ParameterModel(QAbstractItemModel):
    
    def __init__(self, root_node, parent=None):
        QAbstractTableModel.__init__(self, parent)
        
        self._root = root_node

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            if section == 0:
                return QVariant("Name")
            elif section == 1:
                return QVariant("Value")
        return QVariant()

    def index(self, row, column, parent=QModelIndex()):
        if not self.hasIndex(row, column, parent):
            return QModelIndex()

        parent_item = self.node(parent)
        child_item = parent_item.children()[row]
        return self.createIndex(row, column, child_item) \
                if child_item is not None \
                else QModelIndex()

    def parent(self, index):
        parent_item = self.node(index).parent()
        if parent_item == self._root:
            return QModelIndex()
        grandparent_item = parent_item.parent()
        row = 0 if grandparent_item is None else \
            grandparent_item.children().index(parent_item)
        return self.createIndex(row, 0, parent_item)
    
    def node(self, index):
        return index.internalPointer() if index.isValid() else self._root
    
    def columnCount(self, parent=QModelIndex()):
        return 2

    def rowCount(self, parent=QModelIndex()):
        if parent.column() > 0:
            return 0

        parent_item = self.node(parent)
        return parent_item.children_count()

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return QVariant()

        item = self.node(index)
        if index.column() == 0 and role == Qt.DisplayRole:
            return QVariant(item.text())
        elif index.column() == 1 and role == Qt.DisplayRole:
            return QVariant(item.value())
        else:
            return QVariant()

    def setData(self, index, value):
        if index.column() == 1:
            item = self.node(index)
            
            parent_item = item.parent()
            node_index = parent_item.children().index(item)
            index_new = self.createIndex(node_index, 0, parent_item)

            self.beginRemoveRows(index_new, 0, item.children_count() - 1)
            item.set_value(None)
            self.endRemoveRows()
            
            item.set_value(value)
            child_count = item.children_count()
            item.set_value(None)

            self.beginInsertRows(index_new, 0, child_count - 1)
            item.set_value(value)
            self.endInsertRows()

            self.dataChanged.emit(index, index)
    
    def flags(self, index):
        if not index.isValid():
            return Qt.NoItemFlags
        elif index.column() == 1:
            return Qt.ItemIsEnabled | Qt.ItemIsEditable
        else:
            return Qt.ItemIsSelectable | Qt.ItemIsEnabled

    def classifier_params(self):
        return self._root.classifier_params()

    def dump(self):
        return self._root.dump()

    def load(self, values_dict):
        self.beginResetModel()
        self._root.load(values_dict)
        self.endResetModel()
    
