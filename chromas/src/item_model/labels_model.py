# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

class ChangeLabelNameCommand(QUndoCommand):

    def __init__(self, labels_model, row, new_label_name, parent=None):
        QUndoCommand.__init__(self, "Change label name", parent)
        
        self._labels_model = labels_model
        self._row = row
        self._old_label_name = self._labels_model.name(row)
        self._new_label_name = new_label_name

    def redo(self):
        self._labels_model.setName(self._row, self._new_label_name)

    def undo(self):
        self._labels_model.setName(self._row, self._old_label_name)

class ChangeLabelColourCommand(QUndoCommand):

    def __init__(self, labels_model, row, new_label_colour, parent=None):
        QUndoCommand.__init__(self, "Change label colour", parent)
        
        self._labels_model = labels_model
        self._row = row
        self._old_label_colour = self._labels_model.colour(row)
        self._new_label_colour = new_label_colour

    def redo(self):
        self._labels_model.setColour(self._row, self._new_label_colour)

    def undo(self):
        self._labels_model.setColour(self._row, self._old_label_colour)

class LabelsModel(QAbstractTableModel):

    colourChanged = pyqtSignal([int])

    def __init__(self, undo_stack, parent=None):
        QAbstractTableModel.__init__(self, parent)
        
        self._undo_stack = undo_stack
        self._data = []

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            if section == 0:
                return QVariant("Name")
            elif section == 1:
                return QVariant("Colour")
        else:
            return QVariant()

    def rowCount(self, parent=QModelIndex()):
        return len(self._data)

    def columnCount(self, parent=QModelIndex()):
        return 2

    def data(self, index, role=Qt.DisplayRole):
        if role == Qt.DisplayRole or role == Qt.EditRole:
            if index.column() == 0:
                return QVariant(self._data[index.row()][0])
            elif index.column() == 1:
                r, g, b = self._data[index.row()][1]
                return QVariant(QColor(r, g, b))
        else:
            return QVariant()

    def setData(self, index, value, role=Qt.EditRole):
        if role == Qt.EditRole:
            if index.column() == 0:
                
                command = ChangeLabelNameCommand( \
                        self, index.row(), value)
                self._undo_stack.push(command)
                return True
            elif index.column() == 1:
                colour = QColor(value)
                r, g, b = colour.red(), colour.green(), colour.blue()
                command = ChangeLabelColourCommand( \
                        self, index.row(), (r, g, b))
                self._undo_stack.push(command)
                return True
        return False
    
    def flags(self, index):
        if index.column() == 0:
            return Qt.ItemIsSelectable | Qt.ItemIsEditable | Qt.ItemIsEnabled
        elif index.column() == 1:
            return Qt.ItemIsEditable | Qt.ItemIsEnabled
    
    def insertRows(self, row, count, parent=QModelIndex()):
        self.beginInsertRows(parent, row, row + count - 1)
        self._data = self._data[:row] + count * [["", (0, 0, 0)]] \
                + self._data[row:]
        self.endInsertRows()

    def removeRows(self, row, count, parent=QModelIndex()):
        self.beginRemoveRows(parent, row, row + count - 1)
        self._data = self._data[:row] + self._data[row + count:]
        self.endRemoveRows()

    def append(self, name, colour):
        self.beginInsertRows(QModelIndex(), len(self._data), len(self._data))
        self._data.append([name, colour])
        self.endInsertRows()
        QAbstractTableModel.insertRows(self, len(self._data), 1, QModelIndex())

    def insert(self, row, name, colour):
        self.beginInsertRows(QModelIndex(), row, row)
        self._data = self._data[:row] + [[name, colour]] \
                + self._data[row:]
        self.endInsertRows()

    def clear(self):
        if len(self._data) > 0:
            self.beginRemoveRows(QModelIndex(), 0, len(self._data) - 1)
            self._data = []
            self.endRemoveRows()
    
    def pop(self):
        self.beginRemoveRows(QModelIndex(), len(self._data) - 1, len(self._data) - 1)
        self._data.pop()
        self.endRemoveRows()

    def name(self, row):
        return self._data[row][0]

    def setName(self, row, name):
        self._data[row][0] = name
        index = self.index(0, row)
        self.dataChanged.emit(index, index)
    
    def colour(self, row):
        return self._data[row][1]

    def setColour(self, row, colour):
        self._data[row][1] = colour
        index = self.index(1, row)
        self.colourChanged.emit(row)
        self.dataChanged.emit(index, index)

