# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import cv2
import numpy as np

class ChangeDataNameCommand(QUndoCommand):

    def __init__(self, data_model, row, new_data_name, parent=None):
        QUndoCommand.__init__(self, "Change data name", parent)
        
        self._data_model = data_model
        self._row = row
        self._old_data_name = self._data_model.name(row)
        self._new_data_name = new_data_name

    def redo(self):
        self._data_model.setName(self._row, self._new_data_name)

    def undo(self):
        self._data_model.setName(self._row, self._old_data_name)

class DataModel(QAbstractTableModel):

    featuresChanged = pyqtSignal([int])

    def __init__(self, undo_stack, labels_model, parent=None):
        QAbstractTableModel.__init__(self, parent)
        
        self._undo_stack = undo_stack
        self._labels_model = labels_model
        self._labels_model.rowsInserted.connect(self._labelsInserted)
        self._labels_model.rowsRemoved.connect(self._labelsRemoved)
        self._document = []
        self._preview_width = 64

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            if section == 0:
                return QVariant("Data")
            else:
                return QVariant()
        else:
            return QVariant()

    def rowCount(self, parent=QModelIndex()):
        return len(self._document)

    def columnCount(self, parent=QModelIndex()):
        return 1

    def setData(self, index, value, role=Qt.EditRole):
        if role == Qt.EditRole:
            if index.column() == 0:
                command = ChangeDataNameCommand( \
                        self, index.row(), value.toString())
                self._undo_stack.push(command)
                return True
        return False

    def data(self, index, role=Qt.DisplayRole):
        if role == Qt.DisplayRole or role == Qt.EditRole:
            if index.column() == 0:
                return QVariant(self._document[index.row()][0])
            else:
                return QVariant()
        elif role == Qt.DecorationRole:
            if index.column() == 0:
                return QVariant(self._document[index.row()][2])
            else:
                return QVariant()
        else:
            return QVariant()
    
    def removeRows(self, row, count, parent=QModelIndex()):
        self.beginRemoveRows(parent, row, row + count - 1)
        map(lambda fm: fm.cancel(),
            (data_row[4] for data_row in self._document[row : row + count] \
             if data_row[4] is not None))
        self._document = self._document[:row] + self._document[row + count:]
        self.endRemoveRows()

    def insertRows(self, row, count, parent=QModelIndex()):
        self.beginInsertRows(parent, row, row + count - 1)
        self._document = self._document[:row] + count * [["", (0, 0, 0)]] \
                + self._document[row:]
        self.endInsertRows()
    
    def flags(self, index):
        if index.column() == 0:
            return Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsEditable
    
    def name(self, row):
        return self._document[row][0]
    
    def setName(self, row, name):
        self._document[row][0] = name
        index = self.index(0, row)
        self.dataChanged.emit(index, index)

    def image(self, row):
        return self._document[row][1]

    def labels(self, row):
        return self._document[row][3]
    
    def featureMaps(self, row):
        return self._document[row][4]

    def setFeatureMaps(self, row, X_f):
        if self._document[row][4] is not None:
            self._document[row][4].cancel()
        self._document[row][4] = X_f
        self.featuresChanged.emit(row)
    
    def clear(self):
        if len(self._document) > 0:
            self.beginRemoveRows(QModelIndex(), 0, len(self._document) - 1)
            map(lambda fm: fm.cancel(),
                (data_row[4] for data_row in self._document \
                 if data_row[4] is not None))
            self._document = []
            self.endRemoveRows()

    def append(self, name, image, labels, X_f):
        image = image
        labels = labels
        preview_height = int(float(self._preview_width) \
                * image.shape[0] / image.shape[1])
        preview = cv2.resize(image, (self._preview_width, preview_height))
        preview_image = QImage(preview.data, \
                preview.shape[1], preview.shape[0], \
                QImage.Format_RGB888)
        preview_pixmap = QPixmap.fromImage(preview_image)

        self.beginInsertRows(QModelIndex(), len(self._document), \
                len(self._document))
        self._document += [[name, image, preview_pixmap, labels, X_f]]
        self.endInsertRows()
    
    def insert(self, row, name, image, labels, X_f):
        image = image
        labels = labels
        preview_height = int(float(self._preview_width) \
                * image.shape[0] / image.shape[1])
        preview = cv2.resize(image, (self._preview_width, preview_height))
        preview_image = QImage(preview.data, \
                preview.shape[1], preview.shape[0], \
                QImage.Format_RGB888)
        preview_pixmap = QPixmap.fromImage(preview_image)

        self.beginInsertRows(QModelIndex(), row, row)
        self._document = self._document[:row] \
                + [[name, image, preview_pixmap, labels, X_f]] \
                + self._document[row:]
        self.endInsertRows()
    
    def pop(self):
        self.beginRemoveRows(QModelIndex(), len(self._document) - 1, \
                len(self._document) - 1)
        if self._document[-1][4] is not None:
            self._document[-1][4].cancel()
        self._document.pop()
        self.endRemoveRows()
    
    def _labelsInserted(self, parent, start, end):
        for row, (_, _, _, labels, _) in enumerate(self._document):
            where = np.where(labels > end)
            labels[where] += end - start + 1

    def _labelsRemoved(self, parent, start, end):
        for row, (_, _, _, labels, _) in enumerate(self._document):
            where = np.where((start <= labels - 1) \
                    & (labels - 1 <= end))
            labels[where] = 0
            where = np.where(labels > end)
            labels[where] -= end - start + 1
    
