# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import copy

class ImageEnhancerNode:

    def __init__(self, image_enhancer = None, parent=None):
        self._parent = parent
        self._image_enhancer = image_enhancer
        self._children = []

    def image_enhancer(self):
        return self._image_enhancer
    
    def set_image_enhancer(self, image_enhancer):
        self._image_enhancer = image_enhancer

    def rowCount(self):
        return len(self._children)

    def parent(self):
        return self._parent

    def child(self, row):
        return self._children[row]
    
    def row(self):
        if self._parent is None:
            return 0
        return self._parent._children.index(self)
    
    def insertRows(row, count):
        self._children = self._children[:row] \
                + [ImageEnhancerNode(None, self) for _ in xrange(count)] \
                + self._children[row:]

    def insert(self, row, node):
        node._parent = self
        self._children = self._children[:row] \
                + [node] \
                + self._children[row:]

    def append(self, node):
        node._parent = self
        self._children.append(node)

    def removeRows(self, row, count):
        self._children = self._children[:row] \
                + self._children[row + count:]

    def clear(self):
        self._children = []

    def pop(self):
        self._children.pop()

class ImageEnhancersModel(QAbstractItemModel):
    
    def __init__(self, parent=None):
        QAbstractTableModel.__init__(self, parent)

        self._root = ImageEnhancerNode()

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            if section == 0:
                return QVariant("Name")
        return QVariant()

    def index(self, row, column, parent=QModelIndex()):
        if not self.hasIndex(row, column, parent):
            return QModelIndex()

        parent_item = self.node(parent)
        child_item = parent_item.child(row)
        return self.createIndex(row, column, child_item) \
                if child_item is not None \
                else QModelIndex()

    def parent(self, index):
        parent_item = self.node(index).parent()
        if parent_item == self._root:
            return QModelIndex()
        return self.createIndex(parent_item.row(), index.column(), parent_item)
    
    def node(self, index):
        return index.internalPointer() if index.isValid() else self._root
    
    def columnCount(self, parent=QModelIndex()):
        return 1

    def rowCount(self, parent=QModelIndex()):
        if parent.column() > 0:
            return 0

        parent_item = self.node(parent)
        return parent_item.rowCount()

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return QVariant()

        item = self.node(index)
        if index.column() == 0 and role == Qt.DisplayRole:
            return QVariant(item.image_enhancer().name)
        else:
            return QVariant()

    def insertRows(self, row, count, parent=QModelIndex()):
        parent_item = self.node(parent)
        self.beginInsertRows(parent, row, row + count - 1)
        parent_item.insertRows(row, count)
        self.endInsertRows()

    def insert(self, row, image_enhancer, parent=QModelIndex()):
        parent_item = self.node(parent)
        self.beginInsertRows(parent, row, row)
        parent_item.insert(row, ImageEnhancerNode(image_enhancer))
        self.endInsertRows()

    def append(self, image_enhancer, parent=QModelIndex()):
        parent_item = self.node(parent)
        self.beginInsertRows(parent, parent_item.rowCount(), \
                parent_item.rowCount())
        parent_item.append(ImageEnhancerNode(image_enhancer))
        self.endInsertRows()
    
    def removeRows(self, row, count, parent=QModelIndex()):
        parent_item = self.node(parent)
        self.beginRemoveRows(parent, row, row + count - 1)
        parent_item.removeRows(row, count)
        self.endRemoveRows()

    def pop(self, parent=QModelIndex()):
        parent_item = self.node(parent)
        self.beginRemoveRows(parent, parent_item.rowCount() - 1, \
                parent_item.rowCount() - 1)
        parent_item.pop()
        self.endRemoveRows()

    def clear(self, parent=QModelIndex()):
        parent_item = self.node(parent)
        self.beginRemoveRows(parent, 0, parent_item.rowCount() - 1)
        parent_item.clear()
        self.endRemoveRows()

    def root(self):
        return self._root

    def setRoot(self, root):
        self.clear()
        self.beginInsertRows(QModelIndex(), 0, root.rowCount() - 1)
        self._root = copy.deepcopy(root)
        self.endInsertRows()

    def flags(self, index):
        return Qt.ItemIsSelectable | Qt.ItemIsEnabled

    def image_enhancer(self, index):
        return self.node(index).image_enhancer()
    
    def params(self, index):
        ie = self.image_enhancer(index)
        return {k: v for k, v \
                in ie.__dict__.items() \
                if not k.startswith('_')}
    
    def set_params(self, index, params):
        ie = self.image_enhancer(index)
        index.internalPointer().set_image_enhancer(ie.__class__(**params))
        self.dataChanged.emit(index, index)
    
