#!/usr/bin/env python2
# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

import sklearn.pipeline
import sklearn.decomposition
import sklearn.discriminant_analysis
import sklearn.naive_bayes
import sklearn.ensemble
import sklearn.linear_model

from .item_model.parameter_model import RootSpace, ChoiceSpace, BoolSpace, \
        UniformFloatSpace, UniformIntSpace
import sys

def construct_classifier_space():
    root_node = RootSpace()

    # Preprocessing node
    pre_node = root_node.add_node( \
            'preprocessing', \
            ChoiceSpace('preprocessing', 'Preprocessing', \
            [('none', 'None'), \
             ('rbf', 'RBFSampler'), \
             ('pca', 'Principal Component Analysis'), \
             ('fa', 'Factor Analysis'), \
             ('ica', 'Independent Componenent Analysis')]))

    # PCA parameters
    pca_n_components_node = pre_node.add_node( \
            'pca', 'n_components', \
            UniformFloatSpace('pca_n_components', 'Num Components', sys.float_info.epsilon, 1))
    pca_whiten_node = pre_node.add_node( \
            'pca', 'whiten', \
            BoolSpace('pca_whiten', 'Whiten'))

    # RBFSampler parameters
    rbf_gamma_node = pre_node.add_node( \
            'rbf', 'gamma', \
            UniformFloatSpace('rbf_gamma', 'Gamma', 0.1, 32))
    rbf_n_components_node = pre_node.add_node( \
            'rbf', 'n_components', \
            UniformIntSpace('rbf_n_components', 'Num Components', 1, 128))

    # FA parameters
    fa_n_components_node = pre_node.add_node( \
            'fa', 'n_components', \
            UniformIntSpace('fa_n_components', 'Num Components', 1, 32))

    # ICA parameters
    ica_n_components_node = pre_node.add_node( \
            'ica', 'n_components', \
            UniformIntSpace('ica_n_components', 'Num Components', 1, 32))
    ica_whiten_node = pre_node.add_node( \
            'ica', 'whiten', \
            BoolSpace('ica_whiten', 'Whiten'))

    # Classifier node
    clf_node = root_node.add_node( \
            'classifier', \
            ChoiceSpace('classifier', 'Classifier', \
            [('rf', 'Random forest'), \
             ('etc', 'Extra trees classifier'), \
             ('sgd', 'Stochastic gradient descent'), \
             ('qda', 'Quadratic Discriminant Analysis'), \
             ('lda', 'Linear Discriminant Analysis'), \
             ('nb', 'Naive Bayes')]))

    # Extra trees classifier
    etc_n_estimators_node = clf_node.add_node( \
            'etc', 'n_estimators', \
            UniformIntSpace('etc_n_estimators', 'Number of estimators', 1, 128))
    etc_criterion_node = clf_node.add_node( \
            'etc', 'criterion', \
            ChoiceSpace('etc_criterion', 'Criterion', \
            [('entropy', 'Entropy'), \
             ('gini', 'Gini')]))
    etc_max_features_node = clf_node.add_node( \
            'etc', 'max_features', \
            ChoiceSpace('etc_max_features', 'Max Features', \
            [('sqrt', 'Square Root'), \
             ('log2', 'Log 2'), \
             ('max_features_ratio', 'Ratio')]))
    etc_max_features_ratio_node = etc_max_features_node.add_node( \
            'max_features_ratio', 'max_features_ratio', \
            UniformFloatSpace('etc_max_features_ratio', 'Max features ratio', sys.float_info.epsilon, 1))
    etc_max_depth_node = clf_node.add_node( \
            'etc', 'max_depth', \
            UniformIntSpace('etc_max_depth', 'Max Depth', 1, 32))
    etc_min_samples_split_node = clf_node.add_node( \
            'etc', 'min_samples_split', \
            UniformIntSpace('etc_min_samples_split', 'Min Samples Split', 2, 32))
    etc_min_samples_leaf_node = clf_node.add_node( \
            'etc', 'min_samples_leaf', \
            UniformIntSpace('etc_min_samples_leaf', 'Min Samples Leaf', 1, 32))
    etc_bootstrap_node = clf_node.add_node( \
            'etc', 'bootstrap', \
            BoolSpace('etc_bootstrap', 'Bootstrap'))

    # Random forest parameters
    rf_n_estimators_node = clf_node.add_node( \
            'rf', 'n_estimators', \
            UniformIntSpace('rf_n_estimators', 'Number of estimators', 1, 128))
    rf_criterion_node = clf_node.add_node( \
            'rf', 'criterion', \
            ChoiceSpace('rf_criterion', 'Criterion', \
            [('entropy', 'Entropy'), \
             ('gini', 'Gini')]))
    rf_max_features_node = clf_node.add_node( \
            'rf', 'max_features', \
            ChoiceSpace('rf_max_features', 'Max Features', \
            [('sqrt', 'Square Root'), \
             ('log2', 'Log 2'), \
             ('max_features_ratio', 'Ratio')]))
    rf_max_features_ratio_node = rf_max_features_node.add_node( \
            'max_features_ratio', 'max_features_ratio', \
            UniformFloatSpace('rf_max_features_ratio', 'Max features ratio', sys.float_info.epsilon, 1))
    rf_max_depth_node = clf_node.add_node( \
            'rf', 'max_depth', \
            UniformIntSpace('rf_max_depth', 'Max Depth', 1, 32))
    rf_min_samples_split_node = clf_node.add_node( \
            'rf', 'min_samples_split', \
            UniformIntSpace('rf_min_samples_split', 'Min Samples Split', 2, 32))
    rf_min_samples_leaf_node = clf_node.add_node( \
            'rf', 'min_samples_leaf', \
            UniformIntSpace('rf_min_samples_leaf', 'Min Samples Leaf', 1, 32))
    rf_bootstrap_node = clf_node.add_node( \
            'rf', 'bootstrap', \
            BoolSpace('rf_bootstrap', 'Bootstrap'))

    # Stochastic gradient descent parameters
    sgd_loss_node = clf_node.add_node( \
            'sgd', 'loss', \
            ChoiceSpace('sgd_loss', 'Loss', \
            [('hinge', 'Hinge'), \
             ('log', 'Log'), \
             ('modified_huber', 'Modified Huber'), \
             ('squared_hinge', 'Squared Hinge'), \
             ('perceptron', 'Perceptron')]))
    sgd_penalty_node = clf_node.add_node( \
            'sgd', 'penalty', \
            ChoiceSpace('sgd_penalty', 'Penalty', \
            [('l1', 'L1'), \
             ('l2', 'L2'), \
             ('elasticnet', 'Elastic Net')]))
    sgd_l1_ratio_node = sgd_penalty_node.add_node( \
            'elasticnet', 'l1_ratio', \
            UniformFloatSpace('sgd_l1_ratio', 'L1 Ratio', 0., 1.))
    sgd_alpha_node = clf_node.add_node( \
            'sgd', 'alpha', \
            UniformFloatSpace('sgd_alpha', 'Alpha', 1e-7, 1e-1))

    return root_node

def _construct_rbf(params):
    kwargs = {}
    if params['n_components'] is not None:
        kwargs['n_components'] = params['n_components']
    if params['gamma'] is not None:
        kwargs['gamma'] = params['gamma']
    return sklearn.kernel_approximation.RBFSampler(**kwargs)

def _construct_pca(params):
    kwargs = {}
    if params['n_components'] is not None:
        kwargs['n_components'] = params['n_components']
    if params['whiten'][0] is not None:
        kwargs['whiten'] = params['whiten'][0]
    return sklearn.decomposition.PCA(**kwargs)

def _construct_fa(params):
    kwargs = {}
    if params['n_components'] is not None:
        kwargs['n_components'] = params['n_components']
    return sklearn.decomposition.FactorAnalysis(**kwargs)

def _construct_ica(params):
    kwargs = {}
    if params['n_components'] is not None:
        kwargs['n_components'] = params['n_components']
    if params['whiten'][0] is not None:
        kwargs['whiten'] = params['whiten'][0]
    return sklearn.decomposition.FastICA(**kwargs)

def _construct_etc(params):
    kwargs = {}
    if params['n_estimators'] is not None:
        kwargs['n_estimators'] = int(params['n_estimators'])
    if params['criterion'][0] is not None:
        kwargs['criterion'] = params['criterion'][0]
    if params['max_features'][0] is not None:
        if params['max_features'][0] == 'max_features_ratio':
            if params['max_features'][1]['max_features_ratio'] is not None:
                kwargs['max_features'] = \
                        params['max_features'][1]['max_features_ratio']
        else:
            kwargs['max_features'] = params['max_features'][0]
    if params['max_depth'] is not None:
        kwargs['max_depth'] = int(params['max_depth'])
    if params['min_samples_split'] is not None:
        kwargs['min_samples_split'] = int(params['min_samples_split'])
    if params['min_samples_leaf'] is not None:
        kwargs['min_samples_leaf'] = int(params['min_samples_leaf'])
    if params['bootstrap'][0] is not None:
        kwargs['bootstrap'] = params['bootstrap'][0]
    return sklearn.ensemble.ExtraTreesClassifier(class_weight='balanced', \
            **kwargs)

def _construct_rf(params):
    kwargs = {}
    if params['n_estimators'] is not None:
        kwargs['n_estimators'] = int(params['n_estimators'])
    if params['criterion'][0] is not None:
        kwargs['criterion'] = params['criterion'][0]
    if params['max_features'][0] is not None:
        if params['max_features'][0] == 'max_features_ratio':
            if params['max_features'][1]['max_features_ratio'] is not None:
                kwargs['max_features'] = \
                        params['max_features'][1]['max_features_ratio']
        else:
            kwargs['max_features'] = params['max_features'][0]
    if params['max_depth'] is not None:
        kwargs['max_depth'] = int(params['max_depth'])
    if params['min_samples_split'] is not None:
        kwargs['min_samples_split'] = int(params['min_samples_split'])
    if params['min_samples_leaf'] is not None:
        kwargs['min_samples_leaf'] = int(params['min_samples_leaf'])
    if params['bootstrap'][0] is not None:
        kwargs['bootstrap'] = params['bootstrap'][0]
    return sklearn.ensemble.RandomForestClassifier(class_weight='balanced', \
            **kwargs)

def _construct_sgd(params):
    kwargs = {}
    if params['loss'][0] is not None:
        kwargs['loss'] = params['loss'][0]
    if params['penalty'][0] is not None:
        kwargs['penalty'] = params['penalty'][0]
        if kwargs['penalty'] == 'elasticnet' and \
                params['penalty'][1]['l1_ratio'] is not None:
            kwargs['l1_ratio'] = params['penalty'][1]['l1_ratio']
    if params['alpha'] is not None:
        kwargs['alpha'] = params['alpha']
    return sklearn.linear_model.SGDClassifier(**kwargs)

def construct_classifier(params):
    pre_name, pre_params = params['preprocessing']
    if pre_name == 'none':
        pre = None
    elif pre_name == 'pca':
        pre = _construct_pca(pre_params)
    elif pre_name == 'pca':
        pre = _construct_rbf(pre_params)
    elif pre_name == 'ica':
        pre = _construct_ica(pre_params)
    elif pre_name == 'pca':
        pre = _construct_fa(pre_params)
    else:
        pre = None

    clf_name, clf_params = params['classifier']
    if clf_name == 'nb':
        clf = sklearn.naive_bayes.GaussianNB()
    elif clf_name == 'qda':
        clf = sklearn.discriminant_analysis.QuadraticDiscriminantAnalysis()
    elif clf_name == 'lda':
        clf = sklearn.discriminant_analysis.LinearDiscriminantAnalysis()
    elif clf_name == 'rf':
        clf = _construct_rf(clf_params)
    elif clf_name == 'etc':
        clf = _construct_etc(clf_params)
    elif clf_name == 'sgd':
        clf = _construct_sgd(clf_params)
    else:
        clf = sklearn.naive_bayes.GaussianNB()

    if pre is not None:
        return sklearn.pipeline.Pipeline([('preprocessing', pre), ('classifier', clf)])
    else:
        return  sklearn.pipeline.Pipeline([('classifier', clf)])

