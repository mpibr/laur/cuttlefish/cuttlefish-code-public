#!/usr/bin/env python

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import chromas.document
import itertools
import argparse
from tqdm import tqdm
import numpy as np

def main():
    p = argparse.ArgumentParser('Find common labels')
    p.add_argument('filenames', nargs='+')
    p.add_argument('output')
    args = p.parse_args()
    
    # Open all docs
    docs = [chromas.document.load(filename) \
            for filename in tqdm(args.filenames, desc='Opening Files')]

    # Create merged doc
    for data_labels in tqdm(itertools.izip( \
            *(doc['data_labels'] for doc in docs)), \
            desc='Merging labels', total=len(docs[0]['data_labels'])):
        labels = np.array([label for _, _, label in data_labels])
        # http://stackoverflow.com/questions/12297016/how-to-find-most 
        # -frequent-values-in-numpy-ndarray
        axis = 0
        u, indices = np.unique(labels, return_inverse=True)
        merged = u[np.argmax(np.apply_along_axis( \
                np.bincount, \
                axis, \
                indices.reshape(labels.shape), \
                None, np.max(indices) + 1), axis=axis)]
        data_labels[0][2][:] = merged
    chromas.document.dump(args.output, docs[0])

