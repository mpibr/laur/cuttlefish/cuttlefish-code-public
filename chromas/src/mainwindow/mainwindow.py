#!/usr/bin/env python2
# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *


# Gui
from ..graphics_view.view import View

# Dialogs
from dialog.image_enhancers_configure_dialog \
        import ImageEnhancerConfigureDialog
from dialog.import_video_params_dialog import ImportVideoParamsDialog

# Gui models
from ..item_model.image_enhancers_model import ImageEnhancersModel
from ..item_model.image_enhancers_model import ImageEnhancerNode
from ..item_model.labels_model import LabelsModel
from ..item_model.data_model import DataModel
from ..item_model.parameter_model import ParameterModel

# Scenes
from ..graphics_scene.data_scene import DataScene
from ..graphics_scene.labels_scene import LabelsScene
from ..graphics_scene.editable_labels_scene import EditableLabelsScene

# Item views
from ..item_view.labels_table import LabelsTable
from ..item_view.image_enhancers_table import ImageEnhancersTable
from ..item_view.data_table import DataTable
from ..item_view.parameter_tree import ParameterTree

# Commands
from .command.labels_commands import NewLabelCommand
from .command.labels_commands import DeleteLabelsCommand
from .command.labels_commands import MergeLabelsCommand
from .command.data_commands import ImportDataCommand
from .command.data_commands import ImportDocumentCommand
from .command.data_commands import DeleteDataCommand
from .command.image_enhancers_commands import NewImageEnhancerCommand
from .command.image_enhancers_commands import DeleteImageEnhancerCommand
from .command.image_enhancers_commands import ModifyImageEnhancerCommand

# Base functionalities
from .. import feature
from .. import image_enhancers
from .. import document

# Version number
from .. import version

import cPickle as pickle

# Numeric tools
import cv2
import numpy as np
import random

# Fitting
from ..classifier import construct_classifier_space
from ..classifier import construct_classifier
import sklearn.ensemble
import sklearn.cluster
import sklearn.naive_bayes
import sklearn.svm

# Parallel processing
import pebble
import multiprocessing
import traceback

import copy_reg
import types
import itertools

def _pickle_method(m):
    if m.im_self is None:
        return getattr, (m.im_class, m.im_func.func_name)
    else:
        return getattr, (m.im_self, m.im_func.func_name)
copy_reg.pickle(types.MethodType, _pickle_method)

class MainWindow(QMainWindow):

    IMAGE_ENHANCER_CLASSES = [image_enhancers.Gray, \
            image_enhancers.Red, \
            image_enhancers.Green, \
            image_enhancers.Blue, \
            image_enhancers.Retinex, \
            image_enhancers.RemoveLumi, \
            image_enhancers.StretchLumi, \
            image_enhancers.HistEq, \
            image_enhancers.LUV, \
            image_enhancers.ContrastBrightness, \
            image_enhancers.Normalize]
    DEFAULT_IMAGE_ENHANCER_NODE = ImageEnhancerNode()
    DEFAULT_IMAGE_ENHANCER_NODE.append(ImageEnhancerNode(
        image_enhancers.Identity()))
    DEFAULT_IMAGE_ENHANCER_NODE.append(ImageEnhancerNode(
        image_enhancers.Gray()))
    DEFAULT_IMAGE_ENHANCER_NODE.append(ImageEnhancerNode(
        image_enhancers.Red()))
    DEFAULT_IMAGE_ENHANCER_NODE.append(ImageEnhancerNode(
        image_enhancers.Green()))
    DEFAULT_IMAGE_ENHANCER_NODE.append(ImageEnhancerNode(
        image_enhancers.Blue()))

    def __init__(self):
        QMainWindow.__init__(self)

        self.setWindowTitle('ChromaS')
        self._filename = str("[Unnamed]")

        self._undo_stack = QUndoStack(self)
        self._undo_stack.setUndoLimit(64)
        self._undo_stack.cleanChanged.connect(self._undo_stack_clean_changed)
        
        # Timer for getting prediction results
        self._check_features_timer = QTimer()
        self._check_features_timer.setInterval(250)
        self._check_features_timer.timeout.connect(self._check_features)
        self._check_fitting_timer = QTimer()
        self._check_fitting_timer.setInterval(250)
        self._check_fitting_timer.timeout.connect(self._check_fitting)
        self._show_predict_timer = QTimer()
        self._show_predict_timer.setInterval(250)
        self._show_predict_timer.timeout.connect(self._check_predicting)

        # Data for segmentation 
        self._clf = None # Last classifier
        self._clf_f = None # Future of the model
        self._y_f = None # Future of the predicted labels

        # Parallel processing
        self._executor = pebble.ProcessPool( \
                max(multiprocessing.cpu_count() - 1, 1))

        # Actions
        open_data_action = QAction("Import images ...", self)
        open_data_action.triggered.connect(self._import_image)
        import_document_action = QAction("Import document ...", self)
        import_document_action.triggered.connect(self._import_document)
        open_video_action = QAction("Import videos ...", self)
        open_video_action.triggered.connect(self._import_video)
        open_paired_patches_action = QAction("Import paired patches ...", self)
        open_paired_patches_action.triggered.connect(self._import_paired_patches)
        
        remove_data_action = QAction(QIcon.fromTheme("list-remove"), \
                "Remove data", self)
        remove_data_action.triggered.connect(self._remove_data)

        open_action = QAction("Open ...", self)
        open_action.setShortcut(QKeySequence.Open)
        open_action.triggered.connect(self.open)

        self.save_action = QAction("Save", self)
        self.save_action.setShortcut(QKeySequence.Save)
        self.save_action.triggered.connect(self.save)
        self.save_action.setEnabled(False)

        save_as_action = QAction("Save as ...", self)
        save_as_action.setShortcut(QKeySequence.SaveAs)
        save_as_action.triggered.connect(self.save_as)

        self.export_classifier_action = QAction("Export classifier ...", self)
        self.export_classifier_action.setEnabled(False)
        self.export_classifier_action.triggered.connect(
                self._export_classifier)
                
        add_label_action = QAction(QIcon.fromTheme("list-add"), \
                "Add label", self)
        add_label_action.triggered.connect(self._add_label)

        remove_labels_action = QAction(QIcon.fromTheme("list-remove"), \
                "Remove labels", self)
        remove_labels_action.triggered.connect(self._remove_labels)
        
        self._merge_labels_action = QAction("Merge labels", self)
        self._merge_labels_action.triggered.connect(self._merge_labels)
        self._merge_labels_action.setEnabled(False)

        add_image_enhancer_action = QAction(QIcon.fromTheme("list-add"), \
                "Add image_enhancer", self)
        add_image_enhancer_action.triggered.connect(self._add_image_enhancer)
        
        remove_image_enhancers_action = QAction(QIcon.fromTheme("list-remove"), \
                "Remove image_enhancers", self)
        remove_image_enhancers_action.triggered.connect( \
                self._remove_image_enhancers)
        
        configure_image_enhancer_action = QAction( \
                QIcon.fromTheme("dialog-object-properties", \
                QIcon.fromTheme("document-properties")), \
                "Edit image_enhancer", self)
        configure_image_enhancer_action.triggered.connect( \
                self._configure_image_enhancer)

        action_save_image = QAction( \
                "Save image", self)
        action_save_image.triggered.connect( \
                self._save_image)
        action_save_labels = QAction( \
                "Save labels", self)
        action_save_labels.triggered.connect( \
                self._save_labels)
        action_save_prediction = QAction( \
                "Save prediction", self)
        action_save_prediction.triggered.connect( \
                self._save_prediction)

        action_about = QAction( \
                "About ...", self)
        action_about.triggered.connect(self._about)

        # Labels dock
        self.labels_model = LabelsModel(self._undo_stack, self)
        self.labels_model.rowsRemoved.connect(self._queue_wait_features)
        self.labels_model.rowsInserted.connect(self._queue_wait_features)
        self._labels = LabelsTable(self.labels_model)
        self._labels.selectionModel().selectionChanged.connect( \
                self._label_selected)

        labels_tools_menu = QMenu()
        labels_tools_menu.addAction(self._merge_labels_action)
        
        labels_tools_button = QToolButton()
        labels_tools_button.setText("Tools")
        labels_tools_button.setIcon(QIcon.fromTheme("system-run"))
        labels_tools_button.setMenu(labels_tools_menu)
        labels_tools_button.setPopupMode(QToolButton.InstantPopup)
        
        labels_toolbar = QToolBar()
        labels_toolbar.addAction(add_label_action)
        labels_toolbar.addAction(remove_labels_action)
        labels_toolbar.addWidget(labels_tools_button)

        labels_layout = QVBoxLayout()
        labels_layout.addWidget(self._labels)
        labels_layout.addWidget(labels_toolbar)

        labels_widget = QWidget()
        labels_widget.setLayout(labels_layout)

        self._labels_dock = QDockWidget("Labels")
        self._labels_dock.setWidget(labels_widget)

        # New features dock
        self.feature_model = ParameterModel(feature.construct_feature_space(), self)
        self.feature_model.dataChanged.connect(self._recalculate_features)
        self._feature = ParameterTree( \
                self.feature_model, self._undo_stack, self)
        self._feature_dock = QDockWidget("Features")
        self._feature_dock.setWidget(self._feature)

        # Classifier dock
        self.classifier_model = ParameterModel(construct_classifier_space(), self)
        self.classifier_model.dataChanged.connect(self._classifier_changed)
        self._classifier = ParameterTree( \
                self.classifier_model, self._undo_stack, self)
        self._classifier_dock = QDockWidget("Classifier")
        self._classifier_dock.setWidget(self._classifier)
        
        # Image enhancers dock
        self.image_enhancers_model = ImageEnhancersModel(self)
        self.image_enhancers_model.setRoot(self.DEFAULT_IMAGE_ENHANCER_NODE)
        self.image_enhancers_model.rowsInserted.connect( \
                self._image_enhancers_changed)
        self.image_enhancers_model.rowsRemoved.connect( \
                self._image_enhancers_changed)
        self.image_enhancers_model.dataChanged.connect( \
                self._image_enhancers_changed)
        
        self._image_enhancers = ImageEnhancersTable( \
                self.image_enhancers_model, self)
        self._image_enhancers.selectionModel().select( \
                self.image_enhancers_model.index(0, 0), \
                QItemSelectionModel.Select)
        self._image_enhancers.selectionModel().selectionChanged.connect( \
                self._image_enhancers_selected)

        image_enhancers_toolbar = QToolBar()
        image_enhancers_toolbar.addAction(add_image_enhancer_action)
        image_enhancers_toolbar.addAction(remove_image_enhancers_action)
        image_enhancers_toolbar.addAction(configure_image_enhancer_action)

        image_enhancers_layout = QVBoxLayout()
        image_enhancers_layout.addWidget(self._image_enhancers)
        image_enhancers_layout.addWidget(image_enhancers_toolbar)

        image_enhancers_widget = QWidget()
        image_enhancers_widget.setLayout(image_enhancers_layout)

        self._image_enhancers_dock = QDockWidget("Image enhancers")
        self._image_enhancers_dock.setWidget(image_enhancers_widget)

        # Data view
        self._data_scene = DataScene(self)
        self._data_view = View()
        self._data_view.setScene(self._data_scene)

        self._data_dock = QDockWidget("Data")
        self._data_dock.setWidget(self._data_view)

        # Predicted view
        self._predicted_scene = LabelsScene(self.labels_model, self)
        self._predicted_view = View()
        self._predicted_view.setScene(self._predicted_scene)

        self._predicted_status_bar = QStatusBar()

        predicted_layout = QVBoxLayout()
        predicted_layout.addWidget(self._predicted_view)
        predicted_layout.addWidget(self._predicted_status_bar)
        
        predicted_widget = QWidget()
        predicted_widget.setLayout(predicted_layout)

        self._predicted_dock = QDockWidget("Predicted")
        self._predicted_dock.setWidget(predicted_widget)
        
        # Data view
        self.data_model = DataModel(self._undo_stack, self.labels_model, self)
        self.data_model.rowsInserted.connect(self._queue_wait_features)
        self.data_model.rowsRemoved.connect(self._queue_wait_features)
        self.data_model.featuresChanged.connect(self._queue_wait_features)
        self._data_table = DataTable(self.data_model, self)
        self._data_table.selectionModel().selectionChanged.connect( \
                self._data_selected)
        
        data_import_menu = QMenu("Import data")
        data_import_menu.addAction(open_data_action)
        data_import_menu.addAction(import_document_action)
        data_import_menu.addAction(open_video_action)
        data_import_menu.addAction(open_paired_patches_action)
        
        data_import_button = QToolButton()
        data_import_button.setText("Import data")
        data_import_button.setIcon(QIcon.fromTheme("document-open"))
        data_import_button.setMenu(data_import_menu)
        data_import_button.setPopupMode(QToolButton.InstantPopup)
        
        data_table_toolbar = QToolBar()
        data_table_toolbar.addWidget(data_import_button)
        data_table_toolbar.addAction(remove_data_action)

        data_table_layout = QVBoxLayout()
        data_table_layout.addWidget(self._data_table)
        data_table_layout.addWidget(data_table_toolbar)

        data_table_widget = QWidget()
        data_table_widget.setLayout(data_table_layout)

        data_table_dock = QDockWidget("Data selection")
        data_table_dock.setWidget(data_table_widget)

        # Train view
        self._training_scene = EditableLabelsScene(self._undo_stack, \
                self.labels_model, \
                self._data_table.selectionModel(), self)
        self._training_scene.labelsChanged.connect(self._queue_wait_features)
        
        self._training_view = View()
        self._training_view.toolRadiusChanged.connect(
                self._training_scene.setToolRadius)
        self._training_view.setScene(self._training_scene)
        self.setCentralWidget(self._training_view)

        status_bar = QStatusBar()
        status_bar.showMessage("Left click: Draw, " \
                "Right click: Erase, " \
                "Shift + Click: Preserve non-selected labels, " \
                "Shift + Mouse wheel: Set tool radius, " \
                "Control + Mouse wheel: Zoom, " \
                "Control: Centre views")
        
        main_layout = QVBoxLayout()
        main_layout.addWidget(self._training_view)
        main_layout.addWidget(status_bar)

        self._main_widget = QWidget()
        self._main_widget.setLayout(main_layout)

        self.setCentralWidget(self._main_widget)

        self._training_view.viewCaptured.connect(
                self._data_view.captureView)
        self._training_view.viewReleased.connect(
                self._data_view.releaseView)
        self._training_view.viewCaptured.connect(
                self._predicted_view.captureView)
        self._training_view.viewReleased.connect(
                self._predicted_view.releaseView)
        self._training_view.toolRadiusChanged.connect(
                self._data_view.setToolRadius)
        self._training_view.toolRadiusChanged.connect(
                self._predicted_view.setToolRadius)
        
        self._data_view.viewCaptured.connect(
                self._training_view.captureView)
        self._data_view.viewReleased.connect(
                self._training_view.releaseView)
        self._data_view.viewCaptured.connect(
                self._predicted_view.captureView)
        self._data_view.viewReleased.connect(
                self._predicted_view.releaseView)
        self._data_view.toolRadiusChanged.connect(
                self._training_view.setToolRadius)
        self._data_view.toolRadiusChanged.connect(
                self._predicted_view.setToolRadius)

        self._predicted_view.viewCaptured.connect(
                self._data_view.captureView)
        self._predicted_view.viewReleased.connect(
                self._data_view.releaseView)
        self._predicted_view.viewCaptured.connect(
                self._training_view.captureView)
        self._predicted_view.viewReleased.connect(
                self._training_view.releaseView)
        self._predicted_view.toolRadiusChanged.connect(
                self._data_view.setToolRadius)
        self._predicted_view.toolRadiusChanged.connect(
                self._predicted_view.setToolRadius)

        self.addDockWidget(Qt.LeftDockWidgetArea, data_table_dock)
        self.addDockWidget(Qt.LeftDockWidgetArea, self._labels_dock)
        self.addDockWidget(Qt.RightDockWidgetArea, self._feature_dock)
        self.addDockWidget(Qt.RightDockWidgetArea, self._classifier_dock)
        self.addDockWidget(Qt.RightDockWidgetArea, self._image_enhancers_dock)
        self.addDockWidget(Qt.RightDockWidgetArea, self._data_dock)
        self.addDockWidget(Qt.RightDockWidgetArea, self._predicted_dock)
        
        # Populate menu
        file_menu = self.menuBar().addMenu("File")
        file_menu.addAction(open_action)
        file_menu.addAction(self.save_action)
        file_menu.addAction(save_as_action)
        file_menu.addSeparator()
        file_menu.addAction(self.export_classifier_action)
        edit_menu = self.menuBar().addMenu("Edit")
        undo_action = self._undo_stack.createUndoAction(self)
        undo_action.setShortcut(QKeySequence.Undo)
        edit_menu.addAction(undo_action)
        redo_action = self._undo_stack.createRedoAction(self)
        redo_action.setShortcut(QKeySequence.Redo)
        edit_menu.addAction(redo_action)
        view_menu = self.menuBar().addMenu("View")
        view_menu.addAction(data_table_dock.toggleViewAction())
        view_menu.addAction(self._labels_dock.toggleViewAction())
        view_menu.addAction(self._feature_dock.toggleViewAction())
        view_menu.addAction(self._classifier_dock.toggleViewAction())
        view_menu.addAction(self._image_enhancers_dock.toggleViewAction())
        view_menu.addAction(self._data_dock.toggleViewAction())
        view_menu.addAction(self._predicted_dock.toggleViewAction())
        tools_menu = self.menuBar().addMenu("Tools")
        tools_menu.addAction(action_save_image)
        tools_menu.addAction(action_save_labels)
        tools_menu.addAction(action_save_prediction)
        help_menu = self.menuBar().addMenu("Help")
        help_menu.addAction(action_about)
        
        # Settings
        self._training_view.setToolRadius(2)
        
        self._data_dock.setEnabled(False)
        self._predicted_dock.setEnabled(False)
        self._main_widget.setEnabled(False)

    def closeEvent(self, event):
        if self.closeDocument():
            event.accept()
        else:
            event.ignore()

    def closeDocument(self):
        if not self._undo_stack.isClean():
            button = QMessageBox.question(self, "ChromaS", \
                    "Save changes before saving?", \
                    QMessageBox.Cancel | QMessageBox.Yes | QMessageBox.No)
            if button == QMessageBox.Cancel:
                return False
            elif button == QMessageBox.Yes:
                if self.save_action.isEnabled():
                    self.save()
                else:
                    self.save_as()
                if not self._undo_stack.isClean():
                    return False
        return True
        
    def open(self):
        if not self.closeDocument():
            return

        filename = QFileDialog.getOpenFileName(self, "Open", "", \
                "Chromas files (*hd5)")[0]
        if not filename:
            return
        
        try:
            print(filename)
            print(str(filename))
            doc = document.load(str(filename))
        except Exception as e:
            msg_box = QMessageBox(QMessageBox.Critical, "Cannot open file",
                    "I was trying to open the file \"%s\", but suddenly a "
                    "wild %s appeared. It shouted: %s and I flinched."
                    % (filename, type(e).__name__, e),
                    QMessageBox.Ok)
            msg_box.setDetailedText(traceback.format_exc())
            msg_box.exec_()
            return
        
        self._undo_stack.clear()
        self.labels_model.clear()
        self.data_model.clear()
        self.image_enhancers_model.clear()
        
        self._clf = None
        if self._clf_f is not None:
            self._clf_f.cancel()
        self._clf_f = None
        if self._y_f is not None:
            self._y_f.cancel()
        self._y_f = None
        self.export_classifier_action.setEnabled(False)

        self._filename = filename
        self.setWindowTitle("ChromaS - %s" % self._filename)
        self.save_action.setEnabled(True)
        
        if doc['clf_params'] is not None:
            self.classifier_model.load(doc['clf_params'])
        for name, colour in zip(doc['label_names'], doc['label_colours']):
            self.labels_model.append(str(name), colour)

        self.feature_model.load(doc['feature_params'])
        params = self.feature_model.classifier_params()
        feature_strategies = feature.construct_features(params)
        
        for name, data, labels in doc['data_labels']:
            self.data_model.append(name, data, labels, \
                    self._calculate_features(feature_strategies, data))
       
        doc['image_enhancers'] = self.DEFAULT_IMAGE_ENHANCER_NODE \
                if doc['image_enhancers'] is None \
                else doc['image_enhancers']
        self.image_enhancers_model.setRoot(doc['image_enhancers'])
        
        self._image_enhancers.selectionModel().select( \
                self.image_enhancers_model.index(0, 0), \
                QItemSelectionModel.Select)
                
    def save_as(self):
        filename = QFileDialog.getSaveFileName(self, "Save as", "", \
                "Chromas file (*.hd5)")[0]
        if not filename:
            return

        self._filename = filename
        self.setWindowTitle("ChromaS - %s" % self._filename)
        self.save_action.setEnabled(True)
        self.save()

    def save(self):
        doc = {}
        doc['data_labels'] = [(str(self.data_model.name(row)), \
                               self.data_model.image(row), \
                               self.data_model.labels(row)) \
                              for row in range(self.data_model.rowCount())]
        doc['label_colours'] = [self.labels_model.colour(key) \
                for key in range(self.labels_model.rowCount())]
        doc['label_names'] = [str(self.labels_model.name(key)) \
                for key in range(self.labels_model.rowCount())]
        doc['feature_params'] = {key: value \
                for key, value in self.feature_model.dump().items()
                if value is not None}
        doc['image_enhancers'] = self.image_enhancers_model.root()
        doc['clf_params'] = {key: value \
                for key, value in self.classifier_model.dump().items()
                if value is not None}

        try:
            document.dump(str(self._filename), doc)
        except Exception as e:
            msg_box = QMessageBox(QMessageBox.Critical, "Cannot save file",
                    "I was trying to open the file \"%s\", but suddenly a "
                    "wild %s appeared. It shouted: %s and I flinched."
                    % (self._filename, type(e).__name__, e),
                    QMessageBox.Ok)
            msg_box.setDetailedText(traceback.format_exc())
            msg_box.exec_()
            return
        
        self._undo_stack.setClean()

    def keyPressEvent(self, event):
        if (str(event.text()) != '' and str(event.text()) in '12345689'):
            label_num = int(event.text())
            self._labels.selectRow(label_num - 1)
            return

        QMainWindow.keyPressEvent(self, event)

    def _undo_stack_clean_changed(self, clean):
        if clean:
            self.setWindowTitle("ChromaS - %s" % self._filename)
        else:
            self.setWindowTitle("ChromaS - %s*" % self._filename)

    def _import_image(self):
	

       	filenames = QFileDialog.getOpenFileNames(self, "Import images")[0]
	
        if filenames:

	    images=[]
            for filename in filenames:
                filename = str(filename)
                
                if filename.endswith('.npy'):
                    images.append(np.load(filename))
                else:
                    images.append(cv2.cvtColor(cv2.imread(str(filename)), \
                        cv2.COLOR_BGR2RGB))
            command = ImportDataCommand(self, \
                    [(QFileInfo(filename).fileName(), \
                    image) \
                    for filename, image in zip(filenames, images)])
	                 
	    self._undo_stack.push(command)

  
    def _import_paired_patches(self):
    	filenames = QFileDialog.getOpenFileNames(self, "Import videos", "", \
                    "Videos (*.avi *mov *.mp4 *.mkv)")[0]
        if filenames:
            dialog = ImportVideoParamsDialog()
            dialog.setWindowTitle('Import video parameters')
            dialog.exec_()
            width, height, prob = dialog.params()
            
            all_patches = []
            for filename in filenames:
                name_prefix = QFileInfo(filename).fileName()
                
                cap = cv2.VideoCapture(str(filename))
                if cv2.__version__.split('.')[0] >= '3':
                    video_width = \
                            int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
                    video_height = \
                            int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                    video_frames = \
                            int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
                else:
                    video_width = \
                            int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
                    video_height = \
                            int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
                    video_frames = \
                            int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
                num_patches = int(video_width / width) \
                        * int(video_height / height) \
                        * video_frames
                sample_size = int(num_patches * prob)
                
                # All possible pathes
                patch_positions = list(itertools.product( \
                        xrange(0, video_frames), \
                        xrange(0, video_height - height, height), \
                        xrange(0, video_width - width, width)))
        
                # Select some random patches
                sample_patch_positions = random.sample(patch_positions, \
                        sample_size)
                sample_patch_positions.sort(key=lambda p: p[0])
                
                # Create a progress window
                progress = QProgressDialog("Extracting %d patches ..." \
                        % sample_size, \
                        "Cancel", 0, sample_size, self)
                progress.setWindowModality(Qt.WindowModal)
                progress.setWindowTitle('Import video %s' % filename)
    
                # Extract patches
                def extract_patch_pair(i, f, y, x):
                    if cv2.__version__.split('.')[0] >= '3':
                        cap.set(cv2.CAP_PROP_POS_FRAMES, f)
                    else:
                        cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, f)
                    succ, img = cap.read()
                    succ2, img2 = cap.read()
                    if succ and succ2:
                        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                        img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
                        progress.setValue(i)
                        QCoreApplication.processEvents()
                        return img[y : y + height, x : x + width].copy(), img2[y : y + height, x : x + width].copy()
    
                    else:
                        return None
              
                for ind, vals in  enumerate(sample_patch_positions):
                    p = extract_patch_pair(ind, vals[0],vals[1],vals[2])
                    if p is not None:  
                        patches1=("%s - %d:%d:%d" % (name_prefix, vals[0], vals[1], vals[2]), p[0])
                        patches2=("%s - %d:%d:%d_frame2" % (name_prefix, vals[0], vals[1], vals[2]), p[1])
                        all_patches.append(patches1)
                        all_patches.append(patches2)
                 
                progress.setValue(sample_size)
                cap.release()
            
            command = ImportDataCommand(self, all_patches)
            self._undo_stack.push(command)




    def _import_video(self):
        filenames = QFileDialog.getOpenFileNames(self, "Import videos", "", \
                "Videos (*.avi *mov *.mp4 *.mkv)")[0]
        if filenames:
            dialog = ImportVideoParamsDialog()
            dialog.setWindowTitle('Import video parameters')
            dialog.exec_()
            width, height, prob = dialog.params()
            
            all_patches = []
            for filename in filenames:
                name_prefix = QFileInfo(filename).fileName()
                
                cap = cv2.VideoCapture(str(filename))
                if cv2.__version__.split('.')[0] >= '3':
                    video_width = \
                            int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
                    video_height = \
                            int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                    video_frames = \
                            int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
                else:
                    video_width = \
                            int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
                    video_height = \
                            int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
                    video_frames = \
                            int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
                num_patches = int(video_width / width) \
                        * int(video_height / height) \
                        * video_frames
                sample_size = int(num_patches * prob)
                
                # All possible pathes
                patch_positions = list(itertools.product( \
                        xrange(0, video_frames), \
                        xrange(0, video_height - height, height), \
                        xrange(0, video_width - width, width)))

                # Select some random patches
                sample_patch_positions = random.sample(patch_positions, \
                        sample_size)
                sample_patch_positions.sort(key=lambda p: p[0])
                
                # Create a progress window
                progress = QProgressDialog("Extracting %d patches ..." \
                        % sample_size, \
                        "Cancel", 0, sample_size, self)
                progress.setWindowModality(Qt.WindowModal)
                progress.setWindowTitle('Import video %s' % filename)

                # Extract patches
                def extract_patch(i, f, y, x):
                    if cv2.__version__.split('.')[0]>= '3':
                        cap.set(cv2.CAP_PROP_POS_FRAMES, f)
                    else:
                        cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, f)
                    succ, img = cap.read()
                    if succ:
                        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                        progress.setValue(i)
                        QCoreApplication.processEvents()
                        return img[y : y + height, x : x + width].copy()
                    else:
                        return None
                patches = map(lambda (i, (f, y, x)): \
                        ("%s - %d:%d:%d" % (name_prefix, f, x, y), \
                         extract_patch(i, f, y, x)), \
                    enumerate(sample_patch_positions))
                patches = filter(lambda (name, patch): patch is not None,
                        patches)
                progress.setValue(sample_size)
                cap.release()

                all_patches.extend(patches)

            command = ImportDataCommand(self, patches)
            self._undo_stack.push(command)

    def _import_document(self):
        filename = QFileDialog.getOpenFileName(self, "Open", "", \
                "Chromas files (*hd5)")[0]
        
        if not filename:
            return
        
        try:
            doc = document.load(str(filename))
        except Exception as e:
            msg_box = QMessageBox(QMessageBox.Critical, "Cannot open file",
                    "I was trying to open the file \"%s\", but suddenly a "
                    "wild %s appeared. It shouted: %s and I flinched."
                    % (filename, type(e).__name__, e),
                    QMessageBox.Ok)
            msg_box.setDetailedText(traceback.format_exc())
            msg_box.exec_()
            return

        command = ImportDocumentCommand(self, \
                doc['label_colours'], doc['label_names'], doc['data_labels'])
        self._undo_stack.push(command)

    def _export_classifier(self):
        #Recalculate all features for BGR image instead RGB
        self._recalculate_features_BGR()

        #Acitvely wait for calculation of classifier to finish
        while not self._clf_f or not self._clf_f.done():
            QCoreApplication.processEvents()
        
        filename = QFileDialog.getSaveFileName(self, "Export classifier", "", \
                "Classifier (*.clf)")[0]
        if not filename:
            return
        
        params = self.feature_model.classifier_params()
        feature_strategies = feature.construct_features(params)
        
        classifier_doc = {\
                "label_colours" : \
                    [(str(self.labels_model.name(row)), \
                      self.labels_model.colour(row)) \
                     for row in range(self.labels_model.rowCount())], \
                "features" : feature_strategies, \
                "classifier" : self._clf}
        with open(str(filename), 'wb') as f:
            pickle.dump(classifier_doc, f)

        #And back to RGB
        self._recalculate_features()

    def _remove_data(self):
        indexes = sorted([index.row() \
                for index in self._data_table.selectedIndexes()])
        command = DeleteDataCommand(self, indexes)
        self._undo_stack.push(command)

    def _add_label(self):
        data_selection = self._data_table.selectionModel().selection()
        command = NewLabelCommand(self, data_selection)
        self._undo_stack.push(command)
    
    def _remove_labels(self):
        data_selection = self._data_table.selectionModel().selection()
        indexes = sorted([index.row() \
                for index in self._labels.selectedIndexes()])
        command = DeleteLabelsCommand(self, data_selection, indexes)
        self._undo_stack.push(command)

    def _merge_labels(self):
        data_selection = self._data_table.selectionModel().selection()
        indexes = sorted([index.row() \
                for index in self._labels.selectedIndexes()])
        command = MergeLabelsCommand(self, data_selection, \
                self._labels.currentIndex().row(), indexes)
        self._undo_stack.push(command)

    def _label_selected(self, selected, deselected):
        num_selected = len(self._labels.selectedIndexes())
        if num_selected == 0:
            self._training_scene.setEditingLabel(-1)
            self._merge_labels_action.setEnabled(False)
        elif num_selected == 1:
            index = self._labels.selectedIndexes()[0]
            row = index.row()
            self._training_scene.setEditingLabel(row)
            self._merge_labels_action.setEnabled(False)
        else:
            self._training_scene.setEditingLabel(-1)
            self._merge_labels_action.setEnabled(True)

    def _data_selected(self, selected, deselected):
        if len(selected.indexes()) == 0:
            self._data_dock.setEnabled(False)
            self._predicted_dock.setEnabled(False)
            self._main_widget.setEnabled(False)
            data = np.empty((0, 0, 3), 'uint8')
            labels = np.empty((0, 0), 'uint8')
        elif len(selected.indexes()) == 1:
            self._data_dock.setEnabled(True)
            self._predicted_dock.setEnabled(True)
            self._main_widget.setEnabled(True)
            index = selected.indexes()[0]
            row = index.row()
            data = self.data_model.image(row)
            labels = self.data_model.labels(row)

            # Apply image enhancers
            selected = self._image_enhancers.selectionModel().selection()
            if len(selected.indexes()) == 1:
                index = selected.indexes()[0]
                pipeline = []
                while index.isValid():
                    pipeline = [self.image_enhancers_model \
                            .image_enhancer(index)] + pipeline
                    index = self.image_enhancers_model.parent(index)
                for ie in pipeline:
                    data = ie.apply(data)
        else:
            raise RuntimeError("Bug: Make data table only select one item")

        self._data_scene.setData(data)
        self._training_scene.setData(data)
        self._predicted_scene.setData(data)
        self._training_scene.setLabels(labels)
        self._predicted_scene.setLabels(np.empty((0, 0), 'uint8'))

        self._training_view.fitInView(self._training_scene.sceneRect(),
                Qt.KeepAspectRatio)
        self._data_view.fitInView(self._data_scene.sceneRect(),
                Qt.KeepAspectRatio)
        self._predicted_view.fitInView(self._predicted_scene.sceneRect(),
                Qt.KeepAspectRatio)
        
        if not self._clf:
            self._queue_wait_features()
        else:
            self._queue_wait_predicting()

    def _queue_wait_features(self):
        # Check preconditions for calculating features
        if not self.data_model.rowCount():
            return
        no_features = sum(self.data_model.featureMaps(row) is None \
                for row in range(self.data_model.rowCount()))
        if no_features:
            self._predicted_status_bar.showMessage("Add features!")
            self._predicted_scene.setLabels(np.empty((0, 0), 'uint8'))
            return
        self._check_features_timer.start()

    def _check_features(self):
        '''
        Called periodically to wait for features to be calculated
        '''
        # Wait for the features to be calculated
        for row in range(self.data_model.rowCount()):
            X_f = self.data_model.featureMaps(row)
            if not X_f:
                return
            if not X_f.done():
                self._predicted_status_bar.showMessage( \
                        "Updating (Preprocessing) ...")
                return
        try:
            X_labels = [(self.data_model.featureMaps(row).result(), \
                         self.data_model.labels(row)) \
                        for row in range(self.data_model.rowCount())]
        except Exception as e:
            msg_box = QMessageBox(QMessageBox.Warning,
                    "Fetching features failed",
                    "I was trying to receive the features, but suddenly a "
                    "wild %s appeared. It shouted: %s. But I escaped "
                    "luckily."
                    % (type(e).__name__, e),
                    QMessageBox.Ok)
            msg_box.setDetailedText(traceback.format_exc())
            msg_box.exec_()
            return
        self._check_features_timer.stop()
        
        # Check preconditions on fitting
        no_labels = not sum(np.any(self.data_model.labels(row)) \
                for row in range(self.data_model.rowCount()))
        if no_labels:
            labels = self._training_scene.labels()
            self._predicted_scene.setLabels(np.empty((0, 0), 'uint8'))
            self._predicted_status_bar.showMessage("Add labels!")
            return

        # Fitting is running, trying again later
        if self._clf_f is not None:
            self._queue_wait_features()
            return
        self._clf_f = None

        # Construct classifier
        params = self.classifier_model.classifier_params()
        
        clf = construct_classifier(params)
        if clf is None:
            return
        
        # Construct training data
        w_arr = [np.where(labels) for _, labels in X_labels]
        X = np.vstack([X[w] for w, (X, _) \
                in itertools.izip(w_arr, X_labels)])
        y = np.hstack([labels[w] - 1 for w, (_, labels) \
            in itertools.izip(w_arr, X_labels)])

        # Fit in the background
        self._clf_f = self._executor.schedule(type(clf).fit, \
                    (clf, X, y))
        self._check_fitting_timer.start()

    def _check_fitting(self):
        # Wait for classifier to be fitted
        if not self._clf_f:
            self._check_fitting_timer.stop()
            return
        if not self._clf_f.done():
            self._predicted_status_bar.showMessage("Updating (Fitting) ...")
            return
        self._check_fitting_timer.stop()
        
        #  Fetch classifier
        clf_f = self._clf_f
        self._clf_f = None
        try:
            self._clf = clf_f.result()
        except Exception as e:
            msg_box = QMessageBox(QMessageBox.Warning,
                    "Fetching fitted classifier failed",
                    "I was trying to receive the fitted classifier, but "
                    "suddenly a wild %s appeared. It shouted: %s. But I "
                    "escaped luckily."
                    % (type(e).__name__, e),
                    QMessageBox.Ok)
            msg_box.setDetailedText(traceback.format_exc())
            msg_box.exec_()
            return

        self.export_classifier_action.setEnabled(True)
        self._predicted_status_bar.showMessage("Updated on %s" \
                % str(QTime.currentTime().toString(Qt.SystemLocaleShortDate)))

        self._queue_wait_predicting()

    def _queue_wait_predicting(self):
        # Stop previous predicting job
        self._show_predict_timer.stop()
        if self._y_f is not None:
            self._y_f.cancel()
        self._y_f = None
        
        # Start new predicting job
        selection = self._data_table.selectionModel().selectedIndexes()
        if not selection:
            return

        if self.data_model.featureMaps(selection[0].row()) is None:
            self._predicted_scene.setLabels(np.empty((0, 0), 'uint8'))
            self._predicted_status_bar.showMessage("Add features!")
            return

        X = self.data_model.featureMaps(selection[0].row()).result()
        self._y_f = self._executor.schedule(feature.predict_feature_maps, \
                (self._clf, X))
        
        self._show_predict_timer.start()

    def _check_predicting(self):
        # Wait for predicting to be available
        if not self._y_f:
            self._show_predict_timer.stop()
            return
        if not self._y_f.done():
            return
        self._show_predict_timer.stop()
        
        # Fetch prediction
        y_f = self._y_f
        self._y_f = None
        try:
            y = y_f.result()
        except Exception as e:
            msg_box = QMessageBox(QMessageBox.Warning,
                    "Fetching prediction failed",
                    "I was trying to receive the prediction, but suddenly a "
                    "wild %s appeared. It shouted: %s. But I escaped "
                    "luckily."
                    % (type(e).__name__, e),
                    QMessageBox.Ok)
            msg_box.setDetailedText(traceback.format_exc())
            msg_box.exec_()
            return
        
        # Draw prediction
        self._predicted_scene.setLabels(y + 1)

    def _classifier_changed(self):
        if self._clf_f is not None:
            self._clf_f.cancel()
        self._clf_f = None

        self._check_features()
    
    def _add_image_enhancer(self):
        selected = self._image_enhancers.selectionModel().selection()
        if len(selected.indexes()) == 1:
            index = selected.indexes()[0]
        elif len(selected.indexes()) == 0:
            index = QModelIndex()
        else:
            raise RuntimeError("Bug: Make image enhancers view only select " \
                    "one item")

        if not index.isValid():
            return

        items = {fc.name : fc for fc in self.IMAGE_ENHANCER_CLASSES}

        fc_name, ok = QInputDialog.getItem(self, "Add image_enhancer", \
                "Image enhancer",
                sorted(items.keys()), 0, False)
        if ok:
            command = NewImageEnhancerCommand(self, index, items[str(fc_name)]())
            self._undo_stack.push(command)
    
    def _remove_image_enhancers(self):
        indexes = filter(lambda index: \
                self.image_enhancers_model.parent(index).isValid() == True, \
                self._image_enhancers.selectedIndexes())
        indexes = sorted(indexes, key=lambda index:index.row())
        if not indexes:
            return
        command = DeleteImageEnhancerCommand(self, indexes)
        self._undo_stack.push(command)

    def _image_enhancers_selected(self, selected, deselected):
        if len(selected.indexes()) == 0:
            pipeline = []
        elif len(selected.indexes()) == 1:
            index = selected.indexes()[0]
            pipeline = []
            while index.isValid():
                pipeline = [self.image_enhancers_model \
                        .image_enhancer(index)] + pipeline
                index = self.image_enhancers_model.parent(index)
        else:
            raise RuntimeError("Bug: Make data table only select one item")
        
        # Grab data
        selected = self._data_table.selectionModel().selection()
        if len(selected.indexes()) == 0:
            data = np.empty((0, 0, 3), 'uint8')
        elif len(selected.indexes()) == 1:
            index = selected.indexes()[0]
            row = index.row()
            data = self.data_model.image(row)
            
            # Apply image enhancers
            for ie in pipeline:
                data = ie.apply(data)
        
        # Set images
        self._data_scene.setData(data)
        self._training_scene.setData(data)
        self._predicted_scene.setData(data)

    def _configure_image_enhancer(self):
        if len(self._image_enhancers.selectedIndexes()) == 0:
            return
        elif len(self._image_enhancers.selectedIndexes()) == 1:
            index = self._image_enhancers.selectedIndexes()[0]
        else:
            raise RuntimeError("Bug: Make image_enhancers table only select " \
                    "one item")

        params = self.image_enhancers_model.params(index)
        types = {key: type(value) for key, value in params.items()}
        names = {key: key for key in params}

        dialog = ImageEnhancerConfigureDialog(params, types, names, self)
        dialog.setWindowTitle('Configure')
        
        dialog.exec_()
        
        command = ModifyImageEnhancerCommand(self, index, dialog.params())
        self._undo_stack.push(command)

    def _image_enhancers_changed(self):
        selected = self._data_table.selectionModel().selection()

        if len(selected.indexes()) == 0:
            data = np.empty((0, 0, 3), 'uint8')
        elif len(selected.indexes()) == 1:
            index = selected.indexes()[0]
            row = index.row()
            data = self.data_model.image(row)
            
            # Apply image enhancers
            selected = self._image_enhancers.selectionModel().selection()
            if len(selected.indexes()) == 1:
                index = selected.indexes()[0]
                pipeline = []
                while index.isValid():
                    pipeline = [self.image_enhancers_model \
                            .image_enhancer(index)] + pipeline
                    index = self.image_enhancers_model.parent(index)
                for ie in pipeline:
                    data = ie.apply(data)
        else:
            raise RuntimeError("Bug: Make data table only select one item")

        self._data_scene.setData(data)
        self._training_scene.setData(data)
        self._predicted_scene.setData(data)
    
    def _recalculate_features(self):
        # Invalidate last classifier
        self._clf = None
        if self._clf_f is not None:
            self._clf_f.cancel()
        self._clf_f = None
        params = self.feature_model.classifier_params()

        feature_strategies = feature.construct_features(params)

        for row in range(self.data_model.rowCount()):
            data = self.data_model.image(row)
            self.data_model.setFeatureMaps(row, \
                    self._calculate_features(feature_strategies, data))
        self._queue_wait_features()
        
    #Only used for export to convert classifier to BGR
    def _recalculate_features_BGR(self):
        # Invalidate last classifier
        self._clf = None
        if self._clf_f is not None:
            self._clf_f.cancel()
        self._clf_f = None
        
        params = self.feature_model.classifier_params()
        feature_strategies = feature.construct_features(params)
        
        for row in range(self.data_model.rowCount()):
            rgb_data = self.data_model.image(row)
            data=np.empty_like(rgb_data)
            data[:,:,0] = rgb_data[:,:,2]
            data[:,:,1] = rgb_data[:,:,1]
            data[:,:,2] = rgb_data[:,:,0]
            
            self.data_model.setFeatureMaps(row, \
                    self._calculate_features(feature_strategies, data))
        self._queue_wait_features()
    
    def _calculate_features(self, feature_strategies, data):
        feature_map_sizes = [feature_strategy.num_feature_maps(data.shape[2]) \
                for feature_strategy in feature_strategies]
        X_shape = data.shape[:2] + (sum(feature_map_sizes),)
        if X_shape[2] != 0:
            return self._executor.schedule( \
                    feature.get_feature_maps, \
                    (feature_strategies, data))
        else:
            return None
    
    def _save_image(self):
        data = self._data_scene.data()
        filename = QFileDialog.getSaveFileName(self, "Save as", "", \
                "PNG image (*.png)")[0]
        filename = str(filename)
        if not filename:
            return
        cv2.imwrite(filename, data[..., ::-1])

    def _save_labels(self):
        labels = self._training_scene.labels()
        labels_image = np.zeros(labels.shape + (3, ), 'uint8')
        
        for row in xrange(self.labels_model.rowCount()):
            labels_image[np.where(labels == row + 1)] = \
                    self.labels_model.colour(row)
        filename = QFileDialog.getSaveFileName(self, "Save as", "", \
                "PNG image (*.png);;NumPy Array (*.npy)")[0]
        filename = str(filename)
        if not filename:
            return
        if filename.endswith('.png'):
            cv2.imwrite(filename, labels_image[..., ::-1])
        elif filename.endswith('.npy'):
            np.save(filename, labels)
        else:
            msg_box = QMessageBox(QMessageBox.Critical, "Cannot save file",
                    "Unknown extension in %s." % filename, \
                    QMessageBox.Ok)
            msg_box.exec_()
            return
    
    def _save_prediction(self):
        labels = self._predicted_scene.labels()
        labels_image = np.zeros(labels.shape + (3, ), 'uint8')
        
        for row in xrange(self.labels_model.rowCount()):
            labels_image[np.where(labels == row + 1)] = \
                    self.labels_model.colour(row)
        filename = QFileDialog.getSaveFileName(self, "Save as", "", \
                "PNG image (*.png);;NumPy Array (*.npy)")[0]
        filename = str(filename)
        if not filename:
            return
        if filename.endswith('.png'):
            cv2.imwrite(filename, labels_image[..., ::-1])
        elif filename.endswith('.npy'):
            np.save(filename, labels)
        else:
            msg_box = QMessageBox(QMessageBox.Critical, "Cannot save file",
                    "Unknown extension in %s." % filename, \
                    QMessageBox.Ok)
            msg_box.exec_()
            return

    def _about(self):
        QMessageBox.information(self, "About", \
                "Version %s" % version.__version__)

