# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

class NewImageEnhancerCommand(QUndoCommand):

    def __init__(self, mainwindow, index, image_enhancer, parent=None):
        QUndoCommand.__init__(self, "New image enhancer", parent)

        self._mainwindow = mainwindow
        self._index = index
        self._image_enhancer = image_enhancer

    def redo(self):
        self._mainwindow.image_enhancers_model.append(self._image_enhancer, \
                self._index)
        self._mainwindow._image_enhancers.expand(self._index)

    def undo(self):
        self._mainwindow.image_enhancers_model.pop(self._index)

class DeleteImageEnhancerCommand(QUndoCommand):

    def __init__(self, mainwindow, indexes, parent=None):
        QUndoCommand.__init__(self, "Delete image enhancer", parent)
        
        self._mainwindow = mainwindow
        self._parents = [self._mainwindow.image_enhancers_model.parent(index) \
                for index in indexes]
        self._rows = [index.row() for index in indexes]

        # Save labels state
        self._image_enhancers = \
                [(self._mainwindow.image_enhancers_model.image_enhancer(index),)
                 for index in indexes]

    def redo(self):
        for parent, row in zip(self._parents, self._rows)[::-1]:
            self._mainwindow.image_enhancers_model.removeRows(row, 1, parent)
        
    def undo(self):
        for parent, row, (image_enhancer ,) \
                in zip(self._parents, self._rows, self._image_enhancers):
            self._mainwindow.image_enhancers_model \
                    .insert(row, image_enhancer, parent)
            self._mainwindow._image_enhancers.expand(parent)

class ModifyImageEnhancerCommand(QUndoCommand):

    def __init__(self, mainwindow, index, params, parent=None):
        QUndoCommand.__init__(self, "Modify image enhancer", parent)
        
        self._mainwindow = mainwindow
        self._index = index
        self._old_params = self._mainwindow.image_enhancers_model \
                .params(self._index)
        self._params = params

    def redo(self):
        self._mainwindow.image_enhancers_model \
                .set_params(self._index, self._params)
        
    def undo(self):
        self._mainwindow.image_enhancers_model \
                .set_params(self._index, self._old_params)


