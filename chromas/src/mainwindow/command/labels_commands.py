# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import numpy as np

_new_label_num = 0

class NewLabelCommand(QUndoCommand):

    COLOURS = [Qt.red, Qt.blue, Qt.green, \
            Qt.cyan, Qt.magenta, Qt.yellow, \
            Qt.darkRed, Qt.darkGreen, Qt.darkBlue, \
            Qt.darkCyan, Qt.darkMagenta, Qt.darkYellow]
    
    def __init__(self, mainwindow, data_selection, parent=None):
        QUndoCommand.__init__(self, "New label", parent)

        self._mainwindow = mainwindow
        self._data_selection = data_selection

    def redo(self):
        self._mainwindow._data_table.selectionModel().select(self._data_selection, \
                QItemSelectionModel.SelectCurrent)
        colour = QColor(NewLabelCommand.COLOURS[ \
                _new_label_num % len(NewLabelCommand.COLOURS)])
        name = "Label " + str(_new_label_num)
        self._mainwindow.labels_model.append(name, \
                (colour.red(), colour.green(), colour.blue()))
        global _new_label_num
        _new_label_num += 1

    def undo(self):
        self._mainwindow._data_table.selectionModel().select(self._data_selection, \
                QItemSelectionModel.SelectCurrent)
        self._mainwindow.labels_model.pop()
        global _new_label_num
        _new_label_num -= 1

class DeleteLabelsCommand(QUndoCommand):

    def __init__(self, mainwindow, data_selection, indexes, parent=None):
        QUndoCommand.__init__(self, "Delete labels", parent)
        
        self._mainwindow = mainwindow
        self._indexes = indexes

        # Save labels state
        self._data_selection = data_selection
        self._previous_labels = [self._mainwindow.data_model.labels(row).copy() \
                for row in range(self._mainwindow.data_model.rowCount())]

        # Save label names and colour
        self._label_names = [mainwindow.labels_model.name(index) \
                for index in self._indexes]
        self._label_colours = [mainwindow.labels_model.colour(index) \
                for index in self._indexes]

    def redo(self):
        self._mainwindow._data_table.selectionModel().select(self._data_selection, \
                QItemSelectionModel.SelectCurrent)
        for index in self._indexes[::-1]:
            self._mainwindow.labels_model.removeRows(index, 1)
        
    def undo(self):
        self._mainwindow._data_table.selectionModel().select(self._data_selection, \
                QItemSelectionModel.SelectCurrent)
        for index, name, colour \
                in zip(self._indexes, self._label_names, self._label_colours):
            self._mainwindow.labels_model.insert(index, name, colour)
        for row, labels in enumerate(self._previous_labels):
            self._mainwindow.data_model.labels(row)[:] = labels
        self._mainwindow._training_scene.repaintLabels()

class MergeLabelsCommand(QUndoCommand):

    def __init__(self, mainwindow, data_selection, current_index, indexes, \
            parent=None):
        QUndoCommand.__init__(self, "Merge labels", parent)

        self._mainwindow = mainwindow
        self._indexes = indexes
        self._current_index = current_index

        # Save labels state
        self._data_selection = data_selection
        self._previous_labels = [self._mainwindow.data_model.labels(row).copy() \
                for row in range(self._mainwindow.data_model.rowCount())]

        # Save label names and colour
        self._label_names = [mainwindow.labels_model.name(index) \
                for index in self._indexes]
        self._label_colours = [mainwindow.labels_model.colour(index) \
                for index in self._indexes]

    def redo(self):
        self._mainwindow._data_table.selectionModel().select(self._data_selection, \
                QItemSelectionModel.SelectCurrent)

        # Add a new label
        if self._current_index not in self._indexes:
            colour = QColor(*self._mainwindow.labels_model.colour( \
                    self._current_index))
            name = self._mainwindow.labels_model.name(self._current_index)
            self._mainwindow.labels_model.append(name, \
                    (colour.red(), colour.green(), colour.blue()))
            global _new_label_num
            _new_label_num += 1
            added_index = self._mainwindow.labels_model.rowCount()
        else:
            added_index = self._current_index + 1
        
        # Set colours to new labels
        for row in range(self._mainwindow.data_model.rowCount()):
            labels = self._mainwindow.data_model.labels(row)
            for index in self._indexes:
                if self._current_index in self._indexes:
                    labels[np.where(labels == index + 1)] = added_index
        
        # Remove old labels
        for index in self._indexes[::-1]:
            if index != self._current_index:
                self._mainwindow.labels_model.removeRows(index, 1)
    
    def undo(self):
        self._mainwindow._data_table.selectionModel().select(self._data_selection, \
                QItemSelectionModel.SelectCurrent)
        
        # Restore old labels
        for index, name, colour \
                in zip(self._indexes, self._label_names, self._label_colours):
            if index != self._current_index:
                self._mainwindow.labels_model.insert(index, name, colour)

        # Restore labels
        for row, labels in enumerate(self._previous_labels):
            self._mainwindow.data_model.labels(row)[:] = labels
        
        # Remove label
        if self._current_index not in self._indexes:
            self._mainwindow.labels_model.pop()
            global _new_label_num
            _new_label_num -= 1

        self._mainwindow._training_scene.repaintLabels()

