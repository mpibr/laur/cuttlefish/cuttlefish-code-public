# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

import cv2
import numpy as np
import itertools
from chromas import feature

class ImportDataCommand(QUndoCommand):

    def __init__(self, mainwindow, data, parent=None):
        QUndoCommand.__init__(self, "Import data", parent)

        self._mainwindow = mainwindow
        self._data = data

    def redo(self):
        params = self._mainwindow.feature_model.classifier_params()
        feature_strategies = feature.construct_features(params)
        for name, image in self._data:
            self._mainwindow.data_model.append(name, image, \
                    np.zeros(image.shape[:2], 'uint8'), \
                    self._mainwindow._calculate_features(feature_strategies, \
                    image))

    def undo(self):
        for _ in self._data:
            self._mainwindow.data_model.pop()

class DeleteDataCommand(QUndoCommand):
    
    def __init__(self, mainwindow, indexes, parent=None):
        QUndoCommand.__init__(self, "Delete data", parent)

        self._mainwindow = mainwindow
        self._indexes = indexes
        self._data = [(index, \
                       self._mainwindow.data_model.name(index), \
                       self._mainwindow.data_model.image(index), \
                       self._mainwindow.data_model.labels(index))
                      for index in self._indexes]

    def redo(self):
        for index in self._indexes[::-1]:
            self._mainwindow.data_model.removeRows(index, 1)

    def undo(self):
        params = self._mainwindow.feature_model.classifier_params()
        feature_strategies = feature.construct_features(params)
        for index, name, image, labels in self._data:
            self._mainwindow.data_model.insert(index, name, image, labels, \
                    self._mainwindow._calculate_features(feature_strategies, \
                    image))

class ImportDocumentCommand(QUndoCommand):
    
    def __init__(self, mainwindow, label_colours, label_names, \
            data_labels, parent=None):
        QUndoCommand.__init__(self, "Import document", parent)

        self._mainwindow = mainwindow
        self._label_colours = label_colours
        self._label_names = label_names
        self._data_labels = data_labels

    def redo(self):
        label_offset = self._mainwindow.labels_model.rowCount()

        # Add labels
        for name, colour in itertools.izip(self._label_names, \
                self._label_colours):
            self._mainwindow.labels_model.append(name, colour)

        # Add data
        params = self._mainwindow.feature_model.classifier_params()
        feature_strategies = feature.construct_features(params)
        for name, image, labels in self._data_labels:
            labels[labels > 0] += label_offset
            self._mainwindow.data_model.append(name, image, labels, \
                    self._mainwindow._calculate_features(feature_strategies, \
                    image))

    def undo(self):
        # Remove data
        for _ in self._data_labels:
            self._mainwindow.data_model.pop()

        # Remove labels
        for _ in self._label_colours:
            self._mainwindow.labels_model.pop()

