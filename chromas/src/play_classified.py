#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

import argparse
import cv2
import h5py
import numpy as np
from tqdm import tqdm
import time

def iter_buffered(dataset):
    '''
    Iterates through a hdf5 dataset using a local buffer for efficient reading.
    '''

    length = len(dataset)
    chunksize = dataset.chunks[0]
    chunk_ranges = [(s, min(length, s + chunksize)) \
            for s in range(0, length, chunksize)]
    for start, end in chunk_ranges:
        chunk = dataset[start : end]
        for item in chunk:
            yield item

def main():
    p = argparse.ArgumentParser('Play segmentation')
    import pdb; pdb.set_trace()
    # Input/output
    p.add_argument('segmentation', help='Segmentation file')
    p.add_argument('--crop', help='x,y,width,height')
    p.add_argument('--zoom', default=1., type=float)
    p.add_argument('--fps', type=float)
    p.add_argument('--codec', default="MP4V")
    p.add_argument('--output')
    args = p.parse_args()
    
    # Open up the segmentation
    segmentation_file = h5py.File(args.segmentation, 'r')
    num_frames, height, width, tmp = segmentation_file['segmentation'].shape
    if args.fps is not None:
        fps = args.fps
    else:
        fps = segmentation_file.attrs['fps']
    label_colours = segmentation_file.attrs['label_colours']

    if args.crop is not None:
        crop_x, crop_y, crop_width, crop_height = \
                (int(s) for s in args.crop.split(','))
    else:
        crop_x, crop_y, crop_width, crop_height = (0, 0, width, height)

    out_shape = (int(crop_width * args.zoom + .5), \
                int(crop_height * args.zoom + .5))
    if args.output is not None:
        if args.codec == 'H264':
            fourcc = 0x21  # https://stackoverflow.com/q/34024041
        else:
            if cv2.__version__.split('.')[0] >= '3':
                fourcc = cv2.VideoWriter_fourcc(*args.codec)
            else:
                fourcc = cv2.cv.CV_FOURCC(*args.codec)
        wr = cv2.VideoWriter(args.output, fourcc, fps, out_shape)
    else:
        cv2.namedWindow('segmentation',cv2.WINDOW_NORMAL)
        cv2.resizeWindow('segmentation', out_shape[0], out_shape[1])
    
    segmentation_iter = iter_buffered(segmentation_file['segmentation'])

    last_t = -float('inf')
    for segmentation in tqdm(segmentation_iter, \
            total=len(segmentation_file['segmentation'])):
      #  img = np.ones((crop_height, crop_width) + (3,), 'uint8')
       # for label, colour in enumerate(label_colours):
         #   img[ segmentation[crop_y : crop_y+crop_height, \
            #                crop_x : crop_x+crop_width] == label ] = colour
        img=np.squeeze(segmentation) #just worry about 1 label for now
        if args.zoom is not None:
            img = cv2.resize(img, out_shape, interpolation = cv2.INTER_NEAREST)
        if args.output is not None:
            assert(img.shape[:2][::-1] == out_shape)
            wr.write(img)
        else:
            cv2.imshow("segmentation", img)
            wait_t = int(max(last_t - time.time() + 1 / fps, 1))
            cv2.waitKey(wait_t)
            last_t = time.time()
    if args.output is not None:
        wr.release()
    segmentation_file.close()
