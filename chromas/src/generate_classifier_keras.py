#!/usr/bin/env python

import argparse
import pickle
import numpy as np
import h5py

from keras.models import load_model
from . import feature

def export_classifier(keras_model, output_classifier, chromas_annotation=None, \
    classes=None, lambda_str=None):
    '''
    add argmax
    '''

    try:
        model = load_model(keras_model)
    except ValueError:
        err = "Error loading model. " +\
            "If first layer after input is a lambda layer, try passing as lambda string."
        raise ValueError(err)

    clf = feature.KerasClassifier(model)

    if lambda_str is not None:
        clf.lambda_str = lambda_str

    if chromas_annotation is not None:
        with h5py.File(chromas_annotation, 'r') as hf:
            label_colours = [(str(hf['labels'][entry].attrs['name']), \
                            hf['labels'][entry].attrs['colour'][::-1]) \
                            for entry in hf['labels'].attrs['entries']]
    elif classes is not None:
        colours = np.repeat(np.expand_dims( 
            np.linspace(0, 255, num=len(classes), dtype='uint8'), -1), 3, axis=1)
        label_colours = zip(classes, colours)
    else:
        raise RuntimeError('Provide either an annotation file or a list of classes.')
        
    classifier_doc = {\
            "label_colours" : label_colours, \
            "classifier" : clf}
    
    with open(output_classifier, 'wb') as f:
        pickle.dump(classifier_doc, f)

def main():
    p = argparse.ArgumentParser('Classify video')
    p.add_argument('--keras-model', required=True, help='Keras model, h5py')
    g = p.add_mutually_exclusive_group(required=True)
    g.add_argument('--annotation', help='Chromas annotation, h5py')
    g.add_argument('--classes', nargs='+', help='List of classes')
    p.add_argument('--lambda-str', help='Lambda function for input, e.g. lambda x: x/255.')
    p.add_argument('output', help='Output classifier file')
    args = p.parse_args()

    export_classifier(args.keras_model, args.output, args.annotation, \
        args.classes, args.lambda_str)
