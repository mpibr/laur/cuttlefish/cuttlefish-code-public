# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

# Import h5py somehow this is messy
import sys
import site
sys.path = [site.getusersitepackages()] + sys.path
import h5py

from PyQt5.QtCore import *
import image_enhancers
import item_model.image_enhancers_model
import itertools

class NotAChromasFile(Exception):
    def __init__(self, msg):
        Exception.__init__(self, msg)

def __read_hdf5(f):
    # Check for signature
    try:
        if f.attrs['signature'] != 'chromas':
            raise IOError("Not a chromas file")
    except KeyError:
            raise IOError("Not a chromas file")
    
    # Parse version
    try:
        version = f.attrs['version']
    except KeyError:
        version = '0.0.1'
    mayor, minor, revision = (int(s) for s in version.split('.'))

    if mayor == 0:
        data = f['data'].value
        labels = f['labels'].value
        try:
            label_names = [str(c) for c in f['label_names']]
        except KeyError:
            label_names = ['Label %d' % (l + 1) for l in np.unique(labels)]
        label_colours = [tuple((int(comp) for comp in c)) \
                for c in f['label_colours']]
        feature_params = {}
        return {'data_labels' : [('Unnamed', data, labels)], \
                'label_names' : label_names, \
                'label_colours' : label_colours, \
                'feature_params' : feature_params,
                'image_enhancers' : None, \
                'clf_params': None}

    elif mayor == 1:
        data_labels = [(f['data'][uuid].attrs['name'], \
                        f['data'][uuid]['image'].value, \
                        f['data'][uuid]['labels'].value) \
                       for uuid in f['data'].attrs['entries']]
        label_names = [label_name.tostring() \
                       for label_name in f['label_names']]
        label_colours = [tuple((int(comp) for comp in c)) \
                       for c in f['label_colours']]
        feature_params = {}
        return {'data_labels' : data_labels, \
                'label_names' : label_names, \
                'label_colours' : label_colours, \
                'feature_params' : feature_params, \
                'image_enhancers' : None, \
                'clf_params': None}

    elif mayor == 2:
        data_labels = [(f['data'][uuid].attrs['name'], \
                        f['data'][uuid]['image'].value, \
                        f['data'][uuid]['labels'].value) \
                       for uuid in f['data'].attrs['entries']]
        label_names = [f['labels'][uuid].attrs['name'] \
                       for uuid in f['labels'].attrs['entries']]
        label_colours = [tuple((int(comp) \
                         for comp in f['labels'][uuid].attrs['colour'])) \
                         for uuid in f['labels'].attrs['entries']]
        feature_params = {}
        return {'data_labels' : data_labels, \
                'label_names' : label_names, \
                'label_colours' : label_colours, \
                'feature_params' : feature_params, \
                'image_enhancers' : None, \
                'clf_params': None}
    elif mayor == 3:
        data_labels = [(f['data'][uuid].attrs['name'], \
                        f['data'][uuid]['image'].value, \
                        f['data'][uuid]['labels'].value) \
                       for uuid in f['data'].attrs['entries']]
        label_names = [f['labels'][uuid].attrs['name'] \
                       for uuid in f['labels'].attrs['entries']]
        label_colours = [tuple((int(comp) \
                         for comp in f['labels'][uuid].attrs['colour'])) \
                         for uuid in f['labels'].attrs['entries']]
        feature_params = {}

        def read_node(group, parent):
            class_name = group.attrs['name']
            params = {key : value \
                     for key, value \
                     in group['params'].attrs.items()}
            node = item_model.image_enhancers_model.ImageEnhancerNode(
                    image_enhancers.__dict__[class_name](**params), \
                    parent)

            uuids = group.attrs['entries']
            for uuid in uuids:
                child_node = read_node(group[uuid], node)
                node.append(child_node)
            return node
        
        root_node = item_model.image_enhancers_model.ImageEnhancerNode()
        uuids = f['image_enhancers'].attrs['entries']
        for uuid in uuids:
            child_node = read_node(f['image_enhancers'][uuid], root_node)
            root_node.append(child_node)

        return {'data_labels': data_labels, \
                'label_names': label_names, \
                'label_colours': label_colours, \
                'feature_params': feature_params, \
                'image_enhancers': root_node, \
                'clf_params': None}

    elif mayor == 4:
        data_labels = [(f['data'][uuid].attrs['name'], \
                        f['data'][uuid]['image'].value, \
                        f['data'][uuid]['labels'].value) \
                       for uuid in f['data'].attrs['entries']]
        label_names = [f['labels'][uuid].attrs['name'] \
                       for uuid in f['labels'].attrs['entries']]
        label_colours = [tuple((int(comp) \
                         for comp in f['labels'][uuid].attrs['colour'])) \
                         for uuid in f['labels'].attrs['entries']]
        feature_params = {}

        def read_node(group, parent):
            class_name = group.attrs['name']
            params = {key : value \
                     for key, value \
                     in group['params'].attrs.items()}
            node = item_model.image_enhancers_model.ImageEnhancerNode(
                    image_enhancers.__dict__[class_name](**params), \
                    parent)

            uuids = group.attrs['entries']
            for uuid in uuids:
                child_node = read_node(group[uuid], node)
                node.append(child_node)
            return node
        
        root_node = item_model.image_enhancers_model.ImageEnhancerNode()
        uuids = f['image_enhancers'].attrs['entries']
        for uuid in uuids:
            child_node = read_node(f['image_enhancers'][uuid], root_node)
            root_node.append(child_node)
        
        clf_params = {k: v for k, v in f['clf_params'].attrs.items()} \
            if 'clf_params' in f else None

        return {'data_labels': data_labels, \
                'label_names': label_names, \
                'label_colours': label_colours, \
                'feature_params': feature_params, \
                'image_enhancers': root_node, \
                'clf_params': clf_params}
    
    elif mayor == 5:
        data_labels = [(f['data'][uuid].attrs['name'], \
                        f['data'][uuid]['image'].value, \
                        f['data'][uuid]['labels'].value) \
                       for uuid in f['data'].attrs['entries']]
        label_names = [f['labels'][uuid].attrs['name'] \
                       for uuid in f['labels'].attrs['entries']]
        label_colours = [tuple((int(comp) \
                         for comp in f['labels'][uuid].attrs['colour'])) \
                         for uuid in f['labels'].attrs['entries']]
        feature_params = {k: v for k, v in f['feature_params'].attrs.items()}

        def read_node(group, parent):
            class_name = group.attrs['name']
            params = {key : value \
                     for key, value \
                     in group['params'].attrs.items()}
            node = item_model.image_enhancers_model.ImageEnhancerNode(
                    image_enhancers.__dict__[class_name](**params), \
                    parent)

            uuids = group.attrs['entries']
            for uuid in uuids:
                child_node = read_node(group[uuid], node)
                node.append(child_node)
            return node
        
        root_node = item_model.image_enhancers_model.ImageEnhancerNode()
        uuids = f['image_enhancers'].attrs['entries']
        for uuid in uuids:
            child_node = read_node(f['image_enhancers'][uuid], root_node)
            root_node.append(child_node)
        
        clf_params = {k: v for k, v in f['clf_params'].attrs.items()} \
                if 'clf_params' in f else None

        return {'data_labels': data_labels, \
                'label_names': label_names, \
                'label_colours': label_colours, \
                'feature_params': feature_params, \
                'image_enhancers': root_node, \
                'clf_params': clf_params}

    else:
        raise RuntimeError("Cannot open the file, upgrade to a new version!")

def load(filename):
    # Open the file, make sure it is closed, even when an exception occurs
    f = h5py.File(str(filename), 'r')
    try:
        return __read_hdf5(f)
    finally:
        f.close()

def __write_hdf5(f, doc):
    f.attrs['version'] = '5.0.0'
    f.attrs['signature'] = 'chromas'
    
    data_group = f.create_group('data')
    uuids = [str(QUuid.createUuid().toString())[1:-1] \
             for _ in doc['data_labels']]
    data_group.attrs['entries'] = uuids
    for uuid, (data_name, image, labels) in itertools.izip(uuids, \
            doc['data_labels']):
        data_labels_group = data_group.create_group(uuid)
        data_labels_group.attrs['name'] = data_name
        data_labels_group.create_dataset('image', data=image, \
                compression='gzip')
        data_labels_group.create_dataset('labels', data=labels, \
                compression='gzip')

    label_group = f.create_group('labels')
    uuids = [str(QUuid.createUuid().toString())[1:-1] \
             for _ in doc['label_names']]
    label_group.attrs['entries'] = uuids
    for uuid, label_name, colour in itertools.izip(uuids, \
            doc['label_names'], doc['label_colours']):
        name_colour_group = label_group.create_group(uuid)
        name_colour_group.attrs['name'] = label_name
        name_colour_group.attrs['colour'] = colour


    feature_params_group = f.create_group('feature_params')
    for param, value in doc['feature_params'].items():
        feature_params_group.attrs[param] = value

    def write_node(group, node):
        uuids = [str(QUuid.createUuid().toString())[1:-1] \
                 for _ in xrange(node.rowCount())]
        group.attrs['entries'] = uuids

        class_name = node.image_enhancer().__class__.__name__
        params = {k : v for k, v in node.image_enhancer().__dict__.items() \
                  if not k.startswith('_')}
        params_group = group.create_group('params')

        group.attrs['name'] = class_name
        for key, value in params.items():
            params_group.attrs[key] = value

        for row in xrange(node.rowCount()):
            child_node = node.child(row)
            child_group = group.create_group(uuids[row])
            write_node(child_group, child_node)

    image_enhancers_group = f.create_group('image_enhancers')
    uuids = [str(QUuid.createUuid().toString())[1:-1] \
             for _ in xrange(doc['image_enhancers'].rowCount())]
    image_enhancers_group.attrs['entries'] = uuids
    for row in xrange(doc['image_enhancers'].rowCount()):
        uuid = uuids[row]
        child_node = doc['image_enhancers'].child(row)
        child_group = image_enhancers_group.create_group(uuid)
        write_node(child_group, child_node)
    
    if doc['clf_params'] is not None:
        clf_params_group = f.create_group('clf_params')
        for param, value in doc['clf_params'].items():
            clf_params_group.attrs[param] = value

def dump(filename, doc):
    # Open the file, make sure it is closed, even when an exception occurs
    f = h5py.File(str(filename), 'w')
    try:
        __write_hdf5(f, doc)
    finally:
        f.close()

