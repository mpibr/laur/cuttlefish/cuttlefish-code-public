# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

import cv2
import numpy as np
import math
import itertools

from .item_model.parameter_model import RootSpace, ChoiceSpace, BoolSpace, \
        UniformFloatSpace, UniformIntSpace

class __ColorspaceFeatureStrategy(object):

    def __init__(self, code):
        self.__code = code
    
    def num_feature_maps(self, num_channels):
        return 3

    def get_feature_maps(self, image, out=None):
        if image.shape[2] != 3:
            raise RuntimeError("Colourspace conversion not supported")
        if out is not None:
            out[:] = cv2.cvtColor(image, self.__code)
        else:
            out = cv2.cvtColor(image, self.__code)
        return out

class RGB:

    def num_feature_maps(self, num_channels):
        return 3

    def get_feature_maps(self, image, out=None):
        if image.shape[2] != 3:
            raise RuntimeError("Colourspace conversion not supported")
        if out is not None:
            out[:] = image
        else:
            out = image
        return out

class HSV(__ColorspaceFeatureStrategy):

    def __init__(self):
        super(HSV, self).__init__(cv2.COLOR_RGB2HSV)

class HLS(__ColorspaceFeatureStrategy):

    def __init__(self):
        super(HLS, self).__init__(cv2.COLOR_RGB2HLS)

class LUV(__ColorspaceFeatureStrategy):

    def __init__(self):
        super(LUV, self).__init__(cv2.COLOR_RGB2LUV)

class YUV(__ColorspaceFeatureStrategy):

    def __init__(self):
        super(YUV, self).__init__(cv2.COLOR_RGB2YUV)

class YCrCb(__ColorspaceFeatureStrategy):

    def __init__(self):
        super(YCrCb, self).__init__(cv2.COLOR_RGB2YCR_CB)

class DifferenceOfGaussians:

    def __init__(self, min_sigma=1, levels=3, step=2, mode='exp'):
        self.__sigmas = []
        sigma = min_sigma
        for _ in range(levels):
            self.__sigmas.append(sigma)
            if mode == 'exp':
                sigma *= step
            elif mode == 'linear':
                sigma += step
            else:
                raise ValueError('Illegal argument for mode')
        self.__sigmas.append(sigma)
        self.__sigmas = np.array(self.__sigmas, 'float32')
    
    def num_feature_maps(self, num_channels):
        return (len(self.__sigmas) - 1) * num_channels

    def get_feature_maps(self, image, out=None):
        if out is None:
            out = np.empty((image.shape[0], image.shape[1], \
                    image.shape[2] * (len(self.__sigmas) - 1)), 'float32')
        s0 = self.__sigmas[0]
        g0 = cv2.GaussianBlur(image, (0, 0), s0)
        g1 = np.empty_like(g0)
        for i, s1 in enumerate(self.__sigmas[1:]):
            s = math.sqrt(s1 * s1 - s0 * s0)
            cv2.GaussianBlur(g0, (0, 0), s, dst=g1)
            n = 2 * s0 / (s1 - s0)
            out[..., i * image.shape[2] : (i + 1) * image.shape[2]] = \
                    n * (g1 - g0)
            g0, g1 = g1, g0
            s0 = s1
        return out

class FastDifferenceOfGaussians:

    def __init__(self, min_sigma=1, levels=3, step=2, mode='exp', n=1):
        self.__sigmas = []
        sigma = min_sigma
        for _ in range(levels):
            self.__sigmas.append(sigma)
            if mode == 'exp':
                sigma *= step
            elif mode == 'linear':
                sigma += step
            else:
                raise ValueError('Illegal argument for mode')
        self.__sigmas.append(sigma)
        self.__sigmas = np.array(self.__sigmas, 'float32')
        self.__n = int(n)
        ws = [math.sqrt(12 * sigma_g ** 2 / self.__n + 1) \
                for sigma_g in self.__sigmas]
        ceils = [math.ceil(w) for w in ws]
        floors = [math.floor(w) for w in ws]
        wus = [ceil if ceil % 2 == 1 else ceil + 1 for ceil in ceils]
        wls = [floor if floor % 2 == 1 else floor - 1 for floor in floors]
        self.__m = np.array([round((12 * sigma_g * sigma_g \
                - self.__n * wl * wl - 4 * self.__n * wl - 3 * self.__n) \
                / (-4 * wl - 4)) \
                for wu, wl, sigma_g in zip(wus, wls, self.__sigmas)])
        self.__wls = np.array([int(wl) for wl in wls])
        self.__wus = np.array([int(wu) for wu in wus])
    
    def num_feature_maps(self, num_channels):
        return (len(self.__sigmas) - 1) * num_channels

    def __approx_gaussian(self, image, sigma_g, m, wl, wu, dst=None):
        if dst is None:
            dst = np.empty_like(image)
        dst[:] = image
        for num in range(0, int(m)):
            cv2.blur(dst, (wl, wl), dst)
        for num in range(0, int(self.__n - m)):
            cv2.blur(dst, (wu, wu), dst)
        return dst

    def get_feature_maps(self, image, out=None):
        if out is None:
            out = np.empty((image.shape[0], image.shape[1], \
                    image.shape[2] * (len(self.__sigmas) - 1)), 'float32')
        s0 = self.__sigmas[0]
        m = self.__m[0]
        wl = self.__wls[0]
        wu = self.__wus[0]
        g0 = self.__approx_gaussian(image, s0, m, wl, wu)
        g1 = np.empty_like(g0)
        for i, (s1, m, wl, wu) in enumerate(zip(self.__sigmas[1:], \
                self.__m, self.__wls, self.__wus)):
            s = math.sqrt(s1 * s1 - s0 * s0)
            self.__approx_gaussian(image, s, m, wl, wu, dst=g1)
            n = 2 * s0 / (s1 - s0)
            out[..., i * image.shape[2] : (i + 1) * image.shape[2]] = \
                n * (g1 - g0)
            g0, g1 = g1, g0
            s0 = s1
        return out

class RotatedFastDifferenceOfGaussians:

    def __init__(self, min_sigma=1, levels=3, step=2, mode='exp', n=1, \
            r00=1, r01=1, r02=1, r10=1, r11=1, r12=1, r20=1, r21=1, r22=1):
        self.__sigmas = []
        sigma = min_sigma
        for _ in range(levels):
            self.__sigmas.append(sigma)
            if mode == 'exp':
                sigma *= step
            elif mode == 'linear':
                sigma += step
            else:
                raise ValueError('Illegal argument for mode')
        self.__sigmas.append(sigma)
        self.__sigmas = np.array(self.__sigmas, 'float32')
        self.__n = int(n)
        ws = [math.sqrt(12 * sigma_g ** 2 / self.__n + 1) \
                for sigma_g in self.__sigmas]
        ceils = [math.ceil(w) for w in ws]
        floors = [math.floor(w) for w in ws]
        wus = [ceil if ceil % 2 == 1 else ceil + 1 for ceil in ceils]
        wls = [floor if floor % 2 == 1 else floor - 1 for floor in floors]
        self.__m = np.array([round((12 * sigma_g * sigma_g \
                - self.__n * wl * wl - 4 * self.__n * wl - 3 * self.__n) \
                / (-4 * wl - 4)) \
                for wu, wl, sigma_g in zip(wus, wls, self.__sigmas)])
        self.__wls = np.array([int(wl) for wl in wls])
        self.__wus = np.array([int(wu) for wu in wus])
        self.__mixing = np.array([[r00, r01, r02], \
                                  [r10, r11, r12], \
                                  [r20, r21, r22]], 'float32')
    
    def num_feature_maps(self, num_channels):
        return (len(self.__sigmas) - 1) * num_channels

    def __approx_gaussian(self, image, sigma_g, m, wl, wu, dst=None):
        if dst is None:
            dst = np.empty_like(image)
        dst[:] = image
        for num in range(0, int(m)):
            cv2.blur(dst, (wl, wl), dst)
        for num in range(0, int(self.__n - m)):
            cv2.blur(dst, (wu, wu), dst)
        return dst

    def get_feature_maps(self, image, out=None):
        if out is None:
            out = np.empty((image.shape[0], image.shape[1], \
                    image.shape[2] * (len(self.__sigmas) - 1)), 'float32')
        s0 = self.__sigmas[0]
        m = self.__m[0]
        wl = self.__wls[0]
        wu = self.__wus[0]
        g0 = self.__approx_gaussian(image, s0, m, wl, wu)
        g1 = np.empty_like(g0)
        for i, (s1, m, wl, wu) in enumerate(zip(self.__sigmas[1:], \
                self.__m, self.__wls, self.__wus)):
            s = math.sqrt(s1 * s1 - s0 * s0)
            self.__approx_gaussian(image, s, m, wl, wu, dst=g1)
            n = 2 * s0 / (s1 - s0)
            out[..., i * image.shape[2] : (i + 1) * image.shape[2]] = \
                np.tensordot(n * (g1 - g0), self.__mixing, 1)
            g0, g1 = g1, g0
            s0 = s1
        return out

class LaplacianOfGaussians:

    def __init__(self, min_sigma=1, levels=3, step=2, mode='exp'):
        self.__sigmas = []
        sigma = min_sigma
        for _ in range(levels):
            self.__sigmas.append(sigma)
            if mode == 'exp':
                sigma *= step
            elif mode == 'linear':
                sigma += step
            else:
                raise ValueError('Illegal argument for mode')
        self.__sigmas = np.array(self.__sigmas, 'float32')
    
    def num_feature_maps(self, num_channels):
        return len(self.__sigmas) * num_channels
    
    def get_feature_maps(self, image, out=None):
        if out is None:
            out = np.empty((image.shape[0], image.shape[1], \
                    image.shape[2] * len(self.__sigmas)), 'float32')
        for i, s in enumerate(self.__sigmas):
            g = cv2.GaussianBlur(image, (0, 0), s)
            out[..., i * image.shape[2] : (i + 1) * image.shape[2]] = \
                    s * cv2.Laplacian(g, cv2.CV_32F)
        return out

class HessianOfGaussians:

    def __init__(self, min_sigma=1, levels=3, step=2, mode='exp'):
        self.__sigmas = []
        sigma = min_sigma
        for _ in range(levels):
            self.__sigmas.append(sigma)
            if mode == 'exp':
                sigma *= step
            elif mode == 'linear':
                sigma += step
            else:
                raise ValueError('Illegal argument for mode')
        self.__sigmas = np.array(self.__sigmas, 'float32')
    
    def num_feature_maps(self, num_channels):
        return len(self.__sigmas) * num_channels
    
    def get_feature_maps(self, image, out=None):
        if out is None:
            out = np.empty((image.shape[0], image.shape[1], \
                    image.shape[2] * len(self.__sigmas)), 'float32')
        for i, s in enumerate(self.__sigmas):
            g = cv2.GaussianBlur(image, (0, 0), s)
            out[..., i * image.shape[2] : (i + 1) * image.shape[2]] = \
                    HogPyramid.__calc_curvature(g)
        return out

    @staticmethod
    def __calc_curvature(image):
        matrix = np.empty(image.shape + (2, 2), 'float32')
        matrix[:, :, :, 0, 0] = cv2.Sobel(image, -1 , 2, 0)
        matrix[:, :, :, 1, 1] = cv2.Sobel(image, -1 , 0, 2)
        matrix[:, :, :, 0, 1] = matrix[:, :, :, 1, 0] = \
                cv2.Sobel(image, -1 , 1, 1)
        return np.linalg.det(matrix)

class Sobel:

    def __init__(self, min_sigma=1, levels=3, step=2, mode='exp'):
        self.__sigmas = []
        sigma = min_sigma
        for _ in range(levels):
            self.__sigmas.append(sigma)
            if mode == 'exp':
                sigma *= step
            elif mode == 'linear':
                sigma += step
            else:
                raise ValueError('Illegal argument for mode')
        self.__sigmas = np.array(self.__sigmas, 'float32')
    
    def num_feature_maps(self, num_channels):
        return len(self.__sigmas) * num_channels
    
    def get_feature_maps(self, image, out=None):
        if out is None:
            out = np.empty((image.shape[0], image.shape[1], \
                    image.shape[2] * len(self.__sigmas)), 'float32')
        for i, s in enumerate(self.__sigmas):
            g = cv2.GaussianBlur(image, (0, 0), s)
            xs = cv2.Sobel(g, -1, 1, 0)
            ys = cv2.Sobel(g, -1, 0, 1)
            out[..., i * image.shape[2] : (i + 1) * image.shape[2]] = \
                    np.sqrt(sx * sx + sy * sy)
        return out

class Harris:
    
    def __init__(self, min_sigma=1, levels=3, step=2, mode='exp', k=0.04):
        self.__sigmas = []
        sigma = min_sigma
        for _ in range(levels):
            self.__sigmas.append(sigma)
            if mode == 'exp':
                sigma *= step
            elif mode == 'linear':
                sigma += step
            else:
                raise ValueError('Illegal argument for mode')
        self.__sigmas = np.array(self.__sigmas, 'float32')
        self.k = k
    
    def num_feature_maps(self, num_channels):
        return len(self.__sigmas) * num_channels

    def get_feature_maps(self, image, out=None):
        if out is None:
            out = np.empty((image.shape[0], image.shape[1], \
                    image.shape[2] * len(self.__sigmas)), 'float32')
        for i, s in enumerate(self.__sigmas):
            g = cv2.GaussianBlur(image, (0, 0), s)
            for ch in range(g.shape[2]):
                out[..., i * image.shape[2] + ch] = \
                        cv2.cornerHarris(g[..., ch], 2, 3, self.k)
        return out

def construct_feature_space():
    root_node = RootSpace()

    dog_node = root_node.add_node( \
            'dog', \
            BoolSpace('dog', 'Difference of Gaussians'))
    dog_node.add_node(True, 'min_sigma',
            UniformFloatSpace('dog_min_sigma', 'Min Sigma', 0.8, 30))
    dog_node.add_node(True, 'levels',
            UniformIntSpace('dog_levels', 'Levels', 1, 16))
    dog_node.add_node(True, 'step',
            UniformFloatSpace('dog_step', 'Step', 1, 4))
    dog_node.add_node(True, 'mode',
            ChoiceSpace('dog_mode', 'Mode', \
                    [('exp', 'Exponential'), \
                     ('linear', 'Linear')]))

    fast_dog_node = root_node.add_node( \
            'fast_dog', \
            BoolSpace('fast_dog', 'Fast Difference of Gaussians'))
    fast_dog_node.add_node(True, 'min_sigma',
            UniformFloatSpace('fast_dog_min_sigma', 'Min Sigma', 0.8, 30))
    fast_dog_node.add_node(True, 'levels',
            UniformIntSpace('fast_dog_levels', 'Levels', 1, 16))
    fast_dog_node.add_node(True, 'step',
            UniformFloatSpace('fast_dog_step', 'Step', 1, 4))
    fast_dog_node.add_node(True, 'mode',
            ChoiceSpace('fast_dog_mode', 'Mode', \
                    [('exp', 'Exponential'), \
                     ('linear', 'Linear')]))
    fast_dog_node.add_node(True, 'n',
            UniformIntSpace('fast_dog_step', 'n', 1, 6))

    rotated_fast_dog_node = root_node.add_node( \
            'rotated_fast_dog', \
            BoolSpace('rotated_fast_dog', 'Rotated Fast Difference of Gaussians'))
    rotated_fast_dog_node.add_node(True, 'min_sigma',
            UniformFloatSpace('rotated_fast_dog_min_sigma', 'Min Sigma', 0.8, 30))
    rotated_fast_dog_node.add_node(True, 'levels',
            UniformIntSpace('rotated_fast_dog_levels', 'Levels', 1, 16))
    rotated_fast_dog_node.add_node(True, 'step',
            UniformFloatSpace('rotated_fast_dog_step', 'Step', 1, 4))
    rotated_fast_dog_node.add_node(True, 'mode',
            ChoiceSpace('rotated_fast_dog_mode', 'Mode', \
                    [('exp', 'Exponential'), \
                     ('linear', 'Linear')]))
    rotated_fast_dog_node.add_node(True, 'n',
            UniformIntSpace('rotated_fast_dog_step', 'n', 1, 6))
    rotated_fast_dog_node.add_node(True, 'r00',
            UniformFloatSpace('rotated_fast_dog_r00', 'r00', -255, 255))
    rotated_fast_dog_node.add_node(True, 'r01',
            UniformFloatSpace('rotated_fast_dog_r01', 'r01', -255, 255))
    rotated_fast_dog_node.add_node(True, 'r02',
            UniformFloatSpace('rotated_fast_dog_r02', 'r02', -255, 255))
    rotated_fast_dog_node.add_node(True, 'r10',
            UniformFloatSpace('rotated_fast_dog_r10', 'r10', -255, 255))
    rotated_fast_dog_node.add_node(True, 'r11',
            UniformFloatSpace('rotated_fast_dog_r11', 'r11', -255, 255))
    rotated_fast_dog_node.add_node(True, 'r12',
            UniformFloatSpace('rotated_fast_dog_r12', 'r12', -255, 255))
    rotated_fast_dog_node.add_node(True, 'r20',
            UniformFloatSpace('rotated_fast_dog_r20', 'r20', -255, 255))
    rotated_fast_dog_node.add_node(True, 'r21',
            UniformFloatSpace('rotated_fast_dog_r21', 'r21', -255, 255))
    rotated_fast_dog_node.add_node(True, 'r22',
            UniformFloatSpace('rotated_fast_dog_r22', 'r22', -255, 255))


    log_node = root_node.add_node( \
            'log', \
            BoolSpace('log', 'Laplacian of Gaussians'))
    log_node.add_node(True, 'min_sigma',
            UniformFloatSpace('log_min_sigma', 'Min Sigma', 0.8, 30))
    log_node.add_node(True, 'levels',
            UniformIntSpace('log_levels', 'Levels', 1, 16))
    log_node.add_node(True, 'step',
            UniformFloatSpace('log_step', 'Step', 1, 4))
    log_node.add_node(True, 'mode',
            ChoiceSpace('log_mode', 'Mode', \
                    [('exp', 'Exponential'), \
                     ('linear', 'Linear')]))

    hog_node = root_node.add_node( \
            'hog', \
            BoolSpace('hog', 'Hessian of Gaussians'))
    hog_node.add_node(True, 'min_sigma',
            UniformFloatSpace('hog_min_sigma', 'Min Sigma', 0.8, 30))
    hog_node.add_node(True, 'levels',
            UniformIntSpace('hog_levels', 'Levels', 1, 16))
    hog_node.add_node(True, 'step',
            UniformFloatSpace('hog_step', 'Step', 1, 4))
    hog_node.add_node(True, 'mode',
            ChoiceSpace('hog_mode', 'Mode', \
                    [('exp', 'Exponential'), \
                     ('linear', 'Linear')]))

    sobel_node = root_node.add_node( \
            'sobel', \
            BoolSpace('sobel', 'Sobel'))
    sobel_node.add_node(True, 'min_sigma',
            UniformFloatSpace('sobel_min_sigma', 'Min Sigma', 0.8, 30))
    sobel_node.add_node(True, 'levels',
            UniformIntSpace('sobel_levels', 'Levels', 1, 16))
    sobel_node.add_node(True, 'step',
            UniformFloatSpace('sobel_step', 'Step', 1, 4))
    sobel_node.add_node(True, 'mode',
            ChoiceSpace('sobel_mode', 'Mode', \
                    [('exp', 'Exponential'), \
                     ('linear', 'Linear')]))

    harris_node = root_node.add_node( \
            'harris', \
            BoolSpace('harris', 'Harris'))
    harris_node.add_node(True, 'min_sigma',
            UniformFloatSpace('harris_min_sigma', 'Min Sigma', 0.8, 30))
    harris_node.add_node(True, 'levels',
            UniformIntSpace('harris_levels', 'Levels', 1, 16))
    harris_node.add_node(True, 'step',
            UniformFloatSpace('harris_step', 'Step', 1, 4))
    harris_node.add_node(True, 'mode',
            ChoiceSpace('harris_mode', 'Mode', \
                    [('exp', 'Exponential'), \
                     ('linear', 'Linear')]))
    harris_node.add_node(True, 'k',
            UniformFloatSpace('harris_k', 'k', 0, 1))

    root_node.add_node('rgb', BoolSpace('rgb', 'RGB'))
    root_node.add_node('hsv', BoolSpace('hsv', 'HSV'))
    root_node.add_node('hls', BoolSpace('hls', 'HLS'))
    root_node.add_node('luv', BoolSpace('luv', 'LUV'))
    root_node.add_node('yuv', BoolSpace('yuv', 'YUV'))
    root_node.add_node('ycrcb', BoolSpace('ycrcb', 'YCrCb'))

    return root_node

def _construct_multiscale(feature_class, params, kwargs={}):
    if params['min_sigma'] is not None:
        kwargs['min_sigma'] = params['min_sigma']
    if params['levels'] is not None:
        kwargs['levels'] = int(params['levels'])
    if params['step'] is not None:
        kwargs['step'] = params['step']
    if params['step'] is not None:
        kwargs['step'] = params['step']
    if params['mode'][0] is not None:
        kwargs['mode'] = params['mode'][0]
    return feature_class(**kwargs)

def _construct_fast_multiscale(feature_class, params, kwargs={}):
    if params['min_sigma'] is not None:
        kwargs['min_sigma'] = params['min_sigma']
    if params['levels'] is not None:
        kwargs['levels'] = int(params['levels'])
    if params['step'] is not None:
        kwargs['step'] = params['step']
    if params['step'] is not None:
        kwargs['step'] = params['step']
    if params['mode'][0] is not None:
        kwargs['mode'] = params['mode'][0]
    if params['n'] is not None:
        kwargs['n'] = int(params['n'])
    return feature_class(**kwargs)

def _construct_rotated_fast_dog(feature_class, params, kwargs={}):
    if params['min_sigma'] is not None:
        kwargs['min_sigma'] = params['min_sigma']
    if params['levels'] is not None:
        kwargs['levels'] = int(params['levels'])
    if params['step'] is not None:
        kwargs['step'] = params['step']
    if params['step'] is not None:
        kwargs['step'] = params['step']
    if params['mode'][0] is not None:
        kwargs['mode'] = params['mode'][0]
    if params['n'] is not None:
        kwargs['n'] = int(params['n'])
    if params['r00'] is not None:
        kwargs['r00'] = params['r00']
    if params['r01'] is not None:
        kwargs['r01'] = params['r00']
    if params['r02'] is not None:
        kwargs['r02'] = params['r00']
    if params['r10'] is not None:
        kwargs['r10'] = params['r00']
    if params['r11'] is not None:
        kwargs['r11'] = params['r00']
    if params['r12'] is not None:
        kwargs['r12'] = params['r00']
    if params['r20'] is not None:
        kwargs['r20'] = params['r00']
    if params['r21'] is not None:
        kwargs['r21'] = params['r00']
    if params['r22'] is not None:
        kwargs['r22'] = params['r00']
    return feature_class(**kwargs)

def construct_features(params):
    features = []
    if params['dog'][0]:
        f = _construct_multiscale(DifferenceOfGaussians, params['dog'][1])
        features.append(f)
    elif params['fast_dog'][0]:
        f = _construct_fast_multiscale(FastDifferenceOfGaussians, \
                params['fast_dog'][1])
        features.append(f)
    elif params['rotated_fast_dog'][0]:
        f = _construct_rotated_fast_dog(RotatedFastDifferenceOfGaussians, \
                params['rotated_fast_dog'][1])
        features.append(f)
    elif params['log'][0]:
        f = _construct_multiscale(LaplacianOfGaussians, params['log'][1])
        features.append(f)
    elif params['hog'][0]:
        f = _construct_multiscale(LaplacianOfGaussians, params['hog'][1])
        features.append(f)
    elif params['sobel'][0]:
        f = _construct_multiscale(LaplacianOfGaussians, params['sobel'][1])
        features.append(f)
    elif params['harris'][0]:
        kwargs = {}
        if params['harris'][1]['k'] is not None:
            kwargs['step'] = params['harris'][1]['k']
        f = _construct_multiscale(Harris, params['harris'][1], kwargs)
        features.append(f)
    elif params['rgb'][0]:
        features.append(YCrCb())
    elif params['hsv'][0]:
        features.append(HSV())
    elif params['hls'][0]:
        features.append(HLS())
    elif params['luv'][0]:
        features.append(LUV())
    elif params['yuv'][0]:
        features.append(YUV())
    elif params['ycrcb'][0]:
        features.append(YCrCb())
    return features

def get_feature_maps(features, image, out=None):
    height, width, channels = image.shape
    num_feature_maps = [feature.num_feature_maps(image.shape[2]) \
            for feature in features]
    if out is None:
        out = np.empty((height, width, sum(num_feature_maps)), 'float32')
    index = 0
    for s, feature in itertools.izip(num_feature_maps, features):
        feature.get_feature_maps(image, out[..., index : index + s])
        index += s
    return out

def train_classifier(clf, X_labels_arr):
    w_arr = [np.where(labels) for _, labels in X_labels_arr]
    X = np.vstack([X[w] for w, (X, _) \
            in itertools.izip(w_arr, X_labels_arr)])
    y = np.hstack([labels[w] - 1 for w, (_, labels) \
            in itertools.izip(w_arr, X_labels_arr)])
    clf.fit(X, y)
    return clf

def predict_feature_maps(clf, X, y_img=None):
    if y_img is None:
        y_img = clf.predict(X.reshape((-1,) + X.shape[-1:])) \
                .reshape(X.shape[:-1])
    else:
        y_img[:] = clf.predict(X.reshape((-1,) + X.shape[-1:])) \
                .reshape(X.shape[:-1])
    return y_img

def predict_image(clf, features, image, y_img=None):
    X = get_feature_maps(features, image)
    return predict_feature_maps(clf, X, y_img)

def predict_image_keras(clf, image, y_img=None, return_prob=False):
    X = np.expand_dims(image, axis=0)
    if return_prob:
        return clf.predict_prob(X).squeeze()
    else:
        return clf.predict(X).squeeze()

class KerasClassifier:
    def __init__(self, keras_model, lambda_str=None):
        self.model = keras_model
        self.lambda_str = lambda_str

    def predict(self, X):
        if self.lambda_str is not None:
            f = eval(self.lambda_str)
            X = f(X)
        Y_prob = self.model.predict(X, verbose=0)
        return np.argmax(Y_prob, axis=-1)

    def predict_prob(self, X):
        if self.lambda_str is not None:
            f = eval(self.lambda_str)
            X = f(X)
        Y_prob = self.model.predict(X, verbose=0)
        return Y_prob
