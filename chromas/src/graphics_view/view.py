# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import QPainter, QPen
import PyQt5
import math

class View(QGraphicsView):
    ZOOM_LEVEL = 1.5
    TRANSLATE_LEVEL = 16
    
    viewCaptured = pyqtSignal([QPointF, bool])
    viewReleased = pyqtSignal()
    toolRadiusChanged = pyqtSignal([int])

    def __init__(self, parent=None):
        QGraphicsView.__init__(self, parent)
        self.setMouseTracking(True)
        self.setInteractive(True)

        self._capture_pos = None
        self._show_crosses = False
        self._tool_radius = 1.
        self.setCursor(Qt.CrossCursor)
	

    def toolRadius(self):
        return self._tool_radius

    def setToolRadius(self, tool_radius):
        if self._tool_radius != tool_radius:
            self._tool_radius = tool_radius
            self.toolRadiusChanged.emit(self._tool_radius)
            self.viewport().update()

    def captureView(self, pos, center_view):
        if center_view:
            self.centerOn(pos)
        self._show_crosses = center_view
        self._capture_pos = pos
        self.viewport().update()

    def releaseView(self):
        self._capture_pos = None
        self.viewport().update()

    def drawForeground(self, painter, rect):
        QGraphicsView.drawForeground(self, painter, rect)
 	pen = QPen()
	pen.setWidthF(.1)
	painter.setPen(pen)
        if self._capture_pos != None:
            if self._show_crosses:
                painter.drawLine(QPointF(self._capture_pos.x(), \
                                         rect.topLeft().y()), \
                                 QPointF(self._capture_pos.x(), \
                                         rect.bottomRight().y()))
                painter.drawLine(QPointF(rect.topLeft().x(), \
                                         self._capture_pos.y()), \
                                 QPointF(rect.bottomRight().x(), \
                                         self._capture_pos.y()))
            painter.drawEllipse(self._capture_pos, \
                    self._tool_radius, self._tool_radius)


    def mouseMoveEvent(self, event):
        if event.modifiers() & Qt.ControlModifier:
            self.viewCaptured.emit(self.mapToScene(event.pos()), True)
        else:
            self.viewCaptured.emit(self.mapToScene(event.pos()), False)
        self._show_crosses = False
        self._capture_pos = self.mapToScene(event.pos())
        self.viewport().update()
        QGraphicsView.mouseMoveEvent(self, event)

    def leaveEvent(self, event):
        self.viewReleased.emit()
        self._capture_pos = None
        self.viewport().update()
        QGraphicsView.leaveEvent(self, event)
    
    def wheelEvent(self, event):
        degrees = event.angleDelta() / 8.
	steps = float(degrees.y() / 15.)
        transform = self.transform()
        if event.modifiers() == Qt.ControlModifier:
            transform.scale(math.pow(self.ZOOM_LEVEL, steps),
                    math.pow(self.ZOOM_LEVEL, steps))
            self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
            self.setTransform(transform)
        elif event.modifiers() == Qt.ShiftModifier:
            self.setToolRadius(max(0, self.toolRadius() + steps))
        else:
            QGraphicsView.wheelEvent(self, event)
        
        self._show_crosses = False
        self._capture_pos = self.mapToScene(event.pos())
        self.viewport().update()
        
