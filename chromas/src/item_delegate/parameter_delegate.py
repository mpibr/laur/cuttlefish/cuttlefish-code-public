#!/usr/bin/env python2
# -*- coding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import math

class ModifyClassifierCommand(QUndoCommand):

    def __init__(self, model, index, previous_value, value, parent=None):
        QUndoCommand.__init__(self, "Modify parameter", parent)

        self._model = model
        self._index = index
        self._previous_value = previous_value
        self._value = value

    def redo(self):
        self._model.setData(self._index, self._value)
        
    def undo(self):
        self._model.setData(self._index, self._previous_value)
    
class ParameterDelegate(QItemDelegate):

    def __init__(self, undo_stack, parent):
        QItemDelegate.__init__(self, parent)
        self._undo_stack = undo_stack

    def createEditor(self, parent, option, index):
        node = index.internalPointer()
        return node.createEditor(parent, option)

    def setEditorData(self, editor, index):
        node = index.internalPointer()
        node.setEditorData(editor)

    def setModelData(self, editor, model, index):
        node = index.internalPointer()
        value = node.getEditorData(editor)

        self._undo_stack.push( \
                ModifyClassifierCommand(model, index, node.value(), value))

    def paint(self, painter, option, index):
        node = index.internalPointer()
        node.paint(painter, option)

