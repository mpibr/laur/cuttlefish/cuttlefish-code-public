#!/usr/bin/env python2
# -*- coding: utf-8 -*-

__author__ = "Philipp Hülsdunk"
__email__ = "huelsdunk@fias.uni-frankfurt.de"
__copyright__ = "Frankfurt Institute for Advanced Studies"

from mpi4py import MPI

import argparse

import cv2
import sklearn.naive_bayes
import sklearn.ensemble
import sklearn.cluster
import sklearn.naive_bayes
import sklearn.svm

import os.path
import cPickle as pickle

from tqdm import tqdm
import h5py
import itertools
import sys
import logging
import warnings
from . import feature
from .feature import KerasClassifier
import os
import random
import string
import threading
import numpy as np
import mpi_iter

def video_iter(cap):
    '''
    Makes OpenCV's VideoCapture to an iterable.
    '''
    while True:
        succ, img = cap.read()
        if not succ:
            break
        yield img

def chunk(iterable, chunksize):
    '''
    Returns chunks from an iterable.
    '''
    iterable = iter(iterable)
    while True:
        buffer = []
        try:
            for _ in range(chunksize):
                buffer.append(next(iterable))
        except StopIteration:
            if buffer:
                yield buffer
            break
        yield buffer
 
BLOCKSIZE_MB = 32

class Logger:

    def __init__(self, comm, filename, level):
        self._comm = MPI.COMM_WORLD
        if filename is not None:
            mode = MPI.MODE_WRONLY | MPI.MODE_CREATE
            self._fh = MPI.File.Open(comm, filename, mode)
            self._fh.Set_atomicity(True)
        else:
            self._fh = None
        self._level = level

    def info(self, str):
        if self._fh is not None and self._level >= 20:
            msg = '%d: %s\n' % (self._comm.rank, str)
            self._fh.Write_shared(msg)
            self._fh.Sync()

    def close(self):
        if self._fh is not None:
            self._fh.Sync()
            self._fh.Close()

def main():
    comm = MPI.COMM_WORLD
    
    p = argparse.ArgumentParser('Classify video')
    p.add_argument('--classifier', required=True, help='Classifier')
    p.add_argument('--video', required=True, help='Video to process')
    p.add_argument('output', help='Output segmentation file')
    p.add_argument('--video-start', type=float, required=True, \
            help='Video starting time')
    p.add_argument('--start', type=float, help='Start video at')
    p.add_argument('--end', type=float, help='End video at')
    p.add_argument('--log-level', type=int, default=0)
    p.add_argument('--log')
    p.add_argument('--threads', type=int, default=1)
    p.add_argument('--use-keras', action='store_true')
    p.add_argument('--return-prob', action='store_true')
    args = p.parse_args()

    logger = Logger(comm, args.log, args.log_level)
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # Disable Tensorflow stdout
    warnings.filterwarnings("ignore", category=UserWarning)

    if comm.rank == 0:
        logger.info('Open classifier')
        with open(args.classifier, 'rb') as f:
            classifier_doc = pickle.load(f)
        # Setup multithreading if not using keras
        if not args.use_keras:
            n_jobs_params = {key: -1 \
                    for key, value \
                        in classifier_doc['classifier'].get_params().items() \
                    if key.endswith('n_jobs')}
            classifier_doc['classifier'].set_params(**n_jobs_params)
    
    # Parse classifier document
    if comm.rank == 0:
        logger.info('Parsing classifier')
        labels = [lc[0] for lc in classifier_doc['label_colours']]
        label_colours = [lc[1] for lc in classifier_doc['label_colours']]
        if not args.use_keras: features = classifier_doc['features']
        clf = classifier_doc['classifier']
      
    else:
        labels = None
        label_colours = None
        clf = None

        if not args.use_keras: features = None

    # Bcast parsed classifier
    logger.info('Broadcasting classifier')
    if args.use_keras:
        labels, label_colours, clf = comm.bcast( \
            (labels, label_colours, clf))
    else:
        labels, label_colours, clf, features = comm.bcast( \
            (labels, label_colours, clf, features))
    
    # Open video
    err = False
    if comm.rank == 0:
        logger.info('Open video')
        cap = cv2.VideoCapture(args.video)
        if not cap.isOpened():
            err = True
    err = comm.bcast(err)
    if err:
        raise RuntimeError('Video cannot be opened')

    # Master rank prepares input, output
    logger.info('Preparing worker-consumer-pattern')
    frame_iter = None
    func = None
    if comm.rank == 0:
        # Get video info
        if cv2.__version__.split('.')[0] >= '3':
            fps = float(cap.get(cv2.CAP_PROP_FPS))
            height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
            width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
            num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        else:
            fps = float(cap.get(cv2.cv.CV_CAP_PROP_FPS))
            height = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
            width = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
            num_frames = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
        logger.info('Video at %dfps, length %d, %dx%d' \
                % (fps, num_frames, width, height))

        # Setup start and end times
        if args.start is None:
            args.start = args.video_start
        if args.end is None:
            args.end = args.video_start + num_frames // fps
        num_frames = int((args.end - args.start) * fps)

        # Goto the video start
        logger.info('Cutting video at %d-%d' % (args.start, args.end))
        if cv2.__version__.split('.')[0] >= '3':
            prop_pos = cv2.CAP_PROP_POS_MSEC
        else:
            prop_pos = cv2.cv.CV_CAP_PROP_POS_MSEC
        chunkoffset = 0
        while cap.get(prop_pos) / 1000. + args.video_start < args.start:
            cap.grab()
            if cap.get(prop_pos) == 0.0:
                logger.info('Frame grabbed without increasing chunkoffset. {}'.format(cap.get(prop_pos)))
            else:
                chunkoffset += 1
        
        # Setup frame iterator
        logger.info('Setting up iterators')
        frame_iter = video_iter(cap)
        frame_iter = itertools.islice(frame_iter, 0, num_frames)
        
        # Open up output
        output_dirname = os.path.dirname(args.output)
        output_basename = os.path.basename(args.output)
        suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                for _ in range(4))
        tmp_output_basename = '.' + output_basename + '.' + suffix
        tmp_output = os.path.join(output_dirname, tmp_output_basename)
        logger.info('Opening output')
        writer = h5py.File(tmp_output, 'w')
        logger.info('Creating attribute label_colours')
        writer.attrs.create('label_colours', label_colours,
                dtype='uint8')
        logger.info('Creating attribute video')
        writer.attrs.create('video', os.path.relpath(args.video, \
                os.path.dirname(args.output)), \
                dtype=h5py.special_dtype(vlen=unicode))
        logger.info('Creating attribute fps')
        writer.attrs.create('fps', fps, dtype='float32')
        logger.info('Creating attribute chunkoffset')
        writer.attrs.create('chunkoffset', chunkoffset, dtype='uint64')

        chunksize = max(1, BLOCKSIZE_MB * 1024 * 1024 / height / width)
        if args.return_prob:
            chunksize = chunksize / len(labels)

        if (len(labels) == 2) and (not args.return_prob):
            segmentation_dtype = 'bool'
        else:
            segmentation_dtype = h5py.special_dtype( \
                    enum=('uint8', \
                        {label: index for index, label in enumerate(labels)}))

        logger.info('Creating dataset segmentation')
        if args.return_prob:
            segmentation = writer.create_dataset('segmentation', \
                    shape=(num_frames, height, width, len(labels)), \
                    chunks=(chunksize, height, width, len(labels)), \
                    dtype=segmentation_dtype,
                    compression='gzip')
        else:
            segmentation = writer.create_dataset('segmentation', \
                    shape=(num_frames, height, width), \
                    chunks=(chunksize, height, width), \
                    dtype=segmentation_dtype,
                    compression='gzip')
        if len(labels) == 2:
            segmentation.attrs.create('enum', data=labels, \
                    dtype=h5py.special_dtype(vlen=unicode))
    
    # Function to be called
    logger.info('Setting up function call')
    if args.use_keras:
        func = lambda img: feature.predict_image_keras(\
            clf, img, return_prob=args.return_prob)
    else:
        func = lambda img: feature.predict_image(clf, features, img)

    # Process stuff in a worker consumer way
    # Root rank reads frames, slave ranks process them
    logger.info('Processing frames')
    print('tw5', tmp_output, args.output)
    result_iter = mpi_iter.parallel_map(comm, func, frame_iter)
    if comm.rank == 0:
        chunksize = segmentation.chunks[0]
        for num, seg_chunk in \
                enumerate(chunk(tqdm(result_iter, \
                total=num_frames, desc='Segmenting'), chunksize)):
            start = num * chunksize
            end = start + len(seg_chunk)
            # I FUCKING DONT GET THIS WHY THIS MAKES A HUGE SPEED IMPORVEMENT
            # Somehow a list works better than a numpy array
            if len(labels) == 2:
                seg_chunk = [a.astype('bool') for a in seg_chunk]
            if args.return_prob:
                seg_chunk = (np.array(seg_chunk) * 255).astype('uint8')
            segmentation[start : end] = seg_chunk
        writer.close()
        os.rename(tmp_output, args.output)
    logger.info('Done')

