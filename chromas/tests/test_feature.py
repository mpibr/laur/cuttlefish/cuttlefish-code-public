import sys
import numpy as np
import pytest

@pytest.mark.skipif(sys.version_info[0] != 2, reason="requires python2")
@pytest.mark.py2
def test_keras_classifier_predict_argmax():
    from chromas.feature import KerasClassifier

    # Make mock keras model because real Keras takes too long to load.
    class MockKerasModel:
        def predict(self, x, verbose=None):
            return x+100

    input_image_stack = np.random.random((1,2,2,3))
    expected_output = np.argmax(input_image_stack + 100, axis=-1)

    # Use case
    keras_classifier = KerasClassifier(MockKerasModel())
    output = keras_classifier.predict(input_image_stack).squeeze()
    
    assert np.all(output == expected_output)

@pytest.mark.skipif(sys.version_info[0] != 2, reason="requires python2")
@pytest.mark.py2
def test_keras_classifier_predict_prob():
    from chromas.feature import KerasClassifier

    # Make mock keras model because real Keras takes too long to load.
    class MockKerasModel:
        def predict(self, x, verbose=None):
            return x+100

    input_image_stack = np.random.random((1,2,2,3))
    expected_output = (input_image_stack + 100).squeeze()

    # Use case
    keras_classifier = KerasClassifier(MockKerasModel())
    output = keras_classifier.predict_prob(input_image_stack).squeeze()
    
    print('tw0', output.shape, expected_output.shape)
    assert np.all(output == expected_output)

@pytest.mark.skipif(sys.version_info[0] != 2, reason="requires python2")
@pytest.mark.py2
def test_keras_classifier_lambda_function():
    from chromas.feature import KerasClassifier

    # Make mock keras model because real Keras takes too long to load.
    class MockKerasModel:
        def predict(self, x, verbose=None):
            return x+100

    input_image_stack = np.random.random((1,2,2,3))
    input_lambda = 'lambda x: x**2 + x/20.'
    expected_output = np.argmax(MockKerasModel().predict(
                            input_image_stack**2 + input_image_stack/20.), axis=-1)

    # Use case
    keras_classifier = KerasClassifier(MockKerasModel(), lambda_str=input_lambda)
    output = keras_classifier.predict(input_image_stack).squeeze()
    
    assert np.all(output == expected_output)

@pytest.mark.skipif(sys.version_info[0] != 2, reason="requires python2")
@pytest.mark.py2
def test_predict_image_keras_input_output_shapes_conversion():
    from chromas.feature import predict_image_keras

    # Make mock keras model because real Keras takes too long to load.
    class MockKerasModel:
        def predict(self, x, verbose=None):
            assert x.shape[0] == 1  # Keras model take input of shape (1, height, width, channel)
            return x+100
    
    input_image = np.random.random((2,2,3))

    # Use case
    output = predict_image_keras(MockKerasModel(), input_image)

    assert output.shape == input_image.shape