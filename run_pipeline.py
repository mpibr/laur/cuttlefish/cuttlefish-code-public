#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys; print((sys.version))
import os
import sys
import argparse
import subprocess
import time
import datetime
from tqdm import tqdm
from uuid import uuid1
from io import StringIO #py3
import configparser #py3
from ssh_tools import expand_path, retry_ssh, retry_rsync

def delete_old_chunks(chunktimes, video):
    prefix = os.path.splitext(chunktimes)[0]
    with open(chunktimes, 'r') as f:
        for line in f:
            start_f, end_f, _, _, _ = line.strip('\n').split(' ')
            for file_ext in ['mp4', 'seg', 'seg.log', 'reg', 'reg.log', \
                'masterframe', 'masterframe.log', 'chunkaverage', 'chunkaverage.log', \
                'cstitching', 'cstitching.log']:
                old_file = '{}-{}-{}.{}'.format(prefix, start_f, end_f, file_ext)
                if os.path.isfile(old_file):
                    os.remove(old_file)
    os.remove(chunktimes)

def generate_syncfiles(chunktimes, syncfiles):
    prefix = os.path.splitext(chunktimes)[0]
    basename = os.path.basename(prefix)
    if os.path.isfile(syncfiles):
        os.remove(syncfiles)
    files_to_sync = ''
    with open(syncfiles, 'a') as fs:
        with open(chunktimes, 'r') as fc:
            for line in fc:
                start_f, end_f, _, _, _ = line.strip('\n').split(' ')
                for file_ext in ['mp4', 'seg', 'seg.log', 'reg', 'reg.log', \
                    'masterframe', 'masterframe.log', 'chunkaverage', 'chunkaverage.log', \
                    'cstitching', 'cstitching.log']:
                    files_to_sync += '+ {}-{}-{}.{}\n'.format(basename, start_f, end_f, file_ext)
                    
        for file_ext in ['chunktimes', 'stitching', 'stitching.log', \
            'queenframe', 'queenframe.log', 'average', 'average.log', \
            'colourlabels', 'colourlabels.log', 'cleanqueen', 'cleanqueen.log', \
            'areas', 'areas.log']:
            files_to_sync += '+ {}.{}\n'.format(basename, file_ext)
        if args.debug:
            files_to_sync += '+ {}.{}\n'.format(basename, 'filedeps.log')
        files_to_sync += '- *'
        fs.write(files_to_sync)

if __name__ == '__main__':

    pipeline_start_time = time.time()

    # Arguments used in run_pipeline
    p = argparse.ArgumentParser('Running pipeline', add_help=False)
    p.add_argument('--local-config', default='~/.cuttleline.cfg', type=str)
    p.add_argument('--host-config', default='~/.cuttleline.cfg', type=str)
    p.add_argument('--hostname', type=str, help='e.g. localhost')
    p.add_argument('--sshcmd', type=str, help='If specified, override hostname')
    p.add_argument('--shell', type=str, help='e.g. docker exec mycontainer')
    p.add_argument('--uuid', help='UUID (8-4-4-4-12) of the output tmp folder')
    p.add_argument('--debug', action='store_true')
    p.add_argument('--dry-run', action='store_true')
    p.add_argument('--keepfiles', action='store_true')
    p.add_argument('--config')
    p.add_argument('--sleep-between-sync', default=900, type=int)
    p.add_argument('input', nargs='?', help='Input video')
    args, remaining_argv = p.parse_known_args()

    # Arguments passed directly to run_pipeline_remote
    # Included to display in --help
    p_remote = argparse.ArgumentParser(parents=[p])
    p_remote.add_argument('--rerun-registration', action='store_true')
    p_remote.add_argument('--rerun-stitching', action='store_true')
    p_remote.add_argument('--rerun-average', action='store_true')
    p_remote.add_argument('--rerun-colourlabels', action='store_true')
    p_remote.add_argument('--rerun-queenframe', action='store_true')
    p_remote.add_argument('--rerun-cleanqueen', action='store_true')
    p_remote.add_argument('--skip-chunks', action='store_true', help='Start from chunk stitching')
    p_remote.add_argument('--skip-stitching', action='store_true', help='Stop before chunk stitching')
    p_remote.add_argument('--max-queued', help='default=299, confirm in run_pipeline_remote')
    p_remote.add_argument('--check-file-patience', help='default=40, patience (s) for check_filedeps.')
    _ = p_remote.parse_args()

    # Set up basic blocks
    prefix = os.path.splitext(args.input)[0]
    basename = os.path.basename(prefix)
    dirname = os.path.dirname(prefix)
    basepath = os.path.basename(dirname)
    local_code_dir = os.path.dirname(os.path.abspath(__file__))
    if args.uuid is None:
        args.uuid = str(uuid1())
    print(('UUID: ' + args.uuid))

    # Expand addresses to read ~, $HOME, etc.
    args.local_config = expand_path(args.local_config)
    args.host_config = expand_path(args.host_config)
    if args.config is None:
        args.config = prefix + '.cfg'
    args.config = expand_path(args.config)
    
    # Parse dataset config, more options under 'submitting jobs'
    config = configparser.ConfigParser(allow_no_value=True)
    try:
        config.read(args.config)
    except configparser.MissingSectionHeaderError:
        # Backward compatibility for sectionless config
        cmd = ['cat', args.config]
        string = '[sepia]\n' + subprocess.check_output(cmd).decode("utf-8")
        config.readfp(StringIO(string))
    classifier = expand_path(os.path.join(dirname, config.get('sepia', 'classifier')))
    colourica = expand_path(os.path.join(dirname, config.get('sepia', 'colourica')))
    if (args.hostname is None) and (args.sshcmd is None):
        try:
            args.hostname = config.get('sepia', 'hostname')
        except:
            pass

    # Parse pipeline config (local)
    local_config = configparser.ConfigParser()
    try:
        local_config.read(args.local_config)
    except configparser.MissingSectionHeaderError:
        # Backward compatibility for sectionless config
        cmd = ['cat', args.local_config]
        string = '[local]\n' + subprocess.check_output(cmd)
        local_config.readfp(StringIO(string))
    sepia_dir = local_config.get('local', 'sepia_dir')
    outdir = expand_path(os.path.join(local_config.get('local', 'outdir'), basepath))
    subprocess.call(['mkdir', '-p', outdir])  # make output folder with the same name
    if args.debug:
        print(('local local_code_dir: '+ local_code_dir))
        print(('local sepia_dir: '+ sepia_dir))
        print(('local outdir: '+ outdir))

    if (args.hostname is None) and (args.sshcmd is None):
        args.hostname = local_config.get('local', 'hostname')

    # Parse pipeline config (host), more options under 'submitting jobs'
    host_config = configparser.ConfigParser()
    try:
        host_config.read(args.host_config)
    except configparser.MissingSectionHeaderError:
        # Backward compatibility for sectionless config
        cmd = ['cat', args.host_config]
        string = '[host]\n' + subprocess.check_output(cmd)
        host_config.readfp(StringIO(string))
    if args.sshcmd is None:
        args.sshcmd = 'ssh ' + args.hostname  
    
    print(('sshcmd: '+ args.sshcmd))

    working_dir = os.path.join(expand_path(host_config.get('host', 'working_dir'), \
                                            sshcmd=args.sshcmd), args.uuid)
    code_dir = expand_path(host_config.get('host', 'code_dir'), \
                            sshcmd=args.sshcmd)

    # Get shell executor, e.g. container command
    mpicmd = host_config.get('host', 'mpicmd')
    try:
        shell = host_config.get('host', 'shell')
        mpicmd = '{} {}'.format(mpicmd, shell)
    except configparser.NoOptionError:
        shell = ''

    print(('remote working_dir: '+ working_dir))
    print(('remote code_dir: '+ code_dir))
    print(('remote shell: '+ shell))
    print(('full mpicmd: '+ mpicmd))

    # CHUNKING
    # Chunk if chunktimes does not exisit or if a new video or new chunks file is found
    chunktimes = os.path.join(outdir, basename) + '.chunktimes'
    chunks = prefix + '.chunks'
    if not os.path.isfile(chunktimes) or \
        (os.path.getctime(chunktimes) < os.path.getctime(args.input)) or \
        (os.path.getctime(chunktimes) < os.path.getctime(chunks)):

        # Delete old chunks and associated files if exist
        if os.path.isfile(chunktimes):
            delete_old_chunks(chunktimes, args.input)

        # Local chunking
        subprocess.call([local_code_dir + '/chunking/chunk_video.py', \
            '--video', args.input, \
            chunktimes])

    ## SYNCING
    # Determine files to be transferred
    syncfiles = prefix + '.syncfiles'
    generate_syncfiles(chunktimes, syncfiles)
    
    # Make working dir on remote
    cmd_args = '{} mkdir -p {}'.format(shell, working_dir)
    retry_ssh(args.sshcmd, cmd_args)

    print('Sending shared files')
    cmd_args = '{} mkdir -p {}'.format(shell, working_dir)
    retry_rsync(args.sshcmd, '-ap "{}" :"{}"'.format(
        classifier, os.path.join(working_dir, 'classifier.clf')))
    retry_rsync(args.sshcmd, '-ap "{}" :"{}"'.format(
        colourica, os.path.join(working_dir, 'colourica.ica')))
    
    print('Sending files')
    retry_rsync(args.sshcmd, '-ap --include-from="{}" "{}"/ :"{}" --delete --progress'.format(
        syncfiles, outdir, working_dir), flush=args.debug)

    print('Submitting jobs')
    cmd_args = [os.path.join(code_dir, 'run_pipeline_remote.py'), \
        '--background', config.get('sepia', 'background'), \
        '--foreground', config.get('sepia', 'foreground'), \
        '--filedeps-slurm "{} "'.format(config.get('sepia', 'filedeps_slurm')), \
        '--segmentation-slurm "{} "'.format(config.get('sepia', 'segmentation_slurm')), \
        '--segmentation-args "{} "'.format(config.get('sepia', 'segmentation_args')), \
        '--registration-slurm "{} "'.format(config.get('sepia', 'registration_slurm')), \
        '--registration-args "{} "'.format(config.get('sepia', 'registration_args')), \
        '--masterframe-slurm "{} "'.format(config.get('sepia', 'masterframe_slurm')), \
        '--masterframe-args "{} "'.format(config.get('sepia', 'masterframe_args')), \
        '--chunkaverage-slurm "{} "'.format(config.get('sepia', 'chunkaverage_slurm')), \
        '--chunkaverage-args "{} "'.format(config.get('sepia', 'chunkaverage_args')), \
        '--chunk-stitching-slurm "{} "'.format(config.get('sepia', 'chunk_stitching_slurm')), \
        '--chunk-stitching-args "{} "'.format(config.get('sepia', 'chunk_stitching_args')), \
        '--stitching-slurm "{} "'.format(config.get('sepia', 'stitching_slurm')), \
        '--stitching-args "{} "'.format(config.get('sepia', 'stitching_args')), \
        '--queenframe-slurm "{} "'.format(config.get('sepia', 'queenframe_slurm')), \
        '--queenframe-args "{} "'.format(config.get('sepia', 'queenframe_args')), \
        '--average-slurm "{} "'.format(config.get('sepia', 'average_slurm')), \
        '--average-args "{} "'.format(config.get('sepia', 'average_args')), \
        '--colourlabels-slurm "{} "'.format(config.get('sepia', 'colourlabels_slurm')), \
        '--colourlabels-args "{} "'.format(config.get('sepia', 'colourlabels_args')), \
        '--cleanqueen-slurm "{} "'.format(config.get('sepia', 'cleanqueen_slurm')), \
        '--cleanqueen-args "{} "'.format(config.get('sepia', 'cleanqueen_args')), \
        '--areas-slurm "{} "'.format(config.get('sepia', 'areas_slurm')), \
        '--areas-args "{} "'.format(config.get('sepia', 'areas_args')), \
        '--mpicmd "{} "'.format(mpicmd), \
        '--partition "{} "'.format(host_config.get('host', 'partition')), \
        '--sshcmd "{} "'.format(args.sshcmd), \
        os.path.join(working_dir, basename)] + remaining_argv
    cmd_args = ' '.join(cmd_args)
    
    # stdout = retry_ssh(args.sshcmd, cmd_args, flush=False)
    cmd_args = '{} {}'.format(shell, cmd_args)
    print(cmd_args)
    stdout = retry_ssh(args.sshcmd, cmd_args, flush=False)
    final_jobid = stdout.split('FINAL_JOBID=')[-1].rstrip("')").lstrip("'").lstrip(", u'")
    
    print('Waiting for job')
    job_running = True
    while job_running:
        if args.debug:
            filedeps_file = os.path.join(outdir, basename + '.filedeps.log')
            if os.path.isfile(filedeps_file):
                with open(filedeps_file) as f:
                    print(''.join(f.readlines()))
        stdout = retry_ssh(args.sshcmd, 'scontrol show job {} --oneliner'.format(final_jobid))
        status = stdout.split('JobState=')[-1].split(' ')[0]

        if args.debug:
            disable_tqdm = None
        else:
            disable_tqdm = True
        if status == 'PENDING':

            for _ in tqdm(range(args.sleep_between_sync), 
                          desc='Sleep between resync', 
                          disable=disable_tqdm):
                time.sleep(1)
            
            print('Syncing back files ...')
            retry_rsync(args.sshcmd, 
                        '-ap --include-from="{}" :"{}"/ "{}" --delete --progress'.format(
                            syncfiles, working_dir, outdir), flush=args.debug)
        else:
            job_running = False

    print('Syncing back files ...')
    retry_rsync(args.sshcmd, 
                '-ap --include-from="{}" :"{}"/ "{}" --delete --progress'.format(
                    syncfiles, working_dir, outdir), flush=args.debug)

    os.remove(syncfiles)
    
    # Remove working dir
    if not args.dry_run and not args.keepfiles:
        print('Cleaning up')
        cmd_args = '{} rm -rf {}'.format(shell, working_dir)
        retry_ssh(args.sshcmd, cmd_args)

    # do stuff
    if args.debug:
        time_elapsed = time.time() - pipeline_start_time
        print(('Pipeline ran through in ' + str(datetime.timedelta(seconds=time_elapsed))))
