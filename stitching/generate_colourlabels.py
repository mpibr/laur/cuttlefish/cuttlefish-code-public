#!/usr/bin/env python3

import argparse
#import pickle as pickle
import pickle
import h5py
import cv2
import os
import random
import string

if __name__ == '__main__':
    p = argparse.ArgumentParser('Rotate average frame to enhance ' \
            'chromatophores')
    p.add_argument('input', help='Average frame')
    p.add_argument('--ica', help='ica model', required=True)
    p.add_argument('output', help='Colour label image')
    args = p.parse_args()
    
    with open(args.ica,'rb') as ica_file:
        sigma, labels, ica = pickle.load(ica_file, encoding='latin1')

    with h5py.File(args.input, 'r') as averageframe_file:
        averageframe = averageframe_file['frame'].value
        rel_stitching_filename = averageframe_file.attrs['stitching']
        stitching_filename = os.path.join(os.path.dirname(args.input), \
            rel_stitching_filename)
        ref_chunk = averageframe_file.attrs['ref_chunk']
        chunk_mask = averageframe_file.attrs['chunk_mask']

        hp = averageframe - cv2.GaussianBlur(averageframe, (0, 0), sigma)
        colourlabel = ica.transform(hp.reshape((-1, 3))) \
                .reshape(averageframe.shape[:2] + (-1,))

        output_dirname = os.path.dirname(args.output)
        output_basename = os.path.basename(args.output)
        suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                for _ in range(4))
        tmp_output_basename = '.' + output_basename + '.' + suffix
        tmp_output = os.path.join(output_dirname, tmp_output_basename)
        rel_input = os.path.relpath(args.input, \
                    os.path.dirname(args.output))
        rel_stitching = os.path.relpath(stitching_filename, \
                    os.path.dirname(args.output))
        with h5py.File(tmp_output) as colourlabel_file:
            colourlabel_file.attrs.create('labels', \
                    labels, dtype=h5py.special_dtype(vlen=str))
            colourlabel_file.create_dataset('frame', \
                    data=colourlabel, \
                    dtype='float32', \
                    compression='gzip')
            colourlabel_file.attrs.create('averageframe', \
                    rel_input, \
                    dtype=h5py.special_dtype(vlen=str))
            colourlabel_file.attrs.create('stitching', \
                    rel_stitching, \
                    dtype=h5py.special_dtype(vlen=str))
            colourlabel_file.attrs.create('ref_chunk', \
                    ref_chunk, \
                    dtype='uint16')
            colourlabel_file.attrs.create('chunk_mask', \
                    chunk_mask, \
                    dtype='bool')
        os.rename(tmp_output, args.output)
        
        print(f'{args.output} written.')

