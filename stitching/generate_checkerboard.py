#!/usr/bin/env python

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import cv2
import numpy as np

if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('--width', type=int)
    p.add_argument('--height', type=int)
    p.add_argument('--grid', type=int, default=16)
    p.add_argument('output')
    args = p.parse_args()
    height, width, grid = args.height, args.width, args.grid

    img = np.zeros((height / grid + 1, width / grid + 1, 3), 'uint8')
    img[::2, ::2] = (255, 255, 255)
    img[1::2, 1::2] = (255, 255, 255)
    

    # Colour in the midpoint
    my, mx = (s / 2 - 1 for s in img.shape[: 2])
    
    # Dark markers are positioned at even coordinates;
    # Create colours for the marker
    marker_ycrcb = np.array([(0, 0,     0), \
                             (0, 255,   0), \
                             (0, 0,   255), \
                             (0, 255, 255)], 'uint8')
    marker_ycrcb[(0, 3) if mx % 2 and mx % 2 else (1, 2), 0] = 255
    marker_bgr = cv2.cvtColor(marker_ycrcb[None], cv2.COLOR_YCR_CB2BGR)[0]

    # Colour in markers
    img[my + 0, mx + 0] = marker_bgr[0]
    img[my + 0, mx + 1] = marker_bgr[1]
    img[my + 1, mx + 0] = marker_bgr[2]
    img[my + 1, mx + 1] = marker_bgr[3]

    # Scale everything up
    img = cv2.resize(img, (0, 0), fx=args.grid, fy=args.grid, \
            interpolation=cv2.INTER_NEAREST)
    img = img[: args.height, : args.width]
    cv2.imwrite(args.output, img)

