#!/usr/bin/env python2
# coding: utf-8

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import sys
import argparse
import h5py
from tqdm import tqdm
import cv2
import numpy as np
import itertools
from skimage import morphology
import scipy.ndimage
import os.path

CODE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(CODE_DIR)
from pipeline_utils import cuttlefish_mask

def get_reprojection(smat_file):
    smat = smat_file['stitching_matrix']
    gs = smat.attrs['grid_size']
    rel_masterframe_files = smat_file.attrs['chunks']
    num_chunks = len(rel_masterframe_files)

    # Get the list of master frame files
    masterframe_files = [os.path.join( \
            os.path.dirname(smat_file.filename), rel_masterframe_file) \
            for rel_masterframe_file in rel_masterframe_files]
    
    # Get affine scaling
    shrink_t = np.array([[1. / gs, 0., 0.], \
                         [0., 1. / gs, 0.]], 'float32')
    grow_t = np.array([[gs, 0., 0.], \
                       [0., gs, 0.]], 'float32')

    # Open masterframes
    masterframes = []
    small_masks = []
    for masterframe_file in tqdm(masterframe_files, 'Reading master frames'):
        mf_file = h5py.File(masterframe_file, 'r')
        num_frames = mf_file.attrs['num_frames']

        # Get masterframe
        if 'masterframe' in mf_file:
            # New format for array
            labels = mf_file['masterframe'][0]
            h, w, k = labels.shape
            labels = labels.reshape(k, h, w)
        else:
            # Old format
            labels = np.array( \
                    [mf_file[label][:] \
                for label in (key.replace('/', '-') \
                for key in mf_file.attrs['labels'])], 'float32')
        
        forground_probs = np.float32(labels) / np.float32(num_frames)
        mf = (1. - np.product(1. - forground_probs, axis=0))
        masterframes.append(mf)

        # Get cuttlefish mask
        if 'chunkaverage_mask' in mf_file:
            # New format for array
            mask = mf_file['chunkaverage_mask'][:]
        else:
            # Old format
            mask = cuttlefish_mask(mf)
    
        # Shrink and pad mask
        if mask is not None:
            small_mask_padded = np.zeros(smat.shape[2:-1],dtype='bool')
            actual_h, actual_w = mask.shape//gs + 1
            small_mask = cv2.warpAffine( \
                mask.astype('uint8'), \
                shrink_t, \
                (actual_w, actual_h)).astype('bool')
            
            small_mask_padded[:actual_h, :actual_w] = small_mask
            small_masks.append(small_mask_padded)
        else:
            small_masks.append(np.nan)

    # Generate back forth mapping
    reprojection = np.zeros(smat.shape, 'float32')
    for i, j in tqdm(itertools.product(list(range(num_chunks)), list(range(num_chunks))), \
            total=num_chunks * num_chunks, \
            desc='Scoring mappings ...'):
        reprojection[i, j] = cv2.remap(smat[i, j], smat[j,i] / gs, None, \
                interpolation=cv2.INTER_LINEAR)
        reprojection[i, j, ..., 1] -= \
                gs * np.arange(reprojection.shape[2])[:, None]
        reprojection[i, j, ..., 0] -= \
                gs * np.arange(reprojection.shape[3])[None, :]
        reprojection[i, j, ~small_masks[i]] = np.nan
    return reprojection

def mapped_area_from_reprojection(reprojection, dist_thresh):
    reprojection_error = np.linalg.norm(reprojection, axis=-1)

    with np.errstate(invalid='ignore'):
        good_size = np.sum(reprojection_error < dist_thresh, axis=(2,3))
    mask_size = np.sum(np.isfinite(reprojection_error), axis=(2,3))
    return good_size / np.float32(mask_size+1)

def mapped_area(smat_file, dist_thresh):
    reprojection = get_reprojection(smat_file)
    return mapped_area_from_reprojection(reprojection, dist_thresh)

if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('stitching')
    p.add_argument('--dist-thresh', default=3, type=float, \
            help='Mapping distance threshold for marking an area as good')
    p.add_argument('--mapped-area-thresh', default=0.5, type=float, \
            help='Remove chunk if relative amount of mapped area falls under '
            'this value')
    p.add_argument('--resolution', type=float)
    args = p.parse_args()

    # Open the stitching matrix
    smat_file = h5py.File(args.stitching, 'r')
    reprojection = get_reprojection(smat_file)
    mapped_area_matrix = mapped_area_from_reprojection(reprojection, \
            args.dist_thresh)
    smat_file.close()

    # Find the best chunk
    ref_chunk = np.argmax(np.mean(mapped_area_matrix, axis=0))
    chunk_mask = mapped_area_matrix[ref_chunk, :] > args.mapped_area_thresh

    import matplotlib.pyplot as plt
    plt.imshow(mapped_area_matrix, \
            vmin=0, vmax=1, \
            interpolation='none')
    plt.gca().set_xticks([ref_chunk])
    plt.gca().set_xticklabels(['*'])
    plt.gca().set_yticks(np.where(~chunk_mask)[0])
    plt.gca().set_yticklabels(np.sum(~chunk_mask) * ['x'])
    ca = plt.colorbar(ticks=[0,1])
    ca.ax.set_title('Area mapped')

    h = int(np.sqrt(len(reprojection)))
    w = (len(reprojection) + h - 1) // h
    fig, axes = plt.subplots(w, h, sharex=True, sharey=True)
    for repr, ax in zip(reprojection[ref_chunk], axes.flatten()[:len(reprojection)]):
        error = np.linalg.norm(repr, axis=-1)
        mappable = ax.imshow(error, vmin=0, vmax=10)
    for ax in axes.flatten():
        ax.set_axis_off()
    
    fig.subplots_adjust(left=0.17, right=0.80, bottom=0.2)
    cbar_ax = fig.add_axes([0.90, 0.45, 0.015, 0.1])
    cbar = fig.colorbar(mappable, cax=cbar_ax, ticks=[0,10])
    cbar.ax.set_yticks(['0', '10px'])
    if args.resolution is not None:
        cbar_ax2 = cbar.ax.twinx()
        cbar_ax2.set_ylim([0, 10 * args.resolution])
        cbar_ax2.set_yticks([10 * args.resolution])
        cbar_ax2.set_yticklabels(['%0.1fµm' % (10 * args.resolution)])

    plt.show()

