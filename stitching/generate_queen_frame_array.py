#!/usr/bin/env python3
# coding: utf-8

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

'''
This program computes the probability of seeing a chromatophore at a given
position from a video.

As input it receives the stitching matrix, which maps the coordinate system
of cuttefish-trackable parts (called chunks) to one another. As this mapping
matrix is computed between all possible pairs of chunks, a reference coordinate
system has to be found. This will be defined as the minimum mean mapping
distance of a validation matrix. The validation matrix maps chunk A to chunk B
and back to A again and measures to error to the identity transform.

After selection of a reference chunk, chunks can be pruned away if entry in
the validation matrix exceeds a given threshold.

As output, the queen frame, reference chunk and the included chunk mask will be
saved.
'''

import os.path
import argparse
import h5py
from tqdm import tqdm
import cv2
import numpy as np
import mapped_area
import os
import random
import string
import warnings

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('stitching')
    parser.add_argument('output')
    parser.add_argument('--stitching-error-thresh', default=3, type=float, \
            help='Distance a mapping is considered to be false.')
    parser.add_argument('--mapped-area-thresh', default=0.3, type=float, \
            help='Remove chunk if relative amount of mapped area falls under '
            'this value')
    args = parser.parse_args()

    # Open the stitching matrix
    smat_file = h5py.File(args.stitching, 'r')
    smat = smat_file['stitching_matrix']
    gs = smat.attrs['grid_size']
    rel_masterframe_filenames = smat_file.attrs['chunks']
    masterframe_filenames = [os.path.join( \
            os.path.dirname(args.stitching), rel_masterframe_filename) \
            for rel_masterframe_filename in rel_masterframe_filenames]

    grow_t = np.array([[gs, 0., 0.], \
                       [0., gs, 0.]], 'float32')

    try:
        # Try opening reference chunk and chunk mask
        ref_chunk = smat_file.attrs['ref_chunk']
        chunk_mask = smat_file.attrs['chunk_mask']
    except KeyError:
        warnings.warn('ref_chunk and chunk_mask are not defined in the ' \
                'stitching file. I have to calculate them by myself. ' \
                'Consider rerunning the stitching.', \
                DeprecationWarning)
        # Finding best reference chunk
        mapped_area_matrix = mapped_area.mapped_area(smat_file, \
                args.stitching_error_thresh)
        ref_chunk = np.argmax(np.mean(mapped_area_matrix, axis=0))
        chunk_mask = mapped_area_matrix[ref_chunk, :] > args.mapped_area_thresh

    # Generate the queen frame
    good_masterframe_filenames = {masterframe_filename: index \
            for index, (m, masterframe_filename) \
                in enumerate(zip(chunk_mask, masterframe_filenames)) \
            if m}

    # Open files
    good_masterframe_files = {h5py.File(masterframe_filename, 'r'): index \
            for masterframe_filename, index \
                in list(good_masterframe_filenames.items())}

    # Find the labels
    all_labels = [set(masterframe_file.attrs['labels']) \
            for masterframe_file in good_masterframe_files]
    if not all(labels == all_labels[0] \
               for labels in all_labels[1 : ]):
        raise RuntimeError('Conflicting labels in masterframes')
   
    labels = list(all_labels[0])
    label='Chromatophore' #background class doesnt need a queenframe. Hardcode because of craziness
    print(labels)
    # Find frame sizes
    mapHeights=[]
    mapWidths=[]
    
    for masterframe_file in good_masterframe_files:
        [frameHeight, frameWidth] = masterframe_file['masterframe'].shape[1:3] 
        mapHeights.append(frameHeight)
        mapWidths.append(frameWidth)          
    
    qf_height = np.amax(mapHeights)
    qf_width = np.amax(mapWidths)

     # Generate the queen frame
     #import pdb; pdb.set_trace()
    queenframes = {label: np.zeros((qf_height, qf_width) ,'uint32')} 
    
  
    num_frames = 0
    for masterframe_file, index in tqdm(list(good_masterframe_files.items()), \
            total=len(good_masterframe_files), \
            desc='Generating queen frame'):
        num_frames += masterframe_file.attrs['num_frames']
        masterframe = np.squeeze(masterframe_file['masterframe'][()])
          # Get masterframe
# =============================================================================
#         mf=masterframe_file['segmentation'][0,:,:,:]
#         h, w, k =  mf.shape
#         mf =  mf.reshape(k, h, w)
#         if np.max( mf)==255:  #backwards compatability
#              mf= mf/255.0
#         forground_probs = np.float32( mf) 
#         masterframe = (1. - np.product(1. - forground_probs, axis=0))*num_frames
# =============================================================================
              
        [mfH, mfW] =masterframe.shape
        
        if masterframe.shape!=(qf_height, qf_width):
            tmpZ= np.zeros((qf_height, qf_width))
            tmpZ[0:mfH, 0:mfW]=masterframe
            masterframe=tmpZ
        
       # currSmat=np.swapaxes(smat[ref_chunk,index],0,1)
        maps = cv2.warpAffine(smat[index,ref_chunk], grow_t, \
                (qf_width, qf_height))
        masterframe = cv2.remap(masterframe.astype('float64'), maps, None, \
                interpolation=cv2.INTER_LINEAR).astype('uint32')
       
        queenframes[label] += masterframe

    # Save queen frame
    output_dirname = os.path.dirname(args.output)
    output_basename = os.path.basename(args.output)
    suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
            for _ in range(4))
    tmp_output_basename = '.' + output_basename + '.' + suffix
    tmp_output = os.path.join(output_dirname, tmp_output_basename)
    queenframe_file = h5py.File(tmp_output, 'w')
    queenframe_file.attrs.create('num_frames', num_frames, dtype='uint32')
    queenframe_file.attrs.create('labels', labels, \
            dtype=h5py.special_dtype(vlen=str))
    for label, queenframe in list(queenframes.items()):
        queenframe_file.create_dataset(label.replace('/', '-'), \
                data=queenframe, \
                dtype='uint32', \
                chunks=queenframe.shape, \
                compression='gzip')
    queenframe_file.attrs.create('ref_chunk', ref_chunk, dtype='uint32')
    queenframe_file.attrs.create('chunk_mask', chunk_mask, dtype='bool')
    rel_stitching = os.path.relpath(args.stitching, \
                os.path.dirname(args.output))
    queenframe_file.attrs.create('stitching', rel_stitching, \
            dtype=h5py.special_dtype(vlen=str))
    queenframe_file.close()
    os.rename(tmp_output, args.output)

