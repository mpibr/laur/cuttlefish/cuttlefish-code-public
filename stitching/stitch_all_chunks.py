#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
import numpy as np
import argparse
from tqdm import tqdm
import h5py
import os.path
import os
import random
import string
import mapped_area

if __name__ == '__main__':
    p = argparse.ArgumentParser( \
        'Compose stitching matrix.', \
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    p.add_argument('stitching_chunks', nargs='+', help='Maps from all chunks to the same target.')
    p.add_argument('output')
    p.add_argument('--stitching-error-thresh', default=3, type=float, \
        help='Distance a mapping is considered to be false.')
    p.add_argument('--mapped-area-thresh', default=0.5, type=float, \
        help='Remove chunk if relative amount of mapped area falls under '
        'this value')
    args = p.parse_args()
    
    # Filter out non existing input files and the src masterframe
    args.stitching_chunks = [filename for filename in args.stitching_chunks \
            if os.path.exists(filename)]
    if len(args.stitching_chunks) == 0:
        raise RuntimeError('No input files!')

  
    heights = []
    widths = []
    for sc_filename in args.stitching_chunks:
        sc_file = h5py.File( sc_filename, 'r')
        num_masterframes, h, w, _ = sc_file['small_maps'].shape
        heights.append(h)
        widths.append(w)

    grid_size = sc_file.attrs['grid_size']
    # rel_masterframes_filenames = sc_file.attrs['chunks']
    rel_masterframes_filenames = sc_file['chunks']
    masterframes_filenames = [os.path.join( \
                                os.path.dirname(sc_file.filename), str(masterframe))
                             for masterframe in rel_masterframes_filenames]
    chunks = np.array([os.path.relpath(masterframe, \
                os.path.dirname(args.output)) \
                for masterframe in masterframes_filenames], dtype=str)
        
    small_map_height = np.amax(heights)
    small_map_width = np.amax(widths)
        
    output_dirname = os.path.dirname(args.output)
    output_basename = os.path.basename(args.output)
    suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
            for _ in range(4))
    tmp_output_basename = '.' + output_basename + '.' + suffix
    tmp_output = os.path.join(output_dirname, tmp_output_basename)
    stitching_mat_file = h5py.File(tmp_output, 'w')
            
    # Create stitching matrix
    small_map_shape = (small_map_height, \
                       small_map_width, \
                       2)
                      
    stitching_mat_shape = (num_masterframes, num_masterframes) \
            + small_map_shape

    stitching_mat_dset = stitching_mat_file.create_dataset( \
            'stitching_matrix', \
            shape=stitching_mat_shape, \
            dtype='float32')

    stitching_mat_file.attrs.create('chunks',chunks, \
        dtype=h5py.special_dtype(vlen=str))

       
    # Save grid size for map interpolation
    stitching_mat_dset.attrs.create('grid_size', grid_size, \
            dtype='uint16')
    
    for trg_idx, sc_filename in enumerate(tqdm(args.stitching_chunks, 'Reading stitching chunks')):
        try:
            # Open the stitching matrix
            sc_file = h5py.File(sc_filename, 'r')
            small_maps = sc_file['small_maps']

            # Set other non diagonal entries        
            for src_idx, small_map in enumerate(small_maps):
                if (small_map is not None):
                    padded_small_map = np.zeros(small_map_shape, 'float32')
                    actual_h, actual_w, _ = small_map.shape
                    padded_small_map[:actual_h, :actual_w] = small_map
                    stitching_mat_dset[src_idx, trg_idx] = padded_small_map
                else:
                    tqdm.write('Could not find maps for %s -> %s' \
                            % (args.stitching_chunks[src_idx], \
                            args.stitching_chunks[trg_idx]))
                    stitching_mat_dset[src_idx, trg_idx] = np.nan
        except: 
            tqdm.write('Could not write maps for %s -> %s' \
                            % (args.stitching_chunks[src_idx], \
                            args.stitching_chunks[trg_idx]))

    # Set diagonal to identity
    small_map = np.zeros(small_map_shape, 'float32')
    small_map[..., 0] += \
            grid_size * np.arange(small_map.shape[1])[None, :]
    small_map[..., 1] += \
            grid_size * np.arange(small_map.shape[0])[:, None]
    for idx in range(num_masterframes):
        stitching_mat_dset[idx, idx] = small_map
            
    # Finding best reference chunk
    mapped_area_matrix = mapped_area.mapped_area(stitching_mat_file, \
        args.stitching_error_thresh)
    print('tw0', mapped_area_matrix)
    ref_chunk = np.argmax(np.mean(mapped_area_matrix, axis=0))
    chunk_mask = mapped_area_matrix[ref_chunk, :] > args.mapped_area_thresh
    print('tw1', chunk_mask)
    
    stitching_mat_file.create_dataset( \
            'reprojection_matrix', \
            data=mapped_area_matrix, \
            dtype='float32')

    # Write ref chunk
    stitching_mat_file.attrs.create('ref_chunk', ref_chunk, dtype='uint32')
    stitching_mat_file.attrs.create('chunk_mask', chunk_mask, dtype='bool')

    stitching_mat_file.close()
    os.rename(tmp_output, args.output)
