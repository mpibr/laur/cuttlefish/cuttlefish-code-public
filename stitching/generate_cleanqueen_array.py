#!/usr/bin/env python3
#coding: utf-8 
"""
Created on Thu Feb 28 08:01:58 2019
@author: reiters
"""

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import sys
import argparse
import h5py
import skimage.feature
import skimage.morphology
import scipy
import os.path
import random
import numpy as np
import string
import cv2

CODE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(CODE_DIR)
from pipeline_utils import cuttlefish_mask

def cleanqueen(queen, dilation, thresh, peak_footprint):

    filteredImg=scipy.zeros(queen.shape, dtype=scipy.float32)
    scipy.ndimage.filters.gaussian_laplace(queen, dilation, filteredImg)
    filteredImg2=queen-filteredImg
    filteredImg2[filteredImg2<0]=0
    
    blur = cv2.blur(filteredImg2,(5,5))
    chroma_pos = skimage.feature.peak_local_max(blur, indices=False, \
            footprint=np.ones((peak_footprint, peak_footprint),'bool'), \
          threshold_rel=thresh)
    labelled_chroma_pos = scipy.ndimage.label(chroma_pos)[0]
    #pdb.set_trace()
    # Generate the cleanqueen by watershedding
    return skimage.morphology.watershed(-filteredImg2, labelled_chroma_pos)

if __name__ == '__main__':
    p = argparse.ArgumentParser('Compute the cleanqueen')
    p.add_argument('queenframe')
    p.add_argument('cleanqueen')
    p.add_argument('--dilation', default=25, type=int, \
            help='Dilation of cuttlefish detection. Should be 2 times the ' \
            'chromatophore distance.')
    p.add_argument('--thresh', default=0.2, type=float, \
            help='Treshold for cuttlefish detection. Should be roughly ' \
            'smaller than the average time a chromatophore is detectable')
    p.add_argument('--peak-footprint', default=3, type=int, \
            help='Footprint of a peak')
    args = p.parse_args()
    
    # Open the queenframe and read its contents
    with h5py.File(args.queenframe, 'r') as f:
        labels = f.attrs['labels']
        num_frames = f.attrs['num_frames']
        ref_chunk = f.attrs['ref_chunk']
        chunk_mask = f.attrs['chunk_mask']
        rel_stitching = f.attrs['stitching']
        queenframe_dict = {label: f[(label.replace('/', '-'))].value \
                / float(num_frames) for label in labels}
    
    label = labels[0]
    
    qf_height, qf_width = queenframe_dict[label].shape
    
    # Get absoulte paths of previous input files and ref chunk mask
    stitching = os.path.join(os.path.dirname(args.queenframe), \
            rel_stitching)
    
    stitching_file=h5py.File(stitching,'r')
    chunks= stitching_file.attrs['chunks']
    ref_chunk_file=h5py.File(os.path.join(os.path.dirname(args.queenframe), \
            chunks[ref_chunk]),'r')
    ref_chunk_numFrames= ref_chunk_file.attrs['num_frames']
    
    mf = np.squeeze(ref_chunk_file['masterframe'][0])
    mf = np.float32(mf) / np.float32(ref_chunk_numFrames)
    ref_chunk_mask=cuttlefish_mask(mf)
    
    mfH,mfW = ref_chunk_mask.shape
    
    if  ref_chunk_mask.shape!=(qf_height, qf_width):
                tmpZ= np.zeros((qf_height, qf_width))
                tmpZ[0:mfH, 0:mfW]=ref_chunk_mask
                ref_chunk_mask=tmpZ
                
     #mask the queen frame with the ref frame mask
    queen = queenframe_dict[label] * ref_chunk_mask
    
    queenframe_dict = {label: queen}
    
    # Apply the cleanup for every label
    cleanqueen_dict = {label: cleanqueen(queen, \
            args.dilation, args.thresh, args.peak_footprint) \
            for label, queen in list(queenframe_dict.items())}
    
    # Generate relative paths of previous input files
    rel_stitching = os.path.relpath(stitching, \
            os.path.dirname(args.cleanqueen))
    rel_queenframe = os.path.relpath(args.queenframe, \
            os.path.dirname(args.cleanqueen))
    
    # Write data to temp file for safety
    output_dirname = os.path.dirname(args.cleanqueen)
    output_basename = os.path.basename(args.cleanqueen)
    suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
            for _ in range(4))
    tmp_output_basename = '.' + output_basename + '.' + suffix
    tmp_output_file = os.path.join(output_dirname, tmp_output_basename)
    tmp_output = h5py.File(tmp_output_file,'w')
    # Write data
    tmp_output.attrs.create('labels', labels,dtype=h5py.special_dtype(vlen=str))
    tmp_output.attrs.create('num_frames', num_frames)
    tmp_output.attrs.create('ref_chunk', ref_chunk)
    tmp_output.create_dataset('chunk_mask',data= ref_chunk_mask, dtype='uint8')
    tmp_output.attrs.create('stitching', rel_stitching, \
            dtype=h5py.special_dtype(vlen=str))
    tmp_output.attrs.create('queenframe', rel_queenframe, \
            dtype=h5py.special_dtype(vlen=str))
    for label, label_cleanqueen in list(cleanqueen_dict.items()):
            tmp_output.create_dataset(label.replace('/', '-'), data=label_cleanqueen, \
                    dtype='uint32')
    
    # Now rename the file and quit
    os.rename(tmp_output_file, args.cleanqueen)

