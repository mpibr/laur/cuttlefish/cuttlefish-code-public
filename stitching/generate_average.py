#!/usr/bin/env python3
# coding: utf-8

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

'''
This program computes the probability of seeing a chromatophore at a given
position from a video.

As input it receives the stitching matrix, which maps the coordinate system
of cuttefish-trackable parts (called chunks) to one another. As this mapping
matrix is computed between all possible pairs of chunks, a reference coordinate
system has to be found. This will be defined as the minimum mean mapping
distance of a validation matrix. The validation matrix maps chunk A to chunk B
and back to A again and measures to error to the identity transform.

After selection of a reference chunk, chunks can be pruned away if entry in
the validation matrix exceeds a given threshold.

As output, the queen frame, reference chunk and the included chunk mask will be
saved.
'''

import os.path
import argparse
import h5py
from tqdm import tqdm
import cv2
import numpy as np
import mapped_area
import os
import random
import string
import mapped_area
import warnings

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('stitching')
    parser.add_argument('output')
    parser.add_argument('--chunkaverage-ext', default='chunkaverage')
    parser.add_argument('--stitching-error-thresh', default=3, type=float, \
            help='Distance a mapping is considered to be false.')
    parser.add_argument('--mapped-area-thresh', default=0.5, type=float, \
            help='Remove chunk if relative amount of mapped area falls under '
            'this value')
    args = parser.parse_args()

    # Open the stitching matrix
    smat_file = h5py.File(args.stitching, 'r')
    smat = smat_file['stitching_matrix']
    gs = smat.attrs['grid_size']
    rel_masterframe_filenames = smat_file.attrs['chunks']
    masterframe_filenames = [os.path.join( \
            os.path.dirname(args.stitching), rel_masterframe_filename) \
            for rel_masterframe_filename in rel_masterframe_filenames]
    chunkaverage_filenames = \
            [os.path.splitext(filename)[0] + '.' + args.chunkaverage_ext \
             for filename in masterframe_filenames]

    grow_t = np.array([[gs, 0., 0.], \
                       [0., gs, 0.]], 'float32')

    try:
        # Try opening reference chunk and chunk mask
        ref_chunk = smat_file.attrs['ref_chunk']
        chunk_mask = smat_file.attrs['chunk_mask']
    except KeyError:
        warnings.warn('ref_chunk and chunk_mask are not defined in the ' \
                'stitching file. I have to calculate them by myself. ' \
                'Consider rerunning the stitching.', \
                DeprecationWarning)
        # Finding best reference chunk
        mapped_area_matrix = mapped_area.mapped_area(smat_file, \
                args.stitching_error_thresh)
        ref_chunk = np.argmax(np.mean(mapped_area_matrix, axis=0))
        chunk_mask = mapped_area_matrix[ref_chunk, :] > args.mapped_area_thresh

    # Generate the queen frame
    good_chunkaverage_filenames = {chunkaverage_filename: index \
            for index, (m, chunkaverage_filename) \
                in enumerate(zip(chunk_mask, chunkaverage_filenames)) \
            if m}

    # Open files
    good_chunkaverage_files = {h5py.File(chunkaverage_filename, 'r'): index \
            for chunkaverage_filename, index \
                in good_chunkaverage_filenames.items()}

    # Find frame sizes
    framesizes = [chunkaverage_file['frame'].shape \
                      for chunkaverage_file in good_chunkaverage_files]
    if not all(framesize == framesizes[0] for framesize in framesizes[1 : ]):
        raise RuntimeError('Conflicting framesizes per label')
    framesize = framesizes[0]

    # Generate the average frame
    average = np.zeros(framesize, 'float64')
    num_frames = 0
    for chunkaverage_file, index in tqdm(good_chunkaverage_files.items(), \
            total=len(good_chunkaverage_files), \
            desc='Generating average frame'):
        chunkaverage = chunkaverage_file['frame'].value

        # Remap frame
        maps = cv2.warpAffine(smat[index, ref_chunk], grow_t, \
                framesize[1 : : -1])
        chunkaverage = cv2.remap(chunkaverage, maps, None, \
                interpolation=cv2.INTER_LINEAR)

        average += chunkaverage * chunkaverage_file.attrs['num_frames']
        num_frames += chunkaverage_file.attrs['num_frames']
    average /= num_frames

    # Save queen frame
    output_dirname = os.path.dirname(args.output)
    output_basename = os.path.basename(args.output)
    suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
            for _ in range(4))
    tmp_output_basename = '.' + output_basename + '.' + suffix
    tmp_output = os.path.join(output_dirname, tmp_output_basename)
    average_file = h5py.File(tmp_output, 'w')
    average_file.attrs.create('num_frames', num_frames, dtype='uint32')
    average_file.create_dataset('frame', \
            data=average, \
            dtype='float32', \
            chunks=average.shape, \
            compression='gzip')
    average_file.attrs.create('ref_chunk', ref_chunk, dtype='uint32')
    average_file.attrs.create('chunk_mask', chunk_mask, dtype='bool')
    rel_stitching = os.path.relpath(args.stitching, \
                os.path.dirname(args.output))
    average_file.attrs.create('stitching', rel_stitching, \
            dtype=h5py.special_dtype(vlen=str))
    average_file.close()
    os.rename(tmp_output, args.output)

