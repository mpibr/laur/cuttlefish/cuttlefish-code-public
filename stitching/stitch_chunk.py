#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import sys
from mpi4py import MPI
import cv2
import scipy.ndimage
import numpy as np
import moving_least_squares
import libreg.affine_registration
import argparse
import itertools
from tqdm import tqdm
import h5py
import os.path
import sklearn.linear_model
import scipy.fftpack
import os
import random
import string
import mpi_iter
import traceback
import mapped_area
import time
import datetime
import warnings

CODE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(CODE_DIR)
from pipeline_utils import get_masterframe_info

def estimate_affine(src_mask, trg_mask, mode='rotation'):
    '''
    Parameters:
    ===========

    mode: rotation, similarity, full
    '''
    if int(cv2.__version__.split('.')[0]) == 3:
        _, src_cont, _ = cv2.findContours(src_mask.astype('uint8'), \
                cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        _, trg_cont, _ = cv2.findContours(trg_mask.astype('uint8'), \
                cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    else:
        src_cont, _ = cv2.findContours(src_mask.astype('uint8'), \
                cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        trg_cont, _ = cv2.findContours(trg_mask.astype('uint8'), \
                cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    src_ellipse = cv2.fitEllipse(src_cont[0])
    trg_ellipse = cv2.fitEllipse(trg_cont[0])
    rotation = (src_ellipse[2] - trg_ellipse[2]) / 180. * np.pi
    if mode == 'rotation':
        scale_x = scale_y = 1
    elif mode == 'similarity':
        scale_x = scale_y = (trg_ellipse[1][0] / src_ellipse[1][0] \
                + trg_ellipse[1][1] / src_ellipse[1][1]) / 2
    elif mode == 'full':
        scale_x = trg_ellipse[1][0] / src_ellipse[1][0]
        scale_y = trg_ellipse[1][1] / src_ellipse[1][1]
    else:
        raise RuntimeError('mode %s not in ' \
                '[\'rotation\', \'similarity\', \'full\']' % mode)
    shift_src = src_ellipse[0]
    shift_trg = trg_ellipse[0]
    
    # Compute transformation matrices
    alpha = np.cos(rotation)
    beta = np.sin(rotation)
    t0 = np.array([[+alpha, +beta,   (1. - alpha) * shift_src[0] \
                                           - beta * shift_src[1] \
                                   + shift_trg[0] - shift_src[0]], \
                   [-beta, +alpha,           beta * shift_src[0] \
                                   + (1. - alpha) * shift_src[1] \
                                   + shift_trg[1] - shift_src[1]]], 'float32')

    alpha = scale_x * np.cos(np.pi + rotation)
    beta = scale_y * np.sin(np.pi + rotation)
    t1 = np.array([[+alpha, +beta,   (1. - alpha) * shift_src[0] \
                                           - beta * shift_src[1] \
                                   + shift_trg[0] - shift_src[0]], \
                   [-beta, +alpha,           beta * shift_src[0] \
                                   + (1. - alpha) * shift_src[1] \
                                   + shift_trg[1] - shift_src[1]]], 'float32')

    return t0, t1

def warp_image(img, p, q, alpha, grid_size):
    identity_maps = np.dstack(np.meshgrid( \
            *tuple(np.arange(0, s + grid_size, grid_size) \
                  for s in img.shape[1::-1]))).astype('float32')
    coords = identity_maps.reshape((-1, 2))
    mapped_coords = moving_least_squares.similarity( \
            p, q, coords, alpha=alpha)
    maps = mapped_coords.reshape(identity_maps.shape)
    t = np.array([[grid_size, 0, 0], [0, grid_size, 0]], 'float32')
    maps = cv2.warpAffine(maps, t, img.shape[1::-1])
    return cv2.remap(img, maps, None, interpolation=cv2.INTER_LINEAR)

def stitch_chunk(src_mf, trg_mf, \
        src_mask, trg_mask, \
        patch_size, \
        coarse_scale, fine_scale, \
        initial_estimate, \
        search_space, \
        mls_alpha=3., \
        ransac_tolerance=1, \
        ):

    if len(search_space) == 3:
        search_space += ((None, None))
    (shift_space, d_shift), \
            (angle_space, d_angle), \
            (scale_space, d_scale), \
            (shear_space, d_shear) = search_space
            
    def norm(data):
        with np.errstate(invalid='ignore'):
            return (data - np.mean(data)) / np.std(data)

    def get_patch_at(img, pos, patch_size):
        x, y = pos
        t = np.array([[1., 0., patch_size - x], \
                      [0., 1., patch_size - y]], 'float32')
        patch = cv2.warpAffine(img, t, \
                (int(2 * patch_size + .5), int(2 * patch_size + .5)))
        return norm(patch)

    # Estimate initial affine transform
    if args.debug and (comm.rank == 0):
        print('Estimating affine transform...')
    try:
        t0, t1 = estimate_affine(src_mask, trg_mask, mode=initial_estimate)
    except RuntimeError:
        return None
    t0_inv = cv2.invertAffineTransform(t0)
    t1_inv = cv2.invertAffineTransform(t1)

    # Coarse grid
    coarse_grid = np.mgrid[tuple(np.s_[: s : coarse_scale] \
                                 for s in trg_mf.shape)]
    coarse_point_in_mask = trg_mask[coarse_grid[0], coarse_grid[1]]
    coarse_trg_coords = np.float32(coarse_grid[:, coarse_point_in_mask] \
            .T[:, ::-1])
    
    def coarse_alignment(t_inv):
        t_ide = np.identity(3, 'float32')[: 2]

        coarse_src_coords = cv2.transform( \
                coarse_trg_coords[:, None], t_inv)[:, 0]

        # Transform target for coarse grid search
        shape = tuple(int(s / d_shift) for s in src_mf.shape)
        trg_mf_t = cv2.warpAffine(trg_mf, t_inv / d_shift, shape[::-1])
        src_mf_t = cv2.warpAffine(src_mf, t_ide / d_shift, shape[::-1])

        if args.debug and (comm.rank == 0):
            trg_src = np.concatenate((trg_mf_t, src_mf_t), axis=1)
            cv2.namedWindow('trg_src',cv2.WINDOW_NORMAL)
            cv2.resizeWindow('trg_src', 1200, 300)
            cv2.imshow('trg_src', trg_src)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        def match_func(match_job):
            src_coord = match_job
            template = get_patch_at(\
                trg_mf_t, src_coord / d_shift, patch_size / d_shift)
            search_region = get_patch_at(\
                src_mf_t, src_coord / d_shift, shift_space / d_shift)
            img_fft = scipy.fftpack.fft2(search_region)
            t_corr = libreg.affine_registration.match_template_brute( \
                template, img_fft, \
                rotation=slice(0, 1, 1) if angle_space is None \
                else slice(-angle_space, +angle_space, d_angle), \
                logscale_x=slice(0, 1, 1) if scale_space is None \
                    else slice(-scale_space, +scale_space, d_scale), \
                logscale_y=slice(0, 1, 1) if scale_space is None \
                    else slice(-scale_space, +scale_space, d_scale), \
                shear=slice(0, 1, 1) if shear_space is None \
                    else slice(-shear_space, +shear_space, d_shear), \
                find_translation=libreg.affine_registration \
                    .cross_correlation_fft)
            return t_corr

        # Coarse grid search
        if args.debug and (comm.rank == 0):
            print('Coarse alignment...')

        # Set up iterables
        if (not args.patch_par) or (comm.rank == 0):
            match_input_iter = iter(coarse_src_coords)
        else:
            match_input_iter = None

        if args.patch_par:
            # Match patches in parallel
            match_result_iter = mpi_iter.parallel_map(comm, match_func, match_input_iter)
            if comm.rank == 0:
                t_corr_list = [result for result in tqdm(match_result_iter, \
                    total=len(coarse_src_coords), desc='Matching templates')]
        else:
            # Match patches in series, because masterframes were already distributed 
            # in parallel.
            t_corr_list = list(map(match_func, tqdm(match_input_iter, \
                total=len(coarse_src_coords), desc='Matching templates')))

        if (comm.rank == 0) or (not args.patch_par):
            dx = np.array([np.dot(t[:, :2], \
                    (patch_size / d_shift, patch_size / d_shift)) \
                + t[:, 2] - (shift_space / d_shift, shift_space / d_shift) \
                for t, _ in t_corr_list])
            coarse_src_coords += dx * d_shift

            corr = np.array([corr for _, corr in t_corr_list], 'float32')

            return coarse_src_coords, corr
        else:
            return None, None

    coarse_src_coords_0, coarse_corr_0 = coarse_alignment(t0_inv)
    coarse_src_coords_1, coarse_corr_1 = coarse_alignment(t1_inv)
    
    coarse_trg_coords_flt = None
    coarse_src_coords_flt = None
    if (not args.patch_par) or (comm.rank == 0):
        coarse_src_coords, coarse_corr = (coarse_src_coords_0, coarse_corr_0) \
                if np.nanmean(coarse_corr_0) > np.nanmean(coarse_corr_1) else \
                (coarse_src_coords_1, coarse_corr_1)

        # Filter out points
        for _ in range(8):
            median_absolute_deviation = np.median(np.abs(\
                coarse_src_coords - np.median(coarse_src_coords)))
            affine_model = sklearn.linear_model.RANSACRegressor( \
                sklearn.linear_model.LinearRegression(), max_trials=2048, \
                residual_threshold = ransac_tolerance*median_absolute_deviation, \
                loss='squared_loss')
            try:
                affine_model.fit(coarse_trg_coords, coarse_src_coords)
            except ValueError:
                continue
            else:
                break
        else:
            raise RuntimeError('RANSAC did not converge.')
        coarse_trg_coords_flt = np.ascontiguousarray( \
                coarse_trg_coords[affine_model.inlier_mask_], 'float32')
        coarse_src_coords_flt = np.ascontiguousarray( \
                coarse_src_coords[affine_model.inlier_mask_], 'float32')

    if args.patch_par:
        # Broadcast results
        coarse_trg_coords_flt = comm.bcast(coarse_trg_coords_flt, root=0)
        coarse_src_coords_flt = comm.bcast(coarse_src_coords_flt, root=0)

    # Warp trg_mf with the coarse grid transformation
    trg_mf_warped = warp_image(trg_mf, \
            coarse_src_coords_flt, coarse_trg_coords_flt, mls_alpha, 32)

    # Show RANSAC match points
    if args.debug and (comm.rank == 0):
        trg_mf_warped_ransac = cv2.cvtColor(trg_mf_warped, cv2.COLOR_GRAY2BGR)
        inliers = coarse_src_coords_flt
        outliers = np.ascontiguousarray( \
            coarse_src_coords[~affine_model.inlier_mask_], 'float32')
        for x, y in inliers.astype(int):
            cv2.circle(trg_mf_warped_ransac, (x, y), 25, (0, 0, 255), -1, cv2.LINE_AA)
        for x, y in outliers.astype(int):
            cv2.circle(trg_mf_warped_ransac, (x, y), 25, (127, 127, 127), -1, cv2.LINE_AA)

        src_mf_ransac = warp_image(src_mf, \
            coarse_trg_coords_flt, coarse_src_coords_flt, mls_alpha, 32)
        src_mf_ransac = cv2.cvtColor(src_mf_ransac, cv2.COLOR_GRAY2BGR)
        inliers = coarse_trg_coords_flt
        outliers = np.ascontiguousarray( \
            coarse_trg_coords[~affine_model.inlier_mask_], 'float32')
        if np.nanmean(coarse_corr_0) > np.nanmean(coarse_corr_1):
            h, w = src_mf.shape
            inliers = (w, h) - inliers
            outliers = (w, h) - outliers
        for x, y in inliers.astype(int):
            cv2.circle(src_mf_ransac, (x, y), 25, (0, 0, 255), -1, cv2.LINE_AA)
        for x, y in outliers.astype(int):
            cv2.circle(src_mf_ransac, (x, y), 25, (127, 127, 127), -1, cv2.LINE_AA)
        src_mf_ransac = warp_image(src_mf_ransac, \
            coarse_src_coords_flt, coarse_trg_coords_flt, mls_alpha, 32)

        ransac_trg_src = np.concatenate((trg_mf_warped_ransac, src_mf_ransac), axis=1)
        cv2.namedWindow('RANSAC',cv2.WINDOW_NORMAL)
        cv2.resizeWindow('RANSAC', 1200, 300)
        cv2.imshow('RANSAC', ransac_trg_src)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    # Fine grid
    fine_grid = np.mgrid[tuple(np.s_[: s + fine_scale : fine_scale] \
                        for s in trg_mf.shape)]
    fine_point_in_mask = np.zeros(fine_grid.shape[1:], 'bool')
    end_y = fine_grid.shape[1] - 1
    end_x = fine_grid.shape[2] - 1
    fine_point_in_mask[: end_y, : end_x] = \
            trg_mask[fine_grid[0, : end_y, : end_x], \
                    fine_grid[1, : end_y, : end_x]]
    fine_coord_in_grid = fine_point_in_mask[ \
            np.ones(fine_grid.shape[1:], 'bool')]
    fine_trg_coords = np.ascontiguousarray( \
            fine_grid.reshape((2, -1)).T[:, ::-1], 'float32')
    fine_src_coords = moving_least_squares.similarity( \
            coarse_trg_coords_flt, coarse_src_coords_flt, \
            fine_trg_coords, mls_alpha)

    # Estimate new shifts
    def shift_func(shift_job):
        src_coord = shift_job
        src_patch = get_patch_at(trg_mf_warped, src_coord, patch_size)
        src_fft = scipy.fftpack.fft2(src_patch)
        trg_patch = get_patch_at(src_mf, src_coord, patch_size)
        trg_fft = scipy.fftpack.fft2(trg_patch)
        dx_corr = libreg.affine_registration.cross_correlation_fft( \
            src_fft, trg_fft)
        return dx_corr

    if args.debug and (comm.rank == 0):
        print('Fine shift...')

    # Set up iterables
    if (not args.patch_par) or (comm.rank == 0):
        shift_input_iter = iter(fine_src_coords[fine_coord_in_grid])
    else:
        shift_input_iter = None

    if args.patch_par:
        # Shift patches in parallel
        shift_result_iter = mpi_iter.parallel_map(comm, shift_func, shift_input_iter)
        if comm.rank == 0:
            dx_corr_list = [result for result in tqdm(shift_result_iter, \
                total=len(fine_src_coords[fine_coord_in_grid]), \
                desc='Shifting patches')]
    else:
        # Shift patches in series, because masterframes were already distributed
        # in parallel.
        dx_corr_list = list(map(shift_func, tqdm(shift_input_iter, \
            total=len(fine_src_coords[fine_coord_in_grid]), \
            desc='Shifting patches')))

    if (not args.patch_par) or (comm.rank == 0):
        dx = np.array([dx for dx, _ in dx_corr_list], 'float32')
        corr = np.array([corr for _ , corr in dx_corr_list], 'float32')

    if args.debug and (comm.rank == 0):
        fine_corr = np.zeros(fine_grid.shape[1 : ], 'float32')
        fine_corr[fine_point_in_mask] = corr
        fine_corr = (fine_corr * 255).astype('uint8')
        fine_corr = cv2.resize(fine_corr, trg_mf.shape[::-1], interpolation = cv2.INTER_CUBIC)
        fine_corr = cv2.applyColorMap(fine_corr, cv2.COLORMAP_HOT)
        cv2.namedWindow('fine_corr',cv2.WINDOW_NORMAL)
        cv2.resizeWindow('fine_corr', 600, 300)
        cv2.imshow('fine_corr', fine_corr)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    
    # Apply fine shifts
    small_map = np.empty(fine_grid.shape[1 : ] + (2, ), 'float32')
    small_map[np.ones(fine_grid.shape[1:], 'bool')] = fine_src_coords
    small_map[fine_point_in_mask] += dx
            
    return small_map, coarse_trg_coords_flt, coarse_src_coords_flt

if __name__ == '__main__':

    stitching_start_time = time.time()

    comm = MPI.COMM_WORLD
    
    p = argparse.ArgumentParser(\
        'Stitch multiple source masterframes to the same target masterframe.', \
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    p.add_argument('trg_masterframe')
    p.add_argument('src_masterframes', nargs='+')
    p.add_argument('output')
    p.add_argument('--patch-size', type=int, default=64, help='Area around '
            'a point to be matched using cross-correlation.')
    p.add_argument('--grid-size', type=int, default=32, help='Resolution of '
            'the output maps. This is used after the coarse grid search '
            'refining the maps using fft cross-correlation.')
    p.add_argument('--coarse-grid-size', type=int, default=256, \
            help='Coarse grid size for matching using the brute force search.')
    p.add_argument('--initial-estimate', default='rotation', \
            help='Type of affine transformation to estimate from the '
            'cuttlefish mask, may be: rotation which allows only rotation or '
            'similarity which allows uniform scaling or full which '
            'allows scaling to be independent in x and y.')
    p.add_argument('--search-space', default='512:2,0.35:0.035,:,:', \
            help='Search space as a comma separated list of ' \
            '[shift],[angle],[scale],[shear]; every parameter is passed as '
            '[range:step]. shift is in pixels, angle is in radians. scale is '
            'log transformed and shear is a horizontal one. If no value is '
            'specified value is set to 0:0. E.g. 512:2,0.35:0.035,:,: will '
            'search for a match in a 512px region, step size 2px. Rotations '
            'allowed are from -0.35 ~= -20° to +0.35 ~= +20° with a step '
            'size of 0.035 ~= 2°. No scaling and no rotation.')
    p.add_argument('--ransac-tolerance', default=1, type=float, \
            help='Maximum RANSAC residue to be consider inlier in '
            'coarse alignment, expressed as muliple of the MAD.'
            'Higher value results in more non-linear coarse grid matching.')
    p.add_argument('--patch-par', action='store_true', \
            help='MPI parallel processing at patch level instead of '
            'masterframe level. Useful when number of source masterframes '
            'is small but the search space is large.')
    p.add_argument('--debug', action='store_true', \
            help='Show images for debugging. Press any key when image window'
            'is in-focus to continue.')

    warnings.filterwarnings("ignore", category=FutureWarning, module='scipy.fftpack')

    args = p.parse_args()
    args.search_space = tuple(tuple(None if e == '' else float(e) \
            for e in t.split(':')) \
            for t in args.search_space.split(','))
   
    # Filter out non existing input src files and the trg masterframe
    args.src_masterframes = [filename for filename in args.src_masterframes \
            if os.path.exists(filename)]
    if len(args.src_masterframes) == 0:
        raise RuntimeError('No input files!')

    if not os.path.exists(args.trg_masterframe):
        raise RuntimeError('%s does not exist!' % args.trg_masterframe)

    trg_mf, trg_mask, _ = get_masterframe_info(h5py.File(args.trg_masterframe, 'r'))
    height, width = trg_mf.shape
    
    if trg_mask is None:
        raise RuntimeError('unable to compute mask for %s' % args.trg_masterframe)

    def func(job):
        (src_idx, mf_filename) = job
        try:
            if(mf_filename == args.trg_masterframe):
                small_map = np.nan
                coarse_trg_coords_flt = np.zeros(1)
                coarse_src_coords_flt = np.zeros(1)
            else:
                src_mf, src_mask, _ = get_masterframe_info(h5py.File(mf_filename, 'r'))

                if src_mask is None:
                    small_map = np.nan
                    coarse_trg_coords_flt = np.zeros(1)
                    coarse_src_coords_flt = np.zeros(1)
                else:
                    # small_map = stitch_chunk(src_mf, trg_mf, \
                    #         src_mask, trg_mask, \
                    #         patch_size=args.patch_size, \
                    #         fine_scale=args.grid_size, \
                    #         coarse_scale=args.coarse_grid_size, \
                    #         initial_estimate=args.initial_estimate, \
                    #         search_space=args.search_space, \
                    #         ransac_tolerance=args.ransac_tolerance)
                    small_map, coarse_trg_coords_flt, coarse_src_coords_flt = stitch_chunk(src_mf, trg_mf, \
                            src_mask, trg_mask, \
                            patch_size=args.patch_size, \
                            fine_scale=args.grid_size, \
                            coarse_scale=args.coarse_grid_size, \
                            initial_estimate=args.initial_estimate, \
                            search_space=args.search_space, \
                            ransac_tolerance=args.ransac_tolerance)
        except:
            error = traceback.format_exc()
            # return src_idx, np.nan, error
            return src_idx, np.nan, np.nan, np.nan, error
        # return src_idx, small_map, None
        return src_idx, small_map, coarse_trg_coords_flt, coarse_src_coords_flt, None

    if args.patch_par or (comm.rank == 0):

        # Process stitching for trg_masterframe
        small_maps = []
        small_maps_shape = (len(args.src_masterframes), (height + 2 * args.grid_size - 1) // args.grid_size, \
                           (width  + 2 * args.grid_size - 1) // args.grid_size, \
                           2)
        
        small_maps = np.zeros(small_maps_shape, 'float32')
        
        output_dirname = os.path.dirname(args.output)
        output_basename = os.path.basename(args.output)
        suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                for _ in range(4))
        tmp_output_basename = '.' + output_basename + '.' + suffix
        tmp_output = os.path.join(output_dirname, tmp_output_basename)
        stitching_chunk_file = h5py.File(tmp_output, 'w')
        
        stitching_mat_dset = stitching_chunk_file.create_dataset( \
                    'small_maps', \
                    shape=small_maps_shape, \
                    dtype='float32')

        coarse_grid = np.mgrid[tuple(np.s_[: s : args.coarse_grid_size] \
                                 for s in trg_mf.shape)]

        src_coords_dset = stitching_chunk_file.create_dataset( \
                 'coarse_src_coords', \
                 shape=(len(args.src_masterframes),coarse_grid.shape[1]*coarse_grid.shape[2],2), \
                 dtype='float32')
            
        trg_coords_dset = stitching_chunk_file.create_dataset( \
                 'coarse_trg_coords', \
                 shape=(len(args.src_masterframes),coarse_grid.shape[1]*coarse_grid.shape[2],2), \
                 dtype='float32')

                    
        # Save grid size for map interpolation
        stitching_chunk_file.attrs.create('grid_size', args.grid_size, \
            dtype='uint16')

    input_iter = iter(enumerate(args.src_masterframes))
            
    # Process stitching
    if args.patch_par:
        # Stitch masterframes in series
        if comm.rank == 0:
            input_iter = tqdm(input_iter, \
                    total=(len(args.src_masterframes) - 1), \
                    desc='Stitching')
        for mf in input_iter:
            src_idx, small_map, coarse_trg_coords, coarse_src_coords, error = func(mf)
            stitching_mat_dset[src_idx] = small_map
            if small_map is not np.nan:
                src_coords_dset[src_idx,0:coarse_src_coords.shape[0],:] = coarse_src_coords
                trg_coords_dset[src_idx,0:coarse_trg_coords.shape[0],:] = coarse_trg_coords
    else:
        # Stitch masterfrmaes in in parallel
        stitching_mat_iter = mpi_iter.parallel_map(comm, func, input_iter)
        if comm.rank == 0:
            # for src_idx, small_map, error in tqdm(stitching_mat_iter, \
            for src_idx, small_map, coarse_trg_coords, coarse_src_coords, error in \
                tqdm(stitching_mat_iter, \
                    total=(len(args.src_masterframes) - 1), \
                    desc='Stitching'):
                stitching_mat_dset[src_idx] = small_map
                if small_map is not np.nan:
                    src_coords_dset[src_idx,0:coarse_src_coords.shape[0],:] = coarse_src_coords
                    trg_coords_dset[src_idx,0:coarse_trg_coords.shape[0],:] = coarse_trg_coords

        
    if comm.rank == 0:

        # Save chunk filenames
        rel_masterframes = [os.path.relpath(masterframe, \
                    os.path.dirname(args.output)) \
                    for masterframe in args.src_masterframes]
        rel_masterframes = [x.encode('utf8') for x in rel_masterframes]
        stitching_chunk_file.create_dataset('chunks', data=rel_masterframes, \
            dtype=h5py.special_dtype(vlen=str))

        stitching_mat_mask = stitching_chunk_file.create_dataset( \
                    'chunkaverage_mask', \
                    shape=(height, width), \
                    dtype='bool')
                    
        stitching_mat_mask[...] = trg_mask
        stitching_chunk_file.close()
        os.rename(tmp_output, args.output)

        print(args.output + " written.")

        if args.debug:
            time_elapsed = time.time() - stitching_start_time
            print('Stitching completed in ' + str(datetime.timedelta(seconds=time_elapsed)))
