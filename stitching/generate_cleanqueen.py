#!/usr/bin/env python3

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import h5py
import cv2
import skimage.feature
from skimage.segmentation import watershed
import scipy
import os.path
import random
import numpy as np
import string
#import pdb

def cleanqueen(queen, dilation, thresh, peak_footprint):
    # Find cuttlefish mask
    
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, \
            (dilation, dilation))
    dilated = cv2.dilate(queen, kernel)
    cuttlefish_mask = dilated > thresh

    # Find chromatophore positions by peak detection

    filteredImg=scipy.zeros(queen.shape, dtype=scipy.float32)
    scipy.ndimage.filters.gaussian_laplace(queen, 15, filteredImg)
    filteredImg2=queen-filteredImg
    filteredImg2[filteredImg2<0]=0
    
    chroma_pos = skimage.feature.peak_local_max(filteredImg2, indices=False, \
            footprint=np.ones((peak_footprint, peak_footprint),'bool'), \
          threshold_rel=0.2, labels=cuttlefish_mask)
    labelled_chroma_pos = scipy.ndimage.label(chroma_pos)[0]
    #pdb.set_trace()
    # Generate the cleanqueen by watershedding
    return watershed(-filteredImg2, labelled_chroma_pos, \
            mask=cuttlefish_mask)

if __name__ == '__main__':
    p = argparse.ArgumentParser('Compute the cleanqueen')
    p.add_argument('queenframe')
    p.add_argument('cleanqueen')
    p.add_argument('--dilation', default=61, type=int, \
            help='Dilation of cuttlefish detection. Should be 2 times the ' \
            'chromatophore distance.')
    p.add_argument('--thresh', default=0.25, type=float, \
            help='Treshold for cuttlefish detection. Should be roughly ' \
            'smaller than the average time a chromatophore is detectable')
    p.add_argument('--peak-footprint', default=5, type=int, \
            help='Footprint of a peak')
    args = p.parse_args()
    
    # Open the queenframe and read its contents
    with h5py.File(args.queenframe, 'r') as f:
        labels = f.attrs['labels']
        num_frames = f.attrs['num_frames']
        ref_chunk = f.attrs['ref_chunk']
        chunk_mask = f.attrs['chunk_mask']
        rel_stitching = f.attrs['stitching']
        queenframe_dict = {label: f[label.replace('/', '-')].value \
                / float(num_frames) for label in labels}
    
    # Get absoulte paths of previous input files
    stitching = os.path.join(os.path.dirname(args.queenframe), \
            rel_stitching)
    
    # Apply the cleanup for every label
    cleanqueen_dict = {label: cleanqueen(queen, \
            args.dilation, args.thresh, args.peak_footprint) \
            for label, queen in queenframe_dict.items()}
    
    # Generate relative paths of previous input files
    rel_stitching = os.path.relpath(stitching, \
            os.path.dirname(args.cleanqueen))
    rel_queenframe = os.path.relpath(args.queenframe, \
            os.path.dirname(args.cleanqueen))
    
    # Write data to temp file for safety
    output_dirname = os.path.dirname(args.cleanqueen)
    output_basename = os.path.basename(args.cleanqueen)
    suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
            for _ in range(4))
    tmp_output_basename = '.' + output_basename + '.' + suffix
    tmp_output = os.path.join(output_dirname, tmp_output_basename)

    # Write data
    with h5py.File(tmp_output, 'w') as f:
        f.attrs.create('labels', labels, \
                dtype=h5py.special_dtype(vlen=str))
        f.attrs.create('num_frames', num_frames)
        f.attrs.create('ref_chunk', ref_chunk)
        f.attrs.create('chunk_mask', chunk_mask)
        f.attrs.create('stitching', rel_stitching, \
                dtype=h5py.special_dtype(vlen=str))
        f.attrs.create('queenframe', rel_queenframe, \
                dtype=h5py.special_dtype(vlen=str))
        for label, label_cleanqueen in cleanqueen_dict.items():
            f.create_dataset(label.replace('/', '-'), data=label_cleanqueen, \
                    dtype='uint32')
    
    # Now rename the file and quit
    os.rename(tmp_output, args.cleanqueen)

