#!/usr/bin/env python
# coding: utf-8

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import cv2
import scipy.ndimage
import numpy as np
import sklearn.neighbors
import h5py
import matplotlib.pyplot as plt

def get_marker(img, color, corners):
    '''
    Finds the four nearest corner indices that lie on the coloured marker
    '''

    dist = np.linalg.norm(img.astype('float32') - color, axis=2)
    mask = dist < 10
    marker_pos = scipy.ndimage.center_of_mass(mask)[::-1]
    corner_dist = np.linalg.norm(corners - marker_pos, axis=1)
    dist_argsort = np.argsort(corner_dist)
    return dist_argsort[: 4]
    
def contiguous_regions(condition):
    """
    Finds contiguous True regions of the boolean array "condition". Returns
    a 2D array where the first column is the start index of the region and the
    second column is the end index.

    Source: http://stackoverflow.com/questions/4494404/find-large-number-of- \
            consecutive-values-fulfilling-condition-in-a-numpy-array
    
    """

    # Find the indicies of changes in "condition"
    d = np.diff(condition)
    idx, = d.nonzero() 

    # We need to start things after the change in "condition". Therefore, 
    # we'll shift the index by 1 to the right.
    idx += 1

    if condition[0]:
        # If the start of condition is True prepend a 0
        idx = np.r_[0, idx]

    if condition[-1]:
        # If the end of condition is True, append the length of the array
        idx = np.r_[idx, condition.size] # Edit

    # Reshape the result into two columns
    idx.shape = (-1,2)
    return idx

def expand_east(grid, grid_set, ungraph):
    # Array of found indices
    expands = np.empty(grid.shape[: 1], 'int64')
    expands[:] = -1

    # Expand all contigous regions of found indicesa
    for start, end in contiguous_regions(grid[:, -1] >= 0):
        if end - start >= 3:
            # The interval is large enough for expansion

            # Expand without corners
            for row in range(start + 1, end - 1):
                idx = grid[row, -1]
                neighbours, = np.where(ungraph[idx])
                next_indexes = set(neighbours) - grid_set
                if len(next_indexes) == 1:
                    expands[row] = next(iter(next_indexes))
            
            # Find the top corner
            if expands[start + 1] >= 0:
                idx = grid[start, -1]
                neighbours, = np.where(ungraph[idx])
                next_indexes = [index \
                        for index in (set(neighbours) - grid_set) \
                        if ungraph[expands[start + 1], index]]
                if len(next_indexes) == 1:
                    expands[start] = next(iter(next_indexes))
            
            # Find the bottom corner
            if expands[end - 2] >= 0:
                idx = grid[end - 1, -1]
                neighbours, = np.where(ungraph[idx])
                next_indexes = [index \
                        for index in (set(neighbours) - grid_set) \
                        if ungraph[expands[end - 2], index]]
                if len(next_indexes) == 1:
                    expands[end - 1] = next(iter(next_indexes))

    return expands

if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('--grid', type=int, default=16)
    p.add_argument('--width', type=int)
    p.add_argument('--height', type=int)
    p.add_argument('--input')
    p.add_argument('output')
    args = p.parse_args()
    
    # Read in image 
    img = cv2.imread(args.input)
    if args.width is not None:
        img = img[:, : args.width]
    if args.height is not None:
        img = img[: args.height, :]
    height, width, _ = img.shape

    # Get corners
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    harris = cv2.cornerHarris(gray, 3, 3, 0.001)
    thresh = harris > 0.0025
    lbls, nl = scipy.ndimage.label(thresh)
    corners = np.array(scipy.ndimage.measurements.center_of_mass( \
            thresh, lbls, range(1, nl +1)))[:, ::-1].astype('float32')
    cv2.cornerSubPix(gray, corners, (5, 5), (-1, -1), \
            (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.001))
    plt.imshow(img[...,::-1])
    plt.scatter(corners[:, 0], corners[:, 1])
    plt.show()

    # Construct graph from corners
    graph = sklearn.neighbors.kneighbors_graph(corners, 5)
    graph = np.array(graph.astype('bool').todense())
    ungraph = graph & graph.T

    graph_img = np.zeros_like(img)
    for i in range(ungraph.shape[0]):
        p = corners[i]
        p_int = tuple((p * 16).astype(int))
        js, = np.where(ungraph[i])
        for j in js:
            q = corners[j]
            q_int = tuple((q * 16).astype(int))
            cv2.line(graph_img, \
                    p_int, \
                    q_int, \
                    color=(255, 255, 255), shift=4)
    plt.imshow(graph_img[..., ::-1])
    plt.show()

    # Generate the marker colours
    orig_shape = (height / args.grid + 1, width / args.grid + 1)
    my, mx = (s / 2 - 1 for s in orig_shape)
    
    # Dark markers are positioned at even coordinates;
    # Create colours for the marker
    marker_ycrcb = np.array([(0, 0,     0), \
                             (0, 255,   0), \
                             (0, 0,   255), \
                             (0, 255, 255)], 'uint8')
    marker_ycrcb[(0, 3) if mx % 2 and mx % 2 else (1, 2), 0] = 255
    marker_bgr = cv2.cvtColor(marker_ycrcb[None], cv2.COLOR_YCR_CB2BGR)[0]

    # Retrieve our markers
    markers = [set(get_marker(img, marker_bgr[i], corners)) for i in range(4)]
    
    # Find the marker corners based on the colours
    midpoints = set.intersection(*markers)
    if len(midpoints) != 1:
        raise RuntimeError('M marker not found')
    midpoint = next(iter(midpoints))
    norths = (markers[0] & markers[1]) - midpoints
    if len(norths) != 1:
        raise RuntimeError('N marker not found')
    north = next(iter(norths))
    souths = (markers[2] & markers[3]) - midpoints
    if len(souths) != 1:
        raise RuntimeError('S marker not found')
    south = next(iter(souths))
    easts = (markers[1] & markers[3]) - midpoints
    if len(easts) != 1:
        raise RuntimeError('E marker not found')
    east = next(iter(easts))
    wests = (markers[0] & markers[2]) - midpoints
    if len(wests) != 1:
        raise RuntimeError('W marker not found')
    west = next(iter(wests))
    north_wests = markers[0] - set.union(*markers[ : 0] + markers[1 : ])
    if len(north_wests) != 1:
        raise RuntimeError('NW marker not found')
    north_west = next(iter(north_wests))
    north_easts = markers[1] - set.union(*markers[ : 1] + markers[2 : ])
    if len(north_easts) != 1:
        raise RuntimeError('NE marker not found')
    north_east = next(iter(north_easts))
    south_wests = markers[2] - set.union(*markers[ : 2] + markers[3 : ])
    if len(south_wests) != 1:
        raise RuntimeError('SW marker not found')
    south_west = next(iter(south_wests))
    south_easts = markers[3] - set.union(*markers[ : 3] + markers[4 : ])
    if len(south_easts) != 1:
        raise RuntimeError('SE marker not found')
    south_east = next(iter(south_easts))
    
    # Run iterative expansion on the sides
    grid = np.array([[north_west, north, north_east],
                     [west, midpoint, east],
                     [south_west, south, south_east]])
    grid_set = set(grid.flatten())
    grid_mx, grid_my = 1, 1
    while True:
        expanded_indexes = set()
        
        # Expand to east
        expand = expand_east(grid, grid_set, ungraph)
        expand_set = set(expand) - set([-1])
        if expand_set:
            grid = np.c_[grid, expand]
            expanded_indexes |= expand_set
            grid_set |= expand_set

        # Expand to west
        expand = expand_east(grid[:, ::-1], grid_set, ungraph)
        expand_set = set(expand) - set([-1])
        if expand_set:
            grid_mx += 1
            grid = np.c_[expand, grid]
            expanded_indexes |= expand_set
            grid_set |= expand_set

        # Expand to south
        expand = expand_east(grid.T, grid_set, ungraph)
        expand_set = set(expand) - set([-1])
        if expand_set:
            grid = np.r_[grid, [expand]]
            expanded_indexes |= expand_set
            grid_set |= expand_set

        # Expand to north
        expand = expand_east(grid.T[:, ::-1], grid_set, ungraph)
        expand_set = set(expand) - set([-1])
        if expand_set:
            grid_my += 1
            grid = np.r_[[expand], grid]
            expanded_indexes |= expand_set
            grid_set |= expand_set

        if len(expanded_indexes) == 0:
            break
    
    # Now calculate the maps

    # Same size as the small checkerboard, but 1 row added to have the corners
    # in
    small_maps = np.empty((height / args.grid + 2, width / args.grid + 2, 2), \
            'float32')
    small_maps[:] = np.nan
    shape = (height / args.grid + 1, width / args.grid + 1)
    my, mx = (s / 2 for s in shape)
    coord_ranges = tuple(range(s) for s in small_maps.shape[:2][::-1])
    coords_x, coords_y = np.meshgrid(*coord_ranges)
    for x, y in zip(coords_x.flatten(), coords_y.flatten()):
        # Lookup index in grid
        grid_x = x + grid_mx - mx
        grid_y = y + grid_my - my
        if 0 <= grid_y < grid.shape[0] and 0 <= grid_x < grid.shape[1] and \
            grid[grid_y, grid_x] >= 0:
            small_maps[y, x] = corners[grid[grid_y, grid_x]]
    # Estimate position of the square centres
    small_maps = np.mean((small_maps[1 : , :], small_maps[: -1, :]), axis=0)
    small_maps = np.mean((small_maps[:, 1 : ], small_maps[:, : -1]), axis=0)
    # Resize grid up
    maps = cv2.resize(small_maps, (0, 0), fx=args.grid, fy=args.grid)
    maps = maps[: height, : width]
    
    found_corners = small_maps[np.isfinite(small_maps[..., 0]) & np.isfinite(small_maps[..., 1])]
    plt.imshow(img[..., ::-1])
    plt.scatter(found_corners[:, 0], found_corners[:, 1])
    plt.show()

    # Save maps
    output = h5py.File(args.output)
    try:
        output.create_dataset('maps', data=maps)
    finally:
        output.close()

