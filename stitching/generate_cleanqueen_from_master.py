#!/usr/bin/env python2

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import h5py
import cv2
import skimage.feature
import skimage.morphology
import skimage.measure
import scipy
import os.path
import random
import numpy as np
import string
from tqdm import tqdm



#import pdb
def extract_areas(segmentation, **kwargs):
    # chunk_index = job[0]
    # segmentation = job[1]
    cleanqueen = kwargs['cleanqueen']
    # maps = kwargs['maps']
    num_labels = kwargs['num_labels']

    # Warp segmentation
    # registered = cv2.remap(registered, \
                           # maps, None, \
                           # interpolation=cv2.INTER_LINEAR) / 255
    registered = segmentation.astype(float) / 255
    print(registered.max())
    labeledBool = registered > 0.3
    labeled_regions = labeledBool * cleanqueen
    areas = np.bincount(labeled_regions.flatten(), \
                        minlength=int(num_labels + 1), \
                        weights=registered.flatten())[1:]
    return areas


def find_the_large_mask(input_labeled_image, input_intensity_image):
    # count = 0

    prop_rmask = skimage.measure.regionprops(input_labeled_image,
                                             intensity_image=input_intensity_image)
    print(len(prop_rmask))
    mark = prop_rmask[0]
    max_area = prop_rmask[0].area
    for region in prop_rmask:
        if region.area > max_area:
            # count = count +1
            max_area = region.area
            mark = region

    return (mark)


def cleanqueen(queen, dilation, thresh, peak_footprint):
    # Find cuttlefish mask
    
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, \
            (dilation, dilation))
    dilated = cv2.dilate(queen, kernel)

    dl = np.ndarray.flatten(dilated)
    
    # thresh = np.median(dl)
    
    # thresh = np.median(dl[dl > dl.max() / 10])
    print(thresh)
    cuttlefish_mask = dilated > thresh
    labels = skimage.measure.label(cuttlefish_mask, connectivity=2)
    # prop_rmask = skimage.measure.regionprops(labels, intensity_image=dilated)
    props=find_the_large_mask(labels,dilated)
    cuttlefish_mask2=np.zeros(cuttlefish_mask.shape)
    cuttlefish_mask2[props._slice]=props.filled_image
    cuttlefish_mask = (cuttlefish_mask2 ==1)
    # plt.figure()
    # plt.imshow(queen)
    # plt.imshow(cuttlefish_mask, alpha=0.1)
    # Find chromatophore positions by peak detection

    filteredImg=scipy.zeros(queen.shape, dtype=scipy.float32)
    scipy.ndimage.filters.gaussian_laplace(queen, 15, filteredImg)
    filteredImg2=queen-filteredImg
    filteredImg2[filteredImg2<0]=0
    # filteredImg3=scipy.ndimage.filters.gaussian_filter(filteredImg2, 5)
    
    chroma_pos = skimage.feature.peak_local_max(filteredImg2, indices=False, \
            footprint=np.ones((peak_footprint, peak_footprint),'bool'), \
          threshold_rel=0.08, labels=cuttlefish_mask)
    labelled_chroma_pos = scipy.ndimage.label(chroma_pos)[0]
    #pdb.set_trace()
    # Generate the cleanqueen by watershedding
    cq = skimage.morphology.watershed(-filteredImg2, labelled_chroma_pos, \
            mask=cuttlefish_mask)
    return cq


if __name__ == '__main__':
    p = argparse.ArgumentParser('Compute the cleanqueen')
    p.add_argument('segmentation')
    p.add_argument('cleanqueen')
    p.add_argument('areas')
    p.add_argument('--dilation', default=61, type=int, \
            help='Dilation of cuttlefish detection. Should be 2 times the ' \
            'chromatophore distance.')
    p.add_argument('--thresh', default=0.25, type=float, \
            help='Treshold for cuttlefish detection. Should be roughly ' \
            'smaller than the average time a chromatophore is detectable')
    p.add_argument('--peak-footprint', default=5, type=int, \
            help='Footprint of a peak')
    args = p.parse_args()
    
    # Open the queenframe and read its contents
    segmentation_file = h5py.File(args.segmentation, 'r')
    # with h5py.File(args.segmentation, 'r') as f:
    labels = segmentation_file.attrs['labels']
    num_frames = segmentation_file.attrs['num_frames']
    queen = segmentation_file['masterframe'][0,:,:,0]
    segmentation = segmentation_file['segmentation']

    queen = (queen).astype(float)/num_frames
    print(queen.max())
    label_cleanqueen=cleanqueen(queen, args.dilation, args.thresh, args.peak_footprint)
    
    # Write data to temp file for safety
    output_dirname = os.path.dirname(args.cleanqueen)
    output_basename = os.path.basename(args.cleanqueen)
    suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
            for _ in range(4))
    tmp_output_basename = '.' + output_basename + '.' + suffix
    tmp_output = os.path.join(output_dirname, tmp_output_basename)

    # Write data
    with h5py.File(tmp_output, 'w') as cq_f:
        cq_f.attrs.create('labels', labels, \
                dtype=h5py.special_dtype(vlen=str))
        cq_f.attrs.create('num_frames', num_frames)
        cq_f.create_dataset(labels[0].replace('/', '-'), data=label_cleanqueen, \
                         dtype='uint32')

    # Now rename the file and quit
    os.rename(tmp_output, args.cleanqueen)

    output_dirname = os.path.dirname(args.areas)
    output_basename = os.path.basename(args.areas)
    suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) \
                     for _ in range(4))
    tmp_output_basename = '.' + output_basename + '.' + suffix
    tmp_output = os.path.join(output_dirname, tmp_output_basename)
    areas_file = h5py.File(tmp_output, 'w')
    chunksize = 1  # min(num_frames, max(1, BLOCKSIZE_MB * 1024 * 1024 / all_num_labels / 4)).astype('int')

    num_labels = np.max(label_cleanqueen, axis=(0, 1))
    all_num_labels = int(np.sum(num_labels))
    areas = areas_file.create_dataset('areas', \
                                      shape=(num_frames, all_num_labels), \
                                      dtype='uint32', \
                                      compression='gzip', \
                                      shuffle=True, \
                                      chunks=(chunksize, all_num_labels))
    for i in tqdm(range(segmentation.shape[0])):
        seg = segmentation[i,:,:,0]#*mask
        areas[i,:]=extract_areas(seg,cleanqueen=label_cleanqueen, num_labels=num_labels)

    areas_file.close()
    os.rename(tmp_output, args.areas)
    segmentation_file.close()


