#!/usr/bin/env python2

#
# (C) Copyright 2015 Frankfurt Institute for Advanced Studies
# (C) Copyright 2016 Max-Planck Institute for Brain Research
#
# Author: Philipp Huelsdunk  <huelsdunk@fias.uni-frankfurt.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#     * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
#     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import cv2
import numpy as np
import skimage.morphology
import skimage.measure
from tqdm import tqdm
import os.path
import h5py
import warnings
import buffertools
import itertools
from mp4file.mp4file import Mp4File

def register_frame(chunk_index, video_msec, registration_small_map, frame, \
        **kwargs):
    cleanqueens = kwargs['cleanqueens']
    stitching_grid_size = kwargs['stitching_grid_size']
    stitching_column = kwargs['stitching_column']
    registration_grid_sizes = kwargs['registration_grid_sizes']
    label_indexes = kwargs['label_indexes']

    registration_grid_size = registration_grid_sizes[chunk_index]

    # Combine the maps
    stitching_small_map = stitching_column[chunk_index]
    small_maps = cv2.remap(registration_small_map, \
            stitching_small_map / registration_grid_size, None, \
            interpolation=cv2.INTER_LINEAR)
    enlarge_t = np.array([[stitching_grid_size, 0, 0], \
                          [0, stitching_grid_size, 0]], 'float32')
    maps = cv2.warpAffine(small_maps, enlarge_t, \
            frame.shape[1::-1])

    # Warp segmentation
    registered = cv2.remap(frame, maps, None, \
            interpolation=cv2.INTER_LINEAR)

    return chunk_index, video_msec, registered

if __name__ == '__main__':
    p = argparse.ArgumentParser('Play registered video')
    p.add_argument('--crop')
    p.add_argument('--scale', type=float)
    p.add_argument('cleanqueen')
    p.add_argument('--output')
    p.add_argument('--codec', default="MP4V")
    p.add_argument('--use-chunks', type=int, nargs='*')
    args = p.parse_args()

    cleanqueen_file = h5py.File(args.cleanqueen, 'r')

    queenframe_filename = os.path.join(os.path.dirname(args.cleanqueen), \
            cleanqueen_file.attrs['queenframe'])
    queenframe_file = h5py.File(queenframe_filename, 'r')

    stitching_filename = os.path.join(os.path.dirname(args.cleanqueen), \
            cleanqueen_file.attrs['stitching'])
    stitching_file = h5py.File(stitching_filename, 'r')

    masterframe_filenames = [ \
            (chunk_index, os.path.join( \
                os.path.dirname(queenframe_filename), \
                masterframe_filename)) \
            for chunk_index, (masterframe_filename, m) \
            in enumerate(zip(stitching_file['chunks'], \
                   queenframe_file.attrs['chunk_mask'])) \
            if m]
    
    # Read info all ranks need to know
    cleanqueens = {label: cleanqueen_file[label.replace('/', '-')].value \
            for label in cleanqueen_file.attrs['labels']}
    stitching_column = stitching_file['stitching_matrix'] \
            [:, queenframe_file.attrs['ref_chunk']]
    stitching_grid_size = stitching_file['stitching_matrix'] \
            .attrs['grid_size']
    registration_grid_sizes = {}
    label_indexes = {}
    num_frames = 0
    for chunk_index, masterframe_filename in tqdm(masterframe_filenames, \
            desc='Reading masterframes'):
        if args.use_chunks is not None:
            if chunk_index not in args.use_chunks:
                continue
        masterframe_file = h5py.File(masterframe_filename, 'r')

        segmentation_filename = os.path.join( \
                os.path.dirname(masterframe_filename), \
                masterframe_file.attrs['segmentation'])
        segmentation_file = h5py.File(segmentation_filename, 'r')

        registration_filename = os.path.join( \
                os.path.dirname(masterframe_filename), \
                masterframe_file.attrs['registration'])
        registration_file = h5py.File(registration_filename, 'r')

        video_filename = os.path.join( \
                os.path.dirname(masterframe_filename), \
                masterframe_file.attrs['video'])
        video_file = cv2.VideoCapture(video_filename)
        if cv2.__version__.split('.')[0] == '3':
            fps = float(video_file.get(cv2.CAP_PROP_FPS))
            height = int(video_file.get(cv2.CAP_PROP_FRAME_HEIGHT))
            width = int(video_file.get(cv2.CAP_PROP_FRAME_WIDTH))
        else:
            fps = float(video_file.get(cv2.cv.CV_CAP_PROP_FPS))
            height = int(video_file.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
            width = int(video_file.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))

        # Read chunk time
        metadata_handler = Mp4File(video_filename)
        metadata = {atom.parent.name: atom.get_attribute('data') \
                for atom in metadata_handler.findall('.//data')}
        start_s, end_s = (float(s) for s in metadata['comment'].split(':'))
        num_frames += len(segmentation_file['segmentation'])
        registration_grid_sizes[chunk_index] = \
                registration_file['maps'].attrs['grid_size']
        label_indexes[chunk_index] = \
                h5py.check_dtype(enum=segmentation_file['segmentation'].dtype)

        segmentation_file.close()
        masterframe_file.close()
        video_file.release()
    stitching_file.close()
    queenframe_file.close()
    cleanqueen_file.close()
    
    # Calculate output video frame size
    output_framesize = (width, height)
    if args.crop is not None:
        x, y, width, height = (int(s) for s in args.crop.split(','))
        output_framesize = (width, height)
    if args.scale is not None:
        output_framesize = tuple(int(s * args.scale + .5) \
                for s in output_framesize)
    
    # Open video output
    if args.output is not None:
        fourcc = cv2.cv.FOURCC(*args.codec)
        ouput_video_file = cv2.VideoWriter(args.output, fourcc, \
                fps, output_framesize)
        if not ouput_video_file.isOpened():
            raise RuntimeError("Cannot write video")

    # Iterate over registration and segmentation
    def input_iter():
        for chunk_index, masterframe_filename in masterframe_filenames:
            if args.use_chunks is not None:
                if chunk_index not in args.use_chunks:
                    continue
            masterframe_file = h5py.File(masterframe_filename, 'r')

            segmentation_filename = os.path.join( \
                    os.path.dirname(masterframe_filename), \
                    masterframe_file.attrs['segmentation'])
            segmentation_file = h5py.File(segmentation_filename, 'r')

            registration_filename = os.path.join( \
                    os.path.dirname(masterframe_filename), \
                    masterframe_file.attrs['registration'])
            registration_file = h5py.File(registration_filename, 'r')

            video_filename = os.path.join( \
                    os.path.dirname(masterframe_filename), \
                    masterframe_file.attrs['video'])
            video_file = cv2.VideoCapture(video_filename)
            for _ in range(segmentation_file.attrs['chunkoffset']):
                video_file.grab()
            
            # Read chunk time
            metadata_handler = Mp4File(video_filename)
            metadata = {atom.parent.name: atom.get_attribute('data') \
                    for atom in metadata_handler.findall('.//data')}
            start_s, end_s = (float(s) for s in metadata['comment'].split(':'))

            with buffertools.Reader( \
                    segmentation_file['segmentation'], \
                    segmentation_file['segmentation'].chunks[0]) \
                    as segmentation_reader, \
                 buffertools.Reader( \
                    registration_file['maps'], \
                    registration_file['maps'].chunks[0]) \
                    as registration_reader:
                while not (registration_reader.done() \
                        or segmentation_reader.done()):
                    if cv2.__version__.split('.')[0] == '3':
                        video_msec = video_file.get( \
                                cv2.CAP_PROP_POS_MSEC)
                    else:
                        video_msec = video_file.get( \
                                cv2.cv.CV_CAP_PROP_POS_MSEC)
                    succ, frame = video_file.read()
                    if not succ:
                        raise RuntimeError('Video finished too early')
                    yield chunk_index, video_msec + start_s * 1000., \
                            registration_reader.read(), frame

            segmentation_file.close()
            masterframe_file.close()
            video_file.release()

    iter_segmentation = input_iter()
    func = lambda (chunk_index, video_msec, registration_small_map, frame): \
            register_frame(chunk_index, video_msec, \
            registration_small_map, frame,
                    cleanqueens=cleanqueens, \
                    stitching_column=stitching_column, \
                    stitching_grid_size=stitching_grid_size, \
                    registration_grid_sizes=registration_grid_sizes, \
                    label_indexes=label_indexes)

    result_iter = itertools.imap(func, iter_segmentation)
    for chunk_index, video_msec, frame in tqdm(result_iter, total=num_frames, \
                    desc='Processing video'):
        if args.crop is not None:
            x, y, width, height = (int(s) for s in args.crop.split(','))
            frame = frame[y : y + height, x : x + width]
        if args.scale is not None:
            frame = cv2.resize(frame, output_framesize)
        
        # Prepare font
        font = cv2.FONT_HERSHEY_SIMPLEX
        font_scale = 1.
        font_thickness = 1

        # Write timestamp of video
        s, ms = divmod(video_msec, 1000)
        m, s = divmod(s, 60)
        h, m = divmod(m, 60)
        text = 'Chunk:%d @%d:%02d:%02d' % (chunk_index, h, m, s)
        (font_width, font_height), font_baseline = \
                cv2.getTextSize(text, font, font_scale, font_thickness)
        pos = (output_framesize[0] - font_width, \
               output_framesize[1] - font_height + font_baseline)
        cv2.putText(frame, text, pos, font, font_scale, (255, 255, 255), \
                font_thickness, cv2.cv.CV_AA)

        # Show or write video
        if args.output is None:
            cv2.imshow('video', frame)
            cv2.waitKey(1)
        else:
            ouput_video_file.write(frame)

    if args.output is not None:
        ouput_video_file.release()

