#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Fri May 29 15:46:05 2020

@author: sam
"""

import os
import argparse
import time
import numpy as np
from pipeline_utils import submit_job
import pandas as pd
import h5py
import glob
import datetime
from ssh_tools import retry_ssh
from tqdm import tqdm

if __name__ == '__main__':

    p = argparse.ArgumentParser('Running pipeline on remote', add_help=False)
    p.add_argument('--background', help='Class name of background', type=str)
    p.add_argument('--foreground', help='Class name of chromatophores', type=str)
    
    p.add_argument('--filedeps-slurm', default="")
    
    p.add_argument('--panoConfig-slurm', default="")
    p.add_argument('--panoConfig-args', default="")
    p.add_argument('--panoParams-slurm', default="")
    p.add_argument('--panoParams-args', default="")
    p.add_argument('--segmentation-slurm', default="")
    p.add_argument('--segmentation-args', default="")
    p.add_argument('--segmentation-partition', default="")
    p.add_argument('--segPano-slurm', default="")
    p.add_argument('--segPano-args', default="")
    p.add_argument('--segPano-partition', default="")
    
    p.add_argument('--rerun-panorama', action='store_true')
    p.add_argument('--skip-segmentation', action='store_true')
    p.add_argument('--rerun-segPano', action='store_true')
    p.add_argument('--skip-segPano', action='store_true')
    
    p.add_argument('--skip-chunks', action='store_true', help='Start from chunk stitching')
    p.add_argument('--skip-stitching', action='store_true', help='Skip chunk stitching')
    p.add_argument('--chunk-stitching-slurm', default="")
    p.add_argument('--chunk-stitching-args', default="")
    p.add_argument('--chunk-stitching-partition', default="")
    p.add_argument('--rerun-chunk-stitching', default="False",type=str)
    
    p.add_argument('--stitching-slurm', default="")
    p.add_argument('--stitching-args', default="", help='Arguments for stitching')
    p.add_argument('--stitching-partition', default="")
    p.add_argument('--rerun-stitching', default="False",type=str)

    p.add_argument('--skip-queenframe', action='store_true', help='Stop before queen.')
    p.add_argument('--queenframe-slurm', default="", help='SLURM options for queenframe')
    p.add_argument('--queenframe-args', default="", help='Arguments for queenframe')
    p.add_argument('--cleanqueen-slurm', default="", help='SLURM options for cleanqueen')
    p.add_argument('--cleanqueen-args', default="", help='Arguments for cleanqueen')
    p.add_argument('--rerun-queenframe', action='store_true')
    p.add_argument('--rerun-cleanqueen', action='store_true')
    
    p.add_argument('--chunk-areas-slurm', default="", help='SLURM options for chunk areas')
    p.add_argument('--chunk-areas-args', default="", help='Arguments for chunk areas')
    p.add_argument('--areas-slurm', default="", help='SLURM options for areas')
    p.add_argument('--areas-args', default="", help='Arguments for areas')
    
    p.add_argument('--debug', action='store_true')
    p.add_argument('--max-queued', default=300,type=int)
    p.add_argument('--min-queued', default=0,type=int)
    p.add_argument('--sleep-between-submits', default=0.5, type=float)  # new
    p.add_argument('--wait-for-queue', default=100, help='In seconds') 
    p.add_argument('--check-file-patience', default=40, \
        help='Max seceonds to wait when checking input file dependencies.')
    p.add_argument('--mpicmd', default='mpiexec', \
        help='Command to run jobs in parallel using MPI')
    p.add_argument('--partition', help='SLURM partition to submit to')
    p.add_argument('--sshcmd', help='SSH command for command outside of shell.')
    p.add_argument('--data-dir', default="")
    p.add_argument('prefix', help='Input video')
    
    args = p.parse_args()
       
    #adjust arguments
    if args.segPano_partition == '':
        args.segPano_partition = args.partition
    if args.chunk_stitching_partition == '':
        args.chunk_stitching_partition = args.partition
    if args.stitching_partition == '':
        args.stitching_partition = args.partition
    if args.segmentation_partition == '':
        args.segmentation_partition = args.partition


    if args.rerun_segPano == 'True':
        rerun_segPano = True
    else:
        rerun_segPano = False
    if args.rerun_chunk_stitching == 'True':
        rerun_chunk_stitching = True
    else:
        rerun_chunk_stitching = False
    if args.rerun_stitching == 'True':
        rerun_stitching = True
    else:
        rerun_stitching = False

        
    if args.skip_segPano == 'True':
        skip_segPano = True
    else:
        skip_segPano = False
        
    # Setup file names
    code_dir = os.path.dirname(os.path.abspath(__file__))    #load the right modules
    dirname = os.path.dirname(args.prefix)
    working_dir = dirname
    array_config=os.path.join(dirname, 'array_config')
    classifier = os.path.join(dirname, 'classifier.clf')
    basename = os.path.basename(args.prefix)
    chunktimes = args.prefix + '.chunktimes'
    stitching = args.prefix + '.stitching'
    stitching_log = args.prefix + '.stitching.log'
    queenframe = args.prefix + '.queenframe'
    queenframe_log = args.prefix + '.queenframe.log'
    cleanqueen = args.prefix + '.cleanqueen'
    cleanqueen_log = args.prefix + '.cleanqueen.log'
    areas = args.prefix + '.areas'
    areas_log = args.prefix + '.areas.log'
    filedeps_log = args.prefix + '.filedeps.log'
    try:
        df =  pd.read_csv(array_config)
    except:
        df =  pd.read_pickle(array_config)
    
    now = datetime.datetime.now()
    # Add horizontal separator to file dependency log
    with open(filedeps_log, 'a') as f:
        f.write('======{}======\n'.format(now.strftime("%Y-%m-%d %H:%M:%S")))
    
    with open(chunktimes, 'r') as f:
        size_list = []
        line_list = []
        for line in f:
            line = line.strip('\n').split(' ')
            start_f, end_f, _, _, _ = line
            line_list.append(line)

        sorted_chunktimes = np.array(line_list)
    
    _, _, chunk_video_start, chunk_start_secs, chunk_end_secs = sorted_chunktimes.T
    
    # Set chunk-level filenames
    chunk_prefix = ['{}-{}-{}'.format(args.prefix, start_f, end_f) for 
        (start_f, end_f, _, _, _) in sorted_chunktimes]
    chunk_name = [os.path.split(cp)[1] for cp in chunk_prefix]
    chunk = [cp + '.mp4' for cp in chunk_prefix]
    nchunks = len(chunk)
    chunk_png = [cp + '.png' for cp in chunk_prefix]
    panoConfig = [cp + '_.mat' for cp in chunk_prefix]
    panoConfig_log = [cp + '_.mat.log' for cp in chunk_prefix]
    panoConfig_jobid = []
    panoParams = [cp + '_panoParams.mat' for cp in chunk_prefix]
    panoParams_log = [cp + '_panoParams.mat.log' for cp in chunk_prefix]
    panoParams_jobid = []
    segPano = [cp + '.segPano' for cp in chunk_prefix]
    segPano_log = [cp + '.segPano.log' for cp in chunk_prefix]
    segPano_jobid = []
    chunk_stitching = [cp + '.cstitching' for cp in chunk_prefix]
    chunk_stitching_log = [cp + '.cstitching.log' for cp in chunk_prefix]
    chunk_stitching_jobid = []
    chunk_areas = [cp + '.careas' for cp in chunk_prefix]
    chunk_areas_log = [cp + '.careas.log' for cp in chunk_prefix]
    chunk_areas_jobid = []
            
    array_params_str = '_'.join(os.path.splitext(os.path.split(array_config)[1])[0].split('_')[1:])
    
    all_jobid = []
    
    # Job submission parameters
    submit_params = {
        'mpicmd': args.mpicmd,
        'max_queued': args.max_queued,
        'wait_time': args.wait_for_queue,
        'partition': args.partition,
        'sshcmd': args.sshcmd,
        'debug': args.debug
    }

    # Submit all pano config
    if not args.skip_chunks:
        for chunkidx, output in enumerate(panoConfig):
            
            slurm_args = ['--job-name', 'check-panoConfig', \
                            '--output', '/dev/null', \
                            '--error', filedeps_log, \
                            '--open-mode', 'append']
            if args.filedeps_slurm is not '':
                slurm_args += args.filedeps_slurm.split(' ')
            job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                        '--required', array_config, \
                        '--output', output, \
                        '--patience', str(args.check_file_patience)]
            if args.rerun_panorama:
                job_args += ['--skip-mtime']
            check_panoConfig_jobid = submit_job(job_args, slurm_args, **submit_params)
            
            slurm_args = ['--dependency', 'afterok:{}'.format(check_panoConfig_jobid), \
                            '--output', panoConfig_log[chunkidx], \
                            '--kill-on-invalid-dep=yes', \
                            '--job-name', 'panoConfig']
            if args.panoConfig_slurm is not '':
                slurm_args += args.panoConfig_slurm.split(' ')
            job_args = [os.path.join(code_dir, 'panorama', 'panoramaConfig.py'), \
                    '--directory', dirname, 
                    '--data-dir', dirname,
                        '--chunk', chunk_name[chunkidx],
                        '--arrayConfig', array_config]
            
            if args.panoConfig_args is not '':
                job_args += args.panoConfig_args.split(' ')
            panoConfig_jobid.append(submit_job(job_args, slurm_args, **submit_params))

            print('>>> Submitted {}: {}'.format(panoConfig_jobid[chunkidx], output))
            
            time.sleep(args.sleep_between_submits)
            
        all_jobid += panoConfig_jobid
            
        # Submit checkpoint job
        slurm_args = ['--dependency', 'afterany:' + ':'.join(panoConfig_jobid), \
                        '--kill-on-invalid-dep=yes', \
                        '--job-name', 'checkpoint']
        if args.filedeps_slurm is not '':
            slurm_args += args.filedeps_slurm.split(' ')

        # Submit checkpoint job (continued)               
        job_args = ['hostname']
        checkpoint_jobid = submit_job(job_args, slurm_args, **submit_params)
        
        print('Waiting for job')
        job_running = True
        while job_running:
            stdout = retry_ssh(args.sshcmd, 'scontrol show job {} --oneliner'.format(checkpoint_jobid))
            status = stdout.split('JobState=')[-1].split(' ')[0]

            if args.debug:
                disable_tqdm = None
            else:
                disable_tqdm = True
            if status == 'PENDING':
                # with open(filedeps_log, 'a') as f:
                #     f.write('still waiting ')
                #     f.write(status + '\n')
                time.sleep(10)
            else:
                job_running = False
        
        # Make sure pano config succeeded, and print log if not.
        for chunkidx, output in enumerate(panoConfig):
            assert os.path.isfile(output), f'Missing {output}, log: \n' \
                + open(output + '.log', 'r').read()
        
        
        # Set video-level filenames
        segmentationVid = []
        chunk_video_start_Reg=[]
        chunk_start_secs_Reg=[]
        chunk_end_secs_Reg=[]
        
        for c in range(len(sorted_chunktimes)):
            
            currFiles = glob.glob(os.path.join(working_dir, '*{}-{}.mp4'.format(
                sorted_chunktimes[c,0], sorted_chunktimes[c,1])))
            currRelCams = glob.glob(os.path.join(working_dir, '*{}-{}_relCams'.format(
                sorted_chunktimes[c,0], sorted_chunktimes[c,1])))
            
            # #FIX: clean hotfix
            # file_list = np.array(sorted(glob.glob(dirname + '/*' +  basename + '-{}-{}.png'.format(
            #     sorted_chunktimes[c,0], sorted_chunktimes[c,1])))) 
            # print('tw2', len(file_list))
            # existing_names = np.array([os.path.split(f)[1][:10] for f in file_list])
            # df_chunk = df.copy()
            # print('tw3', df_chunk.names)
            # df_chunk = df_chunk.loc[np.in1d(df_chunk.names, existing_names)]
            # df_chunk.index = np.arange(len(df_chunk), dtype=int)

            if currRelCams != []:  #There is a relcams file
                currRelCams=currRelCams[0]
                
            with open(currRelCams, 'r') as fr:
                relCamsC=fr.read()
            relCamsC = relCamsC.strip('[]\n').split()
            for ind in range(len(relCamsC)):
                relCamsC[ind] = relCamsC[ind].strip(',')

            for f in relCamsC:
                # strMatch=np.array([fname.find(df['names'][int(f)]) for fname in currFiles])
                strMatch=np.array([fname.find(df['names'].iloc[int(f)]) for fname in currFiles])
                currVid=int(np.where(np.array(strMatch)!=-1)[0])
                segmentationVid.append(currFiles[currVid])
                chunk_video_start_Reg.append(sorted_chunktimes[c,2])
                chunk_start_secs_Reg.append(sorted_chunktimes[c,3])
                chunk_end_secs_Reg.append(sorted_chunktimes[c,4])
                
        seg_prefix = [os.path.splitext(sv)[0] for sv in segmentationVid]
        segmentation = [cp + '.seg' for cp in seg_prefix]
        segmentation_log = [cp + '.seg.log' for cp in seg_prefix]
        segmentation_jobid = []
        
        # Submit all pano stitching
        for chunkidx, output in enumerate(panoParams):
            
            slurm_args = ['--dependency', 'afterany:{}'.format(panoConfig_jobid[chunkidx]), \
                            '--kill-on-invalid-dep=yes', \
                            '--job-name', 'check-panoParams', \
                            '--output', '/dev/null', \
                            '--error', filedeps_log, \
                            '--open-mode', 'append']
            if args.filedeps_slurm is not '':
                slurm_args += args.filedeps_slurm.split(' ')
            job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                        panoConfig[chunkidx], \
                        '--output', output, \
                        '--patience', str(args.check_file_patience)]
            if args.rerun_panorama:
                job_args += ['--skip-mtime']
            check_panoParams_jobid = submit_job(job_args, slurm_args, **submit_params)
            
            slurm_args = ['--dependency', 'afterok:{}'.format(check_panoParams_jobid), \
                            '--output', panoParams_log[chunkidx], \
                            '--error', panoParams_log[chunkidx], \
                            '--kill-on-invalid-dep=yes', \
                            '--job-name', 'panoParams']
            if args.panoParams_slurm is not '':
                slurm_args += args.panoParams_slurm.split(' ')
            job_args = [os.path.join(code_dir, 'panorama', 'panorama_stitching.py'), \
                    '--directory', dirname, '--data-dir', dirname,
                        '--panorama-config', panoConfig[chunkidx]]
            
            if args.panoParams_args is not '':
                job_args += args.panoParams_args.split(' ')
            panoParams_jobid.append(submit_job(job_args, slurm_args, **submit_params))

            print('>>> Submitted {}: {}'.format(panoParams_jobid[chunkidx], output))
            
            time.sleep(args.sleep_between_submits)
                
        all_jobid += panoParams_jobid

        if not args.skip_segmentation:
            # Submit all segmentation jobs for the chunks
            for chunkidx, output in enumerate(segmentation):
            
                slurm_args = ['--job-name', 'check-segmentation', \
                                '--output', '/dev/null', \
                                '--error', filedeps_log, \
                                '--open-mode', 'append']
                if args.filedeps_slurm is not '':
                    slurm_args += args.filedeps_slurm.split(' ')
                job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                            '--required', classifier, segmentationVid[chunkidx], \
                            '--output', output, \
                            '--patience', str(args.check_file_patience)]
                check_segmentation_jobid = submit_job(job_args, slurm_args, **submit_params)
                
                slurm_args = ['--dependency', 'afterok:{}'.format(check_segmentation_jobid), \
                                '--output', segmentation_log[chunkidx], \
                                '--kill-on-invalid-dep=yes', \
                                '--job-name', 'segmentation']
                if args.segmentation_slurm is not '':
                    slurm_args += args.segmentation_slurm.split(' ')
                job_args = [os.path.join(code_dir, 'neuralNetSegmentation', 'classify_videoNN.py'), \
                            '--video', segmentationVid[chunkidx], \
                            '--classifier', classifier, \
                            '--video-start', chunk_video_start_Reg[chunkidx], \
                            '--start', chunk_start_secs_Reg[chunkidx], \
                            '--array-params', array_config, \
                            '--end', chunk_end_secs_Reg[chunkidx], \
                            output]
                
                if args.segmentation_args is not '':
                    job_args += args.segmentation_args.split(' ')
                segmentation_jobid.append(submit_job(job_args, slurm_args, **submit_params))

                print('>>> Submitted {}: {}'.format(segmentation_jobid[chunkidx], output))
                
                time.sleep(args.sleep_between_submits)
                
            all_jobid += segmentation_jobid
                  
        # Submit all segPano jobs for the chunks  
        if not args.skip_segPano:        
            
            for chunkidx, output in enumerate(segPano):
                chunk_segmentation = [s for s in segmentation if chunk_name[chunkidx] in s]
                
                slurm_args = ['--job-name', 'check-segPano', \
                                '--output', '/dev/null', \
                                '--error', filedeps_log, \
                                '--open-mode', 'append']
                job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                            '--required', panoParams[chunkidx], *chunk_segmentation, \
                            '--output', output, \
                            '--patience', str(args.check_file_patience)]
                if args.filedeps_slurm is not '':
                    slurm_args += args.filedeps_slurm.split(' ')
                if args.rerun_segPano or args.rerun_panorama:
                    job_args += ['--skip-mtime']
                check_segPano_jobid = submit_job(job_args, slurm_args, **submit_params)
                
                slurm_args = ['--dependency', 'afterok:{}'.format(check_segPano_jobid), \
                                '--output', segPano_log[chunkidx], \
                                '--kill-on-invalid-dep=yes', \
                                '--job-name', 'segPano']
                if args.segPano_slurm is not '':
                    slurm_args += args.segPano_slurm.split(' ')
                job_args = [os.path.join(code_dir, 'registration', 'composeSegPano.py'), \
                            '--directory', dirname, \
                            '--array-params', array_config,
                            '--data-dir', dirname, \
                            '--chunk', chunk_name[chunkidx], \
                            output]
            
                if args.segPano_args is not '':
                    job_args += args.segPano_args.split(' ')
                segPano_jobid.append(submit_job(job_args, slurm_args, **submit_params))

                print('>>> Submitted {}: {}'.format(segPano_jobid[chunkidx], output))
                
            all_jobid += segPano_jobid
            
            time.sleep(args.sleep_between_submits)
        
    if not args.skip_stitching:
        
        # Submit all chunk-stitching jobs for the chunks
        for chunkidx, output in enumerate(chunk_stitching):
            print ('args.skip_chunks', args.skip_chunks)            
            if args.skip_chunks:
                slurm_args = ['--kill-on-invalid-dep=yes', \
                          '--job-name', 'check-chunk-stitching', \
                          '--output', '/dev/null', \
                          '--error', filedeps_log, \
                          '--open-mode', 'append']
            else:
                chunk_stitching_deps = ':'.join(segPano_jobid)
                slurm_args = ['--dependency', 'afterany:' + chunk_stitching_deps, \
                          '--kill-on-invalid-dep=yes', \
                          '--job-name', 'check-chunk-stitching', \
                          '--output', '/dev/null', \
                          '--error', filedeps_log, \
                          '--open-mode', 'append']
            if args.filedeps_slurm is not '':
                slurm_args += args.filedeps_slurm.split(' ')
                
            job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                        '--required', segPano[chunkidx], \
                        '--output', output, \
                        '--patience', str(args.check_file_patience)] 
            if args.rerun_stitching:
                job_args += ['--skip-mtime']
            check_chunk_stitching_jobid = submit_job(job_args, slurm_args, **submit_params)
            
            slurm_args = ['--dependency', f'afterok:{check_chunk_stitching_jobid}', \
                          '--output', chunk_stitching_log[chunkidx], \
                          '--kill-on-invalid-dep=yes', \
                          '--job-name', 'chunk_stitching']
            if args.chunk_stitching_slurm is not '':
                slurm_args += args.chunk_stitching_slurm.split(' ')
            job_args = [os.path.join(code_dir, 'stitching', 'stitch_chunk.py'), \
                        segPano[chunkidx]] \
                        + segPano \
                        + [output]
            if args.chunk_stitching_args is not '':
                job_args += args.chunk_stitching_args.split(' ')
            chunk_stitching_jobid.append(submit_job(job_args, slurm_args, **submit_params))
            
            print('>>> Submitted {}: {}'.format(chunk_stitching_jobid[chunkidx], output))

            time.sleep(args.sleep_between_submits)
            
        all_jobid += chunk_stitching_jobid
        
        # Submit stitching job
        output = stitching
        slurm_args = ['--dependency', 'afterany:' + ':'.join(chunk_stitching_jobid), \
                      '--kill-on-invalid-dep=yes', \
                      '--job-name', 'check-stitching', \
                      '--output', '/dev/null', \
                      '--error', filedeps_log, \
                      '--open-mode', 'append']
        if args.filedeps_slurm is not '':
            slurm_args += args.filedeps_slurm.split(' ')

        job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                    '--optional'] + chunk_stitching \
                    + ['--output', output, \
                    '--patience', str(args.check_file_patience)] 
        check_stitching_jobid = submit_job(job_args, slurm_args, **submit_params)          
        
        slurm_args = ['--dependency', f'afterok:{check_stitching_jobid}', \
                      '--output', stitching_log, \
                      '--kill-on-invalid-dep=yes', \
                      '--job-name', 'stitching']
        if args.stitching_slurm != '':
            slurm_args += args.stitching_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'stitching', 'stitch_all_chunks.py')] \
                    + chunk_stitching \
                    + [output]
        if args.stitching_args is not '':
            job_args += args.stitching_args.split(' ')
        stitching_jobid = submit_job(job_args, slurm_args, **submit_params)
        
        print('>>> Submitted {}: {}'.format(stitching_jobid, output))
        all_jobid += [stitching_jobid]

        time.sleep(args.sleep_between_submits)
        
    if not args.skip_queenframe:
        
        # Submit queenframe job
        output = queenframe
        
        if args.skip_stitching:
            slurm_args = ['--kill-on-invalid-dep=yes', \
                        '--job-name', 'check-queenframe', \
                        '--output', '/dev/null', \
                        '--error', filedeps_log, \
                        '--open-mode', 'append']
        else:
            slurm_args = ['--dependency', f'afterany:{stitching_jobid}', \
                      '--kill-on-invalid-dep=yes', \
                      '--job-name', 'check-queenframe', \
                      '--output', '/dev/null', \
                      '--error', filedeps_log, \
                      '--open-mode', 'append']
            
        if args.filedeps_slurm is not '':
            slurm_args += args.filedeps_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                    '--required', stitching, \
                    '--output', output, \
                    '--patience', str(args.check_file_patience)] 
        if args.rerun_queenframe:
            job_args += ['--skip-mtime']
        check_queenframe_jobid = submit_job(job_args, slurm_args, **submit_params)         
        
        slurm_args = ['--dependency', f'afterok:{check_queenframe_jobid}', \
                        '--output', queenframe_log, \
                        '--kill-on-invalid-dep=yes', \
                        '--job-name', 'queenframe']
        if args.queenframe_slurm is not '':
            slurm_args += args.queenframe_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'stitching', 'generate_queen_frame.py'), \
                    stitching, \
                    output]
        if args.queenframe_args is not '':
            job_args += args.queenframe_args.split(' ')
        queenframe_jobid = submit_job(job_args, slurm_args, **submit_params)
        
        print('>>> Submitted {}: {}'.format(queenframe_jobid, output))
        all_jobid += [queenframe_jobid]

        time.sleep(args.sleep_between_submits)
        
        # Submit cleanqueen job
        output = cleanqueen

        slurm_args = ['--dependency', f'afterany:{queenframe_jobid}', \
                      '--kill-on-invalid-dep=yes', \
                      '--job-name', 'check-cleanqueen', \
                      '--output', '/dev/null', \
                      '--error', filedeps_log, \
                      '--open-mode', 'append']
        if args.filedeps_slurm is not '':
            slurm_args += args.filedeps_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                    '--required', queenframe, \
                    '--output', output, \
                    '--patience', str(args.check_file_patience)] 
        if args.rerun_cleanqueen:
            job_args += ['--skip-mtime']
        check_cleanqueen_jobid = submit_job(job_args, slurm_args, **submit_params)         
        
        slurm_args = ['--dependency', f'afterok:{check_cleanqueen_jobid}', \
                        '--output', cleanqueen_log, \
                        '--kill-on-invalid-dep=yes', \
                        '--job-name', 'cleanqueen']
        if args.cleanqueen_slurm is not '':
            slurm_args += args.cleanqueen_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'stitching', 'generate_cleanqueen.py'), \
                    queenframe, \
                    output]
        if args.cleanqueen_args is not '':
            job_args += args.cleanqueen_args.split(' ')
        cleanqueen_jobid = submit_job(job_args, slurm_args, **submit_params)
        
        print('>>> Submitted {}: {}'.format(cleanqueen_jobid, output))
        all_jobid += [cleanqueen_jobid]

        time.sleep(args.sleep_between_submits)
        
        # Submit areas by chunk
        for chunkidx, output in enumerate(chunk_areas):

            slurm_args = ['--dependency', f'afterany:{cleanqueen_jobid}', \
                        '--kill-on-invalid-dep=yes', \
                        '--job-name', 'check-areas', \
                        '--output', '/dev/null', \
                        '--error', filedeps_log, \
                        '--open-mode', 'append']
            if args.filedeps_slurm is not '':
                slurm_args += args.filedeps_slurm.split(' ')
            job_args = [os.path.join(code_dir, 'check_filedeps.py'), \
                        '--required', cleanqueen, \
                        '--output', output, \
                        '--patience', str(args.check_file_patience)] 
            check_chunk_areas_jobid = submit_job(job_args, slurm_args, **submit_params)
            
            slurm_args = ['--dependency', 'afterok:{}'.format(check_chunk_areas_jobid), \
                            '--output', chunk_areas_log[chunkidx], \
                            '--error', chunk_areas_log[chunkidx], \
                            '--kill-on-invalid-dep=yes', \
                            '--job-name', 'chunk_areas']
            if args.chunk_areas_slurm is not '':
                slurm_args += args.chunk_areas_slurm.split(' ')
            job_args = [os.path.join(code_dir, 'extract_chromatophores', 
                                     'extract_chromatophores_array_chunk.py'), \
                        '--chunk', output, '--foreground', args.foreground,
                        '--cleanqueen', cleanqueen, output]
            
            if args.chunk_areas_args is not '':
                job_args += args.chunk_areas_args.split(' ')
            chunk_areas_jobid.append(submit_job(job_args, slurm_args, **submit_params))
            
            print('>>> Submitted {}: {}'.format(chunk_areas_jobid[chunkidx], output))
            
            time.sleep(args.sleep_between_submits)
            
        all_jobid += chunk_areas_jobid
        
        # Submit final areas
        output = areas
        
        slurm_args = ['--dependency', 'afterany:' + ':'.join(chunk_areas_jobid), \
                        '--output', areas_log, \
                        '--kill-on-invalid-dep=yes', \
                        '--job-name', 'areas']
        if args.areas_slurm is not '':
            slurm_args += args.areas_slurm.split(' ')
        job_args = [os.path.join(code_dir, 'extract_chromatophores', 'combine_area_chunks.py')] \
                    + chunk_areas + [output]

        if args.areas_args is not '':
            job_args += args.areas_args.split(' ')

        areas_jobid = submit_job(job_args, slurm_args, **submit_params)
        
        print('>>> Submitted {}: {}'.format(areas_jobid, output))
        all_jobid += [areas_jobid]

        time.sleep(args.sleep_between_submits)
        
    # Submit final job
    slurm_args = ['--dependency', 'afterany:' \
                    + ':'.join(all_jobid), \
                    '--kill-on-invalid-dep=yes', \
                    '--job-name', 'final']
    if args.filedeps_slurm is not '':
        slurm_args += args.filedeps_slurm.split(' ')

    # Submit final job (continued)               
    job_args = ['hostname']
    final_jobid = submit_job(job_args, slurm_args, **submit_params)

    print('FINAL_JOBID=', final_jobid)